<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Centroventa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('centroventa', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable();
            $table->string('tipopedido')->nullable();
            $table->integer('idciudad')->nullable();
            $table->integer('idcatprecio')->nullable();
            $table->integer('provisional')->nullable();

            $table->string('idcliente')->nullable();
            $table->string('nombre')->nullable();
            $table->string('tipocliente')->nullable();
            $table->string('billto')->nullable();
            $table->string('shipto')->nullable();

            $table->timestamps();

            $table->string('organization_id')->nullable();
            $table->string('subinventory_name')->nullable();
            $table->string('clavevendedor')->nullable();
     
            

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('centroventa');
    }
}

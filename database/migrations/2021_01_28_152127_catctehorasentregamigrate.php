<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catctehorasentregamigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create('catctehorasentrega', function (Blueprint $table) {
            $table->increments('id');

           $table->enum('tipocliente', ['Nuevo', 'Existente']);
           $table->integer('organization_id')->nullable();
           $table->integer('dias')->nullable()->default('0');
           $table->integer('horas')->nullable()->default('0');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('catctehorasentrega');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datprecios extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
       Schema::create('dat_precios_hist', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('idproduct')->nullable();
            $table->double('precios',0)->nullable();
            $table->integer('idcatprecio')->nullable();
            $table->integer('idcentroventa')->nullable();
            $table->integer('idcategory')->nullable();

            $table->string('nameproduct')->nullable();
            $table->integer('id_user')->nullable();
            $table->datetime('creation_date')->nullable();
            
            $table->timestamps();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dat_precios_hist');
    }
}

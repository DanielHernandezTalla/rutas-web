<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datpromocionmigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('datpromocion', function (Blueprint $table) {
            $table->increments('id');
            $table->string('idpromocion')->nullable();
            $table->string('descripcion');
            $table->string('tipopromocion')->nullable();
            $table->string('idcliente')->nullable();
            $table->string('producto')->nullable();
            $table->decimal('preciopromocion', 8, 2)->nullable()->default('0.00');
            $table->date('fecha_inicial')->nullable();
            $table->date('fecha_final')->nullable();
            $table->string('ruta')->nullable();
            $table->string('llaveruta')->nullable();
            $table->string('organization_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datpromocion');
    }
}

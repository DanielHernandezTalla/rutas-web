<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catreltipopagocentroventa extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
       Schema::create('catreltipopagocentroventa', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('idtipopago')->nullable();
            $table->integer('idcentrovta')->nullable();
            $table->string('idcliente')->nullable();
            $table->string('nombre')->nullable();
            $table->string('tipocliente')->nullable();

            $table->string('billto')->nullable();
            $table->string('shipto')->nullable();
           

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catreltipopagocentroventa');
    }
}

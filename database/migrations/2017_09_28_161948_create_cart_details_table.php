<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cart_details', function (Blueprint $table) {
            $table->increments('id_cart_details');

            // FK header
            $table->integer('cart_id')->unsigned()->nullable();
            $table->foreign('cart_id')->references('id')->on('carts');

            // FK product
            $table->integer('product_id')->unsigned()->nullable();
            $table->foreign('product_id')->references('id')->on('products');
            
            $table->float('quantity')->nullable();
            $table->integer('discount')->default(0)->nullable();// % int

            $table->float('iva')->nullable();
            $table->float('importe')->nullable();
            $table->timestamps();
            $table->string('uom')->nullable();
            $table->integer('user_id')->nullable();
            $table->integer('last_user_id')->nullable();
            $table->string('uom1')->nullable();
            $table->double('quantity1', 8, 2)->nullable();

            $table->integer('iddatprecio')->unsigned()->nullable();
            $table->double('precio_producto', 8, 2)->nullable();
            $table->date('fechaE')->nullable();
            $table->integer('quantitycaja')->unsigned()->nullable();
            $table->decimal('existencia', 8, 2)->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cart_details');
    }
}

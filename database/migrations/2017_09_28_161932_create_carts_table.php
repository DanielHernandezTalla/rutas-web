<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCartsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('carts', function (Blueprint $table) {
            $table->increments('id');

            $table->date('order_date')->nullable();
            $table->date('arrived_date')->nullable();
            $table->string('status')->nullable(); // Active, Pending, Approved, Cancelled, Finished

            // user_id (FK) customer
            $table->integer('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users');

            $table->float('importe_total')->nullable();
            $table->timestamps();

            $table->bigInteger('idcentrovta')->nullable();
            $table->bigInteger('iddirenv')->nullable();
            $table->bigInteger('iddirfac')->nullable();
            $table->tinyInteger('interfazado')->nullable();
            $table->string('comentario')->nullable();
            $table->string('referencia')->nullable();

            $table->date('fechaE')->nullable();
            $table->string('organization_id')->nullable();
            $table->string('ruta')->nullable();
            $table->enum('factura', ['Si', 'No']);
            $table->string('claverutero')->nullable();
            $table->string('clavevendedor')->nullable();
            $table->date('fecha_interfazado')->nullable();



            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('carts');
    }
}

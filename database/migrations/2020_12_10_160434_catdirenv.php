<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catdirenv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
       Schema::create('catdirenv', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('iduser')->nullable();
            $table->string('codigopostal')->nullable();
            $table->string('direccion')->nullable();
            $table->string('colonia')->nullable();
            $table->string('city')->nullable();
            $table->string('numext')->nullable();
            $table->string('numint')->nullable();
            $table->timestamps();

            $table->string('credito_contado')->nullable();
            $table->tinyInteger('lunes')->nullable();
            $table->tinyInteger('martes')->nullable();
            $table->tinyInteger('miercoles')->nullable();
            $table->tinyInteger('jueves')->nullable();
            $table->tinyInteger('viernes')->nullable();
            $table->tinyInteger('sabado')->nullable();
            $table->string('tipopedido')->nullable();
            $table->string('almacen_ruta')->nullable();
            $table->string('idcliente')->nullable()->default('0');
            $table->string('idclienteenvio')->nullable();
            $table->string('sucursal')->nullable();
            $table->string('nombrecomercial')->nullable();
            $table->string('giro')->nullable();
            $table->string('city2')->nullable();
            $table->string('clave_vendedor')->nullable();
            $table->string('email')->nullable();
            $table->string('clavecliente')->nullable();
            $table->string('estado')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catdirenv');
    }
}

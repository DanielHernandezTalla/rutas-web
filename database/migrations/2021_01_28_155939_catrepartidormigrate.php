<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catrepartidormigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('catrepartidor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('clave_vendedor')->nullable();
            $table->string('name')->nullable();
            $table->string('cuenta_banco')->nullable();
            $table->string('plaza')->nullable();
            $table->string('ruta')->nullable();
            $table->string('idprecio')->nullable();
            $table->string('rol')->nullable();
            $table->string('mayoreo')->nullable();
            $table->string('tipo_pedido')->nullable();
            $table->string('tipo')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catrepartidor');
    }
}

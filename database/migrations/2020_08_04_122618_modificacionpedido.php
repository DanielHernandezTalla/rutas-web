<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Modificacionpedido extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modificacionpedido', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_cart')->nullable();
            $table->integer('id_product')->nullable();
            $table->string('nameProduct')->nullable();
            $table->integer('quantity')->nullable();
            $table->float('price')->nullable();
            $table->string('uom')->nullable();

            $table->float('iva')->nullable();
            $table->float('importe')->nullable();
            $table->timestamps();

            $table->integer('id_cart_detail')->nullable();
            $table->double('quantity1', 8, 2)->nullable();
            $table->string('uom1')->nullable();

            $table->integer('iddtaprecio')->nullable();
            $table->integer('quantitycaja')->nullable()->default('0');
            $table->string('existencia')->nullable()->default('0');



            
        });
    }

    

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::dropIfExists('modificacionpedido');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pagos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
       Schema::create('pagos', function (Blueprint $table) {
            $table->increments('order_id');
            $table->string('forma_pago')->nullable();
            $table->string('name')->nullable();
            $table->string('email_factura')->nullable();
            $table->string('metadopago')->nullable();
            $table->string('usocfdi')->nullable();
            $table->integer('idtpopago')->nullable();
            $table->integer('solfact')->nullable();
            $table->decimal('importe',  10, 0);
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pagos');
    }
}

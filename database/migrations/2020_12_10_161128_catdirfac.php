<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catdirfac extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
       Schema::create('catdirfac', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('userid')->nullable();
            $table->string('idcliente')->nullable();
            $table->string('nombre')->nullable();
            $table->string('rfc')->nullable();
            $table->string('tipocliente')->nullable();

            $table->string('calle')->nullable();
            $table->string('colonia')->nullable();
            $table->string('city')->nullable();
            $table->string('estado')->nullable();
            $table->string('pais')->nullable();

            $table->string('email')->nullable();
            $table->string('codigopostal')->nullable();
            $table->string('numext')->nullable();
            $table->string('numint')->nullable();
            $table->string('billto')->nullable();

            $table->string('shipto')->nullable();
            $table->timestamps();

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('catdirfac');
    }
}

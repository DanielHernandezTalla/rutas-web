<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Datinvcedismigrate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('datinvcedis', function (Blueprint $table) {
            $table->increments('id');
           
            $table->string('idproducto')->nullable();
            $table->string('codigo')->nullable();
            $table->string('organization_id')->nullable();
             $table->decimal('existencia', 8, 2)->nullable();
            $table->string('unidad')->nullable();
            $table->string('unidadllave')->nullable();
           



            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datinvcedis');
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name')->nullable();            
            $table->string('description')->nullable();
            $table->text('long_description')->nullable();
            
            $table->float('factor')->nullable();
            $table->float('iva')->nullable();
            $table->string('uom')->nullable();
            $table->string('uom2')->nullable();
            $table->string('codigo')->nullable();
            
            // FK
            $table->integer('category_id')->unsigned()->nullable();
            $table->foreign('category_id')->references('id')->on('categories');

            $table->timestamps();
            
            $table->string('name_short')->nullable();
            $table->string('name_short1')->nullable();
            $table->string('des_iva')->nullable();

             $table->string('organization_id')->nullable();
             $table->string('pesopromedio')->nullable();
             $table->string('idproducto')->nullable();



           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}

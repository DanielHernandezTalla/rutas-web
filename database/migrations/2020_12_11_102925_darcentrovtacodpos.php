<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Darcentrovtacodpos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
    {
       Schema::create('datcentrovtacodpos', function (Blueprint $table) {
            $table->increments('id');
       
            $table->integer('idcentrovta')->nullable();
           
            $table->string('codigopostal')->nullable();
   

           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('datcentrovtacodpos');
    }
}

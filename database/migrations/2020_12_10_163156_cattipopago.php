<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Cattipopago extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
public function up()
    {
       Schema::create('cattipopago', function (Blueprint $table) {
            $table->increments('id');
            $table->string('descripcion')->nullable();
           
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cattipopago');
    }
}

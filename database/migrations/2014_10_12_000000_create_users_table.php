<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->string('username')->unique()->nullable();
            $table->string('email')->unique()->nullable();

            $table->tinyInteger('active_status');
            $table->tinyInteger('dark_mode');
            $table->string('messenger_color');
            $table->string('avatar');

            $table->string('phone')->nullable();
            $table->string('city')->nullable();
            $table->string('codigoPostal')->nullable();
            $table->string('colonia')->nullable();
            $table->string('address')->nullable();
            $table->string('numeroExterior')->nullable();
            $table->string('numerointerior')->nullable();
            $table->string('password')->nullable();

            $table->boolean('useradmin')->default(false);

            $table->boolean('admin')->default(false);
            $table->rememberToken();
            $table->timestamps();            
            $table->boolean('gester')->default(false);

             $table->string('organization_id')->nullable();
             $table->string('lugar')->nullable();
             $table->boolean('usad')->default(false);
             $table->boolean('adminpedidos')->default(false);
             $table->boolean('rutero')->default(false);
             $table->string('idcliente')->nullable()->default('0');
             $table->string('idclienteenvio')->nullable();
             $table->boolean('picking')->default(false);
             $table->boolean('activo')->default(false);
               $table->string('rfc')->nullable();
                 $table->string('agente')->nullable();

            $table->string('credito_contado')->nullable();
            $table->tinyInteger('lunes')->nullable();
            $table->tinyInteger('martes')->nullable();
            $table->tinyInteger('miercoles')->nullable();
            $table->tinyInteger('jueves')->nullable();
            $table->tinyInteger('viernes')->nullable();
            $table->tinyInteger('sabado')->nullable();
            $table->string('tipopedido')->nullable();
            $table->string('almacen_ruta')->nullable();
     
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

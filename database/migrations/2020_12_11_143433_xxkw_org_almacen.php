<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class XxkwOrgAlmacen extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
   public function up()
    {
        Schema::create('xxkw_org_almacen', function (Blueprint $table) {
            $table->increments('id');
            $table->string('ORGANIZATION_ID')->nullable();
            $table->string('ORGANIZATION_NAME')->nullable();
            $table->string('ORGANIZATION_CODE')->nullable();
            $table->string('SUBINVENTORY_NAME')->nullable();
            $table->string('SUBINVENTORY_DES')->nullable();
            $table->timestamps();
           
           
            
         
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}

<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
        	'name' => 'Ernesto',
            'username' => 'jernesto',
            'email' => 'jernestorivera10@gmail.com',
            'phone' => '6421494958',
            'city' => 'NAVOJOA',
            'codigoPostal' => '85820',
            'colonia' => 'Constitucion',
            'address' => 'Ramon Corona',
            'numeroExterior' => '702',
            'numerointerior' => '702',
            'password' => bcrypt('123456'),
            'admin' => true,
            
        ]);
    }
}

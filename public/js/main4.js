const fila4 = document.querySelector('.contenedor-carousel4');
const peliculas4 = document.querySelectorAll('.pelicula4');

const flechaIzquierda4 = document.getElementById('flecha-izquierda4');
const flechaDerecha4 = document.getElementById('flecha-derecha4');

// ? ----- ----- Event Listener para la flecha derecha. ----- -----
flechaDerecha4.addEventListener('click', () => {
	fila4.scrollLeft += fila4.offsetWidth;

	const indicadorActivo4 = document.querySelector('.indicadores4 .activo');
	if(indicadorActivo4.nextSibling){
		indicadorActivo4.nextSibling.classList.add('activo');
		indicadorActivo4.classList.remove('activo');
	}
});

// ? ----- ----- Event Listener para la flecha izquierda. ----- -----
flechaIzquierda4.addEventListener('click', () => {
	fila4.scrollLeft -= fila4.offsetWidth;

	const indicadorActivo4 = document.querySelector('.indicadores4 .activo');
	if(indicadorActivo4.previousSibling){
		indicadorActivo4.previousSibling.classList.add('activo');
		indicadorActivo4.classList.remove('activo');
	}
});

// ? ----- ----- Paginacion ----- -----
const numeroPaginas4 = Math.ceil(peliculas.length / 5);
for(let i = 0; i < numeroPaginas4; i++){
	const indicador = document.createElement('button');

	if(i === 0){
		indicador.classList.add('activo');
	}

	document.querySelector('.indicadores4').appendChild(indicador);
	indicador.addEventListener('click', (e) => {
		fila4.scrollLeft = i * fila4.offsetWidth;

		document.querySelector('.indicadores .activo').classList.remove('activo');
		e.target.classList.add('activo');
	});
}

// ? ----- ----- Hover ----- -----
peliculas4.forEach((pelicula) => {
	pelicula.addEventListener('mouseenter', (e) => {
		const elemento = e.currentTarget;
		setTimeout(() => {
			peliculas.forEach(pelicula => pelicula.classList.remove('hover'));
			elemento.classList.add('hover');
		}, 400);
	});
});

fila4.addEventListener('mouseleave', () => {
	peliculas.forEach(pelicula => pelicula.classList.remove('hover'));
});


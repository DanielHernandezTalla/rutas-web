const fila5 = document.querySelector('.contenedor-carousel5');
const peliculas5 = document.querySelectorAll('.pelicula5');

const flechaIzquierda5 = document.getElementById('flecha-izquierda5');
const flechaDerecha5 = document.getElementById('flecha-derecha5');

// ? ----- ----- Event Listener para la flecha derecha. ----- -----
flechaDerecha5.addEventListener('click', () => {
	fila5.scrollLeft += fila5.offsetWidth;

	const indicadorActivo5 = document.querySelector('.indicadores5 .activo');
	if(indicadorActivo5.nextSibling){
		indicadorActivo5.nextSibling.classList.add('activo');
		indicadorActivo5.classList.remove('activo');
	}
});

// ? ----- ----- Event Listener para la flecha izquierda. ----- -----
flechaIzquierda5.addEventListener('click', () => {
	fila5.scrollLeft -= fila5.offsetWidth;

	const indicadorActivo5 = document.querySelector('.indicadores5 .activo');
	if(indicadorActivo5.previousSibling){
		indicadorActivo5.previousSibling.classList.add('activo');
		indicadorActivo5.classList.remove('activo');
	}
});

// ? ----- ----- Paginacion ----- -----
const numeroPaginas5 = Math.ceil(peliculas.length / 5);
for(let i = 0; i < numeroPaginas5; i++){
	const indicador = document.createElement('button');

	if(i === 0){
		indicador.classList.add('activo');
	}

	document.querySelector('.indicadores5').appendChild(indicador);
	indicador.addEventListener('click', (e) => {
		fila5.scrollLeft = i * fila5.offsetWidth;

		document.querySelector('.indicadores .activo').classList.remove('activo');
		e.target.classList.add('activo');
	});
}

// ? ----- ----- Hover ----- -----
peliculas5.forEach((pelicula) => {
	pelicula.addEventListener('mouseenter', (e) => {
		const elemento = e.currentTarget;
		setTimeout(() => {
			peliculas.forEach(pelicula => pelicula.classList.remove('hover'));
			elemento.classList.add('hover');
		}, 500);
	});
});

fila5.addEventListener('mouseleave', () => {
	peliculas.forEach(pelicula => pelicula.classList.remove('hover'));
});


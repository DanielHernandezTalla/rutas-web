@extends('layouts.app')

@section('body-class', 'signup-page')

@section('content')



<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
<style type="text/css">
   
    .message{

       color: red;
        font-size: 18px;
        font-weight: 400; 
    }

    html{
    background-color: #fff;
}

#name::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#username::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#email::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#phone::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#citys::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#colonias::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#codigoPostales::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#address::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#numeroExterior::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#numeroInterior::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#RFC::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#password::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#password2::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}


{{-- inputs --}}
#name {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#username {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#email {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#phone {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#citys {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#colonias {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#codigoPostales {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#address {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#numeroExterior {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#numeroInterior {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#RFC {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#password {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}

#password2 {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}



.check{

outline: 2px solid #ff7514;
width: 250px;
}
input[type=checkbox] {
    transform: scale(0.5);
}
.wrapper{
    background-color: #fff;
    height:100%;
}
.navbar{
    display: none;
}
#olvreg{
    color: #838383; font-size: 12px; text-decoration-line: underline;
}
.footer{
   margin-bottom: 10%;
   background-color: #fff;
}
#Iniciar{
background-color: #ff7514; border-radius: 35px; font-size: 18px; font-weight: 500; border-style: none; color: #fff; width:50%;
}
.checkbox .labelcheck{
    color: #838383; font-size: 12px; font-style: italic;
}
.input-group span{
 color: #ff7514;   
}
.text-divider{
  color: #838383; font-weight: 100; font-size: 12px; font-style: italic;  
}
#divinicio{
  background-color: transparent; margin-bottom: -10%;  
}
.titulo{
color: #787B79; font-size: 20px; font-weight: 500;
}
.text_inferior{
   color: #838383;
 font-size: 10px; 
 font-style: italic;
 position: relative;
 margin-top: -60%;
}
@media (min-width: 540px) {
  .mensajes_para_usuarios{
    display: none;
  }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <div class="container" style="">
         <h5 class="mensajes_para_usuarios" style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">Esta aplicacion es para uso escusivo de negocios</h5>
<h5 class="mensajes_para_usuarios" style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -20%;">Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
        <div style="margin-top: 0%; margin-bottom: 25%;" class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card  card-signup"  style="background-color: transparent; -webkit-box-shadow: 0px 0px 21px 16px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 21px 16px rgba(255,255,255,1);
box-shadow: 0px 0px 21px 16px rgba(255,255,255,1);">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div id="divinicio"  class="  text-center">
                            <h4 class="titulo">Regístrate</h4>
                        </div>
                        <p  class="text-divider">Ingresa tus datos</p>

                        <div class="content">

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <input id="name" type="text"  class="form-control" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Nombre" name="name"
                                       value="{{ old('name', $name) }}" required autofocus>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">fingerprint</i>
                                </span>
                                <input  type="text" class="form-control"  placeholder="Username" name="username" id="username"  onfocus="myFunction()" onfocusout="myFunction2()"
                                       value="{{ old('username') }}" required>   
                               <h9 hidden id="mensaje" class="mensaje"></h9>                                                                                     
                            </div> 
 
 
      

   

<script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search2.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('#mensaje').html(data.table_data);
 
   }
  })
 }

 $(document).on('focusout', '#username', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>




                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <input id="email" type="email" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Correo electrónico" class="form-control" name="email" value="{{ old('email', $email) }}"  onblur = "checkmain (this.value)">
                                 <h9 hidden id="mensaje3" class="mensaje"></h9>
                            </div>

 

 <script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search3.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('#mensaje3').html(data.table_data);
   
   }
  })
 }

 $(document).on('focusout', '#email', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <input id="phone" type="phone" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Teléfono" class="form-control" name="phone" value="{{ old('phone') }}" required>
                            </div>   



                            
                            {{Form::open(array('url'=>'/register', 'files'=>true))}}


<label class="text_inferior">*Algunas ciudades no se encuentran disponibles de momento</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 
                                <select name="city" id="citys" class="form-control" >
                                    <option selected>Seleciona una ciudad</option>
                                    @foreach($citys as $id => $city)
                                        <option value="{{ $city }}">
                                            {{ $city }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

     
 <label class="text_inferior" for="">*Algunos codigos postales no se encuentran disponibles de momento</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>
                                 
                                <select name="codigoPostal" id="colonias" class="form-control">
  <option selected>Seleciona un codigo postal</option>

                                </select>
                            </div>





       <label class="text_inferior" for="">*Algunas colonias no se encuentran disponibles de momento</label>                    
                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mark_email_unread</i>
                                </span>
                                 
                                <select name="colonia" id="codigoPostales"  class="form-control">
 
                                  
                                </select>
                            </div>
                       



                       



                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">drag_handle</i>
                                </span>
                                <input id="address" type="text" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Calle" class="form-control" name="address" value="{{ old('address') }}" required>
                            </div> 


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input id="numeroExterior" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Numero Exterior" class="form-control" name="numeroExterior" value="{{ old('numeroExterior') }}" required>
                            </div>  

                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input id="numeroInterior" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Numero Interior" class="form-control" name="numeroInterior" value="{{ old('numeroInterior') }}">
                            </div> 
<label class="text_inferior" for="">*El RFC es opcional</label>   
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">filter_1</i>
                                </span>
                                <input id="RFC" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="RFC" class="form-control" name="rfc" value="XAXX010101000">
                            </div>  
                          

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input placeholder="Contraseña" onfocus="myFunction()" onfocusout="myFunction2()" id="password" type="password" class="form-control" name="password" required />
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input placeholder="Confirmar contraseña" onfocus="myFunction()" onfocusout="myFunction2()" id="password2" type="password" class="form-control" name="password_confirmation" required />
                            </div>

                             <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input value="apprutas" onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" name="lugar" required />
                            </div>

                             <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input  value="1" onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" name="activo" required />
                            </div>
                        </div>
                        <div class="footer text-center">

                            <button id="Iniciar" type="submit"  class=" btn-simple btn-primary btn-lg">Regístrar</button>
                             <a  href="/login">
                    <div class="text-center " id="olvreg">
            Iniciar sesion
                    </div>
                     </a>
                        </div>
                      
                    </form>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    


<script>
 $('#citys').on('change',function(e){
    console.log(e);

    var cit_id = e.target.value;

    //ajax
    $.get('/ajax-colonias?cit_id=' + cit_id, function(data){
        $('#colonias').empty();

        $.each(data, function(index, coloniaObj){

$('#colonias').append('<option value="'+coloniaObj.codigopostal+'">'+coloniaObj.codigopostal+'</option>');



    
        });
    });

 });
 
</script>


<script>
 $('#colonias').on('change',function(e){
    console.log(e);

    var col_id = e.target.value;

    //ajax
    $.get('/ajax-codigoPostales?col_id=' + col_id, function(data){
        $('#codigoPostales').empty();

        $.each(data, function(index, codigoPostalesObj){

$('#codigoPostales').append('<option value="'+codigoPostalesObj.colonia+'">'+codigoPostalesObj.colonia+'</option>');


    
        });
    });

 });
 
</script>


@include('includes.footer')
@endsection


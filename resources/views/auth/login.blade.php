@extends('layouts.app')

@section('body-class', 'signup-page')

@section('content')
<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>

<style type="text/css">

html{
    background-color: #fff;
}
#Usuario::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#Contraseña::placeholder {
  color: #838383;
 font-size: 12px; 


}

#Contraseña {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#Usuario {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
.check{

outline: 2px solid #ff7514;
width: 250px;
}
input[type=checkbox] {
    transform: scale(0.5);
}
.wrapper{
    background-color: #fff;
    height:100%;
}
.navbar{
    display: none;
}
#olvreg{
    color: #838383; font-size: 12px; text-decoration-line: underline;
}
.footer{
   margin-bottom: 10%;
}
#Iniciar{
background-color: #ff7514; border-radius: 35px; font-size: 18px; font-weight: 500; border-style: none; color: #fff; width:50%;
}
.checkbox .labelcheck{
    color: #838383; font-size: 12px; font-style: italic;
}
.input-group span{
 color: #ff7514;   
}
.text-divider{
  color: #838383; font-weight: 100; font-size: 12px; font-style: italic;  
}
#divinicio{
  background-color: transparent; margin-bottom: -10%;  
}
.titulo{
color: #787B79; font-size: 20px; font-weight: 500;
}

 @media (min-width: 540px) {
  .mensajes_para_usuarios{
    display: none;
  }
 }
</style>
    <div class="container">
      <h5 class="mensajes_para_usuarios" style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">Esta aplicacion es para uso escusivo de negocios</h5>
<h5 class="mensajes_para_usuarios" style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -20%;">Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
        <div style="margin-top: 5%; margin-bottom: 15%;" class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">

                <div style="background-color: transparent;  
Copy Text" class=" card-signup">

                    <form class="form" method="POST" action="{{ route('login') }}">
                        {{ csrf_field() }}

                        <div id="divinicio"  class="  text-center">
                            <h4 class="titulo">Inicio de sesión</h4>
                        </div>
                        <p  class="text-divider">Ingresa tus datos</p>
                        <div class="content">

                            <div class="input-group">
                                <span  class="input-group-addon">
                                    <i class="material-icons">fingerprint</i>
                                </span>
                                <input id="Usuario"  onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Usuario" class="form-control" name="username" value="{{ old('username') }}" required autofocus>
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input  id="Contraseña" placeholder="Contraseña" onfocus="myFunction()" onfocusout="myFunction2()" id="password" type="password" class="form-control" name="password" required />
                            </div>
                            
                            <div class="checkbox">
                                <label class="labelcheck" >
                                    <input id="check" type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}>
                                    *Recordar tus datos
                                </label>
                            </div>
                        </div>
                        <br>
                        <br>
                        <div class="footer text-center">

                            <button id="Iniciar" type="submit"  class=" btn-simple btn-primary btn-lg">Iniciar Sesión</button>
                        </div>
                        
                           
                        
                    </form>
                     <a  href="{{ route('password.request') }}">
                    <div class="text-center " id="olvreg">
            Olvidé mi contraseña
                    </div>
                     </a>
                     <a  href="/register">
          <div class="text-center " id="olvreg">
           Registrarme
          </div>
      </a>
                </div>
            </div>
        </div>
    </div>



@include('includes.footer')
@endsection

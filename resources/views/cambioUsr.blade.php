@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
<style type="text/css">
    @media (max-width: 540px) {
    #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }




  
}
@media (min-width: 540px) {
  .mensajes_para_usuarios{
    display: none;
  }
</style>

<div class="main main-raised"style="background-color: #fff;-webkit-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);">
    <div class="container">
  <h5 class="mensajes_para_usuarios" style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">Esta aplicacion es para uso escusivo de negocios</h5>
<h5 class="mensajes_para_usuarios" style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -1%;">Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
        <div class="section">
            <h2 class="title text-center">Datos del Usuario</h2>

            @if (session('notification'))
                <div class="alert alert-success">
                    {{ session('notification') }}
                </div>
            @endif

       
            </div>
 @foreach($User as $Users)

              <form class="form" method="POST" action="{{ url('/modificarUsuario/'.$Users->idus.'/editUser') }}">
                        {{ csrf_field() }}

                     
                        <p class="text-divider">Completa tus datos</p>
                        <div class="content">


                        
                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <input onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" placeholder="Nombre" name="name"
                                       value="{{ old('name',$Users->name ) }}"  >
                            </div>
                            <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">fingerprint</i>
                                </span>
                                <input   onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" placeholder="Username" name="username"
                                       value="{{ old('username',$Users->username) }}" >
                            </div>               

                                         

                            <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <input  onfocus="myFunction()" onfocusout="myFunction2()" id="email" type="email" placeholder="Correo electrónico" class="form-control" name="email" value="{{ old('email', $Users->email) }}">
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <input onfocus="myFunction()" onfocusout="myFunction2()" id="phone" type="phone" placeholder="Teléfono" class="form-control" name="phone" value="{{ old('phone', $Users->phone) }}" >
                            </div>                            

                            
                           {{Form::open(array('url'=>'/register', 'files'=>true))}}
                             <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 
                                <select name="city" id="citys" class="form-control">
                                    <option value="{{ old('ciuname', $Users->ciuname) }}">{{$Users->ciuname}}</option>
                                   
                                    @foreach($citys as $id => $city)
                                        <option value="{{ $city }}">
                                            {{ $city }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mark_email_unread</i>
                                </span>
                                 
                               <select name="codigoPostal" id="colonias" class="form-control">
                                   <option value="{{ old('codname', $Users->codname) }}">{{$Users->codname}}</option>  
                                   
                                  
                                </select>
                            </div>


                             <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>
                                 
                               <select name="colonia" id="codigoPostales" class="form-control">
                                    <option value="{{ old('colname', $Users->colname) }}">{{$Users->colname}}</option>
                                   
                                  
                                </select>
                            </div>



                              

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">class</i>
                                </span>
                                <input id="address" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Dirección" class="form-control" name="address" value="{{ old('address', $Users->address) }}" >
                            </div>  



                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" placeholder="Numero exterior" name="numeroExterior" value="{{ old('numeroExterior',$Users->numeroExterior) }}" >
                            </div> 

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" placeholder="Numero interior" name="numerointerior" value="{{ old('numerointerior',$Users->numerointerior) }}" >
                            </div>  

                            <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" name="lugar" value="{{ old('lugar',$Users->lugar) }}" >
                            </div>  
                        </div>
                        
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-simple btn-primary btn-lg">Confirmar registro</a>
                        </div>
                        <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password? -->

                        </a>
                    </form>
                    {{Form::close()}}
@endforeach
        </div>
 <script>
 $('#citys').on('change',function(e){
    console.log(e);

    var cit_id = e.target.value;

    //ajax
    $.get('/ajax-colonias?cit_id=' + cit_id, function(data){
        $('#colonias').empty();

        $.each(data, function(index, coloniaObj){

$('#colonias').append('<option value="'+coloniaObj.codigopostal+'">'+coloniaObj.codigopostal+'</option>');



    
        });
    });

 });
 
</script>


<script>
 $('#colonias').on('change',function(e){
    console.log(e);

    var col_id = e.target.value;

    //ajax
    $.get('/ajax-codigoPostales?col_id=' + col_id, function(data){
        $('#codigoPostales').empty();

        $.each(data, function(index, codigoPostalesObj){

$('#codigoPostales').append('<option value="'+codigoPostalesObj.colonia+'">'+codigoPostalesObj.colonia+'</option>');


    
        });
    });

 });
 
</script>
    </div>


@include('includes.footer')
@endsection

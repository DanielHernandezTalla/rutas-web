@extends('layouts.app2')


@section('body-class', 'product-page')


@section('content')
<style type="text/css">
  h1{color: red;
    font-size: 28px;
    font-weight: bold;
    font-family: "Gill Sans Extrabold", Helvetica, sans-serif; 
  }
  img{
    width: 20%;
  }
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>
<br>
<br>
<br>
<br>
<div class="main main-raised">
    <div class="container">

        <div class="section">
                       
           <div class="col-sm-12">
            <div class="card">
              
              <h1 class="text-center">Pago Reachazado por el banco.</h1>

               <div class="content"  align="center">
                 <img src="https://cdn.pixabay.com/photo/2020/06/02/13/28/declined-5251003_960_720.png"/>   
                </div>

                <br> 
      
         <div align="center">
            <a class="btn btn-primary btn-round" href="{{ url()->previous() }}">
                        Aceptar
                    </a>
        </div>
                
            </div>
            

        </div>

</div>

</div>

</div>

 

@include('includes.footer')
@endsection


  


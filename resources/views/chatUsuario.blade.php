@extends('layouts.app2')


@section('body-class', 'product-page')


@section('content')

<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
                        <h2 class="title text-center">Chat</h2>

           

           <div class="col-sm-12">
            <div class="card">
              

               <div class="card-body" id="app">
                <chat-app :user="{{ auth()->user() }}"></chat-app>
                </div>
            </div>
        </div>

</div>

</div>

</div>

 @yield('scripts')
  <script src="{{asset('js/app.js')}}" type="text/javascript"></script> <!--Añadimos el js generado con webpack, donde se encuentra nuestro componente vuejs-->

@include('includes.footer')
@endsection


  










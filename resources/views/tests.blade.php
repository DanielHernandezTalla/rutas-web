
@section('title', 'Listado de productos')

@section('body-class', 'product-page')

<style type="text/css">
  .section {
  text-align: center;
  }
.section .title{
  color: #4F4F4F;
}

.section .table{
  width: 100%;
  text-align: center;
}
.section .table td{
  border-bottom-style: solid;
  border-bottom-color: #D5D5D5;
  color: #6E6E6E;
  font-size: 14px;
}
.section .table th{
  color: #7C7C7C;
}
</style>


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

   




        <div class="section">
          @foreach ($pedidos as $pedido)
               @endforeach
            <h2 class="title">Pedidos del día: {{$pedido->order_date}} </h2>




            <div class="team">
                <div class="row">
               <div class="tabpanel">

    <!-- Nav tabs -->
   


    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
        <table class="table">
                        <thead>
                            <tr>
                              <th class="text-center">#idpedido</th>
                              <th class="text-center">Nombre cliente</th>
                              <th class="text-center">Estado</th>                             
                              <th class="text-center">Fecha</th>
                               <th class="text-center">Total</th>
                             
     
                                
                                                             
                               
                            </tr>
                        </thead>
                        <tbody>
                        <!--    @foreach ($pedidos as $pedido) -->
                            <tr>
                                <td class="text-center"> {{ $pedido->id }}</td>
                    <td class="text-center"> {{ $pedido->name }}</td>
  					<td class="text-center"> {{ $pedido->status }}</td>
                                <td class="text-center"> {{ $pedido->order_date }}</td>
                                <td class="text-center"> ${{ $pedido->importe_total }}</td>

                   
                               
                                
                    
                               

                                <td class="text-center" class="td-actions">
                            
                    
                                        
                    
                                                                   
                                </td>
                            </tr>
                         <!--   @endforeach -->
                        </tbody>
                    </table>
              </div>



   
                     
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>





@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
<style type="text/css">
    @media (max-width: 540px) {
    #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .titulo-corto{
    font-size: 20px;
}
.content .input-group{
    width: 100%;
    margin: 0 auto;
}
.content .input-group h3{
    font-size: 20px;
    font-weight: 500;
}
.content .input-group .material-icons{
    color: #495057;
}
#queerror{
font-size: 26px;
}
}

    @media (min-width: 541px) {
    #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .titulo-corto{
    font-size: 24px;
}
.content .input-group{
    width: 30%;
    margin: 0 auto;
}
.content .input-group h3{
    font-size: 28px;
    font-weight: 500;
}
.content .input-group .material-icons{
    color: #495057;
}
.content{
  width: 100%;
}

}

#nosencata{
  font-size: 20px;
  color: #9E9E9E;
}
#nosencata{
margin-top: -3%;

}
.row{
  width: 100%;
  text-align: center;
  margin-top: 3%;
}
#enviar{
  margin: 0 auto;
  font-size: 16px;
}
</style>

<div class="main main-raised">
    <div class="container">

        <div class="section">
          @if (session('notification2'))
                        <div style="background-color: #F41C1C;" class="alert alert-success2">
                            {{ session('notification2') }}
                        </div>         
                    @endif  
                    @if (session('notification'))
                        <div style="background-color: #33D872;" class="alert alert-success">
                            {{ session('notification') }}
                        </div>         
                    @endif  
            <h2 id="queerror" class="title text-center">¿Que error has encontrado?</h2>

             <h2 id="nosencata" class="title text-center">Nos encata saber tu opinion</h2>    
        </div>
 
                    <form method="post" action="{{ url('/reportar') }}">
  {!! csrf_field() !!}    
                        <div class="content">
                            <div  class="input-group">
                                <textarea id="w3review" name="mensajerror" rows="4" cols="45"></textarea>  
                            </div> 
                            <div class="row">
            <button id="enviar" type="submit"  class="btn btn-primary btn-round">
                    Enviar 
                    </button>
                 

                    </div>
        </div>
 </form>
    </div>


@include('includes.footer')
@endsection

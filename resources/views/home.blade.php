@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
    <script>
        function myFunction() {

            if (window.matchMedia('(max-width: 540px)').matches) {
                document.getElementById("menufooters").style.position = "static";
            } else {
                document.getElementById("menufooters").style.position = "fixed";
            }

        }

        function myFunction2() {
            document.getElementById("menufooters").style.position = "fixed";
        }
    </script>
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <style type="text/css">
        .btn-estadio {
            background-color: #ff7514;
            color: #FFFFFF;
        }

        .cantidad {
            position: relative;
            float: right;
            z-index: 10000;
            display: inline-block;
            color: #000000;
            padding-right: 8px;
            font-size: 18px;


        }



        ul#nav li.active .cantidad {
            position: relative;
            float: right;
            z-index: 10000;
            display: inline-block;
            color: #FFFFFF;
            padding-right: 8px;


        }

        .form-control {
            display: inline-block;
            padding: .375rem .75rem;
            font-size: 16px;
            line-height: 1.6;
            color: #495057;
            background-color: #fff;
            background-clip: padding-box;
            border: 1px solid #ccd0d2;
            border-radius: 2px;
            border-left-color: #fff;
            border-top-color: #fff;
            border-right-color: #fff;
            width: 19%;
            text-align: center;
        }

        #fechaE:focus {
            color: #ff7514;
        }

        .wrapper {
            background-color: #fff;
        }

        .footer {
            background-color: #fff;
        }

        .main {
            -webkit-box-shadow: 10px 10px 5px 200px rgba(255, 255, 255, 1);
            -moz-box-shadow: 10px 10px 5px 200px rgba(255, 255, 255, 1);
            box-shadow: 10px 10px 5px 200px rgba(255, 255, 255, 1);
        }

        * {
            box-sizing: border-box
        }

        body {
            font-family: "Lato", sans-serif;
        }

        /* Style the tab */
        .tab {
            float: left;
            border: none;
            background-color: #f1f1f1;
            width: 15%;
            height: 600px;
            border-top-left-radius: 25px;
            border-bottom-left-radius: 25px;
        }

        /* Style the buttons inside the tab */
        .tab button {
            display: block;
            background-color: inherit;
            color: black;

            width: 100%;
            border: none;
            border-top-style: solid;
            border-top-color: #CFCFCF;
            border-top-width: 1px;
            outline: none;
            text-align: left;
            cursor: pointer;
            transition: 0.3s;
            font-size: 12px;
            height: 50px;
            color: #949494;
            font-weight: 600;
        }

        /* Change background color of buttons on hover */
        .tab button:hover {
            background-color: #ddd;
        }

        /* Create an active/current "tab button" class */
        .tab button.active {
            background-color: #3E3E3E;
        }

        /* Style the tab content */
        .tabcontent {
            float: left;
            padding: 0px 12px;
            border: none;
            width: 85%;
            border-left: none;
            height: 600px;
            overflow-y: scroll;
        }

        .table td {
            text-align: center;
            color: #949494;
            font-weight: 600;

        }

        .table td .hyper {
            color: #949494;
            font-size: 16px;
            font-weight: 500;
        }

        .alert-success2 {
            background-color: #FC1313;
        }

        @media (min-width: 540px) {

            #carrito1 {
                display: none;
            }

            #tab1 {
                display: none;
            }

            #opciones1 {
                display: none;
            }

            .titulo {
                display: none;
            }

            #prin {
                margin-top: 220px;
            }
        }

            {
            -- Celular --
        }

        @media (max-width: 540px) {
            .toplink .material-icons {
                position: absolute;
                right: 5px;

                outline: 0;
                font-size: 30px;

            }

            html {
                background-color:
                    #fff;
            }

            #carrito2 {
                display: none;
            }

            .tab {
                float: left;
                border: none;
                background-color: #f1f1f1;
                width: 100%;
                height: auto;
                border-top-left-radius: 25px;
                border-bottom-left-radius: 25px;
                display: flex;
                justify-content: center;
            }

            /* Style the buttons inside the tab */
            .tab button {
                display: block;
                background-color: inherit;
                color: black;

                width: 25%;
                border: none;
                border-top-style: solid;
                border-top-color: #CFCFCF;
                border-top-width: 1px;
                outline: none;
                text-align: left;
                cursor: pointer;
                transition: 0.3s;
                font-size: 12px;
                height: 50px;
                color: #949494;
                font-weight: 600;

            }

            /* Change background color of buttons on hover */
            .tab button:hover {
                background-color: #ddd;
            }

            /* Create an active/current "tab button" class */
            .tab button.active {
                background-color: #3E3E3E;
            }

            .section {
                width: 100%;
            }

            /* Style the tab content */
            .tabcontent {
                float: left;
                padding: 0px 0px;
                border: none;
                width: 100%;
                border-left: none;
                height: auto;
                overflow-y: scroll;
            }

            .table {
                width: 100%;
            }

            .table td {
                text-align: center;
                color: #949494;
                font-weight: 600;
                font-size: 9px;

            }

            .table td .hyper {
                color: #949494;
                font-size: 9px;
                font-weight: 500;
            }

            .table th {
                font-size: 9px;

            }

            #tab2 {
                display: none;
            }

            #London {
                padding-top: 20px;
            }

            .titulo {
                width: 100%;

            }

            .titulo .tituloruta {
                position: relative;
                margin-top: -5%;
                color: #858585;
                margin-left: -0%;
                font-size: 26px;
                font-weight: 500;
                text-align: center;
            }

            .titulo .volver {
                font-size: 7px;
                color: #858585;
            }

            .toplink .material-icons {
                position: absolute;

                outline: 0;
                font-size: 30px;

            }

            .toplink {
                font-size: 100px;
            }

            .toplink .material-icons:hover {
                color: #000;
            }

            #opciones2 {
                display: none;
            }

            .precio {
                color: #000;
                font-size: 40px;
                font-weight: 500;
                display: inline-block;
                width: auto;
            }

            #defaultOpen {
                border-top-left-radius: 10px;
            }

            #final {
                border-top-right-radius: 10px;
            }

            #tab1 {
                border-top-left-radius: 10px;
                border-top-right-radius: 10px;
            }

            #alimen {
                color: #838383;
                font-size: 14px;
                font-weight: 400;
            }

            #politica {
                color: #419AFF;
                font-size: 12px;
                text-decoration-line: underline;
            }

            .longdescripcion {
                font-size: 11px;
                color: #AAAAAA;
                font-weight: 400;
                text-align: center;
                margin-bottom: -5%;
                padding-top: 5%;
            }

            #cityssas {
                display: none;
            }
        }

        @media (min-width: 540px) {
            .mensajes_para_usuarios {
                display: none;
            }
    </style>
    </style>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>jQuery UI Datepicker - Default functionality</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="/resources/demos/style.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script>
        $(function() {
            $("#datepicker").datepicker();
        });
    </script>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">


    <div class="main main-raised">

        <div id="prin" class="container">

            @foreach ($DatPrecios as $centrodeventa)
                <input type="hidden" name="idcentrovta" value="{{ $centrodeventa->idcentroventa }}">
            @endforeach

            @foreach ($ruta as $rutas)
                <input type="hidden" id="tipopedido" name="tipopedido" value="{{ $rutas->tipopedido }}">
                <input type="hidden" id="organizacion" name="organizacion" value="{{ $rutas->organizacion }}">
            @endforeach

            <div class="section" style="width:100%; margin: 0 auto;  overflow-y: scroll; margin-top: -100px;">

                @if (session('notification2'))
                    <div style="background-color: #F41C1C;" class="alert alert-success2">
                        {{ session('notification2') }}
                    </div>
                @endif
                @if (session('notification'))
                    <div style="background-color: #33D872;" class="alert alert-success">
                        {{ session('notification') }}
                    </div>
                @endif

                <div class="tab" id="tab1" style="width:100%; margin: 0 auto;">

                    <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen"><i
                            style="font-size: 14px;" class="material-icons">shopping_cart</i><i
                            style="color: #FF941C; font-style: normal; font-size: 12px;">
                            @foreach ($carts as $cart)
                                {{ auth()->user()->cart->details->count() }}</h4>
                            @endforeach
                        </i>
                    </button>
                    <button class="tablinks" onclick="openCity(event, 'Paris')"><i style="font-size: 12px;"
                            class="material-icons">flag</i><i style="color: #FF941C; font-style: normal; font-size: 14px;">
                            @foreach ($carts as $cart)
                                {{ $cart->cart_count }}
                            @endforeach
                        </i>
                    </button>
                    <button class="tablinks" onclick="openCity(event, 'Tokyo')"><i style="font-size: 12px;"
                            class="material-icons">attach_money</i><i
                            style="color: #FF941C; font-style: normal; font-size: 14px;">
                            @foreach ($carts3 as $cart3)
                                {{ $cart3->cart_count }}
                            @endforeach
                        </i>
                    </button>

                    <button class="tablinks" id="final" onclick="openCity(event, 'Moscu')"><i style="font-size: 12px;"
                            class="material-icons">shopping_bag</i><i
                            style="color: #FF941C; font-style: normal; font-size: 15px;">
                            @foreach ($carts4 as $cart4)
                                {{ $cart4->cart_count }}
                            @endforeach
                        </i>
                    </button>

                </div>

                <div class="tab" id="tab2">
                    <h2 style="font-size: 18px; color: #000; font-weight: 600;" class="title text-center">Panel de compras
                    </h2>
                    <button class="tablinks" onclick="openCity(event, 'London')" id="defaultOpen"><i
                            style="font-size: 12px;" class="material-icons">shopping_cart</i> Carrito de compras<i
                            style="color: #FF941C; font-style: normal; font-size: 14px; margin-left: 16px;">
                            @foreach ($carts as $cart)
                                {{ auth()->user()->cart->details->count() }}</h4>
                            @endforeach
                        </i>
                    </button>
                    <button class="tablinks" onclick="openCity(event, 'Paris')"><i style="font-size: 12px;"
                            class="material-icons">flag</i> Pedidos pendientes <i
                            style="color: #FF941C; font-style: normal; font-size: 14px; margin-left: 14px;">
                            @foreach ($carts as $cart)
                                {{ $cart->cart_count }}
                            @endforeach
                        </i>
                    </button>
                    <button class="tablinks" onclick="openCity(event, 'Tokyo')"><i style="font-size: 12px;"
                            class="material-icons">attach_money</i> Pagos pendientes <i
                            style="color: #FF941C; font-style: normal; font-size: 14px; margin-left: 25px;">
                            @foreach ($carts3 as $cart3)
                                {{ $cart3->cart_count }}
                            @endforeach
                        </i>
                    </button>
                    <button class="tablinks" onclick="openCity(event, 'Df')"><i style="font-size: 12px;"
                            class="material-icons">shopping_bag</i> Pago en linea</button>
                    <button class="tablinks" onclick="openCity(event, 'Moscu')"><i style="font-size: 12px;"
                            class="material-icons">shopping_bag</i> Pedidos hechos <i
                            style="color: #FF941C; font-style: normal; font-size: 15px; margin-left: 35px;">
                            @foreach ($carts4 as $cart4)
                                {{ $cart4->cart_count }}
                            @endforeach
                        </i>
                    </button>

                </div>

                <div id="London" class="tabcontent">

                    <p>
                    <div class="tab-pane fade in active" id="dashboard" role="tabpanel" aria-labelledby="home-tab">


                        <table class="table" id="carrito2">
                            <thead>
                                <tr
                                    style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; ">
                                    <th class="text-center">#</th>
                                    <th>Nombre</th>
                                    <th class="text-center">Precio</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">UM</th>
                                    <th class="text-center">Iva</th>
                                    <th class="text-center">SubTotal</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>

                            <tbody>
                              
                                @foreach (auth()->user()->cart->details as $detail)
                                    <tr
                                        style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; border-top-style: none; height: 100px;">
                                        <td class="text-center">
                                            <img style="width: 80px; height: 80px;"
                                                src="{{ $detail->product->featured_image_url }}" height="50">
                                        </td>
                                        <td
                                            style="width:250px;  text-transform: lowercase; vertical-align: middle; text-align: left;">
                                            <a class="hyper" href="{{ url('/products/' . $detail->product->id) }}"
                                                target="_blank">{{ $detail->product->name }}</a>
                                        </td>
                                        <td style="vertical-align: middle;">$ {{ $detail->precio_producto }}</td>
                                        <td style="vertical-align: middle;">{{ $detail->quantity }}</td>
                                        <td style="vertical-align: middle;">{{ $detail->uom1 }}</td>
                                        <td style="vertical-align: middle;">$ {{ $detail->iva }}</td>
                                        <td style="vertical-align: middle;">$ {{ $detail->importe }}</td>
                                        <td style="vertical-align: middle; margin: 100px;" class="td-actions">
                                            <form method="post" action="{{ url('/cart/' . $detail->id_cart_details) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <input type="hidden" name="cart_detail_id" id="campo1"
                                                    value="{{ $detail->id_cart_details }}">
                                                <input type="hidden" name="productid" id="productid"
                                                    value="{{ $detail->product_id }}">

                                                <a style="margin: -12px;"
                                                    href="{{ url('/products/' . $detail->product->id) }}" target="_blank"
                                                    rel="tooltip" title="Ver producto"
                                                    class="btn btn-info btn-simple btn-xs">
                                                    <i class="fa fa-info"></i>
                                                </a>

                                                <a style="margin: -12px;" data-toggle="modal"
                                                    href="{{ url('/homeedit/' . $detail->id_cart_details . '/' . $detail->product_id) }}"
                                                    rel="tooltip" title="Editar"
                                                    class="btn btn-success btn-simple btn-xs">
                                                    <i class="fa fa-pencil-square-o"></i>
                                                </a>

                                                <form method="post" action="{{ url('/deleteDetalle') }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button style="margin: -12px;"
                                                        onclick="return confirm('¿Seguro desea eliminar este producto?');"
                                                        type="submit" rel="tooltip" title="Eliminar"
                                                        class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times"></i>
                                                    </button>
                                                </form>
                                @endforeach



                                </form>


                                </td>

                                </tr>

                            </tbody>
                        </table>


                        <div class="titulo">
                            <a href="/" class="toplink"><i class="material-icons">replay</i></a><a href="/"
                                class="volver"></a>

                            <h3 class="tituloruta"> Carrito de compras</h3>
                        </div>

                        <table class="table" id="carrito1">

                            <thead>
                                <tr
                                    style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; ">
                                    <th class="text-center">#</th>

                                    <th class="text-center">Precio</th>
                                    <th class="text-center">Cantidad</th>
                                    <th class="text-center">UM</th>
                                    <th class="text-center">Iva</th>
                                    <th class="text-center">SubTotal</th>
                                    <th class="text-center">Opciones</th>
                                </tr>
                            </thead>

                            <tbody>
                                @foreach (auth()->user()->cart->details as $detail)
                                    <tr
                                        style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; border-top-style: none; height: 100px;">
                                        <td class="text-center" style="  ">
                                            <img style="width: 40px; height: 40px;"
                                                src="{{ $detail->product->featured_image_url }}" height="50">
                                            <a style="font-size: 8px;" class="hyper"
                                                href="{{ url('/products/' . $detail->product->id) }}"
                                                target="_blank">{{ $detail->product->name }}</a>
                                        </td>

                                        <td style="vertical-align: middle;">$ {{ $detail->precio_producto }}</td>
                                        <td style="vertical-align: middle;">{{ $detail->quantity }}</td>
                                        <td style="vertical-align: middle;">{{ $detail->uom }}</td>
                                        <td style="vertical-align: middle;">$ {{ $detail->iva }}</td>
                                        <td style="vertical-align: middle;">$ {{ $detail->importe }}</td>
                                        <td style="vertical-align: middle;" class="td-actions">
                                            <form method="post" action="{{ url('/cart/' . $detail->id_cart_details) }}">
                                                {{ csrf_field() }}
                                                {{ method_field('DELETE') }}
                                                <input type="hidden" name="cart_detail_id" id="campo1"
                                                    value="{{ $detail->id_cart_details }}">
                                                <input type="hidden" name="productid" id="productid"
                                                    value="{{ $detail->product_id }}">

                                                <a style="margin: -12px; display: none;"
                                                    href="{{ url('/products/' . $detail->product->id) }}" target="_blank"
                                                    rel="tooltip" title="Ver producto"
                                                    class="btn btn-info btn-simple btn-xs">
                                                    <i class="fa fa-info" style="font-size: 12px;"></i>
                                                </a>

                                                <a style="margin: -12px;" data-toggle="modal"
                                                    href="{{ url('/homeedit/' . $detail->id_cart_details . '/' . $detail->product_id) }}"
                                                    rel="tooltip" title="Editar"
                                                    class="btn btn-success btn-simple btn-xs">
                                                    <i class="fa fa-pencil-square-o" style="font-size: 14px;"></i>
                                                </a>

                                                <form method="post" action="{{ url('/deleteDetalle') }}">
                                                    {{ csrf_field() }}
                                                    {{ method_field('DELETE') }}
                                                    <button style="margin: -12px;"
                                                        onclick="return confirm('¿Seguro desea eliminar este producto?');"
                                                        type="submit" rel="tooltip" title="Eliminar"
                                                        class="btn btn-danger btn-simple btn-xs">
                                                        <i class="fa fa-times" style="font-size: 14px;"></i>
                                                    </button>
                                                </form>
                                @endforeach



                                </form>


                                </td>

                                </tr>

                            </tbody>
                        </table>

                        <form method="post" action="{{ url('/order/' . $centrodeventa->idcentroventa) }}">
                            {{ csrf_field() }}
                            @foreach ($carts as $cart)
                                <table id="opciones1" style="width: 100%;" align="center">
                                    <tr>


                                        <td style="width: 25%; text-align: center; padding-bottom: 20px;">
                                            <select name="direccionselect"
                                                style="border-style: solid; border-color: #fff; font-family: Arial;">



                                                @foreach ($di as $dis)
                                                    <option value="{{ $dis->tipopedido }}">
                                                        {{ $dis->direccion }}
                                                    </option>
                                                @endforeach

                                            </select>


                                        </td>

                                    </tr>

                                    <tr>
                                        <td style="width: 25%; text-align: center;"> Requiere factura: <input
                                                type="checkbox" id="factura" name="factura" value="si"></td>
                                    </tr>
                                    <tr>

                                        <td style="width: 25%; text-align: center;">


                                            <p><input type="text" name="fechaE" id="datepicker" placeholder="fecha">
                                            </p>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td style="width: 40%; text-align: center;">
                                            <p class="longdescripcion">importe a pagar (total):</p>
                                            <h4 align="center" class="precio"> ${{ auth()->user()->cart->total }}</h4>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td style="width:25%; text-align: center;">


                                            <input type="number" hidden name="contador"
                                                value="{{ $cart->cart_count }}">
                                            <input type="number" hidden name="count_products"
                                                value="{{ auth()->user()->cart->details->count() }}">
                            @endforeach
                            @foreach ($carts3 as $cart3)
                                <input type="number" hidden name="contador2" value="{{ $cart3->cart_count }}">
                            @endforeach






                            <button style="text-transform: lowercase; font-size: 20px; "
                                class="btn btn-primary btn-round">
                                Realizar pedido
                            </button>
                            </td>
                            </tr>
                            </table>

                        </form>

                        <div style="text-align:left; display: 
none">
                            <a id="btn-AddDate" class=" btn btn-xs btn-estadio" data-toggle="modal"
                                data-target="#myModal">
                                Direccion del envio

                            </a>
                        </div>




                    </div>
                    </p>
                </div>
                <div id="Paris" class="tabcontent">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>

                                <th class="text-center">Total</th>
                                <th class="text-center">Fecha</th>
                                <th class="text-center">Fecha de entrega</th>
                                <th class="text-center">Nombre cliente</th>


                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($unaut as $unauts)
                                <tr>
                                    <td class="text-center"> <a href="{{ url('/homePendiente/' . $unauts->id) }}"
                                            rel="tooltip" title="Pagar">
                                            <i style="font-size: 16px">{{ $unauts->id }}</i>
                                        </a></td>

                                    <td class="text-center"> {{ $unauts->importe_total }}</td>
                                    <td class="text-center"> {{ $unauts->order_date }}</td>
                                    <td class="text-center"> {{ $unauts->fechaE }}</td>
                                    <td class="text-center"> {{ $unauts->name }}</td>
                            @endforeach

                            </tr>



                            </td>


                            </tr>

                        </tbody>
                    </table>
                </div>

                <div id="Tokyo" class="tabcontent">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>

                                <th class="text-center">Total</th>
                                <th class="text-center">Fecha</th>
                                <th class="text-center">Fecha de entrega</th>
                                <th class="text-center">Nombre cliente</th>

                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($pendientes as $pendiente)
                                <tr>
                                    <td class="text-center"> <a href="{{ url('/confirmacion/show/' . $pendiente->id) }}"
                                            rel="tooltip" title="Pagar">
                                            <i style="font-size: 16px">{{ $pendiente->id }}</i>
                                        </a></td>

                                    <td class="text-center"> {{ $pendiente->importe_total }}</td>
                                    <td class="text-center"> {{ $pendiente->order_date }}</td>
                                    <td class="text-center"> {{ $pendiente->fechaE }}</td>
                                    <td class="text-center"> {{ $pendiente->name }}</td>
                            @endforeach

                            </tr>



                            </td>


                            </tr>

                        </tbody>
                    </table>
                </div>

                <div id="Df" class="tabcontent">


                </div>

                <div id="Moscu" class="tabcontent">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">#</th>

                                <th class="text-center">Total</th>
                                <th class="text-center">Fecha</th>
                                <th class="text-center">Fecha de entrega</th>
                                <th class="text-center">Nombre cliente</th>
                                <th class="text-center">Comprar nuevamente</th>

                            </tr>
                        </thead>

                        <tbody>
                            @foreach ($despachados as $despachado)
                                <tr>
                                    <td class="text-center"> {{ $despachado->id }}</td>

                                    <td class="text-center"> ${{ $despachado->importe_total }}</td>
                                    <td class="text-center"> {{ $despachado->order_date }}</td>
                                    <td class="text-center"> {{ $despachado->fechaE }}</td>
                                    <td class="text-center"> {{ $despachado->name }}</td>
                                    <td class="text-center"> <a rel="tooltip" title="Volver a comprar" class="">
                                            <i style="font-size: 25px; font-weight: bold;"
                                                class="material-icons">replay</i>
                                        </a></td>
                            @endforeach

                            </tr>



                            </td>


                            </tr>




                        </tbody>
                    </table>
                </div>
                <script>
                    function openCity(evt, cityName) {
                        var i, tabcontent, tablinks;
                        tabcontent = document.getElementsByClassName("tabcontent");
                        for (i = 0; i < tabcontent.length; i++) {
                            tabcontent[i].style.display = "none";
                        }
                        tablinks = document.getElementsByClassName("tablinks");
                        for (i = 0; i < tablinks.length; i++) {
                            tablinks[i].className = tablinks[i].className.replace(" active", "");
                        }
                        document.getElementById(cityName).style.display = "block";
                        evt.currentTarget.className += " active";
                    }

                    // Get the element with id="defaultOpen" and click on it
                    document.getElementById("defaultOpen").click();
                </script>



            </div>





            <form method="post" action="{{ url('/order/' . $centrodeventa->idcentroventa) }}">
                {{ csrf_field() }}
                @foreach ($carts as $cart)
                    <table id="opciones2" style="width: 75%;" align="right">
                        <tr>


                            <td style="width: 25%; text-align: center;">
                                <select name="direccionselect"
                                    style="border-style: solid; border-color: #fff; font-family: Arial;">



                                    @foreach ($di as $dis)
                                        <option value="{{ $dis->tipopedido }}">
                                            {{ $dis->direccion }}
                                        </option>
                                    @endforeach

                                </select>


                            </td>

                            <td style="width: 25%; text-align: center;"> Requiere factura: <input type="checkbox"
                                    id="factura" name="factura" value="si"></td>

                            <td style="width: 25%; text-align: center;">


                                <input style="width: 80%;color: #949494;" align="right" type="datetime-local"
                                    class="form-control" id="fechaE" name="fechaE2" value="">
                            </td>


                            <td style="width: 40%; text-align: center;">
                                <h4 align="right" style="color: #949494;"><strong style="color: #949494;">Importe a
                                        pagar: </strong> ${{ auth()->user()->cart->total }}</h4>
                            </td>


                            <td style="width:25%; text-align: right;">


                                <input type="number" hidden name="contador" value="{{ $cart->cart_count }}">
                                <input type="number" hidden name="count_products"
                                    value="{{ auth()->user()->cart->details->count() }}">
                @endforeach
                @foreach ($carts3 as $cart3)
                    <input type="number" hidden name="contador2" value="{{ $cart3->cart_count }}">
                @endforeach


                <script>
                    $(document).ready(function() {
                        $(document).on('submit', 'form', function() {
                            $('button').attr('disabled', 'disabled');
                        });
                    });
                </script>


                <button style="text-transform: lowercase; font-size: 20px; " class="btn btn-primary btn-round">
                    Realizar pedido
                </button>

            </form>
            </td>
            </tr>
            </table>
            <div class="text-center">



            </div>
        </div>
        <h5 class="mensajes_para_usuarios"
            style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">
            Esta aplicacion es para uso escusivo de negocios</h5>
        <h5 class="mensajes_para_usuarios"
            style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -20%;">
            Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
    </div>







    </div>


    </div>

    </div>

    </div>

    <div class="modal fade" id="modalAddToCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Agregar Otro Producto</h4>
                </div>
                @foreach (auth()->user()->cart->details as $detail)
                    <form method="post" action="{{ url('/updateCarrito') }}">
                        {{ csrf_field() }}
                        <input type="hidden" name="id_cart_details" value="{{ $detail->id_cart_details }}">
                @endforeach

                <div class="modal-body">
                    <div class="row">


                        <div class="col-sm-4">
                            <div class="form-group label-floating">
                                <label class="control-label">Cantidad del producto</label>


                                @foreach (auth()->user()->cart->details as $detail)
                                    <input name="clon" value="{{ $detail->product_id }}">
                                    <input name="clon2" value="{{ $detail->iddatprecio }}">
                                @endforeach

                                @foreach ($product as $products)
                                    <?php if($products->factor == "0"){ ?>
                                    <td class="blue"><input type="number" name="quantity" value="1.0"
                                            step="0.01" class="form-control"> KG</td>
                                    <?php }else{ ?>
                                    <td class="blue"><input type="number" name="quantity" value="1"
                                            class="form-control"> Pza</td>
                                    <?php } ?>
                                @endforeach

                            </div>
                        </div>


                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple"
                                data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-info btn-simple">Guardar</button>
                        </div>
                    </div>
                </div>
                </form>
            </div>
        </div>

    </div>






    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                </div>

                <div class="modal-body">
                    <div class="row">

                        <table class="table">
                            <thead>
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Direccion</th>
                                    <th class="text-center">Seleccionar</th>

                                </tr>
                            </thead>

                            <tbody>
                                @foreach ($Direccion as $Direcciones)
                                    <tr>
                                        <td class="text-center"> {{ $Direcciones->id }}</td>
                                        <td class="text-center"> {{ $Direcciones->direccion }}</td>
                                        <td class="text-center"> <input type="radio" id="male" name="gender"
                                                value="male"></td>
                                @endforeach

                                </tr>



                                </td>


                                </tr>

                            </tbody>
                        </table>

                        <br>
                        <br>
                        <a id="btn-AddDate" class=" btn btn-xs btn-estadio" data-toggle="modal"
                            data-target="#myModalDireccion">Agregar nueva dirección</a>

                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-simple"
                                data-dismiss="modal">Cancelar</button>
                            <button type="submit" class="btn btn-info btn-simple">Guardar</button>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>




    <div class="modal fade" id="myModalDireccion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>

                </div>

                <div class="modal-body">
                    <div class="row">

                        <div class="header header-primary text-center">
                            <h4>Registrar nueva direccion</h4>
                        </div>
                        <br>
                        <div class="content">
                            <form method="post" action="{{ url('/createdirec') }}">
                                {{ csrf_field() }}
                                <div class="input-group">



                                    <span class="input-group-addon">
                                        <i class="material-icons"></i>
                                    </span>
                                    <input style="display: none;" id="iduser" type="text"
                                        placeholder="Numero Interior" class="form-control" name="iduser"
                                        value="{{ auth()->user()->id }}">
                                </div>


                                <label>*Algunas ciudades no se encuentran disponibles de momento</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">location_city</i>
                                    </span>

                                    <select name="ciudad" id="citys" class="form-control">
                                        <option selected>Seleciona una ciudad</option>
                                        @foreach ($citys as $id => $city)
                                            <option value="{{ $city }}">
                                                {{ $city }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>



                                <label for="">*Algunos codigos postales no se encuentran disponibles de
                                    momento</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">mark_email_unread</i>
                                    </span>

                                    <select name="codigoPostal" id="colonias" class="form-control">
                                        <option selected>Seleciona un codigo postal</option>

                                    </select>
                                </div>


                                <label for="">*Algunas colonias no se encuentran disponibles de momento</label>
                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">house</i>
                                    </span>

                                    <select name="colonia" id="codigoPostales" class="form-control">


                                    </select>
                                </div>




                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">drag_handle</i>
                                    </span>
                                    <input id="address" type="text" placeholder="Calle" class="form-control"
                                        name="address" value="{{ old('address') }}" required>
                                </div>


                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">format_list_numbered</i>
                                    </span>
                                    <input id="numeroExterior" type="text" placeholder="Numero Exterior"
                                        class="form-control" name="numeroExterior" value="{{ old('numeroExterior') }}"
                                        required>
                                </div>

                                <div class="input-group">
                                    <span class="input-group-addon">
                                        <i class="material-icons">format_list_numbered</i>
                                    </span>
                                    <input id="numeroInterior" type="text" placeholder="Numero Interior"
                                        class="form-control" name="numeroInterior" value="{{ old('numeroInterior') }}">
                                </div>

                        </div>
                    </div>

                    <div class="modal-footer">

                        <button type="submit" class="btn btn-info btn-simple">Guardar</button>
                        </form>
                        <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
                    </div>
                </div>
            </div>

        </div>
    </div>


    <script>
        $('#citys').on('change', function(e) {
            console.log(e);

            var cit_id = e.target.value;

            //ajax
            $.get('/ajax-colonias?cit_id=' + cit_id, function(data) {
                $('#colonias').empty();

                $.each(data, function(index, coloniaObj) {

                    $('#colonias').append('<option value="' + coloniaObj.codigopostal + '">' +
                        coloniaObj.codigopostal + '</option>');




                });
            });

        });
    </script>
    <script>
        $('#colonias').on('change', function(e) {
            console.log(e);

            var col_id = e.target.value;

            //ajax
            $.get('/ajax-codigoPostales?col_id=' + col_id, function(data) {
                $('#codigoPostales').empty();

                $.each(data, function(index, codigoPostalesObj) {

                    $('#codigoPostales').append('<option value="' + codigoPostalesObj.colonia +
                        '">' + codigoPostalesObj.colonia + '</option>');



                });
            });

        });
    </script>

    </div>

    @include('includes.footer')


@endsection

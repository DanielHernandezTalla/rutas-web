@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
            <h2 class="title text-center">Cambiar cantidad de producto</h2>

            

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif


            @foreach ($product as $products)
 <form method="post" action="{{ url('/updateCarrito/'.$products->id_cart_details) }}">
           
                
                {{ csrf_field() }}

                <div class="row">

                <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">ID detalle</label>
                            <input disabled type="text" class="form-control" name="id_cart_details" value="{{ old('id_cart_details', $products->id_cart_details) }}">
                        </div>
                    </div>

                    <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">Nombre</label>
                            <input disabled type="text" class="form-control" name="name" value="{{ old('name', $products->name) }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">precio</label>
                            <input disabled type="text" class="form-control" name="price" value="{{ old('price', $products->price) }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">Cantidad del  producto</label>

        <input type="hidden" name="product_id" value="{{ $products->id }}">
        <input type="hidden" name="factor" value="{{ $products->factor }}">
        
                              <?php if($products->factor == "0"){ ?>

<td class="blue"><input type="number" name="quantity" value="{{ $products->quantity }}" step="0.01" class="form-control"> KG</td>
<?php }else{ ?>
<td class="blue"><input type="number" name="quantity" value="1" class="form-control"> Pza</td>
<?php } ?>

                          
                        </div>
                    </div>

                   
                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">UOM</label>
                            <input disabled type="text" class="form-control" name="uom" value="{{ old('uom', $products->uom) }}">
                        </div>
                    </div>

           

                    
<div class="col-sm-2">
                 <button type="submit" class="btn btn-primary">Guardar</button> 
             
              </form>  
              
   </div>



             
           
                </div>

            @endforeach
                <a href="{{  url()->previous()  }}" class="btn btn-default">Atras</a>


           

        </div>

        
    </div>
</div>

    
@include('includes.footer')
@endsection

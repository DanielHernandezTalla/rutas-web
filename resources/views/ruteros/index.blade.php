@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<style type="">
	@media (max-width: 540px) {
  #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .table{
  	width: 100%;
  }
  .table tr td{
  	font-size: 10px;
  }

  .table tr th{
  	font-size: 10px;
  }
  #invisible{
  	display: none;
  }
}
	  .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>



<div class="main main-raised"style="background-color: #fff;-webkit-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);">
    <div class="container">
<a href="/rutero/reportepedidos" title="Reporte pedidos" class="toplink"><i class="material-icons">print</i> </a>
        <div align="right">
        	  <button class="btn btn-primary btn-round" onclick="javascript:window.location.reload();">
                        <i class="material-icons">autorenew</i> Actualizar pedidos
                    </button>
        </div>




        <div class="section text-center">
            <h2 class="title">Tus rutas</h2>

@if (session('notification2'))
                        <div style="background-color: #F41C1C;" class="alert alert-success2">
                            {{ session('notification2') }}
                        </div>         
                    @endif  
                    @if (session('notification'))
                        <div style="background-color: #33D872;" class="alert alert-success">
                            {{ session('notification') }}
                        </div>         
                    @endif


            <div class="team">
                <div class="row">
               <div class="tabpanel">

    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Pendientes</a>
              <!--     @foreach ($contar as $conta)
<input type="team" name="polla" value="{{ $conta->cart_count }}">
    @endforeach -->
                </li>

                  <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="auth-tab" data-toggle="tab" href="#entregado" role="tab" aria-controls="auth" aria-selected="false">Entregados</a>
                </li>

                <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="auth-tab" data-toggle="tab" href="#auth" role="tab" aria-controls="auth" aria-selected="false">Pagados</a>
                </li>

               

              </ul>

    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
				<table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">#</th>
				                    	<th class="text-center" id="invisible">Estado</th>
				                    	<th class="text-center">Total</th>
				                    	<th class="text-center" id="invisible">Fecha</th>
				                        <th class="text-center" id="invisible">Hora</th>
										<th class="text-center">Nombre cliente</th>
				                        <th class="text-center" id="invisible">Telefono</th>
				                        <th class="text-center">Direccion</th>
										<th class="text-center" id="invisible">Ciudad</th>
										<th class="text-center" id="invisible">Ruta</th>
										<th>Modificar/Entegado</th>
				                    </tr>
				                </thead>
				                <tbody>
				                <!--    @foreach ($pedidos as $pedido) -->
				                    <tr>
				                    	   <form id="myform" action="{{ url('/rutero/cambio1/'.$pedido->id) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
				                        <td class="text-center"> {{ $pedido->id }}</td>
										<td class="text-center" id="invisible"> {{ $pedido->status }}</td>
				                        <td class="text-center"> {{ $pedido->importe_modificar }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedido->order_date }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedido->created_at }}</td>
				                        <td class="text-center"> {{ $pedido->name }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedido->phone }}</td>
				                        <td class="text-center"> {{ $pedido->address }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedido->city }}</td> 
				                        <td class="text-center" id="invisible"> {{ $pedido->descentro }}</td> 
				                        
										
				                        

				                        <td class="text-center" class="td-actions">
				                    
				            <!--       <a  href="{{ url('/promo/'.$pedido->id) }}" rel="tooltip" title="Guardar" class="">
<i style="font-size: 25px; font-weight: bold; margin-top: -20%;" class="material-icons">save</i>
</a>
-->
                                       
                                     
                                        
				                              
				                              <a style="margin-top: -0%; font-size: 15px;" href="/rutero/modificar/{{$pedido->id}}/{{$pedido->centro}}" rel="tooltip" title="Modificar" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a> 
                                        <button onclick="return confirm('¿Seguro que el pedido ya esta listo?');" style=" margin-top: -15%;border-style: none; background-color: #ff7514; color: #fff; font-size: 10px;  font-weight: 500; border-radius: 10%;" type="submit">
                                     Entregado</button>                                
				                        </td>
				                    </form>
				                    </tr>
				                 <!--   @endforeach -->
				                </tbody>
				            </table>
              </div>

              <div class="tab-pane fade" id="auth" role="tabpanel" aria-labelledby="auth-tab">
				<table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">#</th>
				                    	<th class="text-center" id="invisible">Estado</th>
				                    	<th class="text-center">Total</th>
				                    	<th class="text-center" id="invisible">Fecha</th>
				                        <th class="text-center" id="invisible">Hora</th>
										<th class="text-center">Nombre cliente</th>
				                        <th class="text-center" id="invisible">Telefono</th>
				                        <th class="text-center">Direccion</th>
										<th class="text-center" id="invisible">Ciudad</th>
										
				                        
				                       
				                    </tr>
				                </thead>
				                <tbody>
				   <!--                 @foreach ($pedidosAuth as $pedidosAuths)  -->
				                    <tr>
				                       <td class="text-center"> {{ $pedidosAuths->id }}</td>
										<td class="text-center" id="invisible"> {{ $pedidosAuths->status }}</td>
				                        <td class="text-center"> {{ $pedidosAuths->importe_entrega }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosAuths->order_date }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosAuths->created_at }}</td>
				                        <td class="text-center"> {{ $pedidosAuths->name }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosAuths->phone }}</td>
				                        <td class="text-center"> {{ $pedidosAuths->address }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosAuths->city }}</td> 
				                          



				                    </tr>
				               <!--     @endforeach -->
				                </tbody>
				            </table>
				            
              </div>

 <div class="tab-pane fade" id="entregado" role="tabpanel" aria-labelledby="auth-tab">
				<table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">#</th>
				                    	<th class="text-center" id="invisible">Estado</th>
				                    	<th class="text-center">Total</th>
				                    	<th class="text-center" id="invisible">Fecha</th>
				                        <th class="text-center" id="invisible">Hora</th>
										<th class="text-center">Nombre cliente</th>
				                        <th class="text-center" id="invisible">Telefono</th>
				                        <th class="text-center">Direccion</th>
										<th class="text-center" id="invisible">Ciudad</th>
										<th class="text-center">Proceder al pago</th>
				                        
				                       
				                    </tr>
				                </thead>
				                <tbody>
				               @foreach ($pedidosEnt as $pedidosEnts)  
				                    <tr>
				                       <td class="text-center"> {{ $pedidosEnts->id }}</td>
										<td class="text-center" id="invisible"> {{ $pedidosEnts->status }}</td>
				                        <td class="text-center"> {{ $pedidosEnts->importe_entrega }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosEnts->order_date }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosEnts->created_at }}</td>
				                        <td class="text-center"> {{ $pedidosEnts->name }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosEnts->phone }}</td>
				                        <td class="text-center"> {{ $pedidosEnts->address }}</td>
				                        <td class="text-center" id="invisible"> {{ $pedidosEnts->city }}</td> 
				                         <td class="text-center">
				                            <a style="margin-top: -15%; font-size: 25px;" href="/rutero/index/{{$pedidosEnts->id}}" rel="tooltip" title="Seleccionar" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a> </td>    



				                    </tr>
				                   @endforeach 
				                </tbody>
				            </table>
				            
              </div>
   
                     
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>
@foreach ($pedidos as $pedido)

      <div style="z-index: 1000000;" class="modal fade" id="modalAddToCart"  role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Seleccione el metodo de pago</h4>
<br>
<br>
	@foreach ($contar as $conta)

    @endforeach

@if ($conta->cart_count>0)

        <table style="width: 100%; height: 100%;">
        	<tr>
        		
                <td style="text-align: center;">


                	<form method="post" id="myform" action="{{ url('/rutero/pagoRutas/'.$pedido->id) }}">
                		{{ csrf_field() }}
 					
                    

                    <a href="#"  onclick="document.getElementById('myform').submit()" type="submit" class="toplink"><i 
        			style="font-size: 80px;" rel="tooltip" title="Efectivo" class="material-icons">attach_money</i> </a>
        			</form> 
                	

                 </td>

             
        		<td style="text-align: center;"><a href="#" class="toplink"><i 
        			style="font-size: 80px;" rel="tooltip" title="Tarjeta de credito" class="material-icons">credit_card</i> </a>

                    @if (Auth::check())
          {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
          <input type="hidden" name="body" value="confirmar pedido">
          {{ Form::hidden('cart_id', $pedido->id) }}
          <button class="btn btn-primary btn-round" type="submit" >
                        <i class="material-icons">notification_important</i> Tarjeta credito y/o debito
                    </button>
        {{ Form::close() }}
        @endif
        		</td>
        	</tr>

        	<tr>
        		<td style="text-align: center; color: #8C8C8C; font-size: 20px; font-weight: 500;">Efectivo</td>
        		<td style="text-align: center; color: #8C8C8C; font-size: 20px; font-weight: 500;">Tarjeta</td>
        	</tr>
        </table>

           @endif
<br>
<br>

<br>
<br>
    
      </div>
        

</div>
</div>
</div>
@endforeach


@include('includes.footer')
@endsection

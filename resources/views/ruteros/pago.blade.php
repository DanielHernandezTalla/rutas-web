@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
	#credito{
		height: 15px; font-size: 12px; 
		margin-top: -5%; color: #ff7514; 
		background-color: #fff; 
		border-color: #fff; 
		position: relative; 
		-webkit-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		-moz-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
	}
	#credito::before{
		background-color: #fff;
	}
	@media (max-width: 540px) {
  #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;

  }
 
}
@media (max-width: 540px) {
	 

  }
	}
</style>


@foreach ($pedidos as $pedido)
@endforeach
<h4>Pedido #{{ $pedido->id }}</h4>
<div class="main main-raised" style="background-color: #fff;-webkit-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);">

    <div class="container" style="margin-top: -20%;">
<!-- SELECIONAR FACTURA -->
<table style="width: 100%; ">
	<tr style="width: 100%;height: 40px;background-color: #F7F7F7;">
		<td style="width: 100%;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Requiere factura</h6>
		</td>
			
	</tr>
	<tr style="width: 100%;background-color: #E7E7E7;height: 40px; border-bottom-left-radius: 5%;">
		<td style="width: 100%;">
			 @foreach ($fi as $fis)
			 @endforeach

			
			<select disabled name="facturaSelect" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;">				
				 
			 @if ($fis->factura=="Si")
				<option value="Si">
					Si
				</option>
				@else
				<option value="No">
					No
				</option>
				@endif
			</select>
							
		</td>
	</tr>
	

</table>
@foreach ($pedidos as $pedido)
@endforeach
<form method="post" id="myform" action="{{ url('/rutero/pagoRutas/'.$pedido->id) }}">
                		{{ csrf_field() }}
<!-- SELECIONAR TIPO VENTA -->
<table style="width: 100%;">
	<tr style="width: 100%;height: 40px;background-color: #F7F7F7;">
		<td style="width: 100%;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Tipo de Venta</h6>
		</td>
			
	</tr>
	<tr style="width: 100%;background-color: #E7E7E7;height: 40px; border-bottom-left-radius: 5%;">
		<td style="width: 100%;">
 @foreach ($tipodeusuario as $tipodeusuarios)
			 @endforeach
			 
			<select name="TipoVenta" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;">
	 @if ($tipodeusuarios->cart_creditos<1)			
		           
           <option value="2">
            Contado
           </option>
    
           @else
	  @foreach ($di as $dis)          
           <option value="{{ $dis->id }}">
            {{ $dis->descripcion }}
           </option>
          @endforeach
	 @endif		
			</select>

		</td>
	</tr>
	

</table>

    <!-- SELECIONAR FORMA DE PAGO -->
<table style="margin-top: 1%; width: 100%;">
	<tr style="width: 100%;height: 40px;">
		<td style="width: 30%; height: 50px;background-color: #F7F7F7;border-bottom-style: solid;border-color:#E7E7E7;border-bottom-width: 2px;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Forma de pago</h6>
		</td>
		<td style="width: 70%;background-color: #E7E7E7;height: 40px;border-bottom-style: solid;border-color:#F7F7F7;border-bottom-width: 2px;">
			<select name="FormaDePago" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;">
				 @foreach ($ei as $eis)
          
           <option value="{{ $eis->valor }}">
            {{ $eis->Descripcion }}
           </option>
          @endforeach
			</select>
		</td>		
	</tr>
	

</table>
@foreach ($pedidos as $pedido)
@endforeach
 <!-- MOSTRAR IMPORTE -->
<table style="width: 100%;">
	<tr style="width: 100%;height: 50px;">
		<td style="width: 30%; height: 40px;background-color: #F7F7F7;border-bottom-style: solid;border-color:#E7E7E7;border-bottom-width: 2px;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Importe total a cobrar</h6>
		</td>
		<td style="width: 70%;background-color: #E7E7E7;height: 40px;border-bottom-style: solid;border-color:#F7F7F7;border-bottom-width: 2px;">
			
			@foreach ($fi as $fis)
			 @endforeach

			  @if ($fis->status=="Entregado")
			 <input disabled type="text" name="importeAcobrarnoes" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="${{ $pedido->importe_entrega }}">
			<input hidden type="number" name="importeAcobrar" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="{{ $pedido->importe_entrega }}"id="caja1">
			  @else
			  <input disabled type="text" name="importeAcobrarnoes" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="${{ $pedido->importe_entrega }}">
			<input hidden type="number" name="importeAcobrar" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="{{ $pedido->importe_entrega }}"id="caja1">
			  @endif
			
		</td>		
	</tr>
	

</table>      

<!--CONFIRMAR IMPORTE A PAGAR-->
<table style="width: 100%; ">
	<tr style="width: 100%;height: 50px;">
		<td style="width: 30%; height: 40px;background-color: #F7F7F7;border-bottom-style: solid;border-color:#E7E7E7;border-bottom-width: 2px;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Importe a pagar $</h6>
		</td>
		<td style="width: 70%;background-color: #E7E7E7;height: 40px;border-bottom-style: solid;border-color:#F7F7F7;border-bottom-width: 2px;">
			<input type="number" name="importePagar" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;"id="caja2" autofocus>
		</td>		
	</tr>
	
	

</table>

<!--Importe restante-->
<table style="width: 100%; ">
	<tr style="width: 100%;height: 50px;">
		<td style="width: 30%; height: 40px;background-color: #F7F7F7;">
			<h6 style="font-size: 12px;font-weight: 500;color: #636363;">Importe restante $</h6>
		</td>
		<td style="width: 70%;background-color: #E7E7E7;height: 40px;">
			<input disabled type="text" name="importeRestante" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" id="caja3">
		</td>		
	</tr>
	<tr style="width: 100%;">
		<td style="width: 100%; text-align: center;" colspan="2">
			@foreach ($fi as $fis)
			 @endforeach

			  @if ($fis->status=="Entregado")
			  <button style="font-size: 20px; margin: 0 auto; margin-top: 10px;" class="btn btn-primary btn-round" type="submit">Pagar
			</button>
			  @else
			  <button style="font-size: 20px; margin: 0 auto;display: none;" class="btn btn-primary btn-round" type="submit">Pagar
			</button>
			  @endif
		</td>
	</tr>
	

</table>
</form>


   <script>
        let precio1 = document.getElementById("caja1")
        let precio2 = document.getElementById("caja2")
        let precio3 = document.getElementById("caja3")



 
        precio2.addEventListener("change", (event) => {
            var valor = parseFloat(precio2.value) - parseFloat(precio1.value)

   		precio3.value= valor.toFixed(2);
        })
        
    </script>

@foreach ($fi as $fis)
			 @endforeach
				 
			 @if ($fis->status=="Entregado")
				<form method="get" action="{{ url('/rutero/download-jsonfile') }}">
@foreach ($pedidos as $pedido)
@endforeach
<div style="display: none;">
 <input  type="text" name="id_car" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="{{$pedido->id}}">

</div>
	<button style="font-size: 20px; margin-left: 25%; display: none;" class="btn btn-primary btn-round" type="submit">Prueba
			</button>
</form>

				@else
			<form method="get" action="{{ url('/rutero/download-jsonfile') }}">
@foreach ($pedidos as $pedido)
@endforeach
<div style="display: none;">
 <input  type="text" name="id_car" style="border-style: solid; border-color: #E7E7E7;background-color: #E7E7E7; font-family: Arial;width: 100%;" value="{{$pedido->id}}">

</div>
	<button style="font-size: 20px; margin-left: 25%;background-color: #117DFF;" class="btn btn-primary btn-round" type="submit">Imprimir
			</button>
</form>

				@endif

        <div class="section text-center">
          
            <div class="team" >
                <div class="row" >
               <div class="tabpanel" >

    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false"></a>
            
                </li>
              

              </ul>

    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
				
              </div>



   
                     
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>



@include('includes.footer')
@endsection

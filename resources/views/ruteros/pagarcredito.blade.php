@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
<style type="text/css">
    @media (max-width: 540px) {
    #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .titulo-corto{
    font-size: 20px;
}
.content .input-group{
    width: 100%;
    margin: 0 auto;
}
.content .input-group h3{
    font-size: 20px;
    font-weight: 500;
}
.content .input-group .material-icons{
    color: #495057;
}
}

    @media (min-width: 541px) {
    #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .titulo-corto{
    font-size: 24px;
}
.content .input-group{
    width: 30%;
    margin: 0 auto;
}
.content .input-group h3{
    font-size: 28px;
    font-weight: 500;
}
.content .input-group .material-icons{
    color: #495057;
}
}

</style>

<div class="main main-raised">
    <div class="container">

        <div class="section">
            <h2 class="title text-center">Credito del usuario</h2>

            @if (session('notification'))
                <div class="alert alert-success">
                    {{ session('notification') }}
                </div>
            @endif

       
            </div>
 @foreach($User as $Users)

             
                        {{ csrf_field() }}

                     
                       
                        <div class="content">


                        
                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">attach_money</i>
                                    <label class="titulo-corto">Saldo maximo:</label>
                                </span>
                               

                                        <h3>$0.00</h3>
                            </div>
                                      

                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">attach_money</i>
                                    <label class="titulo-corto">Saldo utilizado:</label>
                                </span>
                               
                                   @if ($Users->saldousado>0)
                                     <h3>${{$Users->saldousado}}</h3>
                                    @else
                                      <h3>$0.00</h3>
                                    @endif
                                      
                            </div>                           

                           
                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">attach_money</i>
                                    <label class="titulo-corto">Saldo disponible:</label>
                                </span>
                               

                                       <h3>$0.00</h3>
                            </div> 



                           
                        
                      
                   
                  
@endforeach
        </div>
 
    </div>


@include('includes.footer')
@endsection

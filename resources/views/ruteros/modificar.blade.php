@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
	#credito{
		height: 15px; font-size: 12px; 
		margin-top: -5%; color: #ff7514; 
		background-color: #fff; 
		border-color: #fff; 
		position: relative; 
		-webkit-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		-moz-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
	}
	#credito::before{
		background-color: #fff;
	}
	.title{
		font-size: 20px;
	}
    @media (max-width: 540px) {
  #namess{
    display: none;
  }
  #imagess{
    display: none;
  }
  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }
}
</style>



<div class="main main-raised">
    <div class="container">





        <div class="section text-center">
        	@foreach ($unaut as $unauts)
        	@endforeach
            <h2 class="title">Carrito de compras: #{{$unauts->id}}</h2>




            <div class="team">
                <div class="row">
               <div class="tabpanel">

    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Productos</a>
            
                </li>
              

              </ul>

    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
			 <table class="table">
                <thead>
                    <tr style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; ">
              
                       <th class="text-center">Nombre</th>
                        <th class="text-center">Precio</th>
                        <th class="text-center">Cantidad</th>
                        <th class="text-center">UM</th>
                        <th class="text-center">Iva</th>
                        <th class="text-center">SubTotal</th> 
                        <th class="text-center">codeb</th>
                        <th class="text-center">Packing</th>
                        <th class="text-center">Cantidad Packing</th>

                       
                    </tr>
                </thead>

                <tbody>
                	
                    @foreach ($unaut as $unauts)
             
            

                    <tr >
                   	<form action="{{ url('/rutero/modificar/'.$unauts->id.'/'.$unauts->idmodificacion.'/'.$unauts->idpro.'/'.$unauts->codeb) }}" method="POST" enctype="multipart/form-data">
                    {{csrf_field()}}
                        
                        <td style="text-align: left;">
                            {{ $unauts->name }}
                            <input type="text" name="id_productoss" hidden value="{{$unauts->idpro}}">
                        </td>
                          <td >
                           

${{ $unauts->price }}
                         
                         
                            
                        </td>

                            <td >
                               <input style="display: none;" type="text-center" name="price" value="{{$unauts->price}}">
                                <input style="display: none;" type="text-center" name="iva" value="{{$unauts->iva}}">
                           
                            @if($unauts->quantityentrega == 0)

{{ $unauts->quantity }}KG
                         
                          @else


{{ $unauts->quantityentrega }}KG

                          
                          @endif
                        </td>
                           
                        
                      
                        <td >
                            {{ $unauts->uom }}
                        </td>
                        <td >
                            {{ $unauts->iva }}
                        </td>
                      
                      <td >
                         
<input type="text" hidden name="Importe_actual" value="{{ $unauts->importe }}">

                             @if($unauts->quantityentrega == 0)

 ${{ $unauts->importe }}
                         
                          @else


${{ $unauts->importe_entrega }}

                          
                          @endif
                        </td>
                        <td >
                        
                             {{ $unauts->codeb }}
                        </td>
                        <td >
                          <input  type="hidden" name="codigoProducto" value="{{ $unauts->codigo }}">
                          <input  type="hidden" name="codebProducto" value="{{ $unauts->codeb }}">
                        <input  type="hidden" name="packing" value="{{$unauts->packing}}">

                             {{ $unauts->packing }}
                        </td>
                        <td>
                          @if($unauts->quantityentrega == 0)

@if($unauts->entregado==0)
  <input type="text-center" class="add" name="quantitypacking" value="{{$unauts->cantidad}}">
@else
  <input type="text-center" class="add" name="quantitypacking" value="{{$unauts->quantityentrega}}">
@endif
                         
                          @else


@if($unauts->entregado==0)
  <input type="text-center" class="add" disabled name="quantitypacking" value="{{$unauts->cantidad}}">
@else
  <input type="text-center"  class="add" disabled name="quantitypacking" value="{{$unauts->quantityentrega}}">
@endif

                          
                          @endif
                       
                        </td>
                        <td >
                           @if($unauts->entregado == 0)
                           <button style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">guardar</button>
                           @else
                             <button style="border-style: none;display: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">guardar</button>
                           @endif
                        </td>
</form>
                    </tr>
               @endforeach
                  
                </tbody>
            </table>
              
            <br>
  

              @foreach ($purchases as $purchase)
              @endforeach
 <h3 class="title">Importe total: ${{$unauts->importe_modificar-$purchase->sumafinal}}</h3>

              </div>


   
                     
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>



@include('includes.footer')
@endsection

<!doctype html>
<html lang="es">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="../assets/img/apple-icon.png">
    <link rel="icon" type="image/png" href="../assets/img/favicon.png">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="viewport"
        content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name'))</title>



    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />



    <!--     Fonts and icons     -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" />
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" />


    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"
        integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">


    <!-- CSS Files -->
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/material-kit.css') }}" rel="stylesheet" />
    <link href="{{ asset('css/switch.css') }}" rel="stylesheet" />



    @yield('styles')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
</head>

<style type="text/css">
#colorNav li.green {
    /* This is the color of the menu item */
    background-color: #ff7514;

    /* This is the color of the icon */
    color: #ffffff;
}

img {
    width: 80px;

}

.menufooter {
    /* IMPORTANTE */
    overflow: hidden;
    text-align: center;
    bottom: 0;
    height: 60px;
    padding: 13px 0 4px;
    position: fixed;
    width: 100%;

    z-index: 5000;
    background-color: #242424;
    display: flex;
    display: inline-block;
    text-align: center;

}


#colorNav li.red {
    background-color: #ff7514;
    color: #ffffff;
}

#colorNav li.blue {
    background-color: #ff7514;
    color: #ffffff;
}

#colorNav li.yellow {
    background-color: #ff7514;
    color: #ffffff;
}

#colorNav li.purple {
    background-color: #ff7514;
    color: #ffffff;
}

.option2 {
    background-color: #FFFFFF00;
    color: #ffffff;
    margin-right: 30px;
    margin-left: 30px;
}

.option2:hover {
    color: #000000;
    margin-right: 30px;
    margin-left: 30px;
}


.option2 .material-icons {


    outline: 0;
    font-size: 35px;

}





@media screen and (max-width:820px) {
    .menufooter {
        /* IMPORTANTE */
        overflow: hidden;

        bottom: 0;
        height: 50px;
        padding-left: 20px;
        position: fixed;
        width: 100%;
        z-index: 5000;
        background-color: #2F2F2F;
        display: flex;
        display: inline-block;
        text-align: center;

    }

    #colorNav li {
        border-width: -10px;
        border-color: #FFFFFF00;
        border-style: solid;
        background-color: transparent;
    }


    #colorNav li.green {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.red {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.blue {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.yellow {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.purple {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    .option2 {
        background-color: #FFFFFF00;
        color: #ffffff;
        margin-right: 30px;
        margin-left: 30px;
        font-size: 80px;
    }

    .option2 .material-icons {


        outline: 0;
        font-size: 30px;

    }


    .option2:hover {
        color: #000000;
        margin-right: 5%;
        margin-left: 5%;
    }
}





@media screen and (max-width:540px) {


    .menufooter {
        /* IMPORTANTE */
        overflow: hidden;

        bottom: 0;
        height: 50px;
        padding-left: 20px;
        position: fixed;
        width: 100%;
        z-index: 5000;
        background-color: #242424;
        display: flex;
        display: inline-block;
        text-align: center;

    }

    #colorNav li {
        border-width: -10px;
        border-color: #FFFFFF00;
        border-style: solid;
        background-color: transparent;
    }


    #colorNav li.green {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.red {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.blue {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.yellow {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    #colorNav li.purple {
        background-color: #FFFFFF00;
        color: #ffffff;
    }

    .option2 {
        background-color: #FFFFFF00;
        color: #ffffff;
        margin-right: 5%;
        margin-left: 5%;
        font-size: 120px;
    }

    .option2 .material-icons {


        outline: 0;
        font-size: 35px;
        margin-bottom: 100%;

    }

    .option2:hover {
        color: #000000;
        margin-right: 5%;
        margin-left: 5%;
    }
}

.navbar {
    position: fixed;
    top: 0;
}

.iniciar {
    color: #ffffff;
    text-transform: none;
}

.registrar {
    margin-left: -16%;
    color: #ffffff;
    text-transform: lowercase;
}

@media (max-width: 540px) {
    .iniciar {
        color: #ffffff;
        text-transform: none;
    }

    .registrar {
        margin-left: 0%;
        margin-top: -2%;
        color: #ffffff;
        text-transform: lowercase;
    }

    .iniciar i {
        display: none;
    }
}

* {
    margin: 0;
    padding: 0;
}

body {
    background: #000;

}

header {
    width: 100%;

}

header nav {
    width: 90%;
    max-width: 1000px;
    margin: 20px auto;
    background: #fff;
    z-index: 3000;
}

.menu_bar {
    display: none;
}

header nav ul {
    overflow: hidden;
    list-style: none;
}

header nav ul li {
    float: left;
}

header nav ul li a {
    color: #000;
    padding: 20px;
    display: block;
    text-decoration: none;
}

header nav ul li span {
    margin-right: 10px;
}

header nav ul li a:hover {
    background: #fff;
}

section {
    padding: 20px;
}

@media screen and (min-width:800px) {
    #menunuevo {
        display: none;
    }
}

@media screen and (max-width:800px) {
    #menuviejo {
        display: none;
    }

    header nav {
        width: 80%;
        height: 100%;
        left: -100%;
        margin: 0;

        position: absolute;

    }

    header nav ul li {
        display: block;
        float: none;
        border-bottom: 1px solid rgba(255, 255, 255, .3);

    }

    .menu_bar {
        display: block;
        width: 100%;
        background: #242424;
        height: 50px;
        text-align: right;


    }

    .menu_bar .bt-menu {
        display: block;
        padding: 20px;
        background: #000;
        color: #fff;
        text-decoration: none;
        font-weight: bold;
        font-size: 25px;
        -webkit-box-sizing: border-box;
        -moz-box-sizing: border-box;
        box-sizing: border-box;
    }

    .menu_bar span {
        float: right;
        font-size: 40px;


    }

    .menu_bar #nav-icon4 {

        margin-left: 5%;

    }

    .menu_bar #nav-icon4 {

        margin-top: 0%;

    }


    .menu_bar #nav-icon4 span {

        margin-top: 15%;

    }

    #nav-icon1,
    #nav-icon2,
    #nav-icon3,
    #nav-icon4 {
        width: 50%;
        height: 10px;
        position: relative;
        margin: 0px auto;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-transition: .5s ease-in-out;
        -moz-transition: .5s ease-in-out;
        -o-transition: .5s ease-in-out;
        transition: .5s ease-in-out;
        cursor: pointer;
    }

    #nav-icon1 span,
    #nav-icon3 span,
    #nav-icon4 span {
        display: block;
        position: absolute;
        height: 3px;
        width: 40%;
        background: #fff;
        border-radius: 9px;
        opacity: 1;
        left: 0;
        -webkit-transform: rotate(0deg);
        -moz-transform: rotate(0deg);
        -o-transform: rotate(0deg);
        transform: rotate(0deg);
        -webkit-transition: .25s ease-in-out;
        -moz-transition: .25s ease-in-out;
        -o-transition: .25s ease-in-out;
        transition: .25s ease-in-out;
    }

    #nav-icon4 {
        width: 20%;

    }

    #nav-icon4 span:nth-child(1) {
        top: 0px;
        -webkit-transform-origin: left center;
        -moz-transform-origin: left center;
        -o-transform-origin: left center;
        transform-origin: left center;
    }

    #nav-icon4 span:nth-child(2) {
        top: 12px;
        -webkit-transform-origin: left center;
        -moz-transform-origin: left center;
        -o-transform-origin: left center;
        transform-origin: left center;
    }

    #nav-icon4 span:nth-child(3) {
        top: 24px;
        -webkit-transform-origin: left center;
        -moz-transform-origin: left center;
        -o-transform-origin: left center;
        transform-origin: left center;
    }

    #nav-icon4.open span:nth-child(1) {
        -webkit-transform: rotate(45deg);
        -moz-transform: rotate(45deg);
        -o-transform: rotate(45deg);
        transform: rotate(45deg);
        top: -4px;
        left: 8px;
    }

    #nav-icon4.open span:nth-child(2) {
        width: 0%;
        opacity: 0;
    }

    #nav-icon4.open span:nth-child(3) {
        -webkit-transform: rotate(-45deg);
        -moz-transform: rotate(-45deg);
        -o-transform: rotate(-45deg);
        transform: rotate(-45deg);
        top: 18px;
        left: 8px;
    }


    .datosuserli {
        width: 100%;
        height: auto;
        background-color: #242424;
        border-bottom-style: solid;
        border-bottom-color: #fff;

    }

    .datosuser {
        width: 100%;
        background-color: #242424;
        height: 90px;
    }

    .datosuser table {
        width: 80%;
        background-color: #242424;
        margin: 0 auto;
        vertical-align: middle;


    }

    .datosuser table tr td {
        width: 50%;

    }

    .datosuser table tr td p {
        width: 100%;
        color: #fff;
        font-size: 26px;
        font-weight: 500;
        margin-bottom: -2%;
    }

    .datosuser table tr td .cor {
        width: 50%;
        color: #fff;
        font-size: 12px;
        margin-top: -2%;
    }

    .input-group span {
        color: #ff7514;
    }

    header nav ul li {

        width: 100%;

    }

    header nav ul li a {
        height: 70px;
        width: 80%;
        margin-top: -20px;
        margin-left: auto;
        margin-right: auto;

    }

    header nav ul li a h3 {

        font-size: 20px;
        font-weight: 500;
        color: #9F9F9F;
    }

    .menu_bar img {
        margin-left: auto;
        margin-right: auto;
        display: inline-block;

    }

    .menu_bar p {
        margin-left: auto;
        margin-right: auto;
        display: inline-block;
        color: #BDBDBD;
        font-size: 16px;
        font-weight: 500;
        margin-right: 2%;
    }

    select {
        font-family: 'FontAwesome', 'Second Font name'
    }


}
</style>

<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css" rel="stylesheet" />

<body style="background-color: #fff;" id="body_app" class="@yield('body-class')">


    <div id="app" class="prueba">

        <header id="menunuevo">
            <div class="menu_bar" id="menusuperior">
                <div id="nav-icon4">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>

                @guest

                @else
                <div style="width: 100%;  text-align: center;">
                    <select id="selectciudad"
                        style="margin: 0 auto; border-style: solid; border-color: #fff; border-radius: 15px; border-width: 2px; background-color: #242424; color: #fff; width: 65%; height: 30px;">
                        <option selected value="{{ auth()->user()->city }}">&#xf002; {{ auth()->user()->city }}</option>
                    </select>
                </div>
                <div id="shoppingcar" style="width: 95%; margin-top: -25px; ">
                    <a href="/home" style="margin: 0 auto; font-size: 16px; color: #fff; display: inline-block;"><i
                            class="material-icons">shopping_cart</i></a>
                </div>
                <p id="namess">{{ Auth::user()->name }}</p>
                <img id="imagess" src="{{url('/images/cerdito.png')}}" alt="Image" style="width: 35px; height: 35px;" />
                @endguest
            </div>






            <nav>
                <ul>

                    @guest

                    <li>
                        <a href="{{ route('login') }}">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">login</i>
                                </span>

                                <h3>Iniciar</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="{{ route('register') }}">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">app_registration</i>
                                </span>

                                <h3>Registrar</h3>

                            </div>

                        </a>
                    </li>


                    @else

                    <li class="datosuserli">


                        <div class="datosuser">
                            <table>
                                <tr>
                                    <td><img src="{{url('/images/cerdito.png')}}" alt="Image" /></td>
                                    <td>
                                        <p>{{ Auth::user()->name }}</p>
                                        <p class="cor">{{ Auth::user()->email }}</p>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </li>

                    <li>
                        <a href="/">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>

                                <h3>Home</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="/">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">airplane_ticket</i>
                                </span>

                                <h3>Promociones</h3>

                            </div>

                        </a>
                    </li>

                    <li>
                        <a href="/">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">fastfood</i>
                                </span>

                                <h3>Productos</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="/">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">food_bank</i>
                                </span>

                                <h3>Recetas</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="/">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">shopping_bag</i>
                                </span>

                                <h3>Mis compras</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="/home">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">shopping_cart</i>
                                </span>

                                <h3>Carrito</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <a href="/modificarUsuario">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">build</i>
                                </span>

                                <h3>Ajustes</h3>

                            </div>

                        </a>
                    </li>

                    <li>
                        <a href="{{ url('/creditouser') }}">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">perm_identity</i>
                                </span>

                                <h3>Credito</h3>

                            </div>

                        </a>
                    </li>

                    <li>
                        <a href="/reportar">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">error_outline</i>
                                </span>

                                <h3>Reportar error</h3>

                            </div>

                        </a>
                    </li>
                    <li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                        </form>
                        <a href="{{ route('logout') }}"
                            onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                            <div class="input-group">

                                <span class="input-group-addon">
                                    <i class="material-icons">logout</i>
                                </span>

                                <h3>Cerrar sesión</h3>

                            </div>

                        </a>
                    </li>



                    @endguest
                </ul>
            </nav>


        </header>



        <script type="text/javascript">
        $(document).ready(function() {
            $('#nav-icon1,#nav-icon2,#nav-icon3,#nav-icon4').click(function() {
                $(this).toggleClass('open');
            });
        });

        $(document).ready(main);

        var contador = 1;

        function main() {
            $('#nav-icon4').click(function() {
                // $('nav').toggle(); 

                if (contador == 1) {
                    $('nav').animate({
                        left: '0'
                    });

                    contador = 0;
                } else {
                    contador = 1;
                    $('nav').animate({
                        left: '-100%'
                    });
                }

            });

        };
        </script>

        <nav id="menuviejo" style="background-color: #2F2F2F;" class="navbar navbar-transparent navbar-absolute">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target="#navigation-example">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <a class="navbar-brand" href="{{ url('/') }}"> <img
                            src="https://recetas.kowi.com.mx/Images/images/logo.png" alt="Logo Kowi" width="65%"></a>

                </div>





                <div class="collapse navbar-collapse" id="navigation-example">

                    <ul class="nav navbar-nav navbar-right">
                        @guest
                        <li class="iniciar"><a href="{{ route('login') }}">Iniciar <i>/</i></a></li>
                        <li class="registrar"><a href="{{ route('register') }}">Registrar</a></li>
                        @else

                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-expanded="false">
                                <i class="fa fa-bell-o"
                                    aria-hidden="true">{{count(Auth::user()->unreadNotifications)}}</i>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li>

                                    @forelse (Auth::user()->unreadNotifications as $notificacion)

                                    @if($notificacion->data["notificacion"]["body"]=="tienes un nuevo mensaje")
                                    <a href="{{ route('listado') }}"
                                        class="dropdown-item dropdown-footer"><i>{{ $notificacion->data["notificacion"]["body"] }}</i>
                                    </a>

                                    @else
                                    @if($notificacion->data["notificacion"]["body"]=="confirmar pedido")
                                    <a href="/pago/{{ $notificacion->data["notificacion"]["cart_id"] }}"
                                        class="dropdown-item dropdown-footer"><i>{{ $notificacion->data["notificacion"]["body"] }}</i>
                                    </a>


                                    @else
                                    @if($notificacion->data["notificacion"]["body"]=="pedido preparado")
                                    <a href="/home"
                                        class="dropdown-item dropdown-footer"><i>{{ $notificacion->data["notificacion"]["body"] }}</i>
                                    </a>


                                    @endif

                                    @endif

                                    @endif


                                    @empty
                                    <b class="text-center center-block">No notifications</b>
                                    @endforelse
                                </li>

                            </ul>

                        </li>


                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                @if (auth()->user()->admin=="0")
                                @if (auth()->user()->gester=="0")
                                @if (auth()->user()->useradmin=="0")
                                @if (auth()->user()->usad=="0")
                                @if (auth()->user()->adminpedidos=="0")
                                @if (auth()->user()->rutero=="0")
                                @if (auth()->user()->picking=="0")
                                <li>
                                    <a href="{{ url('/home') }}">Carrito de compras</a>
                                </li>
                                @endif
                                @endif
                                @endif
                                @endif
                                @endif
                                @endif
                                @endif
                                @if (auth()->user()->admin)

                                <li>
                                    <a href="{{ url('/admin/precios') }}">Gestionar precios</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/centrodeventas') }}">Gestionar centro de ventas</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/promolist') }}">Promociones</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/tiempoentrega') }}">Asignar tiempo a las entregas</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/correosusers') }}">Correos de usuarios</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/productosactivos') }}">Activacion de productos</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/newuser') }}">Agregar contraseña a usuarios</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/newuserejecutivo') }}">Enviar  usuarios a oracle</a>
                                </li>
                                <li>
                                    <a href="{{ url('/admin/ReportesPedidos') }}">Reporte pedidos</a>
                                </li>
                                @endif

                                @if (auth()->user()->useradmin)

                                <li>
                                    <a href="{{ url('/useradmin/newuser') }}">Agregar contraseña a usuarios</a>
                                </li>

                                @endif
                                @if (auth()->user()->usad)

                                <li>
                                    <a href="{{ url('/usad/useradmin') }}">Agregar usuarios especiales</a>
                                </li>

                                @endif
                                @if (auth()->user()->adminpedidos)

                                <li>
                                    <a href="{{ url('/adminpedidos/pedidos') }}">Gestionar pedidos</a>
                                </li>

                                <li>
                                    <a href="{{ url('/adminpedidos/asignarPassword') }}">Asignar contraseña a usuarios</a>
                                </li>

                                <li>
                                    <a href="{{ url('/adminpedidos/PedidosUsuario') }}">Reporte usuarios</a>
                                </li>

                                <li>
                                    <a href="{{ url('/adminpedidos/ReportesPedidos') }}">Reporte pedidos</a>
                                </li>

                                <li>
                                    <a href="{{ url('/adminpedidos/consultarExistencia') }}">Lista de existencias</a>
                                </li>

                                @endif

                                @if (auth()->user()->gester)
                                <li>
                                    <a href="{{ url('/gester/categories') }}">Gestionar categorías</a>
                                </li>
                                <li>
                                    <a href="{{ url('/gester/products') }}">Gestionar productos</a>
                                </li>

                                @endif

                                @if (auth()->user()->rutero)
                                <li>
                                    <a href="{{ url('/rutero/index') }}">Rutero</a>
                                </li>


                                @endif
                                @if (auth()->user()->picking)
                                <li>
                                    <a href="{{ url('/picking/index') }}">Almacen</a>
                                </li>


                                @endif
                                <li>
                                    <a href="{{ route('logout') }}" onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        Cerrar sesión
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                        style="display: none;">
                                        {{ csrf_field() }}
                                    </form>
                                </li>
                            </ul>
                        </li>





                        @endguest
                    </ul>

                </div>
            </div>
        </nav>







        <div class="wrapper">
            @yield('content')
        </div>
    </div>

    <div class="menufooter" id="menufooters">


        <a class="option2" href="{{ url('/') }}"><i class="material-icons">home</i></a>


        <a class="option2" href="{{ url('/home') }}"><i class="material-icons">shopping_cart</i></a>

        <a class="option2" href="/modificarUsuario"><i class="material-icons">account_circle</i></a>

        <a class="option2" href="{{ url('/creditouser') }}"><i class="material-icons">attach_money</i></a>




    </div>
    @yield('script')
</body>



<!--   Core JS Files   -->
<script src="{{ asset('/js/jquery.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/material.min.js') }}"></script>
<script src="{{ asset('/js/dropdown.js') }}"></script>






<!--  Plugin for the Sliders, full documentation here: http://refreshless.com/nouislider/ -->
<script src="{{ asset('/js/nouislider.min.js') }}" type="text/javascript"></script>

<!--  Plugin for the Datepicker, full documentation here: http://www.eyecon.ro/bootstrap-datepicker/ -->
<script src="{{ asset('/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>

<!-- Control Center for Material Kit: activating the ripples, parallax effects, scripts from the example pages etc -->
<script src="{{ asset('/js/material-kit.js') }}" type="text/javascript"></script>




@yield('scripts')



<script>
function sendMarkRequest(id = null) {
    return $.ajax("{{ route('markNotification') }}", {
        method: 'POST',
        data: {
            _token: "{{ csrf_token() }}",
            id
        }
    });
}
$(function() {
    $('.mark-as-read').click(function() {
        let request = sendMarkRequest($(this).data('id'));
        request.done(() => {
            $(this).parents('div.alert').remove();
        });

    });
});
</script>

</html>
<!-- BSA AdPacks code. Please ignore and remove.--> 
<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
<script src="http://cdn.tutorialzine.com/misc/adPacks/v1.js" async></script>
<style type="text/css">
/*----------------------------
	The Navigation Menu
-----------------------------*/


#colorNav > ul{
	width: 100%; /* Increase when adding more menu items */
	margin:0 auto;
}

#colorNav > ul > li{ /* will style only the top level li */
	list-style: none;
	box-shadow: 0 0 10px rgba(100, 100, 100, 0.2) inset,1px 1px 1px #CCC;
	display: inline-block;
	line-height: 1;
	margin: 1px;
	border-radius: 3px;
	position:relative;
}

#colorNav > ul > li > a{
	color:inherit;
	text-decoration:none !important;
	font-size:50px;
	padding: 25px;
}

#colorNav li ul{
	position:absolute;
	list-style:none;
	text-align:center;
	width:180px;
	left:50%;
	margin-left:-90px;
	top:70px;
	font:bold 12px 'Open Sans Condensed', sans-serif;
	
	/* This is important for the show/hide CSS animation */
	max-height:0px;
	overflow:hidden;
	
	-webkit-transition:max-height 0.4s linear;
	-moz-transition:max-height 0.4s linear;
	transition:max-height 0.4s linear;
}

#colorNav li ul li{
	background-color:#313131;
}

#colorNav li ul li a{
	padding:12px;
	color:#fff !important;
	text-decoration:none !important;
	display:block;
}

#colorNav li ul li:nth-child(odd){ /* zebra stripes */
	background-color:#363636;
}

#colorNav li ul li:hover{
	background-color:#444;
}

#colorNav li ul li:first-child{
	border-radius:3px 3px 0 0;
	margin-top:25px;
	position:relative;
}

#colorNav li ul li:first-child:before{ /* the pointer tip */
	content:'';
	position:absolute;
	width:1px;
	height:1px;
	border:5px solid transparent;
	border-bottom-color:#313131;
	left:50%;
	top:-10px;
	margin-left:-5px;
}

#colorNav li ul li:last-child{
	border-bottom-left-radius:3px;
	border-bottom-right-radius:3px;
}

/* This will trigger the CSS */
/* transition animation on hover */

#colorNav li:hover ul{
	max-height:200px; /* Increase when adding more dropdown items */
}

#alimen{
	color: #838383; font-size: 14px; font-weight: 400;
}
#politica{
color: #419AFF; font-size: 12px; text-decoration-line: underline;
}
/*----------------------------
	Color Themes
-----------------------------*/


</style>


<footer class="footer">
    <div class="container" >
 
        <div class="text-center " id="alimen" >
            Alimentos Kowi 2020&copy; 
        </div>
       
          <div class="text-center " id="politica">
          	Politica de privacidad.
          </div>
          
    </div>
</footer>
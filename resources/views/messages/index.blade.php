<div class="message-wrapper">
    <ul class="messages">
        @foreach($messages as $message)
        <!--Respuesta enviada del usuario-->
        <li class="message clearfix">
            <div class="{{($message->from == Auth::id()) ? 'send' : 'received'}}">
                <p><strong>{{$message->message}}</strong> </p>
                <p class="date">{{ date ('d M y, h:i a',strtotime($message-> created_at)) }}</p>
            </div>
        </li>
        @endforeach
    </ul>
</div>
<!--Input de mensaje-->
<div class="input-text">
    <input type="text" name="message" class="submit">
</div>
<br>
@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	
           <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">ID pedido</th>
										<th class="text-center">Fecha</th>
				                        <th class="text-center">Status</th>
				                        <th>opciones</th>
				                       
				                        
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($listado as $listados)
				                    <tr>
				                        <td class="text-center"> {{ $listados->id }}</td>
										<td class="text-center"> {{ $listados->order_date }}</td>
				                        <td class="text-center"> {{ $listados->status }}</td>
                           				<td class="text-center" class="td-actions">
				                    
				                        <form method="post">
                                       
                                        <a href="{{ url('/confirmacion/show/'.$listados->id) }}" rel="tooltip" title="Detalles del Pedido" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        
                                    </form>   
				                              
				                                                           
				                        </td>
				                    </tr>
				                 @endforeach
				                </tbody>
				            </table>

			
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

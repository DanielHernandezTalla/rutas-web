@extends('layouts.app')

@section('body-class', 'profile-page')

@section('styles')
<style>
.main-raised {
    margin: 80px 60px 40px !important;
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}

#fondo-chat {
    background-image: url("https://recetas.kowi.com.mx/Recetas/LIVE-CHAT.png");
    width: 100%;
    height: 200px;
    background-size: 100% 100%;
}

h3 {
    text-align: center;

}

p {
    font-size: 18px;
}

ul {
    margin: 0;
    padding: 0;
}

.user-wrapper,
.message-wrapper {
    /*border: 2px solid #b4b4b4;*/
    overflow-y: auto;
}

.user-wrapper {
    height: 51rem;
    background: #e4e4e4;
}

.user {
    cursor: pointer;
    padding: 5px 0;
    position: relative;
}

.user:hover {
    background: #eeeeee;
}

.user:last-child {
    margin-bottom: 0;
}

.pending {
    position: absolute;
    left: 0px;
    background: red;
    margin: 0;
    border-radius: 50%;
    width: 18px;
    height: 18px;
    line-height: 18px;
    padding-left: 5px;
    color: #ffffff;
    font-size: 12px;
    margin-top: 15px;
}

.media-left {
    margin: 0 10px;
}

.media-left img {
    width: 64px;
    border-radius: 64px;
}

.media-body p {
    margin: 6px 0;

}

.message-wrapper {
    padding: 10px;
    height: 45rem;
    background: #e4e4e4;
}

.messages .message {
    margin-bottom: 15px;
}

.messages .message:last-child {
    margin-bottom: 0;
}

.received,
.send {
    width: 45%;
    padding: 3px 10px;
    border-radius: 10px;
}

.received {
    background: #ffffff;
}

.send {
    background: #fd8204;
    float: right;
    text-align: right;
}

.message p {
    margin: 5px 0;
}

.date {
    color: #777777;
    font-size: 13px;
}

.date-send {
    color: #ffffff;
    font-size: 13px;
}

.active {
    background: #a19f9f;
}

input[type=text] {
    width: 100%;
    padding: 12px 20px;
    margin: 15px 0 0 0;
    display: inline-block;
    border-radius: 4px;
    box-sizing: border-box;
    outline: none;
    border: 1px solid #a19f9f;
}

input[type=text]:focus {
    border: 1px solid #fd8204;
}

.send p {
    color: #ffffff;
}


</style>
<!--JQUERY-->
<script src="https://js.pusher.com/5.0/pusher.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

<script>

//receiver_id es el id del cliente
var receiver_id = '';

//my_id es el id del Usuario Logeado
var my_id = "{{ Auth::id() }}";
    $(document).ready(function () {
        // ajax setup form csrf token
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    var pusher = new Pusher('0dc3faefd8a6de9c0660', {
      cluster: 'us2',
      forceTLS: true
    });

    var channel = pusher.subscribe('my-channel');
        channel.bind('my-event', function (data) {
            // alert(JSON.stringify(data));
            if (my_id == data.from) {
                $('#' + data.to).click();
            } else if (my_id == data.to) {
                if (receiver_id == data.from) {
                    // if receiver is selected, reload the selected user ...
                    $('#' + data.from).click();
                } else {
                    // if receiver is not seleted, add notification for that user
                    var pending = parseInt($('#' + data.from).find('.pending').html());

                    if (pending) {
                        $('#' + data.from).find('.pending').html(pending + 1);
                    } else {
                        $('#' + data.from).append('<span class="pending">1</span>');
                    }
                }
            }
        });


    $('.user').click(function() {
        $('.user').removeClass('active');
        $(this).addClass('active');
        $(this).find('.pending').remove();

        receiver_id = $(this).attr('id');
        $.ajax({
            type: "get",
            url: "message/" + receiver_id, //Ruta creada en el web.php
            data: "",
            cache: false,
            success: function(data) {
                $('#messages').html(data);
                scrollToBottomFunc();
            }
        });
    });

    //Envio de mensaje con el event de Enter 
    $(document).on('keyup','.input-text input', function(e){
      var message=$(this).val();
      if(e.keyCode==13 && message!='' && receiver_id!=''){
        $(this).val('');//Limpia

        var datastr="receiver_id="+receiver_id+"&message="+message;
        debugger
        $.ajax({
          type: "post",
          url: "message",//Ruta creada en el web.php
          data: datastr,
          cache: false,
          success:function(data){
            //alert("Mensaje enviado")
          },
          error: function(jqXHR,status,err){
            console.log("ERROR-Mensaje no enviado")
          },
          complete: function(){
            scrollToBottomFunc();
          }
        })
      }
    })

    //Scroll auto
    function scrollToBottomFunc(){
        $('.message-wrapper').animate({
            scrollTop: $('.message-wrapper').get(0).scrollHeight
        },50);
    }
});
</script>
<!--FIN JQUERY-->
@section('content')
<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<div class="main main-raised">
    <div id="fondo-chat"></div>
    <div class="profile-content">
        <h3> <strong>Chat Online Test</strong> </h3>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
                    <div class="user-wrapper">
                        <ul class="users">
                            @foreach($users as $user)
                            <li class="user" id="{{ $user->id }}">
                                @if($user->unread)
                                    <span class="pending">{{ $user->unread }}</span>
                                @endif

                                <div class="media">
                                    <!--Icono perfil-->
                                    <div class="media-left">
                                        <img src="https://recetas.kowi.com.mx/Recetas/avatar.png"
                                            alt="" class="media-object">
                                    </div>
                                    <!--Datos usuario-->
                                    <div class="media-body">
                                        <p class="name"><strong>{{$user->name}}</strong> </p>
                                        <p class="correo">{{$user->email}}</p>
                                    </div>
                                </div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8" id="messages">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
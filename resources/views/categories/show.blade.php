@extends('layouts.app')

@section('body-class', 'profile-page')

@section('styles')
    <style>
        .team {
            padding-bottom: 50px;
        }
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .team .row {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display:         flex;
          flex-wrap: wrap;
        }
        .team .row > [class*='col-'] {
          display: flex;
          flex-direction: column;
        }
        .no-margin {
            margin: 0;
        }
        .team .team-player .title {
            margin-bottom: 0.5em;
        }
       .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
@media (max-width: 540px) {
  #menusuperior {
    display:block;
    width:100%;
    background:#242424;
    height: 90px;
    text-align: right;
  
    padding-top: 30px;

  }
html{
  background-color: #fff;
}
.profile{
  background-color: #fff;
}

.wrapper{
    background-color: #fff;
}
.profile-content .container{
-webkit-box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);
-moz-box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);
box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);
}


  .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 30px; 
 
}
.name .title{
  color: #838383;
  font-size: 20px;
}
.title{
 
  font-size: 11px;
  font-weight: 900;
}
.team{
  padding-top: 20px;
  width: 100%;
}
.col-md-4{
  width: 50%;

  
}


.img-raised{
  border-radius: 5px;
  -webkit-box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);
-moz-box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);
box-shadow: 10px 10px 5px 40px rgba(255,255,255,1);

}
    #selectciudad{
            display: none;
        }
        #shoppingcar{
            display: none;
        }
         #namess{
            display: none;
        }
         #imagess{
            display: none;
        }
}
    </style>
@endsection

@section('content')


<div class="main main-raised" >
    <div class="profile-content">
        <div class="container">
            <div class="row" >
                <div class="profile" >
                    
 <div class="arrow" >
                        <a href="{{ url('/') }}" class="toplink"><i class="material-icons">replay</i></i> </a>
                    </div>
                    

                    <div class="name">
                        <h3 class="title">{{ $category->name }}</h3>
                    </div>

                  
                    
                    @if (session('notification'))
                        <div class="alert alert-success">
                            {{ session('notification') }}
                        </div>
                    @endif
                </div>
            </div>
            
           


            <div class="team text-center" >
                <div class="row">
                    @foreach ($products as $product)
                    <div class="col-md-4" >
                        <div class="team-player">
                             <a href="{{ url('/products/'.$product->id) }}"><img src="{{ $product->featured_image_url }}" alt="Thumbnail Image" class="img-raised img-circle"></a>
                             
                            
                            <h6 class="title" >
                                <a href="{{ url('/products/'.$product->id) }}">{{ $product->name_short }}</a> 
                              </br>
                                <a href="{{ url('/products/'.$product->id) }}">{{ $product->name_short1 }}</a>
                            </h6>

                            
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center">
                    {{ $products->links() }}
                </div>
            </div>

        </div>
    </div>
</div>

        

@include('includes.footer')
@endsection

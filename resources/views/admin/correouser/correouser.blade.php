@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">



    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="width: 100%;">
            <div class="row">
            	  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Usuarios sin correo</h2>
            	<br>
                    <br>

                    

                                

                                 <table class="table" style="width: 100%; font-size: 12px; margin: auto;">
                                
             
                        <thead>
                            <tr>
                               
                              <th class="text-center">Nombre</th>
                             
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Numero</th>
                                <th class="text-center">Colonia</th>
                                <th class="text-center">C.P.</th>
                                <th class="text-center">Ciudad</th>
                        
                                <th class="text-center">Correo electronico</th>
                                <th class="text-center">Numero de telefono</th>
                                
                                <th class="text-center">Agregar</th>
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($listusers as $listuser)
                            <tr>
                                  
                                <form id="myform" action="{{ url('/admin/correosusers/modificar/'.$listuser->id) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}

                               
                                <td class="text-center"> {{ $listuser->name }}</td>
                                
                                <td class="text-center"> {{$listuser->address}}</td>
                                 <td class="text-center"> {{$listuser->numeroexterior}}</td>
                                <td class="text-center"> {{$listuser->colonia}}</td>

                                 <td class="text-center"> {{$listuser->codigopostal}}</td>
                                <td class="text-center"> {{ $listuser->city }}</td>
                             
                                <td class="text-center">{{ $listuser->email }}</td>

                                <td class="text-center"> {{ $listuser->phone }}</td>
                              
                               
                                <td class="text-center"> 
                                <input style="text-align: center;" type="text-center" name="email" > 

                                     <button style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">
                                     Agregar</button>
                                </td>
                                
                             </form> 

                            </tr>
                           @endforeach
                            
                        </tbody>
                    </table> 
                            
                
                </div>
           
                    <br>
                    </div>
            
				          <br>



				           
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
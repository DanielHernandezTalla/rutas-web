@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')




<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Listado de pedidos 2 </h2>

            <div class="team">
                <div class="row">
                    <div class="tabpanel">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="false">Pendiente</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                                    aria-controls="profile" aria-selected="false">Concluido</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedidos as $pedido)
                                        <tr>

                                            <td>$ {{ $pedido->status }}</td>
                                            <td>{{ $pedido->order_date }}</td>
                                            <td>{{ $pedido->created_at }}</td>
                                            <td class="td-actions">

                                                <!-- Trigger/Open The Modal -->
                                                <button class="btn btn-primary btn-round" data-toggle="modal"
                                                    data-target="#modalTomarPedido">
                                                    <i class="material-icons">add</i> Tomar Pedido
                                                </button>


                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>


                        </div>

                    </div>
                </div>
            </div>

        </div>

    </div>

    <div class="modal fade" id="modalTomarPedido" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">Estos son los datos del pedido:</h4>
                </div>
                <div class="modal-body">

                    <table class="table">
                        <thead>
                            <tr>
                                <th class="text-center">Nombre Articulo</th>
                                <th class="text-center">Precio</th>
                                <th class="text-center">Usuario</th>
                                <th class="text-center">Fecha</th>
                                <th class="text-center">Direccion</th>

                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($carts as $cart)
                            <tr>

                                <td>$ {{ $cart->name }}</td>
                                <td>{{ $cart->price }}</td>
                                <td>{{ $cart->username }}</td>
                                <td>{{ $cart->order_date }}</td>
                                <td>{{ $cart->address}}</td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>

                    <hr>
                    <p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Pedido Faltante</button>
                    <button type="button" class="btn btn-primary">Pedido Listo</button>
                </div>
            </div>
        </div>
    </div>

    @include('includes.footer')
    @endsection
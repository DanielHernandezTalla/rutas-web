@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	 <div class="panel-footer text-right">
                        <a href="{{ url('adminpedidos/pedidos') }}" class="btn btn-link"><span class="glyphicon glyphicon-arrow-left"></span> Atrás</a>
                    </div>
           <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Nombre</th>
				                        <th class="text-center">Precio</th>
				                       <th class="text-center">UM</th>
				                        <th class="text-center">Usuario</th>
				                        <th class="text-center">Telefono</th>
				                        <th class="text-center">Direccion</th>
										
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($product as $products)
				                    <tr>
				                        <td class="text-center"> {{ $products->name }}</td>
				                        <td class="text-center"> {{ $products->price }}</td>
				                        <td class="text-center"> {{ $products->uom }}</td>
				                        <td class="text-center">{{ $products->username }}</td>
				                        <td class="text-center">{{ $products->phone }}</td>
				                        <td class="text-center">{{ $products->address }}</td>
				                        
				                    </tr>
				                 @endforeach
				                </tbody>
				            </table>

				           <div class="text-center">

		
		

		<form method="post" action="{{ url('/pagado/'.$products->id) }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">done</i> Pedido listo
                    </button>
                </form>
      </div>
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

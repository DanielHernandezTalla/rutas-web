@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')
<style type="text/css">
.navbar-brand:disabled {
    border-style: none;
}

.toplink .material-icons {
    position: absolute;
    left: 5px;
    top: 5px;
    outline: 0;
    font-size: 55px;

}

.toplink .material-icons:hover {
    color: #000;
}

@media (max-width: 540px) {

    #selectciudad {
        display: none;
    }

    #shoppingcar {
        display: none;
    }
}

/*Inicio Styles Erzu*/
.nav-tabs>li.active>a,
.nav-tabs>li.active>a:hover,
.nav-tabs>li.active>a:focus {
    background-color: #ff7514;
    transition: background-color .1s .2s;
    color: white !important;
}
}

.main-raised {
  
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}

.form{
    
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
} 

.table{
    margin-top: 1%;
}

/*Fin Styles Erzu */
</style>




</div>

<!--JAVASCRIPT-->
<script type="text/javascript">
    $(document).ready(function() {
        
  });
  
  //Guarda el status del pedido
  function guardarModal(){
    let idpedido=$("#inputPedido").val();
    let status=$("#selectStatus").val();
    debugger;
      $.ajax({
          type: "GET",
          dataType: "json",
          url: 'https://apprutas.kowi.com.mx/api/updateStatusPedido',
          data: {'idpedido':idpedido,'status': status},
          success: function(data){
             $("#modalEditPedido").modal('hide');
             $("#inputPedido").val("");
             alert(data.response)
          }
      });
  }
  </script>

<div class="main main-raised form">
    <div class="container">
        <a href="/adminpedidos/reporteusuarios" target="_blank" title="Reporte usuarios dados de alta"
            class="toplink"><i class="material-icons">print</i> </a>

        <div class="row">
            <div class="col-sm" align="right">
                <button class="btn btn-primary btn-round" onclick="javascript:window.location.reload();">
                    <i class="material-icons">autorenew</i> Actualizar pedidos
                </button>
            </div>
            <div class="col-sm" align="right">
                <button class="btn btn-info btn-round"  data-toggle="modal" data-target="#modalEditPedido" >
                    <i class="material-icons">create</i> Editar pedido
                </button>
            </div>
        </div>

        <div class="section text-center">
            <h2 class="title">Listado de pedidos</h2>
            
            @if (session('notification2'))
            <div style="background-color: #F41C1C;" class="alert alert-success2">
                {{ session('notification2') }}
            </div>
            @endif
            @if (session('notification'))
            <div style="background-color: #33D872;" class="alert alert-success">
                {{ session('notification') }}
            </div>
            @endif


            <div class="team">
                <div class="row">
                    <div class="tabpanel">

                        <!-- Nav tabs -->

                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item waves-effect waves-light nav-link active">
                                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="false">Pendientes</a>
                            </li>

                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="preparado-tab" data-toggle="tab" href="#preparado" role="tab"
                                    aria-controls="preparado" aria-selected="false">Preparados</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="despachado-tab" data-toggle="tab" href="#despachado" role="tab"
                                    aria-controls="despachado" aria-selected="false">Despachados</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="auth-tab" data-toggle="tab" href="#auth" role="tab"
                                    aria-controls="auth" aria-selected="false">Entregado</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="pagado-tab" data-toggle="tab" href="#pagado" role="tab"
                                    aria-controls="pagado" aria-selected="false">Pagados</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="cancelado-tab" data-toggle="tab" href="#cancelado" role="tab"
                                    aria-controls="cancelado" aria-selected="false">Cancelados</a>
                            </li>

                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="reporte-tab" data-toggle="tab" href="#reporte" role="tab"
                                    aria-controls="reporte" aria-selected="false">Reporte de rutas</a>
                            </li>

                        </ul>

                        <!-- Tab panes -->

                        <div class="tab-content" id="myTabContent">
                            @foreach ($contarpendientes as $contarpendiente)
                            @endforeach
                            @if ($contarpendiente->cart_count == 0)

                            <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Requiere factura</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">Ruta</th>
                                            <th class="text-center">Guardar</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>
                            @else
                            <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
                               
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Requiere factura</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">Ruta</th>
                                            <th class="text-center">Guardar</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    
                                    <tbody>
                                        @foreach ($pedidos as $pedido)

                                        <tr>
                                            
                                            <form id="myform"
                                                action="{{ url('/adminpedidos/pedido/ruta/'.$pedido->id ) }}"
                                                method="POST" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <td class="text-center"> {{ $pedido->id }}</td>
                                                <td class="text-center"> {{ $pedido->status }}</td>
                                                <td class="text-center"> {{ number_format($pedido->importe_total, 2) }}</td>
                                                <td class="text-center"> {{ $pedido->order_date }}</td>
                                                <td class="text-center"> {{ $pedido->created_at }}</td>
                                                <td style="width: 25%; text-align: center;">
                                                <input type="datetime-local" name="fechaE" value="{{ date('Y-m-d\TH:i', strtotime($pedido->fechaE)) }}">
                                                </td>
                                                <td class="text-center"> {{ $pedido->name }}</td>
                                                <td class="text-center">{{$pedido->factura}} </td>
                                                <td class="text-center"> {{ $pedido->phone }}</td>
                                                <td class="text-center"> {{ $pedido->direccion }}</td>
                                                <td class="text-center"> {{ $pedido->city }}</td>
                                                <td class="text-center">
                                                    <select id="selectRuta" name="act"
                                                        style="border-style: solid; border-color: #fff; font-size: 16px; font-family: Arial; text-align: center;">
                                                        <option selected value="{{ $pedido->idcentrovta }}">
                                                            {{ $pedido->descentro }}</option>
                                                        @foreach($centroventa as $centroventas)

                                                        <option value="{{$centroventas->id}}">
                                                            {{ $centroventas->descripcion }}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                </td>
                                                <td class="text-center"> <button
                                                        onclick="return confirm('¿Seguro que el pedido ya esta listo?');"
                                                        style=" margin-top: -15%;border-style: none; background-color: #ff7514; color: #fff; font-size: 16px;
                                                        font-weight: 500; border-radius: 10%;"
                                                        type="submit">
                                                        Guardar</button> </td>
                                                <td class="text-center" class="td-actions">
                                                    <a style="margin-top: -15%; font-size: 25px;"
                                                        href="{{ url('/adminpedidos/pedidos/'.$pedido->id.'/show/'.$pedido->idreparto) }}"
                                                        rel="tooltip" title="Modificar especificacion"
                                                        class="btn btn-success btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </td>
                                            </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>


                            @endif
                            <div class="tab-pane fade" id="auth" role="tabpanel" aria-labelledby="auth-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedidosAuth as $pedidosAuths)
                                        <tr>
                                            <td class="text-center"> {{ $pedidosAuths->id }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->status }}</td>
                                            <td class="text-center"> {{ number_format($pedidosAuths->importe_total, 2) }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->order_date }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->created_at }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->fechaE }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->name }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->phone }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->address }}</td>
                                            <td class="text-center"> {{ $pedidosAuths->city }}</td>



                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="pagado" role="tabpanel" aria-labelledby="pagado-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedidosConf as $pedidosConfi)
                                        <tr>
                                            <td class="text-center"> {{ $pedidosConfi->id }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->status }}</td>
                                            <td class="text-center"> {{ number_format($pedidosConfi->importe_total, 2) }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->order_date }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->created_at }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->fechaE }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->name }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->phone }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->address }}</td>
                                            <td class="text-center"> {{ $pedidosConfi->city }}</td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>



                            <div class="tab-pane fade" id="pagado" role="tabpanel" aria-labelledby="pagado-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>

                                </table>
                            </div>



                            <div class="tab-pane fade" id="despachado" role="tabpanel" aria-labelledby="despachado-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">ruta</th>



                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($despachados as $despachado)
                                        <tr>
                                            <form action="{{ url('/adminpedidos/pedidos/modificar/'.$despachado->id) }}"
                                                method="POST" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <td class="text-center"> {{ $despachado->id }}</td>
                                                <td class="text-center"> {{ $despachado->status }}</td>
                                                <td class="text-center"> {{ number_format($despachado->importe_total, 2) }}</td>
                                                <td class="text-center"> {{ $despachado->order_date }}</td>
                                                <td class="text-center"> {{ $despachado->created_at }}</td>
                                                <td class="text-center"> {{ $despachado->fechaE }}</td>
                                                <td class="text-center"> {{ $despachado->name }}</td>
                                                <td class="text-center"> {{ $despachado->phone }}</td>
                                                <td class="text-center"> {{ $despachado->address }}</td>
                                                <td class="text-center"> {{ $despachado->city }}</td>
                                                <td class="text-center">{{ $despachado->descentro }} </td>

                                            </form>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>


                            <div class="tab-pane fade" id="preparado" role="tabpanel" aria-labelledby="preparado-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>
                                            <th>Despachar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($preparados as $preparado)
                                        <tr>
                                            <form id="myform"
                                                action="{{ url('/adminpedidos/despachado/'.$preparado->id) }}"
                                                method="POST" enctype="multipart/form-data">
                                                {{csrf_field()}}
                                                <td class="text-center"> {{ $preparado->id }}</td>
                                                <td class="text-center"> {{ $preparado->status }}</td>
                                                <td class="text-center"> {{ number_format($preparado->importe_total, 2) }}</td>
                                                <td class="text-center"> {{ $preparado->order_date }}</td>
                                                <td class="text-center"> {{ $preparado->created_at }}</td>
                                                <td class="text-center"> {{ $preparado->fechaE }}</td>
                                                <td class="text-center"> {{ $preparado->name }}</td>
                                                <td class="text-center"> {{ $preparado->phone }}</td>
                                                <td class="text-center"> {{ $preparado->address }}</td>
                                                <td class="text-center"> {{ $preparado->city }}</td>

                                                <td class="text-center" class="td-actions">

                                                    <button
                                                        style=" margin-top: -15%;border-style: none; background-color: #ff7514; color: #fff; font-size: 16px;  font-weight: 500; border-radius: 10%;"
                                                        type="submit">
                                                        Enviar a ruta</button>

                                                    <a style="margin-top: -15%; font-size: 25px;"
                                                        href="{{ url('/adminpedidos/pedidos/'.$preparado->id.'/cancelacion') }}"
                                                        rel="tooltip" title="Cancelacion"
                                                        class="btn btn-success btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </a>


                                                </td>
                                            </form>
                                        </tr>
                                        @endforeach
                                    
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="cancelado" role="tabpanel" aria-labelledby="cancelado-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Estado</th>
                                            <th class="text-center">Total</th>
                                            <th class="text-center">Fecha</th>
                                            <th class="text-center">Hora</th>
                                            <th class="text-center">Fecha Entrega</th>
                                            <th class="text-center">Nombre cliente</th>
                                            <th class="text-center">Telefono</th>
                                            <th class="text-center">Direccion</th>
                                            <th class="text-center">Ciudad</th>
                                            <th>Opciones</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($pedidosCancel as $pedidosCancels)
                                        <tr>
                                            <td class="text-center"> {{ $pedidosCancels->id }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->status }}</td>
                                            <td class="text-center"> {{ number_format($pedidosCancels->importe_total, 2) }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->order_date }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->created_at }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->fechaE }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->name }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->phone }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->address }}</td>
                                            <td class="text-center"> {{ $pedidosCancels->city }}</td>
                                            <td class="text-center" class="td-actions">

                                                <form method="post"
                                                    action="{{ url('/adminpedidos/pedidos/'.$pedidosCancels->id) }}">

                                                    <a href="{{ url('/adminpedidos/pedidos/'.$pedidosCancels->id.'/cancelado') }}"
                                                        rel="tooltip" title="Detalles del Pedido"
                                                        class="btn btn-success btn-simple btn-xs">
                                                        <i class="fa fa-edit"></i>
                                                    </a>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane fade" id="reporte" role="tabpanel" aria-labelledby="reporte-tab">
                                <table class="table">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Descripcion</th>
                                            <th class="text-center">ciudad</th>
                                            <th class="text-center">Tipo pedido</th>


                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($centroventa as $centroventas)
                                        <tr>
                                            <td class="text-center"> {{ $centroventas->id }}</td>
                                            <td class="text-center"> {{ $centroventas->descripcion }}</td>
                                            <td class="text-center"> {{ $centroventas->name }}</td>
                                            <td class="text-center"><a
                                                    href="/adminpedidos/reporte/{{ $centroventas->tipopedido }}/">
                                                    {{ $centroventas->tipopedido }}</a></td>

                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                        </div>



                    </div>

                </div>
            </div>
        </div>

    </div>

    <!-- Modal -->
    <div class="modal fade" id="modalEditPedido" tabindex="-1" role="dialog" aria-labelledby="modalEditPedido" aria-hidden="true">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="modalEditPedido" style="text-align: center;"><strong>Modificar pedido</strong></h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="inputPedido" class="col-sm-4 col-form-label">
                            <strong style="color: black" data-toggle="tooltip" data-placement="top">Número de pedido</strong>
                        </label>
                        <div class="col-sm-7">
                            <input type="number" class="form-control" id="inputPedido" placeholder="Ingrese el número de pedido" >
                        </div>
                    </div> 
                    <div class="form-group row">
                        <label for="inputPedido" class="col-sm-4 col-form-label">
                            <strong style="color: black" data-toggle="tooltip" data-placement="top">Seleccionar estatus</strong>
                        </label>
                        <select class="form-select" id="selectStatus" style="border-style: solid; border-color: #fff; font-size: 16px; font-family: Arial; text-align: center;">
                            <!--<option value="Pendiente">Regresar con el ejecutivo</option>-->
                            <option value="Cancelado">Cancelar pedido</option>
                          </select>
                    </div> 
                     
                </form>                
            </div>
            <div class="modal-footer">
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
              <button type="button" class="btn btn-primary" onclick="guardarModal()">Guardar</button>
            </div>
          </div>
        </div>
      </div> 
</div>




@include('includes.footer')
@endsection
@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">



    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }
}
</style>


<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="width: 100%;">
            <div class="row">

                             <div class="tabpanel">
<a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
@foreach ($listusers as $listuser)
       @endforeach
                  <h2 class="title text-center">Ruta y días para: {{ $listuser->name }}</h2>
    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
     


                   <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false"></a>
                </li>



                
               
              </ul>


  <div class="tab-content" id="myTabContent">

    <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">


                                 <table class="table" style="width: 100%; font-size: 11px; margin: auto;">
                                    

             
                        <thead>
                            <tr>
                              <th class="text-center">Ruta Asignada</th>
                               <th class="text-center">Lunes</th>
                              <th class="text-center">Martes</th>
                              <th class="text-center">Miercoles</th>
                                <th class="text-center">Jueves</th>
                                <th class="text-center">Viernes</th>
                                <th class="text-center">Sabado</th>
                                <th class="text-center">Guardar</th>
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($listusers as $listuser)
                            <tr>
                                 
                                <form id="myform" action="{{ url('/adminpedidos/newuser/'.$listuser->id.'/update') }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <td class="text-center">

          <select name="rutauser" style="width: 200px; border-style: solid; border-color: #fff; font-size: 16px; font-family: Arial; text-align: center;">
            

          
          @foreach($centroventa as $centroventas)

           <option value="{{$centroventas->tipopedido}}">
            {{ $centroventas->descripcion }}
           </option>
          @endforeach

          </select></td>
                                <td class="text-center"><input type="checkbox" name="lunes" value="1"></td>
                                <td class="text-center"><input type="checkbox" name="martes" value="1"></td>
                                <td class="text-center"><input type="checkbox" name="miercoles" value="1"></td>
                                <td class="text-center"><input type="checkbox" name="jueves" value="1"></td>
                                 <td class="text-center"><input type="checkbox" name="viernes" value="1"></td>
                                <td class="text-center"><input type="checkbox" name="sabado" value="1"></td>

                                
                                <td class="text-center">  <input type="text-center" name="quantity" value="0" style="display: none;">
                                     <button style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">
                                     Guardar</button>
                                </td>
                                
                             </form> 

                            </tr>
                           @endforeach
                        </tbody>
                    </table> 
              </div>




               
              </div>

        

</div>
















           
            
                
                </div>
           
                    <br>
                    </div>
            
                


                   
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
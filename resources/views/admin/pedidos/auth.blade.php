@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('adminpedidos/pedidos') }}" class="toplink"><i class="material-icons">reply</i> </a>
            	 
                    </div>
                    <br>
                    <br>
                    <br>
          <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Nombre</th>
				                    	<th class="text-center">Cantiddad</th>
				                        <th class="text-center">Precio</th>
				                        <th class="text-center">IVA</th>
				                        <th class="text-center">Importe</th>
				                       <th class="text-center">UM</th>
				                        <th class="text-center">Usuario</th>
				                        <th class="text-center">Telefono</th>
				                        <th class="text-center">Direccion</th>
									
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($product as $products)
				                    <tr>
				                        <td class="text-center"> {{ $products->name }}</td>
				                        <td class="text-center"> {{ $products->quantity }}</td>
				                        <td class="text-center"> {{ $products->price }}</td>
				                        <td class="text-center"> {{ $products->iva }}</td>
				                        <td class="text-center"> {{ $products->importe }}</td>
				                        <td class="text-center"> {{ $products->uom }}</td>
				                        <td class="text-center">{{ $products->username }}</td>
				                        <td class="text-center">{{ $products->phone }}</td>
				                        <td class="text-center">{{ $products->address }}</td>
				                        
				                    </tr>
				                 @endforeach
				                </tbody>
				            </table>

				           <div class="text-center">
<p><strong>Importe a pagar:</strong> {{ $products->importe_total }}</p>
				
@if (Auth::check())
          {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
          <input type="hidden" name="body" value="confirmar pedido">
          {{ Form::hidden('cart_id', $products->id) }}
          <button class="btn btn-primary btn-round" type="submit" >
                        <i class="material-icons">notification_important</i> Enviar notificacion
                    </button>
        {{ Form::close() }}
        @endif

		<form method="post" action="{{ url('/Confirmorder/'.$products->id) }}">
                    {{ csrf_field() }}
                   
                </form>

				
      </div>
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

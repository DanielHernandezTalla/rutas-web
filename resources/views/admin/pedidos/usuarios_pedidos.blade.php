@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<!--INICIO Styles-->
<style type="text/css">

.main-raised {
  
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}

.form{
    
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}
</style>
<!--FIN Styles-->


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="form">
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Listado de usuarios</h2>
            <div class="team">
                <div class="row">
                    <div class="tabpanel">
                        <!-- Tab panes -->
                        <table class="table table-striped">
                            <thead style="background-color: #ff7514; color:white">
                                <tr>
                                    <th class="text-center">#</th>
                                    <th class="text-center">Nombre</th>
                                    <th class="text-center">Dirección</th>
                                    <th class="text-center">Teléfono</th>
                                    <th class="text-center">Ciudad</th>
                                    <th class="text-center">Num. Exterior</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($users as $key=>$usuarios)
                                <tr>
                                    <td>{{ $key+1 }}</td>
                                    <td>{{ $usuarios->name }}</td>
                                    <td>{{ $usuarios->address }}</td>
                                    <td>{{ $usuarios->phone==null?"S/N":$usuarios->phone }}</td>
                                    <td>{{ $usuarios->city }}</td>
                                    <td>{{ $usuarios->numeroexterior==null?"S/N":$usuarios->numeroexterior}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('includes.footer')
    @endsection
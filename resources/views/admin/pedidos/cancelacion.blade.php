@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
@media (max-width: 540px) {

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }
  }
</style>

 
<div class="main main-raised">
    <div class="profile-content">
        <div class="container">

            <div class="row">

                   
            	
                      
                        <a href="{{ url('adminpedidos/pedidos') }}" class="toplink"><i class="material-icons">reply</i> </a>
                    </div>
                    <br>
                    <br>
                    <br>
          <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Nombre</th>
				                    	<th class="text-center">Cantidad</th>
				                      
				                        <th class="text-center">IVA</th>
				                        <th class="text-center">Importe</th>                       
				                        
				                        <th class="text-center">UM</th>		                        
				                        <th class="text-center">Usuario</th>
				                        <th class="text-center">Telefono</th>
				                        <th class="text-center">Direccion</th>
										<th>Opciones</th>
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($product as $products)
				                    <tr>
				                        <td class="text-center"> {{ $products->name }}</td>
				                        <td class="text-center"> {{ $products->quantity }}</td>
				                       
				                        <td class="text-center"> {{ $products->iva }}</td>
				                        <td class="text-center"> {{ $products->importe }}</td>
				                       
				                        <td class="text-center"> {{ $products->uom }}</td>           
				                        <td class="text-center">{{ $products->username }}</td>
				                        <td class="text-center">{{ $products->phone }}</td>
				                        <td class="text-center">{{ $products->address }}</td>

                                        <input  type="hidden" class="form-control" id="id" name="id" value="$products->userid">
                                      
                                        

				                        <td class="text-center" class="td-actions">
				                    
				                        <form method="post" action="{{ url('/adminpedidos/pedidos/'.$products->id) }}">
                                       
                                        <a href="{{ url('/adminpedidos/pedidos/'.$products->id_cart.'/edit') }}"  rel="tooltip" title="Detalles del Pedido" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        
                                    </form>   
				                              
				                                                           
				                        </td>
				                    </tr>

				                 @endforeach
				                </tbody>
				            </table>

				           <div class="text-center">
			         
<h4><strong >Importe a pagar:</strong> {{ $products->importe_modificar }}</h4>
 



      
                    
                   
             <input type="text" name="motivoCancelacion" id="motivoCancelacion" placeholder="Motivo Cancelacion">
             
             <form method="post" action="{{ url('/CancelorderCancelacion/'.$products->id) }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">done</i> Cancelar pedido
                    </button>
                </form>


    
      </div>
           
        </div>
    </div>
</div>

<script>
function proccess(){
   let data={
      'tokenExpo': document.getElementById('tokenExpo').value
   }
   fetch('https://exp.host/--/api/v2/push/send', {
      headers:{
     'Host': 'exp.host',
     'Accept': 'application/json',
     'Accept-encoding': 'gzip, deflate',
     'Content-Type': 'application/json',
      },
      method: 'POST',
      mode: 'no-cors',
      credentials: 'same-origin',
      body: JSON.stringify({
      to : data,
      title : "Actualizacion de su pedido",
      body : "su pedido se encuentra en el picking"
                        
   })
  })
  .then(response => response.json())
  .then(function(result){
      alert(result.message);
  })
  .catch(function (error) {
    console.log(error);
  });

  }

</script>


@include('includes.footer')
@endsection

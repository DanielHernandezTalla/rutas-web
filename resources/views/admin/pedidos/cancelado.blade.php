@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	 <div class="panel-footer text-right">
                        <a href="{{ url('adminpedidos/pedidos') }}" class="btn btn-link"><span class="glyphicon glyphicon-arrow-left"></span> Atrás</a>
                    </div>
           <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Nombre</th>
				                        <th class="text-center">Precio</th>
				                       
				                        <th class="text-center">Usuario</th>
				                        <th class="text-center">Telefono</th>
				                        <th class="text-center">Direccion</th>
										<th>Opciones</th>
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($product as $products)
				                    <tr>
				                        <td class="text-center"> {{ $products->name }}</td>
				                        <td class="text-center"> {{ $products->price }}</td>
				                        
				                        <td class="text-center">{{ $products->username }}</td>
				                        <td class="text-center">{{ $products->phone }}</td>
				                        <td class="text-center">{{ $products->address }}</td>
				                        <td class="text-center" class="td-actions">
				                    
				                        <form method="post" action="{{ url('/adminpedidos/pedidos/'.$products->id) }}">
                                       
                                        <a href="{{ url('/adminpedidos/pedidos/'.$products->id.'/cancelado') }}" rel="tooltip" title="Detalles del Pedido" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        
                                    </form>   
				                              
				                                                           
				                        </td>
				                    </tr>
				                 @endforeach
				                </tbody>
				            </table>

				           <div class="text-center">
				           	<p><strong>Importe a pagar:</strong> {{ $products->importe_total }}</p>

		@if (Auth::check())
          {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
          <p>{{ Form::textarea('body', old('body')) }}</p>
          {{ Form::hidden('cart_id', $products->id) }}
          <p>{{ Form::submit('Pedido Faltante') }}</p>
        {{ Form::close() }}
        @endif
		

		<form method="post" action="{{ url('/pagado/'.$products->id) }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">done</i> Pedido listo
                    </button>
                </form>
      </div>
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

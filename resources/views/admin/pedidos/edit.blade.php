@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.col-sm-2{
    width: 135px;
}
@media (max-width: 540px) {

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }
  }
</style>

</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
              @foreach ($product as $products)
              @endforeach
              <a href="/adminpedidos/pedidos/{{ $products->carroid }}/show" class="toplink"><i class="material-icons">reply</i> </a>
            <h2 class="title text-center">Actualizar Pedido</h2>

              

            <div class="row">
            <button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modalAddToCart">
                        <i class="material-icons">add</i> Agregar 
                    </button>
                    </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            @foreach ($product as $products)

           
                <form method="post" action="{{ url('/adminpedidos/pedidos/'.$products->id.'/update') }}">
                {{ csrf_field() }}

                    <input style="display: none;" type="text" name="preciobueno" value="{{$products->price}}">
                     <input  style="display: none;" type="text" name="ivabueno" value="{{$products->iva}}">
                    
                <div class="row">

                <div class="col-sm-2" style="display: none;">
                        <div class="form-group label-floating">
                            <label class="control-label">ID del carrito</label>
                            <input type="text" class="form-control" name="cart_id" value="{{ old('cart_id', $products->id_cart) }}">
                  
                        </div>
                    </div>

                    <div class="col-sm-2" style="display: none;">
                        <div class="form-group label-floating">
                            <label class="control-label">ID del Producto</label>
                            <input type="text" class="form-control" name="product_id" value="{{ old('product_id', $products->id_product) }}">
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <label class="control-label">Nombre del producto</label>
                            <input disabled type="text" class="form-control" name="name" value="{{ old('name', $products->name) }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">Cantidad del  producto</label>
                            <input type="text" class="form-control" name="cantidadbuena" value="{{$products->quantity}}">
                        </div>
                    </div>



                   

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                        <label  class="control-label">Precio del producto</label>
                        <input disabled type="number" step="0.01" class="form-control" name="price" value="{{ old('price', $products->price) }}">
                        </div>
                    </div>

<div class="col-sm-2">
                        <div class="form-group label-floating">
                        <label class="control-label">IVA</label>
                        <input disabled type="number" step="0.01" class="form-control" name="iva" value="{{ old('iva', $products->iva) }}">
                        </div>
                    </div>

                      <div class="col-sm-2">
                        <div class="form-group label-floating">
                        <label class="control-label">Importe</label>
                        <input disabled type="number" step="0.01" class="form-control" name="importe" value="{{ old('importe', $products->importe) }}">
                        </div>
                    </div>

 <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">UOM</label>
                            <input disabled disabled type="text" class="form-control" name="uom" value="{{ old('uom', $products->uom) }}">
                        </div>
                    </div>
                    
<div class="col-sm-2">
                 <button class="btn btn-primary">Guardar</button> 
             
              </form>  
              
   </div>

<td class="td-actions text-right">
                                    <form method="post" action="{{ url('/adminpedidos/pedidos/'.$products->id) }}">
                                        {{ csrf_field() }}
                                        {{ method_field('DELETE') }}                                
                                        <button type="submit" rel="tooltip" title="Eliminar" class="btn btn-danger btn-simple btn-xs">
                                            <i class="fa fa-times"></i>
                                        </button>
                                    </form>                                    
                                </td>

             
           
                </div>

            @endforeach
<p><strong>Importe a pagar:</strong> {{ $products->importe_modificar }}</p>
               
<br>

           

        </div>

        
    </div>
</div>

<div class="modal fade" id="modalAddToCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Agregar Otro Producto</h4>
      </div>

      <form method="post" action="{{ url('adminpedidos/pedidos/create') }}">
        {{ csrf_field() }}
        <input type="hidden" name="product_id" value="{{ $products->id }}">
        <div class="modal-body">
        <div class="row">
        <div class="col-sm-2" hidden>
                        <div class="form-group label-floating">
                            <label class="control-label">ID del carrito</label>
                            <input type="text" class="form-control" name="cart_id" value="{{ old('cart_id', $products->id_cart) }}">
                        </div>
                    </div>


                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Codigo del producto</label>
                            <input type="text" class="form-control" name="product_codigo" value="">
                        </div>
                    </div>
                    

                    <div class="col-sm-4">
                        <div class="form-group label-floating">
                            <label class="control-label">Cantidad del  producto</label>
                            <input type="number" class="form-control" name="cantidadbuena" value="">
                        </div>
                    </div>

          <div class="modal-footer">
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
            <button type="submit" class="btn btn-info btn-simple">Añadir al carrito</button>
          </div>

      </form>
    </div>
  </div>
</div>    
@include('includes.footer')
@endsection

@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('adminpedidos/pedidos')}}" class="toplink"><i class="material-icons">reply</i> </a>
            	<br>
                    <br>
                  
                </div>
           
                    <br>
                    </div>
           <table class="table">
           	 @foreach ($product as $products)
           	  @endforeach
				                <thead>
				                    <tr>
				                    	<th class="text-center">Numero de orden</th>
				                        <th class="text-center">Usuario</th>
				                        <th class="text-center">Total</th>
				                        <th class="text-center">Status</th>
				                        
										
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                
				                    <tr>
				                        <td class="text-center"> {{ $products->id_cart }}</td>
				                        <td class="text-center"> {{ $products->username }}</td>
				                        <td class="text-center"> {{ $products->importe_total }}</td>
				                        <td class="text-center"> {{ $products->status }}</td>
				                        
				                       
				                    </tr>
				                 
				                </tbody>
				            </table>
				            <input type="hidden" name="email" value="{{ $products->email }}">
               



				           <div class="text-center">

@if (Auth::check())
          {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
          <input type="hidden" name="body" value="tu pedido esta en camino">
          {{ Form::hidden('cart_id', $products->id) }}
          <button class="btn btn-primary btn-round" type="submit" >
                        <i class="material-icons">notification_important</i> Enviar notificacion
                    </button>
        {{ Form::close() }}
        @endif


		<form method="post" action="{{ url('/adminpedidos/pedidos/'.$products->id_cart.'/updateDes') }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">done</i> Pedido despachado
                    </button>
                </form>


      </div>
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
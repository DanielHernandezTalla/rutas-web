@extends('layouts.app')

@section('title', 'Listado de existencias')

@section('body-class', 'product-page')

@section('content')

    <!--INICIO Styles-->
    <style type="text/css">
        .main-raised {

            border-radius: 6px;
            box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
        }

        .form {

            border-radius: 6px;
            box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
        }
    </style>
    <!--FIN Styles-->


    <div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
    </div>

    <div class="form">
        <div class="container">
            <div class="section text-center">
                <h2 class="title" style="margin-top: 32px;">Listado de existencias</h2>
                <div class="team">
                    <div class="row">
                        <div class="tabpanel">
                            <!-- Tab panes -->

                            <form class="row" method="POST" action="{{ route('adminpedidos.consultarExistencia') }}" >
                                @csrf
                                <div class="col col-lg-10">
                                    <input type="text" class="form-control col-sm mb-2 mr-sm-2"
                                        placeholder="Codigo de producto" name="codigo">
                                        @if (isset($errors) && $errors->any())
                                            <ul>
                                                @foreach ($errors->all() as $error)
                                                    <li style="list-style: none; text-align: left; color: #ff7514;">{{ $error }}</li>
                                                @endforeach
                                            </ul>
                                        @endif
                                </div>
                                <div class="col col-lg-2">
                                    <button type="submit" class="col-sm btn btn-primary mb-2">Buscar</button>
                                </div>

                            </form>

                            <table class="table table-striped" style="margin-top: 16px;">
                            <thead style="background-color: #ff7514; color:white">
                                <tr>
                                    <th class="text-center">Codigo</th>
                                    <th class="text-center">Nombre</th>
                                    <th style="text-align: right;" class="text-center">Inventario</th>
                                    <th style="text-align: right;" class="text-center">Reserva</th>
                                    <th style="text-align: right;" class="text-center">Existencia</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($existencias as $key => $existencias)
                                <tr>

                                    <td>{{ $existencias->codigo_producto }}</td>
                                    <td>{{ $existencias->nombre_producto }}</td>
                                    <td style="text-align: right;">{{ $existencias->inventario==null?"0":$existencias->inventario }}</td>
                                    <td style="text-align: right;">{{ $existencias->reserva==null?"0":$existencias->reserva }}</td>
                                    <td style="text-align: right;">{{ $existencias->existencia==null?"0":$existencias->existencia }}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>

        @include('includes.footer')
    @endsection

@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('/admin/promolist')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Asignar promocion</h2>
            	<br>
                    <br>
                  
                </div>
           
                    <br>
                    </div>
            <table class="table">
           
             
                        <thead>
                            <tr>
                              <th class="text-center">Nombre</th>
                               
                                <th class="text-center">Correo electronico</th>
                                <th class="text-center">Codigo del producto</th>
                                <th class="text-center">Descripcion del producto</th>
                                <th class="text-center">Precio alctual</th>
                                <th class="text-center">Precio nuevo</th>
                                <th class="text-center">Agregar</th>
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        
                            <tr>
                                 @foreach ($listusers as $listuser)
                                <td class="text-center"> {{ $listuser->name }}</td>
                               
                                <td class="text-center"> {{ $listuser->email }}</td>

                                <td class="text-center" style="width: 10%;"> <input style="margin-top: -25px; ;" type="text"  class="form-control"  placeholder="Codigo del producto" name="codigo" autofocus required></td>

                                <td class="text-center"> <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Descripcion del producto" name="des" required></td>

                                <td class="text-center">  <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Precio actual" name="actual" required > </td>

                                <td class="text-center">  <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Precio Nuevo" name="nuevo" required > </td>
                                
                                <td class="text-center"> <a  href="{{ url('/promo/'.$listuser->id) }}" rel="tooltip" title="Agregar" class="">
<i style="font-size: 30px; font-weight: bold;" class="material-icons">add</i>
</a></td>
                                
                              
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
				          <br>



				           
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
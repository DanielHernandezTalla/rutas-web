@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Promociones!!!</h2>
            	<br>
                    <br>
                  
                </div>
           
                    <br>
                    </div>
                     <table class="table">
           
             
                        <tbody >
                        
                            <tr >
                                 
                                <td class="text-center"> </td>
                                <td class="text-center"> </td>
                                 <td class="text-center"> </td>
                                <td style="width: 20%; "  class="text-center">  <input style="margin-right: 200px;" type="text"  class="form-control"  placeholder="Nombre del usuario" name="name" required autofocus></td>
                                <td  style="width: 20%; text-align: left;" class="text-center">  <a   rel="tooltip" title="Buscar" class="">
<i style="font-size: 60px; font-weight: bold;" class="material-icons">search</i>
</a></td>
 <td class="text-center"> </td>
  <td class="text-center"> </td>
                                
                             

                              
                            </tr>
                         
                        </tbody>
                    </table>
            <table class="table">
           
             
                        <thead>
                            <tr>
                              <th class="text-center">Nombre</th>
                                <th class="text-center">Nombre de usuario</th>
                                <th class="text-center">Correo electronico</th>
                                <th class="text-center">Numero de telefono</th>
                                <th class="text-center">Ciudad</th>
                                <th class="text-center">Agregar</th>
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        
                            <tr>
                                 @foreach ($listusers as $listuser)
                                <td class="text-center"> {{ $listuser->name }}</td>
                                <td class="text-center"> {{ $listuser->username }}</td>
                                <td class="text-center"> {{ $listuser->email }}</td>
                                <td class="text-center"> {{ $listuser->phone }}</td>
                                <td class="text-center"> {{ $listuser->city }}</td>
                                
                                <td class="text-center"> <a  href="{{ url('/admin/promo/'.$listuser->id) }}" rel="tooltip" title="Agregar" class="">
<i style="font-size: 25px; font-weight: bold;" class="material-icons">add</i>
</a></td>
                                
                              
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
				          <br>



				           
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
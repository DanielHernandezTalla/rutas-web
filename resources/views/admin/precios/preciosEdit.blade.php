@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.col-sm-2{
    width: 135px;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
              <a href="{{ url()->previous() }}" class="toplink"><i class="material-icons">reply</i> </a>
            <h2 class="title text-center">Actualizar Precios</h2>

            

            @foreach ($price as $prices)
                <form method="post" action="{{ url('/admin/precios/'.$prices->filtro.'/update') }}">
                {{ csrf_field() }}

                <div class="row">

                <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">Codigo</label>
                            <input disabled type="text" class="form-control" name="Codigo" value="{{ old('code', $prices->code) }}">
                  
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <label class="control-label">Nombre del producto</label>
                            <input disabled type="text" class="form-control" name="name" value="{{ old('nameproduct', $prices->nameproduct) }}">
                        </div>
                    </div>

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                            <label class="control-label">Precio actual</label>
                            <input disabled type="text" class="form-control" name="actual" value="{{ old('actual', $prices->actual) }}">
                        </div>
                    </div>

                   

                    <div class="col-sm-2">
                        <div class="form-group label-floating">
                        <label  class="control-label">Precio nuevo</label>
                        <input  type="number"  class="form-control" name="nuevo" value="{{ old('nuevo', $prices->nuevo) }}">
                        </div>
                    </div>

                    
<div class="col-sm-2">
                 <button class="btn btn-primary">Guardar</button> 
             
              </form>  
              
   </div>



             
           
                </div>

            @endforeach

<br>

           

        </div>

        
    </div>
</div>
   
@include('includes.footer')
@endsection

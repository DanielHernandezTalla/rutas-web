@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
	.medio
		overflow-y: scroll;
		height: 600px;

	}

.precio{
    text-align: center;
    width: 35%;
    border: hidden;
}
 .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.col-sm-2{
    width: 135px;
}
.btn-estadio{
    background-color: #ff7514;
    color: #FFFFFF;
}
.toplink2 .material-icons {
 position: absolute;
 right: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink2 .material-icons:hover {
  color: #000;
}
.btn-estadio{
    background-color: #ff7514;
    color: #FFFFFF;
}
.col-sm-2{
    width: 135px;
}

.polla{
	text-align: center;
	width: 35%;
	border: hidden;
}
@media (max-width: 540px) {
 #menusuperior {
    display:block;
    width:100%;
    background:#242424;
    height: 120px;
    text-align: right;
    padding-top: 60px;

  }
  .datosuser{
  display: none;
}
#namess{
display: none;
}
#imagess  {
display: none;
}
#selectciudad{
	display: none;
}
#shoppingcar{
		display: none;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
            <h2 class="title text-center">Listado de precios</h2>  

                     

                    <div class="medio">
           <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Codigo</th>
				                    	
				                        <th class="text-center">Catalogo de precios</th>
				                        <th class="text-center">Tipo</th>
				                        
				                         <th class="text-center"></th>
										
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($price as $prices)
				                    <tr>
				                    	<td class="text-center" >
				                        <input disabled="" class="polla" type="text"  name="Codigo"  value="{{ old('filtro', $prices->filtro) }}"></td>

				                        <td class="text-center"> {{ $prices->namecat }}</td>
				                        <td class="text-center"  >{{ $prices->tipo }}</td>
				                         <td class="text-center" class="td-actions">
				                    
				                        
                                       
                                        <a href="{{ url('/admin/precios/'.$prices->filtro.'/edit') }}" rel="tooltip" title="Detalles del Pedido" class="btn btn-success btn-simple btn-xs">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                       			                              
				                                                           
				                        </td>
				                       
				                    </tr>
				                        
				                 @endforeach
				                </tbody>
				            </table>

				         
       </div>   
       <br>

         <div class="text-center">

		
	
            <div class="col-sm-2">
                 
             
            
              
   </div>        
            
               

	
      </div>
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')


<script>
  $("document").ready(function(){
    
  });

 

  function showDia(idCliente){
    debugger;
     //Valor del dia
     let jsonDias={
       "lunes":false,
       "martes":false,
       "miercoles":false,
       "jueves":false,
       "jueves":false,
       "viernes":false,
       "sabado":false,
       "domingo":false
     };
     let diaLunes = $('#lunes'+idCliente).is(':checked');
     let diaMartes = $('#martes'+idCliente).is(':checked');
     let diaMiercoles = $('#miercoles'+idCliente).is(':checked');
     let diaJueves = $('#jueves'+idCliente).is(':checked');
     let diaViernes = $('#viernes'+idCliente).is(':checked');
     let diaSabado = $('#sabado'+idCliente).is(':checked');
     let diaDomningo = $('#domingo'+idCliente).is(':checked');
     let idruta = $('#selectRuta'+idCliente).val();

     if(diaLunes){
       jsonDias.lunes=true;
     }if(diaMartes){
      jsonDias.martes=true;
     }if(diaMiercoles){
      jsonDias.miercoles=true;
     }if(diaJueves){
      jsonDias.jueves=true;
     }if(diaViernes){
      jsonDias.viernes=true;
     }if(diaSabado){
      jsonDias.sabado=true;
     }if(diaDomningo){
      jsonDias.domingo=true;
     }

     //Servicio
     $.ajax({
        type: "POST",
        dataType: "json",
        url: '{{ route('updateUserOracle') }}',
        data: {'idCliente': idCliente,'jsonDias':jsonDias,'idRuta':idruta},
        success: function(data){
          debugger;
           //alert(JSON.stringify(data))
        }
    });
    
   }
</script>

    <style type="text/css">
        .toplink .material-icons {
            position: absolute;
            left: 5px;
            top: 5px;
            outline: 0;
            font-size: 55px;

        }
        .toplink .material-icons:hover {
            color: #000;
        }
        #selectciudad {
            display: none;
        }
        #shoppingcar {
            display: none;
        }
        }
    </style>


    <div class="main main-raised">
        <div class="profile-content">
            <div class="container" style="width: 100%;">
                <div class="row">
                    <div class="tabpanel">
                        <a href="{{ url('/') }}" class="toplink"><i class="material-icons">reply</i> </a>
                        <h2 class="title text-center">Enviar usuarios a oracle</h2>
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item waves-effect waves-light nav-link active">
                                <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                    aria-controls="home" aria-selected="false">Usuarios nuevos</a>
                            </li>
                            <li class="nav-item waves-effect waves-light">
                                <a class="nav-link" id="despachado-tab" data-toggle="tab" href="#despachado" role="tab"
                                    aria-controls="despachado" aria-selected="false">Usuarios dados de alta</a>
                            </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content" id="myTabContent">

                            <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                <h2 style="font-size: 22px;" class="title text-center">Usuarios pendientes</h2>
                                <table class="table" style="width: 100%; font-size: 11px; margin: auto;">
                                    <form method="get" action="{{ url('/useradmin/newuser/search') }}">
                                        <input type="text-center" name="name" placeholder="Nombre del usuario">
                                        <select name="city" id="citys">
                                            <option selected value="">Seleciona una ciudad</option>
                                            @foreach ($citys as $id => $city)
                                                <option value="{{ $city }}">
                                                    {{ $city }}
                                                </option>
                                            @endforeach
                                        </select>
                                        <button type="submit">buscar</button>
                                    </form>
                                    <thead>
                                        <tr>
                                            <th class="text-center">Cliente Ecommerce</th>
                                            <th class="text-center">Sitio</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Rfc</th>
                                            <th class="text-center">Dias de visita</th>
                                            <th class="text-center">Dirección</th>
                                            <th class="text-center">Numero exterior</th>
                                            <th class="text-center">Ruta</th>
                                            <th class="text-center">Colonia</th>
                                            <th class="text-center">C.P.</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">Correo electronico</th>
                                            <th class="text-center">Numero de telefono</th>
                                            <th class="text-center">Giro</th>
                                            <th class="text-center">Agregar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listusers as $listuser)
                                        <form id="myform" action="{{ url('/admin/asignarRuta/'.$listuser->id) }}" method="POST">
                                                    {{ csrf_field() }}
                                                   
                                            <tr>
                                                
                                                    <td class="text-center">{{ $listuser->id }} </td>
                                                    <td class="text-center"> </td>
                                                    <td class="text-center"> <input type="text" name="name"  value="{{ $listuser->name }}" required></td>
                                                    <td class="text-center"> {{ $listuser->rfc }}</td>
                                                    <!--SELECT DE DIAS VISITA-->
                                                      <td class="text-center">
                                                        <div class="input-group">
                                                          <div class="input-group-btn">
                                                              <button tabindex="-1" data-toggle="dropdown" class="btn btn-default dropdown-toggle" type="button">
                                                                Seleccionar dias <span class="caret"></span>
                                                              </button>
                                                              <ul role="menu" class="dropdown-menu">
                                                                  <li>
                                                                      <a href="#"> <input name="dias[]" value="lunes" type="checkbox"  /> <span class="lbl"> Lunes</span> </a>
                                                                      <a href="#"> <input name="dias[]" value="martes" type="checkbox" /><span class="lbl"> Martes</span> </a>
                                                                      <a href="#"> <input name="dias[]" value="miercoles" type="checkbox" /><span class="lbl"> Miércoles</span> </a>
                                                                      <a href="#"> <input name="dias[]" value="jueves" type="checkbox" /><span class="lbl"> Jueves</span> </a>
                                                                      <a href="#"> <input name="dias[]" value="viernes" type="checkbox" /><span class="lbl"> Viernes</span> </a>
                                                                      <a href="#"> <input name="dias[]" value="sabado" type="checkbox" /><span class="lbl"> Sábado</span> </a>
                                                                      
                                                                  </li>
                                                              </ul>
                                                          </div>
                                                        </div>
                                                      </td>
                                                      <!--END SELECT DE DIAS VISITA-->

                                                    <td class="text-center"> <input type="text" name="direccion" value="{{ $listuser->address }}"></td>
                                                    <td class="text-center"> {{ $listuser->numeroexterior }}</td>
                                                    <td class="text-center">
                                                        <select id="ruta{{ $listuser->id }}" name="ruta"
                                                            style="border-style: solid; border-color: #fff; font-size: 16px; font-family: Arial; text-align: center;" required>
                                                            @foreach ($prevendedores as $prevendedor)
                                                                <option value="{{ $prevendedor->tipo_pedido }}">
                                                                    {{ $prevendedor->name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                    <td class="text-center"> {{ $listuser->colonia }}</td>
                                                    <td class="text-center"> {{ $listuser->codigopostal }}</td>
                                                    <td class="text-center"> {{ $listuser->city }}</td>
                                                    <td class="text-center"> {{ $listuser->email }}</td>
                                                    <td class="text-center"> {{ $listuser->phone }}</td>
                                                    <td class="text-center"><input type="text" name="giro" value="" required></td>
                                                  <td class="text-center"> 
                                                        <button
                                                            style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;"
                                                            type="submit">
                                                            Enviar</button>
                                                    </td>
                                                    
                                                   <!--   <td class="text-center"> <input type="text-center" name="quantity"
                                                            value="1" style="display: none;">
                                                        <button
                                                            style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;"
                                                            type="submit">
                                                            Guardar</button>
                                                    </td> -->
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>

                            <div class="tab-pane fade" id="despachado" role="tabpanel" aria-labelledby="despachado-tab">
                                <h2 style="font-size: 22px;" class="title text-center">Usuarios asignados</h2>
                                <form method="get" action="{{ url('/useradmin/newuser/search2') }}">
                                    <input type="text-center" name="name" placeholder="Nombre del usuario">
                                    <select name="city" id="citys">
                                        <option selected value="">Seleciona una ciudad</option>
                                        @foreach ($citys as $id => $city)
                                            <option value="{{ $city }}">
                                                {{ $city }}
                                            </option>
                                        @endforeach
                                    </select>
                                    <button type="submit">buscar</button>
                                </form>
                                <table class="table" style="width: 100%; font-size: 11px;">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Cliente Ecommerce</th>
                                            <th class="text-center">Sitio</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">RFC</th>
                                            <th class="text-center">Dirección</th>
                                            <th class="text-center">Numero exterior</th>
                                            <th class="text-center">Colonia</th>
                                            <th class="text-center">C.P.</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">Correo electronico</th>
                                            <th class="text-center">Numero de telefono</th>
                                            <th class="text-center">Agregar</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($listuserss2 as $listuser2)
                                            <tr>
                                                <form id="myform2"
                                                    action="{{ url('/useradmin/update/' . $listuser2->id) }}"
                                                    method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <td class="text-center">{{ $listuser2->id }} </td>
                                                    <td class="text-center"> </td>
                                                    <td class="text-center"> {{ $listuser2->name }}</td>
                                                    <td class="text-center"> {{ $listuser2->rfc }} </td>
                                                    <td class="text-center"> {{ $listuser2->address }}</td>
                                                    <td class="text-center"> {{ $listuser2->numeroexterior }}</td>
                                                    <td class="text-center"> {{ $listuser2->colonia }}</td>
                                                    <td class="text-center"> {{ $listuser2->codigopostal }}</td>
                                                    <td class="text-center"> {{ $listuser2->city }}</td>
                                                    <td class="text-center"> {{ $listuser2->email }}</td>
                                                    <td class="text-center"> {{ $listuser2->phone }}</td>
                                                    <td class="text-center">
                                                        <input type="text-center" name="quantits" value="0"
                                                            style="display: none;">
                                                        <button
                                                            style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;"
                                                            type="submit">Bajar</button>
                                                    </td>
                                                </form>
                                            </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <br>
            </div>
        </div>
    </div>
    </div>


    @include('includes.footer')
@endsection

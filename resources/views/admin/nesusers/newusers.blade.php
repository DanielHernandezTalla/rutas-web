@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">



    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }
}
</style>


<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="width: 100%;">
            <div class="row">

                             <div class="tabpanel">
<a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Añadir usuarios</h2>
    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
     


                   <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Usuarios nuevos</a>
                </li>



        <li class="nav-item waves-effect waves-light">
                  <a class="nav-link" id="despachado-tab" data-toggle="tab" href="#despachado" role="tab" aria-controls="despachado" aria-selected="false">Usuarios dados de alta</a>
                </li>
                
               
              </ul>

    <!-- Tab panes -->
   
  <div class="tab-content" id="myTabContent">

    <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
             <h2 style="font-size: 22px;" class="title text-center">Usuarios pendientes</h2>

                                 <table class="table" style="width: 100%; font-size: 11px; margin: auto;">



             
                        <thead>
                            <tr>
                            <th class="text-center">Cliente Ecommerce</th>
                                            <th class="text-center">Sitio</th>
                                            <th class="text-center">Nombre</th>
                                            <th class="text-center">Rfc</th>
                                            <th class="text-center">Dias de visita</th>
                                            <th class="text-center">Dirección</th>
                                            <th class="text-center">Numero exterior</th>
                                            <th class="text-center">Ruta</th>
                                            <th class="text-center">Colonia</th>
                                            <th class="text-center">C.P.</th>
                                            <th class="text-center">Ciudad</th>
                                            <th class="text-center">Correo electronico</th>
                                            <th class="text-center">Numero de telefono</th>
                                            <th class="text-center">Giro</th>
                                            <th class="text-center">Agregar</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($listusers as $listuser)
                            <tr>
                                 
                                <form id="myform" action="{{ url('/useradmin/modificar/'.$listuser->id) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                <td class="text-center">{{$listuser->id}} </td>
                                <td class="text-center"> </td>
                                <td class="text-center"> {{ $listuser->name }}</td>
                                <td class="text-center"> {{ $listuser->rfc }}</td>
                                <td class="text-center">{{$listuser->lunes==null?'':'lunes '}}{{$listuser->martes==null?'':',martes '}}{{$listuser->miercoles==null?'':',miercoles '}}{{$listuser->jueves==null?'':',jueves '}}{{$listuser->viernes==null?'':',viernes'}} </td>
                                <td class="text-center"> {{$listuser->address}}</td>
                                 <td class="text-center"> {{$listuser->numeroexterior}}</td>
                                 @foreach ($prevendedores as $prevendedor)
                                 <td class="text-center">{{$prevendedor->name}} </td>
                                 @endforeach
                                <td class="text-center"> {{$listuser->colonia}}</td>
                                 <td class="text-center"> {{$listuser->codigopostal}}</td>
                                <td class="text-center"> {{ $listuser->city2 }}</td>
                                <td class="text-center"> {{ $listuser->email }}</td>
                                <td class="text-center"> {{ $listuser->phone }}</td>
                                <td class="text-center">{{ $listuser->giro }} </td>
                                
                                
                                
                                <td class="text-center">  <input type="text-center" name="quantity" value="1" style="display: none;">
                                     <button style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">
                                     Agregar</button>
                                </td>
                                
                             </form> 

                            </tr>
                           @endforeach
                        </tbody>
                    </table> 
              </div>

                <div class="tab-pane fade" id="despachado" role="tabpanel" aria-labelledby="despachado-tab">
                <h2 style="font-size: 22px;" class="title text-center">Usuarios asignados</h2>
                
                                <table class="table" style="width: 100%; font-size: 11px;">
           
                       
                        <thead>
                            <tr>
                            <th class="text-center">Cliente Ecommerce</th>
                               <th class="text-center">Sitio</th>
                              <th class="text-center">Nombre</th>
                              <th class="text-center">RFC</th>
                                <th class="text-center">Dirección</th>
                                <th class="text-center">Numero exterior</th>
                                <th class="text-center">Colonia</th>
                                <th class="text-center">C.P.</th>
                                <th class="text-center">Ciudad</th>
                         
                                <th class="text-center">Correo electronico</th>
                                <th class="text-center">Numero de telefono</th>
                              
                                <th class="text-center">Agregar</th>
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($listuserss2 as $listuser2)
                            <tr>
                                 
                                 <form id="myform2" action="{{ url('/useradmin/update/'.$listuser2->id) }}" method="POST" enctype="multipart/form-data">
                                {{csrf_field()}}
                                 
                         
                            
                                  
                                  <td class="text-center">{{$listuser2->id}} </td>
                                  <td class="text-center"> </td>
                                <td class="text-center"> {{ $listuser2->name }}</td>
                                <td class="text-center">  {{$listuser2->rfc}} </td>
                                <td class="text-center"> {{$listuser2->address}}</td>
                                 <td class="text-center"> {{$listuser2->numeroexterior}}</td>
                                <td class="text-center"> {{$listuser2->colonia}}</td>

                                 <td class="text-center"> {{$listuser2->codigopostal}}</td>
                                <td class="text-center"> {{ $listuser2->city }}</td>
                            
                                <td class="text-center"> {{ $listuser2->email }}</td>
                                <td class="text-center"> {{ $listuser2->phone }}</td>
                               
                                
                                <td class="text-center">
                                 <input type="text-center" name="quantits" value="0" style="display: none;">
  <button style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;" type="submit">Bajar</button>
                                 </td>
                                
                              </form>
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
                </div>


               
              </div>

        

</div>


                </div>
           
                    <br>
                    </div>
            
                


                   
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
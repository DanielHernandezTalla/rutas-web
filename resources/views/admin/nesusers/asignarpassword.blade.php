@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
.toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;
  }

  .alert.alert-success {
    background-color: #3faa58 !important;
    color: #ffffff;
  }

  .alert.alert-danger {
    background-color: #db1a0c !important;
    color: #ffffff;
  }
}
</style>

<script>
  $("document").ready(function(){
    setTimeout(function(){
       $("div.alert").remove();
    }, 3000 );
});
</script>

<div class="main main-raised">
  <div class="container">
    @if (session('alert'))
        <div class="alert alert-success">
            {{ session('alert') }}
        </div>  
    @endif

    @if (session('alertError'))
      <div class="alert alert-danger">
          {{ session('alertError') }}
      </div>  
    @endif
  </div>
  <div class="profile-content">
    <div class="container" style="width: 100%;">
      <div class="row">
          <div class="tabpanel" style="margin: 3rem;">
                <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                    <h2 class="title text-center">Añadir contraseña a usuarios</h2>
                      <table class="table" style="width: 100%; font-size: 11px; margin: auto;">
                        <thead style="background-color: #ff7514; color:white">
                        <tr>
                        <th class="text-center">ID</th>
                        <th class="text-center">Nombre</th>
                        <th class="text-center">Username</th>
                        <th class="text-center">Email</th>
                        <th class="text-center">Ciudad</th>
                        <th class="text-center">Dirección</th>
                        <th class="text-center">Teléfono</th>
                        <th class="text-center">Password</th>
                        <th class="text-center">Cambiar Contraseña</th>
                        <th class="text-center">Cambiar Usuario</th>
                        </tr>
                      </thead>
                    
                      <tbody>
                      @foreach ($listusers as $listuser)
                      <tr>
                      <td class="text-center">{{$listuser->id}} </td>
                      <td class="text-center"> {{$listuser->name}}</td>
                      <td class="text-center"> {{ $listuser->username }}</td>
                      <td class="text-center"> {{ $listuser->email==null?'No disponible':$listuser->email }}</td>
                      <td class="text-center"> {{$listuser->city}}</td>
                      <td class="text-center"> {{$listuser->address}}</td>
                      <td class="text-center"> {{$listuser->phone==null?'No disponible':$listuser->phone}}</td>
                      @if($listuser->password == "")
                      <td class="text-center" style="color:red;"><span>No tiene contraseña</span></td>
                      @else
                      <td class="text-center">****</td>
                      @endif
                      <td>
                        <button class="btn btn-secondary btn-sm" data-toggle="modal" data-id="{{ $listuser->id }}" data-target="#editModal{{$listuser->id}}">
                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <strong>Cambiar</strong>
                        </button>
                        </td>
                        <td>
                        <button class="btn btn-info btn-sm" data-toggle="modal" data-id="{{ $listuser->id }}" data-target="#editUsername{{$listuser->id}}">
                              <i class="fa fa-pencil-square-o" aria-hidden="true"></i> <strong>Cambiar</strong>
                        </button>
                        </td>
                      </tr>

                <!-- contraseña modal -->
                <div id="editModal{{$listuser->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">×</button>
                  <h4 class="modal-title">Cambiar contraseña </h4>
                  <br>
                    </div>
               <div class="modal-body">
               <form action="/adminpedidos/changePassword/{{$listuser->id}}" method="POST">
               {{csrf_field()}}
               <div class="mb-3">
        
          <div class="input-group mb-3">
          <input type="password"  name="password" placeholder="contraseña">
           
          </div>
        </div>
        <div class="mb-3">
        
          <div class="input-group mb-3">
            <input type="password"  id="Password2" name="Password2" placeholder="Confirmar Contraseña"  >
            
          </div>
        </div>
       
      </div>
                  <div class="modal-footer">
                     <button type="submit" class="btn btn-default" >Guardar</button>
                  </div> 
              </form>
               </div>
            </div>
         </div>
          <!-- usuario modal -->
          <div id="editUsername{{$listuser->id}}" class="modal fade" role="dialog">
                    <div class="modal-dialog">
                    <!-- Modal content-->
                    <div class="modal-content">
                    <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">×</button>
                  <h4 class="modal-title">Cambiar Usuario </h4>
                  <br>
                    </div>
               <div class="modal-body">
           <form action="/adminpedidos/changeUsername/{{$listuser->id}}" method="POST">
           {{csrf_field()}}
               <div class="mb-3">
        
          <div class="input-group mb-3">
          <input type="text-center"  name="username" placeholder="Nombre del usuario">
           
          </div>
        </div>
        <div class="mb-3">
        
          <div class="input-group mb-3">
            <input type="text"  id="username2" name="username2" placeholder="Confirmar Username" aria-label="Username" >
            
          </div>
        </div>
       
      </div>
                  <div class="modal-footer">
                     <button type="submit" class="btn btn-default" >Guardar</button>
                  </div>
                  </form>
               </div>
            </div>
         </div>
         @endforeach
         </tbody>
         </table> 
    </div>
</div>
</div>
</div>
</div>

                      
                 

@include('includes.footer')
@endsection
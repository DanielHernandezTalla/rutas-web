@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
	#credito{
		height: 15px; font-size: 12px; 
		margin-top: -5%; color: #ff7514; 
		background-color: #fff; 
		border-color: #fff; 
		position: relative; 
		-webkit-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		-moz-box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
		box-shadow: 0px 0px 5px 2px rgba(255,255,255,1);
	}
	#credito::before{
		background-color: #fff;
	}
	.title{
		font-size: 20px;
	}
     .toplink .material-icons {
 position: absolute;
 left:  5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.pack{
  position: absolute;
 right:   5px;
 top: 5px;
 outline: 0; 
}
</style>

<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">



      
  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>


        <div class="section text-center">
        




            <div class="team">
                <div class="row">
               <div class="tabpanel">

    <!-- Nav tabs -->
   
<ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item waves-effect waves-light nav-link active">
                  <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Pedido</a>
            
                </li>
              

              </ul>

    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">
			 <table class="table">
                <thead>
                    <tr style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; ">
              
                        <th class="text-center">ID PEDIDO</th>
  
                    </tr>
                </thead>

                <tbody>
                	
                    @foreach ($unaut as $unauts)
             
            

                   <tr >
                   
                        
                        <td style="text-align: center;">
                            {{ $unauts->id }}
                        </td>
                        

                    </tr>
               @endforeach
                  
                </tbody>
            </table>
              
            <br>

          

  <form id="myform" action="{{ url('/picking/regresarEjecutivo/'.$unauts->id) }}" method="POST" enctype="multipart/form-data"> 
     {{csrf_field()}}

     <input type="hidden"  id="interfazdo" name=" " value="{{ $unauts->interfazado }}">
     <textarea class="form-control" id="MotivoRegreso" name="MotivoRegreso" rows="3"></textarea>
   <button style="text-transform: lowercase; font-size: 20px; " class="btn btn-primary btn-round" type="submit">
                        Regresar Pedido al Ejecutivo
                    </button>
                  </form>

              </div>                    
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>



@include('includes.footer')
@endsection

@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

    <style type="text/css">
        #credito {
            height: 15px;
            font-size: 12px;
            margin-top: -5%;
            color: #ff7514;
            background-color: #fff;
            border-color: #fff;
            position: relative;
            -webkit-box-shadow: 0px 0px 5px 2px rgba(255, 255, 255, 1);
            -moz-box-shadow: 0px 0px 5px 2px rgba(255, 255, 255, 1);
            box-shadow: 0px 0px 5px 2px rgba(255, 255, 255, 1);
        }

        #credito::before {
            background-color: #fff;
        }

        .title {
            font-size: 20px;
        }

        .toplink .material-icons {
            position: absolute;
            left: 5px;
            top: 5px;
            outline: 0;
            font-size: 55px;

        }

        .toplink .material-icons:hover {
            color: #000;
        }

        .pack {
            position: absolute;
            right: 5px;
            top: 5px;
            outline: 0;
        }
    </style>

    <div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
    </div>

    <div class="main main-raised">
        <div class="container">




            <a href="{{ url('/') }}" class="toplink"><i class="material-icons">reply</i> </a>
            {{-- 
            <div class="pack">
                <input type="text-center" name="packinglist" placeholder="packinglist" value="">
                <button
                    style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;"
                    type="submit">Buscar</button>
            </div> --}}

            <div class="section text-center">
                @foreach ($unaut as $unauts)
                @endforeach
                <h2 class="title">Carrito de compras: #{{ $unauts->id }}</h2>
                <div class="team">
                    <div class="row">
                        <div class="tabpanel">
                            <!-- Nav tabs -->

                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item waves-effect waves-light nav-link active">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="false">Productos</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" id="myTabContent">
                                <div class=" tab-pane fade in active" id="home" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <table class="table">
                                        <thead>
                                            <tr
                                                style=" border-bottom-style: solid;  border-bottom-color: #CFCFCF;  border-bottom-width: 1px; ">

                                                <th class="text-center">Nombre</th>
                                                <th class="text-center">Precio</th>
                                                <th class="text-center">Cantidad</th>
                                                <th class="text-center">UM</th>
                                                <th class="text-center">Iva</th>
                                                <th class="text-center">SubTotal</th>
                                                <th class="text-center">Packing</th>
                                                <th class="text-center">Cantidad Packing</th>


                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($unaut as $unauts)
                                                <tr>
                                                    <form
                                                        action="{{ url('/picking/modificar/' . $unauts->id . '/' . $unauts->idmodificacion) }}"
                                                        method="POST" enctype="multipart/form-data">
                                                        {{ csrf_field() }}
                                                        <td style="text-align: left;">
                                                            {{ $unauts->name }}
                                                        </td>
                                                        <td>
                                                            ${{ $unauts->price }}
                                                        </td>
                                                        <td>
                                                            <input style="display: none;" type="text-center" name="price"
                                                                value="{{ $unauts->price }}">
                                                            <input style="display: none;" type="text-center" name="iva"
                                                                value="{{ $unauts->iva }}">
                                                            ${{ $unauts->quantity }}
                                                        </td>

                                                        <td>
                                                            {{ $unauts->uom }}
                                                        </td>
                                                        <td>
                                                            {{ $unauts->iva }}
                                                        </td>

                                                        <td>
                                                            ${{ $unauts->importe }}
                                                        </td>
                                                        <td>
                                                            <input type="hidden" name="packing"
                                                                value="{{ $unauts->packing }}">
                                                            {{ $unauts->packing }}
                                                        </td>
                                                        <td>
                                                            <input type="text-center" name="quantitypacking"
                                                                value="{{ $unauts->cantidad }}">
                                                        </td>
                                                        <td>
                                                            <button
                                                                style="border-style: none; background-color: #ff7514; color: #fff; font-size: 16px; font-weight: 500; border-radius: 10%;"
                                                                type="submit">guardar</button>
                                                        </td>
                                                    </form>
                                                </tr>
                                            @endforeach

                                        </tbody>
                                    </table>

                                    <br>
                                    <h3 class="title">Importe total: ${{ $unauts->importe_total }}</h3>

                                    @if (Auth::check())
                                        {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
                                        <input type="hidden" name="body" value="pedido preparado">

                                        {{ Form::hidden('cart_id', $unauts->id) }}
                                        <button style="text-transform: lowercase; font-size: 20px; "
                                            class="btn btn-primary btn-round" type="submit">
                                            Notificar cliente
                                        </button>
                                        {{ Form::close() }}
                                    @endif

                                    <form id="myform" action="{{ url('/picking/cambio/' . $unauts->id) }}"
                                        method="POST" enctype="multipart/form-data">
                                        {{ csrf_field() }}

                                        <input type="hidden" name="packingss" value="{{ $unauts->packing }}">
                                        <button style="text-transform: lowercase; font-size: 20px; "
                                            class="btn btn-primary btn-round" type="submit">
                                            Guardar y enviar
                                        </button>
                                    </form>

                                </div>
                            </div>
                        </div>
                    </div>

                </div>


            </div>
        </div>
    </div>



    @include('includes.footer')
@endsection

@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')


<script>
    // Espera 3 segundos (3000 milisegundos) y luego oculta la notificación
    setTimeout(function() {
        document.getElementById('notification2').style.display = 'none';
    }, 3000);
</script>


    <style type="text/css">
        .toplink .material-icons {
            position: absolute;
            left: 5px;
            top: 5px;
            outline: 0;
            font-size: 55px;

        }

        .toplink .material-icons:hover {
            color: #000;
        }

        @media (max-width: 540px) {

            #selectciudad {
                display: none;
            }

            #shoppingcar {
                display: none;
            }
        }
    </style>

    <div class="main main-raised">
        <div class="container">
            <a href="/picking/reportepedidos" target="_blank" title="Reporte Pedidos APP" class="toplink"><i
                    class="material-icons">print</i> </a>

            <div align="right">
                <button class="btn btn-primary btn-round" onclick="javascript:window.location.reload();">
                    <i class="material-icons">autorenew</i> Actualizar pedidos
                </button>
            </div>

            <div class="section text-center">
                <h2 class="title">Almacen(pedidos)</h2>
                
                @if (session('notification2'))
                    <div id="notification2"  style="background-color: #F41C1C;" class="alert alert-success2">
                        {{ session('notification2') }}
                    </div>
                @endif
                @if (session('notification'))
                    <div id="notification2"  style="background-color: #33D872;" class="alert alert-success">
                        {{ session('notification') }}
                    </div>
                @endif

                <div class="team">
                    <div class="row">
                        <div class="tabpanel">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item waves-effect waves-light nav-link active">
                                    <a class="nav-link" id="home-tab" data-toggle="tab" href="#home" role="tab"
                                        aria-controls="home" aria-selected="false">Pendientes</a>
                                    <!--@foreach ($contar as $conta)
                                        <input type="team" name="polla" value="{{ $conta->cart_count }}">
                                        @endforeach -->
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content" id="myTabContent">
                                <div class=" tab-pane fade in active" id="home" role="tabpanel"
                                    aria-labelledby="home-tab">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Estado</th>
                                                <th class="text-center">Total</th>
                                                <th class="text-center">Fecha</th>
                                                <th class="text-center">Hora</th>
                                                <th class="text-center">Nombre cliente</th>
                                                <th class="text-center">Telefono</th>
                                                <th class="text-center">Direccion</th>
                                                <th class="text-center">Ciudad</th>
                                                <th class="text-center">Ruta</th>
                                                <th class="text-center">Opciones Modificar</th>

                                            </tr>
                                        </thead>
                                        <tbody>
                                            <!--@foreach ($pedidos as $pedido)-->
                                            <tr>
                                                <form id="myform" action="{{ url('/picking/cambio/' . $pedido->id) }}"
                                                    method="POST" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                                    <td class="text-center"> {{ $pedido->id }}</td>
                                                    <td class="text-center"> {{ $pedido->status }}</td>
                                                    <td class="text-center"> ${{ $pedido->importe_total }}</td>
                                                    <td class="text-center"> {{ $pedido->order_date }}</td>
                                                    <td class="text-center"> {{ $pedido->order_datetime }}</td>
                                                    <td class="text-center"> {{ $pedido->name }}</td>
                                                    <td class="text-center"> {{ $pedido->phone }}</td>
                                                    <td class="text-center"> {{ $pedido->address }}</td>
                                                    <td class="text-center"> {{ $pedido->city }}</td>
                                                    <td class="text-center"> {{ $pedido->descentro }}</td>
                                                    <td class="text-center" class="td-actions">
                                                        <a style="margin-top: -0%; font-size: 25px;"
                                                            href="/picking/modificar/{{ $pedido->id }}/{{ $pedido->centro }}"
                                                            rel="tooltip" title="Modificar"
                                                            class="btn btn-success btn-simple btn-xs">
                                                            <i style="font-size: 25px;" class="fa fa-edit"></i>
                                                        </a>
                                                    </td>

                                                    <td class="text-center" class="td-actions">
                                                        <a style="margin-top: -0%; font-size: 25px;"
                                                            href="/picking/Regresar/{{ $pedido->id }}/{{ $pedido->centro }}/{{ $pedido->interfazado }}"
                                                            rel="tooltip" title="Regresar"
                                                            class="btn btn-danger btn-simple btn-xs">
                                                            <i style="font-size: 25px;" class="fa fa-undo"></i>
                                                        </a>
                                                    </td>
                                                </form>
                                            </tr>
                                            <!--@endforeach -->
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @foreach ($pedidos as $pedido)
        <div style="z-index: 1000000;" class="modal fade" id="modalAddToCart" role="dialog" aria-labelledby="myModalLabel"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title" id="myModalLabel">Seleccione el metodo de pago</h4>
                        <br>
                        <br>
                        @foreach ($contar as $conta)
                        @endforeach
                        @if ($conta->cart_count > 0)
                            <table style="width: 100%; height: 100%;">
                                <tr>
                                    <td style="text-align: center;">
                                        <form method="post" id="myform"
                                            action="{{ url('/rutero/pagoRutas/' . $pedido->id) }}">
                                            {{ csrf_field() }}
                                            <a href="#" onclick="document.getElementById('myform').submit()"
                                                type="submit" class="toplink"><i style="font-size: 80px;"
                                                    rel="tooltip" title="Efectivo"
                                                    class="material-icons">attach_money</i> </a>
                                        </form>
                                    </td>
                                    <td style="text-align: center;"><a href="#" class="toplink"><i
                                                style="font-size: 80px;" rel="tooltip" title="Tarjeta de credito"
                                                class="material-icons">credit_card</i> </a>

                                        @if (Auth::check())
                                            {{ Form::open(['route' => ['notificaciones.store'], 'method' => 'POST']) }}
                                            <input type="hidden" name="body" value="confirmar pedido">
                                            {{ Form::hidden('cart_id', $pedido->id) }}
                                            <button class="btn btn-primary btn-round" type="submit">
                                                <i class="material-icons">notification_important</i> Tarjeta credito y/o
                                                debito
                                            </button>
                                            {{ Form::close() }}
                                        @endif
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: center; color: #8C8C8C; font-size: 20px; font-weight: 500;">
                                        Efectivo</td>
                                    <td style="text-align: center; color: #8C8C8C; font-size: 20px; font-weight: 500;">
                                        Tarjeta</td>
                                </tr>
                            </table>
                        @endif
                        <br>
                        <br>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


    @include('includes.footer')
@endsection

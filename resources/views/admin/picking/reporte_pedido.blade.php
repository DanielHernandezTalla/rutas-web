
@section('title', 'Listado de productos')

@section('body-class', 'product-page')

<style type="text/css">
  .section {
  text-align: center;
  }
.section .title{
  color: #4F4F4F;
}

.section .table{
  width: 100%;
  text-align: center;
}
.section .table td{
  border-bottom-style: solid;
  border-bottom-color: #D5D5D5;
  color: #6E6E6E;
  font-size: 14px;
}
.section .table th{
  color: #7C7C7C;
}
.alert-success2{
  background-color: #FC1313;
}
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 45px; 
 color: #4F4F4F;
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons" /> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/">

<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

   




        <div class="section">
           <a href="/picking/reportepedido"  title="Refrescar" class="toplink"><i class="material-icons">autorenew</i> </a>
    
            <h2 class="title">Reporte pedidos por fecha</h2>

@if (session('notification2'))
                        <div class="alert alert-success2">
                            {{ session('notification2') }}
                        </div>         
                    @endif 


            <div class="team">
                <div class="row">
               <div class="tabpanel">

    <!-- Nav tabs -->
   


    <!-- Tab panes -->
  <div class="tab-content" id="myTabContent">
                <div class=" tab-pane fade in active" id="home" role="tabpanel" aria-labelledby="home-tab">

        <table class="table">
            <div>
            <form method="get" action="{{ url('/picking/reportepedidos/search') }}">

           <label>Fecha inicial</label>
           <input type="date" name="fechaI" value="">
           <label>Fecha final</label>
           <input type="date" name="fechaF" value="">
          
           <button type="submit">buscar</button>

       </form>
</div>
<br>
<br>
                        <thead >
                            <tr>
                              <th class="text-center">#</th>
                              <th class="text-center">Nombre</th>
                              <th class="text-center">productos</th>
                              <th class="text-center">cantidad</th>
                               
                                
                                                             
                               
                            </tr>
                        </thead>
                        <tbody>
                    @foreach ($pedidos as $pedido) 
                            <tr>
                                <td class="text-center"> {{ $pedido->id }}</td>
                                 <td class="text-center"> {{ $pedido->name }}</td>
                                 <td class="text-center"> </td>
                                 <td class="text-center"> {{ $pedido->quantity }}</td>

                               
                               
                                
                    
                            </tr>
                           @endforeach 
                        </tbody>
                    </table>
              </div>



   
                     
                </div>
            </div>
        </div>

    </div>

    
    </div>
  </div>
</div>





@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Asiganar tiempo de entrega</h2>
            	<br>
                    <br>
                  
                </div>
           <table class="table">
           
             
                        <tbody >
                        
                            <tr >
                                 
                                <td class="text-center"> <select style="margin-top: -25px;" name="tipocliente" id="descripcion" class="form-control">
                                    <option selected value="">Seleccione un tipo de usario</option> 
                                  <option  value="Nuevo">Nuevo</option>
                                  <option value="Existente">Existente</option>
                                </select>  </td>
                                <td class="text-center">   <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Codigo de la organizacion" name="organizacion" required autofocus> </td>
                                <td class="text-center"> <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Días" name="dias" required autofocus></td>
                                <td class="text-center">  <input style="margin-top: -25px;" type="text"  class="form-control"  placeholder="Horas" name="horas" required autofocus></td>
                                <td class="text-center">  <a   rel="tooltip" title="Agregar" class="">
<i style="font-size: 25px; font-weight: bold;" class="material-icons">add</i>
</a></td>
                                
                             

                              
                            </tr>
                         
                        </tbody>
                    </table>
                    <br>
                    </div>
             <table class="table">
           
             
                        <thead>
                            <tr>
                              <th class="text-center">id</th>
                                <th class="text-center">Tipo cliente</th>
                                <th class="text-center">Organizacion</th>
                                <th class="text-center">días</th>
                                <th class="text-center">Horas</th>
                                <th class="text-center">Editar</th>
                                
                                
                    
                                
                            </tr>
                        </thead>
                        <tbody>
                        
                            <tr>
                                 @foreach ($listiempos as $listiempo)
                                <td class="text-center"> {{ $listiempo->id }}</td>
                                <td class="text-center"> {{ $listiempo->tipocliente }}</td>
                                <td class="text-center"> {{ $listiempo->organization_id }}</td>
                                <td class="text-center"> {{ $listiempo->dias }}</td>
                                <td class="text-center"> {{ $listiempo->horas }}</td>
                                
                             

                                <td class="text-center"> <a  href="{{ url('/admin/promo/') }}" rel="tooltip" title="Agregar" class="">
<i style="font-size: 25px; font-weight: bold;" class="material-icons">add</i>
</a></td>
                                
                              
                            </tr>
                           @endforeach
                        </tbody>
                    </table>
				          <br>



				           
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection
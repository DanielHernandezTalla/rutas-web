@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
  
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.col-sm-2{
    width: 135px;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
</div>

<div class="main main-raised">
    <div class="container">

        <div class="section">
              <a href="{{ url()->previous() }}" class="toplink"><i class="material-icons">reply</i> </a>
            <h2 class="title text-center">Agrear centro de venta</h2>



            

           
               <form method="post" action="{{ url('/admin/insertcentroventas') }}">
  {!! csrf_field() !!}
                <div class="row">

                <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">Descripcion</label>
                            <input  type="text" class="form-control" name="Descripcion" >
                  
                        </div>
                    </div>

                    <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">tipo de pedido</label>
                            <input  type="text" class="form-control" name="tipopedido" >
                        </div>
                    </div>

                    <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <label class="control-label">Ciudad</label>
                             <select style="border: transparent;" name="Ciudad" >
                       
                                    @foreach($citys as $id => $name)
                                        <option value="{{ $id }}">
                                            {{ $name }}
                                        </option>
                                    @endforeach
                                </select> 
                        </div>
                    </div>

                   <div class="col-sm-3">
                        <div class="form-group label-floating">
                            <label class="control-label">Catalogo de precio</label>
                             <select style="border: transparent;" name="CatPrecio">
                       
                                    @foreach($catPrecio as $id => $descripcion)
                                        <option value="{{ $id }}">
                                            {{ $descripcion }}
                                        </option>
                                    @endforeach
                                </select> 
                        </div>
                    </div>


               <div class="col-sm-2" >
                        <div class="form-group label-floating">
                            <label class="control-label">Provisional</label>
                            <input  type="text" class="form-control" name="Provisional" >
                        </div>
                    </div>


                  
                    
<div class="col-sm-2">
                 <button class="btn btn-primary">Guardar</button> 
             
            
              
   </div>
</form>
  

             
           
                </div>

            
               
<br>

           

        </div>

        
    </div>
</div>

  
@include('includes.footer')
@endsection
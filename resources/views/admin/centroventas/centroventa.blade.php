@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">

    .medio
        overflow-y: scroll;
        height: 600px;

    }
.precio{
    text-align: center;
    width: 35%;
    border: hidden;
}
 .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
.col-sm-2{
    width: 135px;
}
.btn-estadio{
    background-color: #ff7514;
    color: #FFFFFF;
}
.toplink2 .material-icons {
 position: absolute;
 right: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink2 .material-icons:hover {
  color: #000;
}

</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
             <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
            <h2 class="title text-center">Centros de ventas</h2>    
            <a href="{{ url('/admin/centrodeventas/add')}}" class="toplink2"> <i class="material-icons">library_add</i> </a>
            


                    <div class="medio">
           <table class="table">
                                <thead>
                                    <tr>
                                        <th class="text-center">Codigo</th>
                                        <th class="text-center">Descripcion</th>
                                        <th class="text-center">Tipo de pedido</th>
                                        <th class="text-center">Ciudad</th>
                                        <th class="text-center">Tipo de precio</th>
                                        <th class="text-center">Provisional</th>
                                       
                                        
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                 @foreach ($center as $centers)
                                    <tr>
                                        <td class="text-center"><a href="{{ url('/admin/centrodeventas/'.$centers->code.'/edit') }}">{{ $centers->code }}</a></td>
                                        <td class="text-center"> {{ $centers->descrip }}</td>
                                        <td class="text-center"> {{ $centers->tipope }}</td>
                                        <td class="text-center"> {{ $centers->nombre }}</td>
                                        <td class="text-center"> {{ $centers->precio }}</td>
                                        <td class="text-center"> {{ $centers->provi }}</td>
                                        
                                        
                                        
                                    </tr>
                                 @endforeach
                                </tbody>
                            </table>

                         
       </div>   
       <br>

         <div class="text-center">

        
    
                    
                   
               

    
      </div>
        </div>

    </div>
</div>


@include('includes.footer')
@endsection

@extends('layouts.app')

@section('body-class', 'signup-page')

@section('content')



<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
<style type="text/css">
   
    .message{

       color: red;
        font-size: 18px;
        font-weight: 400; 
    }
</style>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
                <div class="card card-signup">

                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <form class="form" method="POST" action="{{ route('register') }}">
                        {{ csrf_field() }}

                        <div class="header header-primary text-center">
                            <h4>Registro</h4>
                        </div>
                        <p  id="tiitulo" class="text-divider">Completa tus datos</p>
                         <div id="uname_response" ></div>

                        <div class="content">

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <input type="text"  class="form-control" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Nombre" name="name"
                                       value="{{ old('name', $name) }}" required autofocus>
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">fingerprint</i>
                                </span>
                                <input type="text" class="form-control"  placeholder="Username" name="username" id="username"  onfocus="myFunction()" onfocusout="myFunction2()"
                                       value="{{ old('username') }}" required>   
                               <h9 hidden id="mensaje" class="mensaje"></h9>                                                                                     
                            </div> 
 
 
      

   

<script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search2.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('#mensaje').html(data.table_data);
 
   }
  })
 }

 $(document).on('focusout', '#username', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>




                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <input id="email" type="email" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Correo electrónico" class="form-control" name="email" value="{{ old('email', $email) }}"  onblur = "checkmain (this.value)">
                                 <h9 hidden id="mensaje3" class="mensaje"></h9>
                            </div>

 

 <script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search3.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('#mensaje3').html(data.table_data);
   
   }
  })
 }

 $(document).on('focusout', '#email', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">phone</i>
                                </span>
                                <input id="phone" type="phone" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Teléfono" class="form-control" name="phone" value="{{ old('phone') }}" required>
                            </div>   



                            
                            {{Form::open(array('url'=>'/register', 'files'=>true))}}


<label>*Algunas ciudades no se encuentran disponibles de momento</label>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 
                                <select name="city" id="citys" class="form-control" >
                                    <option selected>Seleciona una ciudad</option>
                                    @foreach($citys as $id => $city)
                                        <option value="{{ $city }}">
                                            {{ $city }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>

     

                            <label for="">*Algunos codigos postales no se encuentran disponibles de momento</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mark_email_unread</i>
                                </span>
                                 
                                <select name="codigoPostal" id="colonias" class="form-control">
   <option selected>Seleciona un codigo postal</option>
                                  
                                </select>
                            </div>
                       

<label for="">*Algunas colonias no se encuentran disponibles de momento</label>
                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>
                                 
                                <select name="colonia" id="codigoPostales" class="form-control">

                                  
                                </select>
                            </div>
                       



                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">drag_handle</i>
                                </span>
                                <input id="address" type="text" onfocus="myFunction()" onfocusout="myFunction2()" placeholder="Calle" class="form-control" name="address" value="{{ old('address') }}" required>
                            </div> 


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input id="numeroExterior" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Numero Exterior" class="form-control" name="numeroExterior" value="{{ old('numeroExterior') }}" required>
                            </div>  

                              <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input id="numeroInterior" onfocus="myFunction()" onfocusout="myFunction2()" type="text" placeholder="Numero Interior" class="form-control" name="numeroInterior" value="{{ old('numeroInterior') }}">
                            </div>  
                          
                          

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input placeholder="Contraseña" onfocus="myFunction()" onfocusout="myFunction2()" id="password" type="password" class="form-control" name="password" required />
                            </div>

                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input placeholder="Confirmar contraseña" onfocus="myFunction()" onfocusout="myFunction2()" type="password" class="form-control" name="password_confirmation" required />
                            </div>

                             <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">lock_outline</i>
                                </span>
                                <input value="apprutas" onfocus="myFunction()" onfocusout="myFunction2()" type="text" class="form-control" name="lugar" required />
                            </div>
                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">person_add_alt_1</i>
                                </span>
                                 
                                <select name="city" id="citys" class="form-control" >
                                    <option selected value="0">Seleciona un rol de usuario</option>
                                  
                                        <option value="admin">Administrador</option>
                                          <option value="gester">Productos y categorias</option>
                                            <option value="useradmin">Usuarios</option>
                                              <option value="pedidos">Pedidos</option>
                                               

                                </select>
                            </div>
                        </div>
                        <div class="footer text-center">
                            <button type="submit" class="btn btn-simple btn-primary btn-lg">Confirmar registro</a>
                        </div>
                        <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                            Forgot Your Password? -->
                        </a>

                    </form>
                    {{Form::close()}}
                </div>
            </div>
        </div>
    </div>
    
<script>
 $('#citys').on('change',function(e){
    console.log(e);

    var cit_id = e.target.value;

    //ajax
    $.get('/ajax-colonias?cit_id=' + cit_id, function(data){
        $('#colonias').empty();

        $.each(data, function(index, coloniaObj){

$('#colonias').append('<option value="'+coloniaObj.codigopostal+'">'+coloniaObj.codigopostal+'</option>');



    
        });
    });

 });
 
</script>
<script>
 $('#colonias').on('change',function(e){
    console.log(e);

    var col_id = e.target.value;

    //ajax
    $.get('/ajax-codigoPostales?col_id=' + col_id, function(data){
        $('#codigoPostales').empty();

        $.each(data, function(index, codigoPostalesObj){

$('#codigoPostales').append('<option value="'+codigoPostalesObj.colonia+'">'+codigoPostalesObj.colonia+'</option>');


    
        });
    });

 });
 
</script>
</div>

@include('includes.footer')
@endsection


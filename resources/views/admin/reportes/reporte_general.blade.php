
@extends('layouts.app')
@section('content')

<!--<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" crossorigin="anonymous">-->


<!--INICIO Styles-->
<style type="text/css">
.main-raised {
    margin: 115px 125px 33px !important;
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}

.form{
    margin: 115px 125px 33px !important;
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}
</style>

<script type="text/javascript">

function buscar(){
    let plaza=$("#plaza").val();
    let fechaDesde=$("#fechadesde").val();
    let fechaHasta=$("#fechahasta").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: 'https://apprutas.kowi.com.mx/api/getReporteGeneral/',
        data: {'fechaDesde':fechaDesde,'fechaHasta': fechaHasta},
        success: function(data){
           $("#fechadesde").val("");
           $("#fechahasta").val("");
           alert(data)
        }
    }); 

}


   
</script>

<!--FIN Styles-->
<div class="form">
    <div class="container">
        <div class="section text-center">
            <h2 class="title">Reporte general {{$custom}}</h2>
        </div>
        <div class="row">
            {{ Form::open(['route' => 'searchMonto', 'method' => 'GET']) }}
            <table class="table">
                    <tr>
                        <td  class="text-center">{{ Form::date('fechadesde', $fechadesde, ['class' => 'form-control', 'placeholder' => 'fechadesde']) }}</td>
                        <td  class="text-center">{{ Form::date('fechahasta', $fechahasta, ['class' => 'form-control', 'placeholder' => 'fechahasta']) }}</td>
                        <td  class="text-center"> <button class="btn btn-primary btn-round" type="submit">
                            <i class="material-icons">search</i> Buscar
                        </button></td>
                    </tr>
            </table>
            {{ Form::close() }}

            <div class="row">
                <div class="col-lg-12">
                    <table class="table table-striped">
                        <thead style="background-color: #ff7514; color:white">
                            <tr>
                                <th class="text-center">#</th>
                                <th class="text-center">Sucursal</th>
                                <th class="text-center">Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($totalBySucursal as $key=>$item)
                            <tr>
                                <td  class="text-center">{{ $key+1 }}</td>
                                <td  class="text-center">{{ $item->organization_name }}</td>
                                <td  class="text-center">$ {{ $item->total}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

@include('includes.footer')
@endsection
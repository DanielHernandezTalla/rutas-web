@extends('layouts.app')

@section('title', 'Listado de productos')

@section('body-class', 'product-page')

@section('content')

<!--INICIO Styles-->
<style type="text/css">
.nav-tabs>li.active>a,
.nav-tabs>li.active>a:hover,
.nav-tabs>li.active>a:focus {
    background-color: #ff7514;
    transition: background-color .1s .2s;
    color: white !important;
}


.main-raised {
    margin: 115px 10px 33px !important;
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}

.form{
    margin: 115px 10px 33px !important;
    border-radius: 6px;
    box-shadow: 0 16px 24px 2px rgb(0 0 0 / 14%), 0 6px 30px 5px rgb(0 0 0 / 12%), 0 8px 10px -5px rgb(0 0 0 / 20%);
}
</style>

<script type="text/javascript">
    
    function setFecha() {
        document.getElementById("formChange").action = "./ReportesPedidosFactura";
        document.getElementById("formChange").submit();
    }
</script>

<!--FIN Styles-->
<div class="form">
    <div class="container">
    <div align="right">
        	  <button class="btn btn-primary btn-round" onclick="window.location='{{ url("adminpedidos/ReportesPedidos") }}'">
                        <i class="material-icons">autorenew</i> Actualizar pedidos
                    </button>
        </div>

        <div class="section text-center">
            <h2 class="title">Reporte de pedidos</h2>
            <div class="team">
                <div class="row">
                    <div class="tabpanel">
                        <ul class="nav nav-tabs">
                            <li class="active"><a data-toggle="tab" href="#facturapedidos">Pedidos Facturados</a></li>
                            <li ><a data-toggle="tab" href="#diario">Pedidos diarios</a></li>
                            <li><a data-toggle="tab" href="#pedidos30">Pedidos +30</a></li>
                            <li><a data-toggle="tab" href="#pedidos15">Pedidos +15</a></li>
                            <li><a data-toggle="tab" href="#pedidos7">Pedidos +7</a></li>
                            <li><a data-toggle="tab" href="#productospedidos">Productos +Pedidos</a></li>
                            <li><a data-toggle="tab" href="#clientespedidos">Clientes +Pedidos</a></li>
                            
                        </ul>
                        <div class="tab-content">
                            <div id="diario" class="tab-pane fade ">
                                <h4><strong>Listado de pedidos del dia de hoy</strong></h4>
                                <table class="table table-striped">
                                    <thead style="background-color: #ff7514; color:white">
                                        <tr>
                                            <th class="text-center">#</th>
                                            <th class="text-center">Cliente</th>
                                            <th class="text-center">Producto</th>
                                            <th class="text-center">Importe</th>
                                            <th class="text-center">Fecha pedido</th>
                                            <th class="text-center">Fecha entrega</th>
                                            <th class="text-center">Estatus</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach ($reportByday as $key=>$item)
                                        <tr>
                                            <td>{{ $key+1 }}</td>
                                            <td>{{ $item->cliente }}</td>
                                            <td>{{ $item->producto }}</td>
                                            <td>$ {{ $item->importe }}</td>
                                            <td>{{ $item->fecha_pedido }}</td>
                                            <td>{{ $item->fecha_entrega}}</td>
                                            <td>{{ $item->status}}</td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <div id="pedidos30" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4><strong>Listado de clientes que realizaron pedidos los ultimos 30
                                                dias</strong></h4>
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Teléfono</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportBy30 as $key=>$item)
                                                <tr>

                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->phone==null?'ND':$item->phone }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <div id="pedidos15" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4><strong>Listado de clientes que realizaron pedidos los ultimos 15
                                                dias</strong></h4>
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Teléfono</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportBy15 as $key=>$item)
                                                <tr>

                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->phone==null?'ND':$item->phone }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6">
                                        <h4><strong>Listado de clientes que no realizaron pedidos los ultimos 15
                                                dias</strong></h4>
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Teléfono</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportBy15SP as $key=>$item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->phone==null?'ND':$item->phone }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="pedidos7" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-lg-6">
                                        <h4><strong>Listado de clientes que realizaron pedidos los ultimos 7
                                                dias</strong></h4>
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Teléfono</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportBy7 as $key=>$item)
                                                <tr>

                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->phone==null?'ND':$item->phone }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                    <div class="col-lg-6">
                                        <h4><strong>Listado de clientes que no realizaron pedidos los ultimos 7
                                                dias</strong></h4>
                                        <table class="table">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Teléfono</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportBy7SP as $key=>$item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->phone==null?'ND':$item->phone }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="productospedidos" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-lg-12">
                                        <h4><strong>Listado de productos mas pedidos los ultimos 30
                                                dias</strong></h4>
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Producto</th>
                                                    <th class="text-center">Precio</th>
                                                    <th class="text-center">Total Kg</th>
                                                    <th class="text-center">Total $</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportProducts as $key=>$item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->producto }}</td>
                                                    <td>$ {{ $item->precio }}</td>
                                                    <td> {{ $item->totalKg }} Kg</td>
                                                    <td>$ {{ $item->totalFeria }}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="clientespedidos" class="tab-pane fade">
                            <div class="row">
                                    <div class="col-lg-12">
                                        <h4><strong>Listado de clientes con mas pedidos los ultimos 30
                                                dias</strong></h4>
                                                
                                        <table class="table table-striped">
                                            <thead style="background-color: #ff7514; color:white">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th class="text-center">Cliente</th>
                                                    <th class="text-center">Producto</th>
                                                    <th class="text-center">Precio</th>
                                                    <th class="text-center">Total Kg</th>
                                                    <th class="text-center">Total $</th>
                                                    <th class="text-center">Fecha pedido</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach ($reportClientes as $key=>$item)
                                                <tr>
                                                    <td>{{ $key+1 }}</td>
                                                    <td>{{ $item->cliente }}</td>
                                                    <td>{{ $item->producto }}</td>
                                                    <td>$ {{ $item->precio }}</td>
                                                    <td>{{ $item->totalKg }}</td>
                                                    <td>$ {{ $item->totalFeria }}</td>
                                                    <td>{{ $item->fechaPedido }}</td>   
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div id="facturapedidos" class="tab-pane fade in active">
                            <h4><strong>Listado de clientes que solicitaron factura</strong></h4>
                                
                                <form id="formChange" action="/ReportesPedidosFactura">
                                    <div class="form-group">
                                        <label style="color:black; font-size: 18px">Selecciona fecha </label>
                                        <input style="width: 20%;color: #949494; display: inline !important;" type="date" class="form-control"
                                        id="fecha" name="fecha"  onchange="setFecha()">
                                        <br>           
                                    </div>
                                </form>
                                <div class="row">
                                    <table class="table table-striped">
                                        <thead style="background-color: #ff7514; color:white">
                                            <tr>
                                                <th class="text-center">#</th>
                                                <th class="text-center">Cliente</th>
                                                <th class="text-center">Producto</th>
                                                <th class="text-center">Precio</th>
                                                <th class="text-center">Fecha pedido</th>
                                                <th class="text-center">Fecha entrega</th>
                                                <th class="text-center">Estatus</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if(count($fechaFactura)==0)
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td></td>
                                                <td >No hay registros disponibles </td>
                                                <td></td>
                                                <td></td>
                                            </tr>
                                            @else
                                            @foreach ($fechaFactura as $key=>$item)
                                            <tr>
                                                <td>{{ $key+1 }}</td>
                                                <td>{{ $item->cliente }}</td>
                                                <td>{{ $item->producto}}</td>
                                                <td>$ {{ $item->precio}}</td>
                                                <td>{{ $item->fecha_pedido}}</td>
                                                <td>{{ $item->fecha_entrega}}</td>
                                                <td>{{ $item->status}}</td>
                                            </tr>
                                            @endforeach
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    @include('includes.footer')
    @endsection
@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">



    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container" style="width: 100%;">
            <div class="row">
            	  <a href="{{ url('/')}}" class="toplink"><i class="material-icons">reply</i> </a>
                  <h2 class="title text-center">Productos Activos</h2>

				  <div class="col-md-8">
                    <div class="">
               
                        <h5>
                            Busqueda de Productos
                            {{ Form::open(['route' => 'productos', 'method' => 'GET', 'class' => 'form-inline pull-right']) }}
                                <div class="form-group">
                                    {{ Form::text('name', $name, ['class' => 'form-control', 'placeholder' => 'Nombre']) }}
                                </div>
                                <div class="form-group">
                                    {{ Form::text('codigo', $codigo, ['class' => 'form-control', 'placeholder' => 'Codigo']) }}
                                </div>
                                <!--SELECT DINAMICO PARA CIUDADES-->
                                <div class="form-group">
                                   <select name="city" id="city" class="form-control">
                                      @foreach ($sucursales as $item)
                                          <option {!! $item->organization_id== $city? 'selected':'' !!} value="{{$item->organization_id}}">{{$item->name}}</option>
                                      @endforeach
                                   </select>
                                </div>
                               
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary  btn-sm">
                                        <i class="material-icons">search</i>
                                    </button>
                                </div>
                            {{ Form::close() }}
                        </h5>
                        
                    </div>
            </div>
			
			<div class="col-md-8">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Id </th>
                            <th>Nombre </th>
                            <th>Codigo</th>
                            <th>Estatus</th>
                            <th>Ciudad</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($productos as $producto)
                        <tr>
                            <td>{{ $producto->id }}</td>
                            <td>{{ $producto->name }}</td>
                            <td>{{ $producto->codigo }}</td>
                            <td id="resp{{ $producto->id }}">
                          <br>
                            @if($producto->activo == 1)
                            <button type="button" class="btn btn-sm btn-success">Activa</button>
                                @else
                            <button type="button" class="btn btn-sm btn-danger">Inactiva</button>
                            @endif
                        
                        </td>
                        <td>{{ $producto->city }}</td>
                        <td>
                            <br>
                            <label class="switch">
                                <input data-id="{{ $producto->id }}" class="mi_checkbox" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" {{ $producto->activo ? 'checked' : '' }}>
                                <span class="slider round"></span>
                            </label>
                        </td>
                        <td class="text-center" class="td-actions">
                            <!--<form method="post">
                            </form>-->
                            <button type="button"  data-toggle="modal" data-target="#exampleModal{{$producto->id}}"  class="btn btn-info  btn-xs">
                                <a rel="tooltip" title="Ir al producto" style="color: white">
                                    <i class="fa fa-edit"></i>
                                </a>
                            </button>
                              
                              <!-- Modal -->
                              <div class="modal fade" id="exampleModal{{$producto->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <h5 class="modal-title" id="exampleModalLabel">Modificar: <strong> {{$producto->name}}</strong></h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                      </button>
                                    </div>
                                    <div class="modal-body">
                                        <form>
                                            <div class="form-group row">
                                              <label class="col-sm-4 col-form-label"><strong style="color: black">Asignar imagen default</strong></label>
                                              <div >
                                                <input type="checkbox" id="checkInput" value="imgChecked" checked disabled>
                                            </div>
                                            </div>
                                            <div class="form-group row">
                                                <label for="inputCodigo" class="col-sm-4 col-form-label">
                                                    <strong style="color: black" data-toggle="tooltip" data-placement="top" title="Codigo producto">Codigo</strong>
                                                </label>
                                                <div class="col-sm-7">
                                                  <input type="number" class="form-control" id="inputCodigo" value='{{$producto->codigo }}' disabled>
                                                </div>
                                              </div>
                                            <div class="form-group row">
                                              <label for="inputPieza" class="col-sm-4 col-form-label">
                                                  <strong style="color: black" data-toggle="tooltip" data-placement="top" title="Es el equivalente de kg a piezas">Piezas</strong>
                                              </label>
                                              <div class="col-sm-7">
                                                <input type="number" class="form-control" id="inputPieza" placeholder="Ingrese cantidad de piezas" >
                                              </div>
                                            </div>
                                          </form>
                                    </div>
                                    <div class="modal-footer">
                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                                      <button type="button" class="btn btn-primary" onclick="guardarModal({{$producto->codigo}},{{$producto->id}})">Guardar</button>
                                    </div>
                                  </div>
                                </div>
                              </div>                                  
                        </td>

                        </tr>
                        @endforeach
                    </tbody>
                </table>
               
                {{ $productos->render() }}
            </div>
                     
           
        </div>
        
			</div>	 
    </div>
</div>

@yield('content')   
<script type="text/javascript">
  $(document).ready(function() {
   
$('.mi_checkbox').change(function() {
    //Verifico el estado del checkbox, si esta seleccionado sera igual a 1 de lo contrario sera igual a 0
    var status = $(this).prop('checked') == true ? 1 : 0; 
    var id = $(this).data('id'); 
        console.log(status);
    $.ajax({
        type: "GET",
        dataType: "json",
        //url: '/StatusNoticia',
        url: '{{ route('StatusProducto') }}',
        data: {'status': status, 'id': id},
        success: function(data){
            $('#resp' + id).html(data.var); 
            console.log(data.var)
         
        }
    });
})
      
});

//Guarda datos del producto
function guardarModal(code,productId){
    debugger
    let pza=$("#inputPieza").val();
    $.ajax({
        type: "GET",
        dataType: "json",
        url: '{{ route('updateImgProducto') }}',
        data: {'idproducto':productId,'codigo': code,'cantPieza':pza},
        success: function(data){
           $("#exampleModal"+productId).modal('hide');
           $("#inputPieza").val("");
           alert(data.response)
        }
    });
}
</script>

@yield('scripts')

@include('includes.footer')
@endsection
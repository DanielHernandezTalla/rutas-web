@extends('layouts.app')

@section('title', 'Bienvenido a ' . config('app.name'))

@section('body-class', 'landing-page')

@section('styles')
    <style>
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .team .row {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display:         flex;
          flex-wrap: wrap;
        }
        .team .row > [class*='col-'] {
          display: flex;
          flex-direction: column;
        }

        .tt-query {
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
          color: #999
        }

        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
          width: 222px;
          margin-top: 4px;
          padding: 4px 0;
          background-color: #fff;
          border: 1px solid #ccc;
          border: 1px solid rgba(0, 0, 0, 0.2);
          -webkit-border-radius: 4px;
             -moz-border-radius: 4px;
                  border-radius: 4px;
          -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
             -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
                  box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
          padding: 3px 20px;
          line-height: 24px;
        }

        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
          color: #fff;
          background-color: #0097cf;

        }

        .tt-suggestion p {
          margin: 0;
        }
        .description{
          font-size: 12px;
          
        }
        .title{
          font-size: 14px;
          
        }



/* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles h3 {
  color:#000000;
  font-size: 30px;
}

.contenedor-titulo-controles .indicadores button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles .indicadores button:hover,
.contenedor-titulo-controles .indicadores button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas {
  margin-bottom: 0px;
}

.peliculas-recomendadas .contenedor-principal {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda,
.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.0);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 20px;
  color: #ff7514;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda:hover,
.peliculas-recomendadas .contenedor-principal .flecha-derecha:hover {
  background: rgba(0,0,0, .0);
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda {
  left: 0;
}

.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  right: 0;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas .contenedor-carousel {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas .contenedor-carousel .carousel {
  display: flex;
  flex-wrap: nowrap;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula {
  min-width: 33.3%;
  transition: .3s ease all;
  box-shadow: 5px 5px 10px rgba(0,0,0, .1);
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula.hover {
  transform: scale(1.2);
  transform-origin: center;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula img {
  width: 100%;
  vertical-align: top;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula2 img {
  width: 33.3%;
  vertical-align: top;
}
.peliculas-recomendadas .contenedor-carousel .carousel .pelicula3 img {
  width: 50%;
  vertical-align: top;
}

/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 300px) {
  header .logotipo {
    margin-bottom: 10px;
    font-size: 30px;
  }

  header .contenedor {
    flex-direction: column;
    text-align: center;
  }

  .pelicula-principal {
    font-size: 14px;
  }

  .pelicula-principal .descripcion {
    max-width: 100%;
  }

  .peliculas-recomendadas .contenedor-carousel {
    overflow: visible;
  }

  .peliculas-recomendadas .contenedor-carousel .carousel {
    display: grid;
    grid-template-columns: repeat(3, 1fr);
    gap: 20px;
  }

  .peliculas-recomendadas .indicadores,
  .peliculas-recomendadas .flecha-izquierda,
  .peliculas-recomendadas .flecha-derecha {
    display: none;
  }
}

.productos-carrusel{
  font-size: 10px;
}

/*---------------Slider-------------------------*/

.slider{
  width:95%;
  margin: auto;
  overflow: hidden;

}

.slider ul {
  display: flex;
  padding: 0;
  width: 300%;

  animation: cambio 20s infinite alternate linear;
  
}


.slider li{
  width: 100%;
  list-style: none;
}

.slider img{
  width: 100%;

}


@keyframes cambio {
  0% {margin-left: 0;}
  20%{margin-left: 0;}

  25%{margin-left: -100%;}
  45%{margin-left: -100%;}

  50%{margin-left: -200%;}
  70%{margin-left: -200%;}

}

.form_box
{
  width: 100%;
  height: 100%;      
  display: flex;
  align-items: center;
}

.search-box {
  border: solid 5px; color: #ff7514 ;
  display: inline-block;
  position: relative;
  border-radius: 50px;
}
.search-box input[type="text"] {
  font-family: Raleway, sans-serif;
  font-size: 10px;
  font-weight: bold;
  width: 20px;
  height: 20px;
  padding: 1px 10px 5px 10px;
  border: none;
  box-sizing: border-box;
  border-radius: 50px;
  transition: width 800ms cubic-bezier(0.5, -0.5, 0.5, 0.5) 600ms;
}
.search-box input[type="text"]:focus {
  outline: none;
}
.search-box input[type="text"]:focus, .search-box input[type="text"]:not(:placeholder-shown) {
  width: 300px;
  transition: width 800ms cubic-bezier(0.5, -0.5, 0.5, 1.5);
}
.search-box input[type="text"]:focus + span, .search-box input[type="text"]:not(:placeholder-shown) + span {
  bottom: 13px;
  right: 10px;
  transition: bottom 300ms ease-out 800ms, right 300ms ease-out 800ms;
}
.search-box input[type="text"]:focus + span:after, .search-box input[type="text"]:not(:placeholder-shown) + span:after {
  top: 0;
  right: 10px;
  opacity: 1;
  transition: top 300ms ease-out 1100ms, right 300ms ease-out 1100ms, opacity 300ms ease 1100ms;
}
.search-box span {
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: -16px;
  right: -17px;
  transition: bottom 300ms ease-out 300ms, right 300ms ease-out 300ms;
}
.search-box span:before, .search-box span:after {
  content: '';
  height: 20px;
  border-left: solid 5px;  color: #ff7514 ;
  position: absolute;
  transform: rotate(-45deg);
}
.search-box span:after {
  transform: rotate(45deg);
  opacity: 0;
  top: -20px;
  right: -10px;
  transition: top 300ms ease-out, right 300ms ease-out, opacity 300ms ease-out;
}

.btn-estadio{
    background-color: #ff7514;
    color: #FFFFFF;
}


    </style>
@endsection

@section('content')
<div class="header header-filter" style="background-image: url('{{ asset('/img/city.jpg') }}');">
    <div class="container">
        
    </div>
</div>


    <div class="container">
       
        <div class="section text-center">
        
            <select class="form-control formselect required" placeholder="Seleccionar Ciudad"
                            id="sub_category_name">
                            <option value="0" disabled selected>Select
                                Main Category*</option>
                            @foreach($data as $categories)
                            <option  value="{{ $categories->id }}">
                                {{ ucfirst($categories->name) }}</option>
                            @endforeach
                        </select>

           <div class="slider">
             <ul>
                <li><img src="https://kowipage.kowi.com.mx/wp-content/uploads/2020/09/recurso-61@3x_optimized.png"></li>
                <li><img src="https://kowipage.kowi.com.mx/wp-content/uploads/2020/09/recurso-62@3x_optimized.png"></li>
                <li><img src="https://kowipage.kowi.com.mx/wp-content/uploads/2020/09/recurso-63@3x_optimized.png"></li>
             </ul>
           </div>






  
   
    <div class="search-box">
       <form class="form-inline" method="get" action="{{ url('/search') }}">
  <input name="query"  type="text"   placeholder="¿Qué producto buscas?"/><span></span>
</form>   
</div>

<div style="text-align:right;">
  <a id="btn-AddDate"  class=" btn btn-xs btn-estadio" data-toggle="modal" data-target="#AddDate">
   Estadio

</a>
</div>

<!-- Modal -->
<div class="modal fade" id="AddDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Seleciona un Estadio</h4>
      </div>
   <table class="table">
           <thead>
                            <tr>
                              <th class="text-center">Nombre</th>
                            </tr>
                        </thead>

       @foreach ($Estadios as $products)
       <tbody>
                            <tr>
                             <td class="text-center">  <a href="{{url('/kowiEstadio/'.$products->id)}}">  {{ $products->descripcion }} </a> </td>
                          
                            </tr>

                         @endforeach
                        </tbody>
           </table>   
      
    </div>
  </div>
</div>



            <div class="team">
              @foreach ($categories1 as $category)
              @endforeach
        <div class="peliculas-recomendadas contenedor">
      <div class="contenedor-titulo-controles">
        <h5><a href="{{ url('/categories/'.$category->id) }}">{{ $category->name }}</a> </h5>
        <div class="indicadores"></div>
      </div>

      <div class="contenedor-principal">
        <button role="button" id="flecha-izquierda" class="flecha-izquierda"><i class="fas fa-angle-left"></i></button>

        <div class="contenedor-carousel">
          <div class="carousel">
              @foreach ($DatPrecios as $productos)
            <div class="pelicula">
                <a href="{{ url('/products/'.$productos->id) }}"><img src=" {{ $productos->featured_image_url }}" alt="Imagen representativa de la categoría {{ $productos->name }}"></a> 
                <a href="{{ url('/products/'.$productos->id) }}" class="productos-carrusel">{{$productos->nameproduct}}</a>
                <p >${{$productos->precios}}</p>
            </div>
            @endforeach
            
          </div>
        </div>

        <button role="button" id="flecha-derecha" class="flecha-derecha"><i class="fas fa-angle-right"></i></button>
      </div>
    </div>

      </div>
    </div>

        </div>

@include('includes.footer')
@endsection

@section('scripts')
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
     <script src="{{ asset('/js/main.js') }}"></script>
      <script src="{{ asset('/js/search.js') }}"></script>
    <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>
    <script>
        $(function () {
            // 
            var products = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              prefetch: '{{ url("/products/json") }}'
            });            

            // inicializar typeahead sobre nuestro input de búsqueda
            $('#search').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                name: 'products',
                source: products
            });
        });
    </script>
    
@endsection

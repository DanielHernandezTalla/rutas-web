@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')

<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 left: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}
    @media (max-width: 540px) {

  #selectciudad{
    display: none;
  }
  #shoppingcar{
    display: none;

  }
 
}
</style>

<div class="main main-raised" style="background-color: #fff;-webkit-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	  <a href="{{ url('/home') }}" class="toplink"><i class="material-icons">reply</i> </a>
            	 
                    </div>
                    <br>
                    <br>
                    <br>
          <table class="table">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                                        
                                        <th class="text-center">Total</th>
                                        <th class="text-center">Fecha</th>
                                        <th class="text-center">Hora</th>
                                        <th class="text-center">Nombre cliente</th>
                                        <th class="text-center">Telefono</th>
                                        <th class="text-center">Direccion</th>
                                        <th class="text-center">Ciudad</th>
                                        
                    </tr>
                </thead>

                <tbody>
                   @foreach ($unaut as $unauts)
                                    <tr>
                                         <td class="text-center">{{ $unauts->id}}</td>
                                       
                                        <td class="text-center"> {{ $unauts->importe_total }}</td>
                                        <td class="text-center"> {{ $unauts->order_date }}</td>
                                        <td class="text-center"> {{ $unauts->created_at }}</td>
                                        <td class="text-center"> {{ $unauts->name }}</td>
                                        <td class="text-center"> {{ $unauts->phone }}</td>
                                        <td class="text-center"> {{ $unauts->address }}</td>
                                        <td class="text-center"> {{ $unauts->city }}</td>  
                                        
                                        
                    @endforeach
                                       
                                    </tr>
                                    


                        </td>

                            
                    </tr>
                    
                </tbody>
            </table>


				           <div class="text-center">
<p><strong>Importe a pagar:</strong> {{ $unauts->importe_total }}</p>
				
<br>
<br>
<form method="post" action="{{ url('/CancelHomePendiente/'.$unauts->id) }}">
                    {{ csrf_field() }}
                    
                    <button onclick="return confirm('¿Seguro desea cancelar este pedido?');" class="btn btn-primary btn-round">
                        <i class="material-icons">close</i> Cancelar pedido
                    </button>
                </form>
<form method="post" action="{{ url('/RefreshHomePendiente/'.$unauts->id) }}">
                    {{ csrf_field() }}

			<div align="center">
        	<button  class="btn btn-primary btn-round">
                      <i class="material-icons">refresh</i> Reenviar pedido
                    </button>
        </div>	
        </form>
      </div>
           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

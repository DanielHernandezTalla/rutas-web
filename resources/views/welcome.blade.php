@extends('layouts.app')

@section('title', 'Bienvenido a ' . config('app.name'))

@section('body-class', 'landing-page')

@section('styles')

    <style>
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .team .row {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display:         flex;
          flex-wrap: wrap;
        }
        .team .row > [class*='col-'] {
          display: flex;
          flex-direction: column;
        }

        .tt-query {
          -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
             -moz-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                  box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
        }

        .tt-hint {
          color: #999
        }

        .tt-menu {    /* used to be tt-dropdown-menu in older versions */
          width: 222px;
          margin-top: 4px;
          padding: 4px 0;
          background-color: #fff;
          border: 1px solid #ccc;
          border: 1px solid rgba(0, 0, 0, 0.2);
          -webkit-border-radius: 4px;
             -moz-border-radius: 4px;
                  border-radius: 4px;
          -webkit-box-shadow: 0 5px 10px rgba(0,0,0,.2);
             -moz-box-shadow: 0 5px 10px rgba(0,0,0,.2);
                  box-shadow: 0 5px 10px rgba(0,0,0,.2);
        }

        .tt-suggestion {
          padding: 3px 20px;
          line-height: 24px;
        }

        .tt-suggestion.tt-cursor,.tt-suggestion:hover {
          color: #fff;
          background-color: #0097cf;

        }

        .tt-suggestion p {
          margin: 0;
        }
        .description{
          font-size: 12px;
          
        }
        .title{
          font-size: 14px;
          
        }






/*---------------Slider-------------------------*/

.slider{
  width:95%;
  margin: auto;
  overflow: hidden;

}

.slider ul {
  display: flex;
  padding: 0;
  width: 300%;

  animation: cambio 20s infinite alternate linear;
  
}


.slider li{
  width: 100%;
  list-style: none;
}

.slider img{
  width: 100%;

}


@keyframes cambio {
  0% {margin-left: 0;}
  20%{margin-left: 0;}

  25%{margin-left: -100%;}
  45%{margin-left: -100%;}

  50%{margin-left: -200%;}
  70%{margin-left: -200%;}

}

.form_box
{
  width: 100%;
  height: 100%;      
  display: flex;
  align-items: center;
}

.search-box {
  border: solid 5px; color: #ff7514 ;
  display: inline-block;
  position: relative;
  border-radius: 50px;
}
.search-box input[type="text"] {
  font-family: Raleway, sans-serif;
  font-size: 10px;
  font-weight: bold;
  width: 20px;
  height: 20px;
  padding: 1px 10px 5px 10px;
  border: none;
  box-sizing: border-box;
  border-radius: 50px;
  transition: width 800ms cubic-bezier(0.5, -0.5, 0.5, 0.5) 600ms;
}
.search-box input[type="text"]:focus {
  outline: none;
}
.search-box input[type="text"]:focus, .search-box input[type="text"]:not(:placeholder-shown) {
  width: 300px;
  transition: width 800ms cubic-bezier(0.5, -0.5, 0.5, 1.5);
}
.search-box input[type="text"]:focus + span, .search-box input[type="text"]:not(:placeholder-shown) + span {
  bottom: 13px;
  right: 10px;
  transition: bottom 300ms ease-out 800ms, right 300ms ease-out 800ms;
}
.search-box input[type="text"]:focus + span:after, .search-box input[type="text"]:not(:placeholder-shown) + span:after {
  top: 0;
  right: 10px;
  opacity: 1;
  transition: top 300ms ease-out 1100ms, right 300ms ease-out 1100ms, opacity 300ms ease 1100ms;
}
.search-box span {
  width: 20px;
  height: 20px;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  bottom: -16px;
  right: -17px;
  transition: bottom 300ms ease-out 300ms, right 300ms ease-out 300ms;
}
.search-box span:before, .search-box span:after {
  content: '';
  height: 20px;
  border-left: solid 5px;  color: #ff7514 ;
  position: absolute;
  transform: rotate(-45deg);
}
.search-box span:after {
  transform: rotate(45deg);
  opacity: 0;
  top: -20px;
  right: -10px;
  transition: top 300ms ease-out, right 300ms ease-out, opacity 300ms ease-out;
}

.btn-estadio{
    background-color: #ff7514;
    color: #FFFFFF;
}

@media (max-width: 540px) {
 .peliculas-recomendadas .contenedor-principal .flecha-izquierda,
.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.0);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 20px;
  color: #000;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}
  #menusuperior {
    display:block;
    width:100%;
    background:#242424;
    height: 140px;
    text-align: right;
    border-bottom-right-radius:20px; 
    border-bottom-left-radius:20px;
    padding-top: 40px;

  }


.categoriname{
color: #000; font-weight: 600; font-size: 14px; text-transform: lowercase;
}
.productname{
  color: #000;  font-size: 12px; text-transform: lowercase; 
}



}
.botom{
  margin-bottom: -1%;
}
.botom2{
  margin-top: 5%;
  margin-bottom: -1%;
  font-size: 12px;
  font-weight: 500;
color: #A0A0A0;
}


/*---------------Slider-------------------------*/

.slider{
  width:100%;
  margin: auto;
  overflow: hidden;
  position: relative;
  align-items: center;


}

.slider ul {
  display: flex;
  padding: 0;
  width: 300%;

  animation: cambio 20s infinite alternate linear;
  
}


.slider li{
  width: 100%;
  list-style: none;
}

.slider img{
  width: 100%;

}
#citys{
border-style: solid;
border-color: #CBCBCB;
width: 100%;
border-radius: 50px;
height: 40px;
color: #CBCBCB;
font-size: 16px;
text-align: right;
font-weight: 400;

  }
#pc{
  display: none;
}
#local{
  color: #CBCBCB;
}

#pc2{
  display: none;
}

#cell select{
  display: inline-block;
  width: 75%;
  cursor: pointer;
    padding: 7px 10px;
    height: 42px;
    outline: 0; 
    border: 0;
  
  background: #fff;
  color: #858585;
  font-size: 16px;
  color: #CBCBCB;
  font-family: 
  'Quicksand', sans-serif;
  border:1px solid #CBCBCB;
  border-radius: 40px;
    position: relative;
    transition: all 0.25s ease;
    text-align: right;
}
#cell{
  padding: 0px;

}
#cell .cantidad{
  color: #CBCBCB;
}
.cantidad{
width: 75%; font-size: 16px; border-style: solid; border-color: #CBCBCB; border-width: 1px; border-radius: 18px; height: 40px; text-align: center;
}
.cantidad::placeholder {
  color: #CBCBCB;
   text-decoration: underline;
   font-weight: 400;
    text-decoration-color: #CBCBCB;
     text-align: center;
     font-style: italic;

}

.datosuser{
  display: none;
}
#namess{
display: none;
}
#imagess  {
display: none;
}

#menutop {
    color: #FFF;
    text-transform: none;
    text-align: center;
    font-size: 20px;
    word-spacing: 1em;
    font-weight: normal;
    font-family: 'airship_27regular', sans-serif;
    letter-spacing: 0.05em;
    z-index: 9000;
    background: #EAEAEA;
    width: 100%;
    
    margin-top: -18%;
    border-radius: 20px;
}
#menutop p {
    margin: 0;
    padding: 20px;
}
.fixed .peliculas-recomendadas5{
 padding: 30px; 
}
.fixed {
    position: fixed;
    top: 6.5%;
    left: 0;
    width: 100%;
    height: 120px;

}



.dropdownwrap {
    height: 150px;
    background-color: rgb(245, 245, 245);
    display: none;
    padding: 20px;
    width: auto;
    margin-top: 0px;
    margin-right: auto;
    margin-left: auto;
}

#dropdown {
    text-transform: none;
    text-align: center;
    font-size: 20px;
    letter-spacing: 0.05em;
    color: rgb(102, 102, 102);
    position: relative;
    z-index: 99999;
    text-decoration: none;
}

#dropdown:hover {
    text-decoration: none;
    color: #FFFFFF;
}

#CenterMenu {
    display: block;
    height: 100px;
    width: 700px;
    margin-top: 20px;
    margin-bottom: 0px;
    position: relative;
    z-index: 999999;
    float: none;
    margin-left: auto;
    margin-right: auto;
}

.insideMenu {
    display: block;
    float: right;
    height: 88px;
    width: 130px;
    margin-top: 12px;
}

.insideMenu h1 {
    font-family: 'airship_27regular', sans-serif;
    font-size: 14px;
    line-height: 14px;
    font-weight: lighter;
    word-spacing: 12px;
    letter-spacing: 0.1em;
    color: rgb(0, 0, 0);
    text-align: left;
}

.insideMenu p {
    font-size: 10px;
    line-height: 18px;
    font-weight: 400;
    word-spacing: 1.5px;
    letter-spacing: 0.1em;
    color: rgb(0, 102, 102);
    text-align: left;
}
#menutop{
  display: none;
}
}


@media (min-width: 541px) {
#cell{
 display: none; 
}
.cantidad{
  display: none;
}
#statusactivo{
  display: none;
}

}

/* -------------- Carruseles -------------------*/
/* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles .indicadores button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles .indicadores button:hover,
.contenedor-titulo-controles .indicadores button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas {
  margin-bottom: 70px;
}

.peliculas-recomendadas .contenedor-principal {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda,
.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.3);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #fff;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda:hover,
.peliculas-recomendadas .contenedor-principal .flecha-derecha:hover {
  background: rgba(0,0,0, .9);
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda {
  left: 0;
}

.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  right: 0;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas .contenedor-carousel {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas .contenedor-carousel .carousel {
  display: flex;
  flex-wrap: nowrap;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula {
    min-width: 25%;
  transition: .3s ease all;
  margin-left: 5%;
  margin-top: 5%;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula.hover {
  transform: scale(1.2);
  transform-origin: center;
}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula img {
  width: 100%;
  vertical-align: top;
}


/* --------------Carrusel 2 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles2 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles2 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles2 .indicadores2 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles2 .indicadores2 button:hover,
.contenedor-titulo-controles2 .indicadores2 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas2 {
  margin-bottom: 70px;
}

.peliculas-recomendadas2 .contenedor-principal2 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2,
.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2 {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.3);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #fff;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2:hover,
.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2:hover {
  background: rgba(0,0,0, .9);
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2 {
  left: 0;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2 {
  right: 0;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas2 .contenedor-carousel2 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 {
  display: flex;
  flex-wrap: nowrap;
}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 {
    min-width: 25%;
  transition: .3s ease all;
  margin-left: 5%;
  margin-top: 5%;
}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2.hover {
  transform: scale(1.2);
  transform-origin: center;
}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 img {
  width: 100%;
  vertical-align: top;
}



/* --------------Carrusel 3 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles3 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles3 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles3 .indicadores3 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles3 .indicadores3 button:hover,
.contenedor-titulo-controles3 .indicadores3 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas3 {
  margin-bottom: 70px;
}

.peliculas-recomendadas3 .contenedor-principal3 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3,
.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3 {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.3);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #fff;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3:hover,
.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3:hover {
  background: rgba(0,0,0, .9);
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3 {
  left: 0;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3 {
  right: 0;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas3 .contenedor-carousel3 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 {
  display: flex;
  flex-wrap: nowrap;
}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 {
    min-width: 25%;
  transition: .3s ease all;
  margin-left: 5%;
  margin-top: 5%;
}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3.hover {
  transform: scale(1.2);
  transform-origin: center;
}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 img {
  width: 100%;
  vertical-align: top;
}




/* --------------Carrusel 4 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles4 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles4 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles4 .indicadores4 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles4 .indicadores4 button:hover,
.contenedor-titulo-controles4 .indicadores4 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas4 {
  margin-bottom: 70px;
}

.peliculas-recomendadas4 .contenedor-principal4 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4,
.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4 {
  position: absolute;
  border: none;
  background: rgba(0,0,0,0.3);
  font-size: 40px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #fff;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4:hover,
.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4:hover {
  background: rgba(0,0,0, .9);
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4 {
  left: 0;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4 {
  right: 0;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas4 .contenedor-carousel4 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 {
  display: flex;
  flex-wrap: nowrap;
}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 {
    min-width: 25%;
  transition: .3s ease all;
  margin-left: 5%;
  margin-top: 5%;

}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4.hover {
  transform: scale(1.2);
  transform-origin: center;
}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 img {
  width: 100%;
  vertical-align: top;
}




/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 540px) {
  /* --------------Carrusel 1 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles .indicadores button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles .indicadores button:hover,
.contenedor-titulo-controles .indicadores button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas {
  margin-bottom: 70px;
}

.peliculas-recomendadas .contenedor-principal {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda,
.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  position: absolute;
  border: none;
  background-color: transparent;
  font-size: 20px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #B3B3B3;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda:hover,
.peliculas-recomendadas .contenedor-principal .flecha-derecha:hover {
  background-color: transparent;
  color: #000;
}

.peliculas-recomendadas .contenedor-principal .flecha-izquierda {
  left: 0;
  margin-left: -20px;
}

.peliculas-recomendadas .contenedor-principal .flecha-derecha {
  right: 0;
  margin-right: -25px;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas .contenedor-carousel {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas .contenedor-carousel .carousel {
  display: flex;
  flex-wrap: nowrap;

}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula {
  min-width: 40%;
  transition: .3s ease all;
  text-align: left;
  background-color: #fff;
box-shadow: none;
border-style: solid;
border-color: #AFAFAF;
border-width: 1px;
height: auto;
margin-left: 5%;
margin-right: 5%;
  
} 
.peliculas-recomendadas .contenedor-carousel .carousel .datoscarrusel{
  width: 100%;
  text-align: left;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #AFAFAF;
  height: 80px;
}
.peliculas-recomendadas .contenedor-carousel .carousel .pelicula .productname{
    font-size: 12px;
  color: #A0A0A0;
  font-weight: 600;
  padding-left: 5%;
  margin-top: -20px;

}

.peliculas-recomendadas .contenedor-carousel .carousel .pelicula .preciooo{
  font-size: 12px;
  bottom: 0px;
   font-weight: 500;
  color: #ff7514;
  padding-left: 5%;
}




.peliculas-recomendadas .contenedor-carousel .carousel .pelicula img {
margin: 0 auto;
  height: auto;
  width: 100%;


}
}





/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 540px) {
  /* --------------Carrusel 2 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles2 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles2 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles2 .indicadores2 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles2 .indicadores2 button:hover,
.contenedor-titulo-controles2 .indicadores2 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas2 {
  margin-bottom: 70px;
}

.peliculas-recomendadas2 .contenedor-principal2 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2,
.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2 {
  position: absolute;
  border: none;
  background-color: transparent;
  font-size: 20px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #B3B3B3;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2:hover,
.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2:hover {
  background-color: transparent;
  color: #000;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-izquierda2 {
  left: 0;
  margin-left: -20px;
}

.peliculas-recomendadas2 .contenedor-principal2 .flecha-derecha2 {
  right: 0;
  margin-right: -25px;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas2 .contenedor-carousel2 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 {
  display: flex;
  flex-wrap: nowrap;

}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 {
  min-width: 40%;
  transition: .3s ease all;
  text-align: left;
  background-color: #fff;
box-shadow: none;
border-style: solid;
border-color: #AFAFAF;
border-width: 1px;
height: auto;
margin-left: 5%;
margin-right: 5%;
  
} 
.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .datoscarrusel2{
  width: 100%;
  text-align: left;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #AFAFAF;
  height: 80px;
}
.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 .productname{
    font-size: 12px;
  color: #A0A0A0;
  font-weight: 600;
  padding-left: 5%;
  margin-top: -20px;

}

.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 .preciooo{
  font-size: 12px;
  bottom: 0px;
   font-weight: 500;
  color: #ff7514;
  padding-left: 5%;
}




.peliculas-recomendadas2 .contenedor-carousel2 .carousel2 .pelicula2 img {
margin: 0 auto;
  height: auto;
  width: 100%;


}
}




/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 540px) {
  /* --------------Carrusel 3 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles3 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles3 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles3 .indicadores3 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles3 .indicadores3 button:hover,
.contenedor-titulo-controles3 .indicadores3 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas3 {
  margin-bottom: 70px;
}

.peliculas-recomendadas3 .contenedor-principal3 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3,
.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3 {
  position: absolute;
  border: none;
  background-color: transparent;
  font-size: 20px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #B3B3B3;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3:hover,
.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3:hover {
  background-color: transparent;
  color: #000;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-izquierda3 {
  left: 0;
  margin-left: -20px;
}

.peliculas-recomendadas3 .contenedor-principal3 .flecha-derecha3 {
  right: 0;
  margin-right: -25px;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas3 .contenedor-carousel3 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 {
  display: flex;
  flex-wrap: nowrap;

}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 {
  min-width: 40%;
  transition: .3s ease all;
  text-align: left;
  background-color: #fff;
box-shadow: none;
border-style: solid;
border-color: #AFAFAF;
border-width: 1px;
height: auto;
margin-left: 5%;
margin-right: 5%;
  
} 
.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .datoscarrusel3{
  width: 100%;
  text-align: left;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #AFAFAF;
  height: 80px;
}
.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 .productname{
    font-size: 12px;
  color: #A0A0A0;
  font-weight: 600;
  padding-left: 5%;
  margin-top: -20px;

}

.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 .preciooo{
  font-size: 12px;
  bottom: 0px;
   font-weight: 500;
  color: #ff7514;
  padding-left: 5%;
}




.peliculas-recomendadas3 .contenedor-carousel3 .carousel3 .pelicula3 img {
margin: 0 auto;
  height: auto;
  width: 100%;


}
}









/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 540px) {
  /* --------------Carrusel 4 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles4 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles4 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles4 .indicadores4 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles4 .indicadores4 button:hover,
.contenedor-titulo-controles4 .indicadores4 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas4 {
  margin-bottom: 70px;
}

.peliculas-recomendadas4 .contenedor-principal4 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4,
.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4 {
  position: absolute;
  border: none;
  background-color: transparent;
  font-size: 20px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #B3B3B3;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4:hover,
.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4:hover {
  background-color: transparent;
  color: #000;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-izquierda4 {
  left: 0;
  margin-left: -20px;
}

.peliculas-recomendadas4 .contenedor-principal4 .flecha-derecha4 {
  right: 0;
  margin-right: -25px;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas4 .contenedor-carousel4 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 {
  display: flex;
  flex-wrap: nowrap;

}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 {
  min-width: 40%;
  transition: .3s ease all;
  text-align: left;
  background-color: #fff;
box-shadow: none;
border-style: solid;
border-color: #AFAFAF;
border-width: 1px;
height: auto;
margin-left: 5%;
margin-right: 5%;
  
} 
.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .datoscarrusel4{
  width: 100%;
  text-align: left;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #AFAFAF;
  height: 80px;
}
.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 .productname{
    font-size: 12px;
  color: #A0A0A0;
  font-weight: 600;
  padding-left: 5%;
  margin-top: -20px;

}

.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 .preciooo{
  font-size: 12px;
  bottom: 0px;
   font-weight: 500;
  color: #ff7514;
  padding-left: 5%;
}




.peliculas-recomendadas4 .contenedor-carousel4 .carousel4 .pelicula4 img {
margin: 0 auto;
  height: auto;
  width: 100%;


}
}




/* ---- ----- ----- Media Queries ----- ----- ----- */
@media screen and (max-width: 540px) {
  /* --------------Carrusel 5 -----------------*/
 /* ---- ----- ----- Contenedor Titulo y Controles ----- ----- ----- */
.contenedor-titulo-controles5 {
  display: flex;
  justify-content: space-between;
  align-items: end;
}

.contenedor-titulo-controles5 h3 {
  color: #fff;
  font-size: 30px;
}

.contenedor-titulo-controles5 .indicadores5 button {
  background: #fff;
  height: 3px;
  width: 10px;
  cursor: pointer;
  border: none;
  margin-right: 2px;
}

.contenedor-titulo-controles5 .indicadores5 button:hover,
.contenedor-titulo-controles5 .indicadores5 button.activo {
  background: red;
}


/* ---- ----- ----- Contenedor Principal y Flechas ----- ----- ----- */
.peliculas-recomendadas5 {
  margin-bottom: 70px;
}

.peliculas-recomendadas5 .contenedor-principal5 {
  display: flex;
  align-items: center;
  position: relative;
}

.peliculas-recomendadas5 .contenedor-principal5 .flecha-izquierda5,
.peliculas-recomendadas5 .contenedor-principal5 .flecha-derecha5 {
  position: absolute;
  border: none;
  background-color: transparent;
  font-size: 20px;
  height: 50%;
  top: calc(50% - 25%);
  line-height: 40px;
  width: 50px;
  color: #B3B3B3;
  cursor: pointer;
  z-index: 500;
  transition: .2s ease all;
}

.peliculas-recomendadas5 .contenedor-principal5 .flecha-izquierda5:hover,
.peliculas-recomendadas5 .contenedor-principal5 .flecha-derecha5:hover {
  background-color: transparent;
  color: #000;
}

.peliculas-recomendadas5 .contenedor-principal5 .flecha-izquierda5 {
  left: 0;
  margin-left: -10px;
}

.peliculas-recomendadas5 .contenedor-principal5 .flecha-derecha5 {
  right: 0;
  margin-right: -10px;
}

/* ---- ----- ----- Carousel ----- ----- ----- */
.peliculas-recomendadas5 .contenedor-carousel5 {
  width: 100%;
  padding: 20px 0;
  overflow: hidden;
  scroll-behavior: smooth;
}

.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 {
  display: flex;
  flex-wrap: nowrap;

}

.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 .pelicula5 {
  min-width: 40%;
  transition: .3s ease all;
  text-align: left;
  background-color: #fff;
box-shadow: none;
border-style: solid;
border-color: #AFAFAF;
border-width: 1px;
height: auto;
margin-left: 5%;
margin-right: 5%;
  
} 
.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 .datoscarrusel5{
  width: 100%;
  text-align: left;
  border-top-style: solid;
  border-top-width: 1px;
  border-top-color: #AFAFAF;
  height: 80px;
}
.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 .pelicula5 .productname{
    font-size: 12px;
  color: #A0A0A0;
  font-weight: 600;
  padding-left: 5%;
  margin-top: -20px;

}

.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 .pelicula5 .preciooo{
  font-size: 12px;
  bottom: 0px;
   font-weight: 500;
  color: #ff7514;
  padding-left: 5%;
}




.peliculas-recomendadas5 .contenedor-carousel5 .carousel5 .pelicula5 img {
margin: 0 auto;
  height: auto;
  width: 100%;


}
#selectciudad{
  margin-top: 200px;
}
}
@media (min-width: 540px) {
  .mensajes_para_usuarios{
    display: none;
  }

    </style>

@endsection

@section('content')
<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">


<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/solid.css">   

<br>

    <div class="container">
<h5 class="mensajes_para_usuarios" style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">Esta aplicacion es para uso escusivo de negocios</h5>
<h5 class="mensajes_para_usuarios" style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: 0%;">Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
  @guest

@else

      <div id="menutop">
  
       

   



<div class="peliculas-recomendadas5 contenedor">
 
      <div class="contenedor-principal5">
        <button role="button" id="flecha-izquierda5" class="flecha-izquierda5"><i class="fas fa-angle-left"></i></button>

        <div class="contenedor-carousel5">
          <div class="carousel5">
                  

 @foreach ($allcategories as $allcategorie)
            <div class="pelicula5" id="peliculas5" style="border-style: none; width: 100%;">
         
            <div class="datoscarrusel5" style="text-align: center;background-color: #EAEAEA; border-style: none; height: 45px; width: 100%; padding-left: 10%; padding-right: 10%;">

                <a class="productname" href="/categories/{{ $allcategorie->id }}/" class="productos-carrusel" style="color: #ff7514; font-size: 14px;">

                  <i class="material-icons">fastfood</i>
                  {{$allcategorie->name}}</a>
             
               </div>
            </div>
            @endforeach

            
          </div>
        </div>
           <button role="button" id="flecha-derecha5" class="flecha-derecha5"><i class="fas fa-angle-right"></i></button>
          </div>
        </div>





</div>
@endguest

<br>
       
<script type="text/javascript">
  $(document).ready(function (e) {

    new mainMenu().load();

});

function mainMenu() {

    var thisObj = this,
        menu = $("#menutop"),
        win = $(window),
        pos =  menu.offset().top;

    thisObj.load = function() {

        // Bind slideToggle

        $('#dropdown').on('click', function () {

            $('.dropdownwrap').slideToggle();

        });

        // Set Fixed

        win.on("scroll", function() {    

            if( win.scrollTop() > pos) {
                menu.addClass("fixed");
            } else {
                menu.removeClass("fixed");
            }            
        });        
    }    
}
</script>



       <div class="slider">
             <ul>
                <li><img src="https://recetas.kowi.com.mx/Images/images/1a.jpg"></li>
                <li><img src="https://recetas.kowi.com.mx/Images/images/2a.jpg"></li>
                <li><img src="https://recetas.kowi.com.mx/Images/images/3a.jpg"></li>
             </ul>
           </div>
       
        <div class="section text-center" >
          


                            <div class="input-group" id="pc2" >
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 
                                <select name="citys" id="citys" class="form-control" >
@php

$userss = Auth::user();
@endphp

<?php if($userss != null){ ?>

                                 
                                   <option selected value="{{ auth()->user()->city }}">{{ auth()->user()->city }}</option>
                             
<?php }else{ ?> 
   <option selected value="seleccione una ciudad">seleccione una ciudad</option>
<?php } ?>       


                          
                                   @foreach($ciudades as $id => $tipo)
                                        <option value="{{ $id }}">
                                            {{ $tipo }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>





   
    <div class="search-box">
       <form class="form-inline" method="get" action="{{ url('/search') }}">
  <input  onfocus="myFunction()" onfocusout="myFunction2()" name="query"  type="text"   placeholder="¿Qué producto buscas?"/><span></span>
</form>   
</div>


<div hidden style="text-align:right;">
  <a  id="btn-AddDate" class=" btn btn-xs btn-estadio" data-toggle="modal" data-target="#AddDate">
   Estadio

</a>
  
</div>

<!-- Modal -->
<div class="modal fade" id="AddDate" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h2 class="modal-title">Centros de ventas </h2>
      </div>
    <div class="table-responsive">
      <h3 align="center">Total de tiendas : <span id="total_records"></span></h3>
      <table class="table table-striped table-bordered">
       <thead>
        <tr>
        <th>descripcion</th>
         
        </tr>
       </thead>
       <tbody>

       </tbody>
      </table>
     </div>   
      

<script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('tbody').html(data.table_data);
    $('#total_records').text(data.total_data);
   }
  })
 }

 $(document).on('change', '#citys', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>

 
  <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>

    </div>
  </div>
</div>

<!-- ---------------------------Carruseles-------------------- -->
@foreach ($categories1 as $category)
              @endforeach
<div class="peliculas-recomendadas contenedor">
      <div class="contenedor-titulo-controles">
      <h5 class="botom"><a class="categoriname" href="{{ url('/categories/'.$category->id) }}">{{ $category->name }}</a> </h5>
        <div class="indicadores"></div>
      </div>

      <div class="contenedor-principal">
        <button role="button" id="flecha-izquierda" class="flecha-izquierda"><i class="fas fa-angle-left"></i></button>

        <div class="contenedor-carousel">
          <div class="carousel">
                  @foreach ($DatPrecios as $productos)
            <div class="pelicula" id="peliculas">
                

@if (Auth::check())

@if(is_null($productos->image))
   <a  href="{{ url('/products/'.$productos->idproduct.'/'.$productos->idcentroventa) }}"><img  src=" {{ $productos->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a  href="{{ url('/products/'.$productos->idproduct.'/'.$productos->idcentroventa) }}"><img  src="{{ asset('images/products/'.$productos->image) }}" alt="Imagen representativa de del productos"></a>
@endif

@else
@if(is_null($productos->image))
<a href="/login"><img src=" {{ $productos->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a href="/login"><img src="{{ asset('images/products/'.$productos->image) }}" alt="Imagen representativa de del productos"></a>
@endif
@endif
            <div class="datoscarrusel">
                <a class="productname"
                href="{{ url('/products/'.$productos->idproduct.'/'.$productos->idcentroventa) }}" class="productos-carrusel">{{$productos->nameproduct}}</a>
                <p class="preciooo" >${{$productos->precios}}</p>
               </div>
            </div>
            @endforeach


            
          </div>
        </div>
           <button role="button" id="flecha-derecha" class="flecha-derecha"><i class="fas fa-angle-right"></i></button>
          </div>
        </div>
        <!-- ---------------------------Carruseles2-------------------- -->
@foreach ($categories2 as $category)
              @endforeach
<div class="peliculas-recomendadas2 contenedor">
      <div class="contenedor-titulo-controles2">
      <h5 class="botom"><a class="categoriname" href="{{ url('/categories/'.$category->id) }}">{{ $category->name }}</a> </h5>
        <div class="indicadores2"></div>
      </div>

      <div class="contenedor-principal2">
        <button role="button" id="flecha-izquierda2" class="flecha-izquierda2"><i class="fas fa-angle-left"></i></button>

        <div class="contenedor-carousel2">
          <div class="carousel2">
                  @foreach ($DatPrecios2 as $productos2)
            <div class="pelicula2" id="peliculas2">
                

@if (Auth::check())

@if(is_null($productos2->image))
   <a  href="{{ url('/products/'.$productos2->idproduct.'/'.$productos2->idcentroventa) }}"><img  src=" {{ $productos2->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a  href="{{ url('/products/'.$productos2->idproduct.'/'.$productos2->idcentroventa) }}"><img  src="{{ asset('images/products/'.$productos2->image) }}" alt="Imagen representativa de del productos"></a>
@endif

@else
@if(is_null($productos2->image))
<a href="/login"><img src=" {{ $productos2->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a href="/login"><img src="{{ asset('images/products/'.$productos2->image) }}" alt="Imagen representativa de del productos"></a>
@endif
@endif
            <div class="datoscarrusel2">
                <a class="productname" href="{{ url('/products/'.$productos2->idproduct.'/'.$productos2->idcentroventa) }}" class="productos-carrusel2">{{$productos2->nameproduct}}</a>
                <p class="preciooo" >${{$productos2->precios}}</p>
               </div>
            </div>
            @endforeach


            
          </div>
        </div>
           <button role="button" id="flecha-derecha2" class="flecha-derecha2"><i class="fas fa-angle-right"></i></button>
          </div>
        </div>

         <!-- ---------------------------Carruseles3-------------------- -->
@foreach ($categories3 as $category)
              @endforeach
<div class="peliculas-recomendadas3 contenedor">
      <div class="contenedor-titulo-controles3">
      <h5 class="botom"><a class="categoriname" href="{{ url('/categories/'.$category->id) }}">{{ $category->name }}</a> </h5>
        <div class="indicadores3"></div>
      </div>

      <div class="contenedor-principal3">
        <button role="button" id="flecha-izquierda3" class="flecha-izquierda3"><i class="fas fa-angle-left"></i></button>

        <div class="contenedor-carousel3">
          <div class="carousel3">
                  @foreach ($DatPrecios3 as $productos3)
            <div class="pelicula3" id="peliculas3">
                

@if (Auth::check())

@if(is_null($productos3->image))
   <a  href="{{ url('/products/'.$productos3->idproduct.'/'.$productos3->idcentroventa) }}"><img  src=" {{ $productos3->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a  href="{{ url('/products/'.$productos3->idproduct.'/'.$productos3->idcentroventa) }}"><img  src="{{ asset('images/products/'.$productos3->image) }}" alt="Imagen representativa de del productos"></a>
@endif

@else
@if(is_null($productos3->image))
<a href="/login"><img src=" {{ $productos3->featured_image_url }}" alt="Imagen representativa de del productos"></a>
@else 
<a href="/login"><img src="{{ asset('images/products/'.$productos3->image) }}" alt="Imagen representativa de del productos"></a>
@endif
@endif
            <div class="datoscarrusel3">
                <a class="productname" href="{{ url('/products/'.$productos3->idproduct.'/'.$productos3->idcentroventa) }}" class="productos-carrusel3">{{$productos3->nameproduct}}</a>
                <p class="preciooo" >${{$productos3->precios}}</p>
               </div>
            </div>
            @endforeach


            
          </div>
        </div>
           <button role="button" id="flecha-derecha3" class="flecha-derecha3"><i class="fas fa-angle-right"></i></button>
          </div>
        </div>



       




@include('includes.footer')
@endsection

@section('scripts')
   <script src="http://code.jquery.com/jquery-3.4.1.js"></script>
    <script src="{{ asset('/js/typeahead.bundle.min.js') }}"></script>
     <script src="{{ asset('/js/main.js') }}"></script>
      <script src="{{ asset('/js/main2.js') }}"></script>
       <script src="{{ asset('/js/main3.js') }}"></script>
         <script src="{{ asset('/js/main4.js') }}"></script>
         <script src="{{ asset('/js/main5.js') }}"></script>
      <script src="{{ asset('/js/search.js') }}"></script>
    <script src="https://kit.fontawesome.com/2c36e9b7b1.js" crossorigin="anonymous"></script>

    

    <script>
        $(function () {
            // 
            var products = new Bloodhound({
              datumTokenizer: Bloodhound.tokenizers.whitespace,
              queryTokenizer: Bloodhound.tokenizers.whitespace,
              prefetch: '{{ url("/products/json") }}'
            });            

            // inicializar typeahead sobre nuestro input de búsqueda
            $('#search').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                name: 'products',
                source: products
            });
        });
    </script>
<script>
function myFunction() {

if (window.matchMedia('(max-width: 540px)').matches) {
  document.getElementById("menufooters").style.position = "static";
}else {
         document.getElementById("menufooters").style.position = "fixed";
    }

}
function myFunction2() {
  document.getElementById("menufooters").style.position = "fixed";
}

</script>
      <script>
                $(document).ready(function () {
                $('#sub_category_name').on('change', function () {
                let id = $(this).val();
                $('#sub_category').empty();
                $('#sub_category').append(`<option value="0" disabled selected>Processing...</option>`);
                $.ajax({
                type: 'GET',
                url: 'GetSubCatAgainstMainCatEdit/' + id,
                success: function (response) {
                var response = JSON.parse(response);
                console.log(response);   
                $('#sub_category').empty();
                $('#sub_category').append(`<option value="0" disabled selected>Select Sub Category*</option>`);
                response.forEach(element => {
                    $('#sub_category').append(`<option value="${element['id']}">${element['name']}</option>`);
                    });
                }
            });
        });
    });
    </script>
    <script>
$(document).ready(function(){

 fetch_customer_data();

 function fetch_customer_data(query = '')
 {
  $.ajax({
   url:"{{ route('live_search.action') }}",
   method:'GET',
   data:{query:query},
   dataType:'json',
   success:function(data)
   {
    $('tbody').html(data.table_data);
    $('#total_records').text(data.total_data);
   }
  })
 }

 $(document).on('change', '#citys', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });
});
</script>
    
@endsection

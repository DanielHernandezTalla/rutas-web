@extends('layouts.app')

@section('body-class', 'product-page')

@section('content')
<style type="text/css">
    .toplink .material-icons {
 position: absolute;
 right: 5px;
 top: 5px;
 outline: 0;
 font-size: 55px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}



@media (max-width: 540px) {

        .toplink .material-icons {
 position: absolute;
 right: 5px;
 top: -10%;
 outline: 0;
 font-size: 30px; 
 
}
.toplink .material-icons:hover {
  color: #000;
}

html{
    background-color: #fff;
}
#name::placeholder {
  color: #838383;
  font-size: 12px;
  font-style: italic;

}
#card::placeholder {
  color: #838383;
 font-size: 12px; 
}

#cvc::placeholder {
  color: #838383;
 font-size: 12px; 
}

#f1::placeholder {
  color: #838383;
 font-size: 12px; 
}
#f2::placeholder {
  color: #838383;
 font-size: 12px; 
}

#f2 {
  color: #838383;
 font-size: 14px; 
 font-style: italic;
}
#f1 {
  color: #838383;
 font-size: 14px; 
 font-style: italic;
}
#cvc {
  color: #838383;
 font-size: 14px; 
 font-style: italic;
}

#card {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
#name {
  color: #838383;
 font-size: 14px; 
 font-style: italic;

}
.check{

outline: 2px solid #ff7514;
width: 250px;
}
input[type=checkbox] {
    transform: scale(0.5);
}
.wrapper{
    background-color: #fff;
    height:100%;
}

#olvreg{
    color: #838383; font-size: 12px;
}
.footer{
   margin-bottom: 10%;
}
#Iniciar{
background-color: #ff7514; border-radius: 35px; font-size: 18px; font-weight: 500; border-style: none; color: #fff; width:50%;
}
.checkbox .labelcheck{
    color: #838383; font-size: 12px; font-style: italic;
}
.input-group span{
 color: #ff7514; 

}
.text-divider{
  color: #838383; font-weight: 100; font-size: 12px; font-style: italic;  
}
#divinicio{
  background-color: transparent; margin-bottom: -10%;  
}
.titulo{
color: #787B79; font-size: 16px; font-weight: 500;
}
.section{
  -webkit-box-shadow: 10px 10px 5px 200px rgba(255,255,255,1);
-moz-box-shadow: 10px 10px 5px 200px rgba(255,255,255,1);
box-shadow: 10px 10px 5px 200px rgba(255,255,255,1);

}
.input-group{
    padding-bottom: 10%;
}
.titulo{
  width: 100%;
  padding-bottom: 10%;

}
.titulo .tituloruta{
position: relative; margin-top: 0%; color: #858585; margin-left: -0%; font-size: 18px; font-weight: 500; text-align: center; 
}
#olvreg{
    color: #838383; font-size: 12px; margin-top: -5%;
} 
}

</style>
<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');">
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog" >
    <div class="modal-content" >
      <div class="modal-header">
        <div class="header header-primary text-center" >
                            <h8>Generar factura</h8>
                        </div>
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
      
      </div>
          
        <div class="modal-body">
        <div class="row">
            
       
           <form method="post" action="{{ url('/createfactura') }}"> 
                  {{ csrf_field() }}   
  @foreach($User as $Users)

  
                            <div class="input-group" style="display: none;">
                                <span class="input-group-addon">
                                    <i class="material-icons">supervisor_account</i>
                                </span>
                                <input id="idus" type="text" placeholder="id usuario" class="form-control" name="idus" value="{{ old('idus', $Users->idus)}}" required>
                            </div> 


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                <input type="text" class="form-control" placeholder="Nombre" name="name"
                                       value="{{ old('name', $Users->name) }}" required autofocus>
                            </div>


                            <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">code</i>
                                </span>
                                <input id="rfc" type="text" placeholder="RFC" class="form-control" name="rfc"  required>
                            </div> 




                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">email</i>
                                </span>
                                <input readonly id="email" type="text" placeholder="Correo" class="form-control" name="email" value="{{ old('email', $Users->email)}}" required>
                            </div> 



                              <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">drag_handle</i>
                                </span>
                                <input readonly id="address" type="text" placeholder="Calle" class="form-control" name="address" value="{{ old('address', $Users->address) }}" required>
                            </div> 

                            


                                <div class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 

                                 
                                <select name="estado" id="estados" class="form-control">
                                   
                                   
                                    @foreach($estados as $id => $estado)
                                        <option value="{{ $estado }}">
                                            {{ $estado }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>



                             <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                 

                                  
                                <select  name="city2" id="citys" class="form-control">
                                    <option value="{{ old('ciuname', $Users->ciuname) }}">{{$Users->ciuname}}</option>
                                   
                                    @foreach($citys as $id => $city)
                                        <option value="{{ $city }}">
                                            {{ $city }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>


                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">location_city</i>
                                </span>
                                <input readonly id="numeroExterior" type="text" placeholder="Numero Exterior" class="form-control" name="city" value="{{ old('ciuname', $Users->ciuname)}}" required>
                            </div>



                            <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mark_email_unread</i>
                                </span>
                                 
                               <select  name="codigoPostal2" id="colonias" class="form-control">
                                   <option value="{{ old('codname', $Users->codname) }}">{{$Users->codname}}</option>  
                                   
                                  
                                </select>
                            </div>



                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">mark_email_unread</i>
                                </span>
                                <input readonly id="numeroExterior" type="text" placeholder="Numero Exterior" class="form-control" name="codigoPostal" value="{{ old('codname', $Users->codname)}}" required>
                            </div>


                             <div style="display: none;" class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>
                                 
                               <select  name="colonia2" id="codigoPostales" class="form-control">
                                    <option value="{{ old('colname', $Users->colname) }}">{{$Users->colname}}</option>
                                   
                                  
                                </select>
                            </div>



                            <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">house</i>
                                </span>
                                <input readonly id="numeroExterior" type="text" placeholder="Numero Exterior" class="form-control" name="colonia" value="{{ old('colname', $Users->colname)}}" required>
                            </div>


                        

                        <div  class="input-group">
                                <span class="input-group-addon">
                                    <i class="material-icons">format_list_numbered</i>
                                </span>
                                <input readonly id="numeroExterior" type="text" placeholder="Numero Exterior" class="form-control" name="numeroExterior" value="{{ old('numeroExterior', $Users->numeroExterior)}}" required>
                            </div>  

                             
@endforeach

          <div class="modal-footer">
            <button type="submit" class="btn btn-info btn-simple">Guardar</button>
           
            <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
           
          </div>
</form> 
            </div>
  </div>
     
    </div>
  </div>

</div> 
</div>

<div class="main main-raised">
    <div class="container">
<a href="" id="btn-AddDate" data-toggle="modal" data-target="#myModal" class="toplink"><i class="material-icons">fact_check</i> </a>
        <div class="section" >

             <div class="titulo">

                   <h3 class="tituloruta" >Pago en línea</h3>
                     <div class="text-center " id="olvreg">
           Ingresa tus datos
          </div>
                 </div>                        

            @if (session('notification'))
                <div class="alert alert-success">
                    {{ session('notification') }}
                </div>
            @endif

           <div class="row">
                <div class="col-md-6 col-md-offset-3">

                   
               <form action="{{ url('/procces') }}" method="POST" id="card-form">
                 {{ csrf_field() }}
                  @foreach ($product as $products)
                 <input type="hidden" name="name" value="{{ $products->name }}">
                  <input type="hidden" name="importe" value="{{ $products->importe_total }}">
                   <input type="hidden" name="quantity" value="{{ $products->quantity }}">
                    <input type="hidden" name="username" value="{{ $products->username }}">
                     <input type="hidden" name="address" value="{{ $products->address }}">
                     <input type="hidden" name="phone" value="{{ $products->phone }}">
                     <input type="hidden" name="email" value="{{ $products->email }}">
                     <input type="hidden" name="city" value="{{ $products->city }}">
                      <input type="hidden" name="cart_id" value="{{ $products->id_cart }}">
                      <input type="hidden" name="conektaTokenId" id="conektaTokenId" value="">
                       @endforeach
  <span class="card-errors"></span>
   <div class="form-group">
                             <div class="row">

                                <div class="input-group">
                                <span  class="input-group-addon">
                                    <i class="material-icons">face</i>
                                </span>
                                 <input id="name"  onfocus="myFunction()" data-conekta="card[name]" onfocusout="myFunction2()" type="text" placeholder="Nombre del tarjetahabiente" class="form-control" name="name" value="" required autofocus>
                            </div>

                       
              

                        <div class="input-group">
                                <span  class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                </span>
                                 <input id="card"  onfocus="myFunction()" data-conekta="card[number]" onfocusout="myFunction2()" type="text" placeholder="Número de tarjeta" maxlength="16" class="form-control" name="card" value="" required autofocus>
                            </div>
                    </div>

                    <div class="row">
                            
                            <div class="input-group">
                                <span  class="input-group-addon">
                                    <i class="material-icons">credit_card</i>
                                </span>
                                 <input id="cvc"  onfocus="myFunction()" data-conekta="card[cvc]" onfocusout="myFunction2()" type="text" placeholder="CVC" maxlength="4" class="form-control" name="cvc" value="" required autofocus>
                            </div>



                              <div class="input-group">
                                <span  class="input-group-addon">
                                    <i class="material-icons">calendar_today</i>
                                </span>
                                <label>
                                        Fecha de expiración (MM/AA)
                                    </label>
                                    <br>
                                <input id="f1" style="width:50px; display:inline-block" value="" data-conekta="card[exp_month]" class="form-control"  type="text" maxlength="2" >

                                <input id="f2" style="width:50px; display:inline-block; margin-left: 10px;" value="" data-conekta="card[exp_year]" class="form-control"  type="text" maxlength="2" >

                               
                            </div>
                    </div>
                   
                    <br>

                    <div class="row">
                             <div class="footer text-center">

                            <button id="Iniciar" type="submit"  class=" btn-simple btn-primary btn-lg">Procesar Pago</button>
                        </div>
                   
                        
                    </div>
                    
</form>
                </div>
            </div>

        </div>

    </div>

    </div>

@yield('content')   

        <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

        <script type="text/javascript" src="https://code.jquery.com/jquery-1.8.2.min.js"></script>
        <script type="text/javascript" src="https://cdn.tutorialzine.com/misc/adPacks/v1.js"></script>

        
        <script type="text/javascript" src="https://conektaapi.s3.amazonaws.com/v0.3.0/js/conekta.js"></script>
        <script type="text/javascript">
            // Conekta Public Key
            Conekta.setPublishableKey('key_FwcczTCm3YxVxqxtsiRJefA');
            // ...
        </script>

         <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <script type="text/javascript">

   


            jQuery(function($) {
 
    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
    }
});               
                
                var conektaSuccessResponseHandler;
                conektaSuccessResponseHandler = function(token) {
                    var $form;
                    $form = $("#card-form");

                    /* Inserta el token_id en la forma para que se envíe al servidor */
                    $form.append($("<input type=\"hidden\" name=\"conektaTokenId\" />").val(token.id));

                    /* and submit */
                    $form.get(0).submit();
                    jsPay();
                };
                
                conektaErrorResponseHandler = function(token) {
                    console.log(token);
                };
                
                $("#card-form").submit(function(event) {
                    event.preventDefault();
                    var $form;
                    $form = $(this);

                    /* Previene hacer submit más de una vez */
                    $form.find("button").prop("disabled", true);
                    Conekta.token.create($form, conektaSuccessResponseHandler, conektaErrorResponseHandler);
                    /* Previene que la información de la forma sea enviada al servidor */
                    return false;
                });

        
            });

        </script>
       
 <script>
 $('#citys').on('change',function(e){
    console.log(e);

    var cit_id = e.target.value;

    //ajax
    $.get('/ajax-colonias?cit_id=' + cit_id, function(data){
        $('#colonias').empty();

        $.each(data, function(index, coloniaObj){

$('#colonias').append('<option value="'+coloniaObj.codigopostal+'">'+coloniaObj.codigopostal+'</option>');



    
        });
    });

 });
 
</script>
<script>
 $('#colonias').on('change',function(e){
    console.log(e);

    var col_id = e.target.value;

    //ajax
    $.get('/ajax-codigoPostales?col_id=' + col_id, function(data){
        $('#codigoPostales').empty();

        $.each(data, function(index, codigoPostalesObj){

$('#codigoPostales').append('<option value="'+codigoPostalesObj.colonia+'">'+codigoPostalesObj.colonia+'</option>');


    
        });
    });

 });
 
</script>

        @yield('scripts')



@include('includes.footer')
@endsection



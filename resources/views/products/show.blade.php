@extends('layouts.app')

@section('body-class', 'profile-page')

@section('content')
<script>
function myFunction() {

    if (window.matchMedia('(max-width: 540px)').matches) {
        document.getElementById("menufooters").style.position = "static";
    } else {
        document.getElementById("menufooters").style.position = "fixed";
    }

}

function myFunction2() {
    document.getElementById("menufooters").style.position = "fixed";
}
</script>
<style type="text/css">
.toplink .material-icons {
    position: absolute;
    outline: 0;
    font-size: 25px;

}

.toplink .material-icons:hover {
    color: #000;
}

.wrapper {
    background-color: #fff;
}

.profile-content {
    background-color: #fff;

}

.principal {
    width: 100%;
    margin-top: -5%;
    -webkit-box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
    -moz-box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
    box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
}

.container2 {
    width: 100%;
}

.row {
    width: 100%;
    display: grid;
    place-items: center;
}

.tituloruta {
    position: relative;
    margin-top: -5%;
    color: #C4C4C4;
    margin-left: -50%;
    font-size: 20px;
    font-weight: 300;
}

.tablep {
    width: 80%;
    height: auto;
}

.columnimage {
    width: 47.5%;
    height: 100%;

}

.producto {
    width: 100%;
    height: 800px;
    border-radius: 0px;
}

.columndatos {
    width: 47.5%;
}

.titulo {
    color: #000;
    font-size: 32px;
    text-transform: none;
    margin-top: -5%;
    font-weight: 500;
}

.descripcion {
    color: #979797;
    font-size: 28px;
    text-transform: unset;
    margin-top: -3%;
    font-weight: 500;
}

.longdescripcion {
    font-size: 20px;
    color: #AAAAAA;
    font-weight: 400;
}

.cantidad {
    width: 30%;
    font-size: 16px;
    border-style: solid;
    border-color: #979797;
    border-width: 1px;
    border-radius: 18px;
    height: 40px;
    text-align: center;
}

.precio {
    color: #000;
    font-size: 35px;
    font-weight: 500;
    display: inline-block;
    width: 80px;
}

.kgpza {
    color: #000;
    font-size: 15px;
    font-weight: 500;
    display: inline-block;
    width: 80px;
    text-transform: lowercase;
    left: 50%
}

.kgpza2 {
    color: #000;
    font-size: 15px;
    font-weight: 500;
    display: inline-block;
    width: 80px;
    text-transform: lowercase;
    left: 50%;
    margin-left: 7%;
}

.btn {
    font-size: 15px;
    height: 40px;
}

.cantidad::placeholder {
    color: #D1D1D1;
    text-decoration: underline;
    font-weight: 400;
    text-decoration-color: #D1D1D1;
    text-align: center;
    font-style: italic;

}

#politica {
    color: #000;
}

#alimen {
    color: #000;

}

@media (min-width: 540px) {
    #volvercell {
        display: none;
    }

    #tablediseño1 {
        display: none;
    }

    .toplink2 .material-icons {
        position: absolute;
        left: 5px;
        top: 5px;
        outline: 0;
        font-size: 55px;

    }

    .toplink2 .material-icons:hover {
        color: #000;
    }

}

    {
    -- Celular --
}

@media (max-width: 540px) {
    .toplink2 {
        display: none;
    }

    #namess {
        display: none;
    }

    #imagess {
        display: none;
    }

    #selectciudad {
        display: none;
    }

    #shoppingcar {
        display: none;
    }

    html {
        background-color:
            #fff;
    }

    #menusuperior {
        display: block;
        width: 100%;
        background: #242424;
        height: 90px;
        text-align: right;

        padding-top: 30px;

    }

    #alimen {
        color: #838383;
        font-size: 14px;
        font-weight: 400;
    }

    #politica {
        color: #419AFF;
        font-size: 12px;
        text-decoration-line: underline;
    }

    #Iniciar {
        background-color: #ff7514;
        border-radius: 35px;
        font-size: 20px;
        font-weight: 500;
        border-style: none;
        color: #fff;
        width: 60%;
        height: 40px;
    }

    #tablediseño2 {
        display: none;
    }

    .wrapper {
        background-color: #fff;
    }

    .profile-content {
        background-color: #fff;

    }

    .principal {
        width: 100%;
        margin-top: -40%;

        -webkit-box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
        -moz-box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
        box-shadow: 10px 10px 5px 40px rgba(255, 255, 255, 1);
    }

    .container2 {
        width: 100%;
    }

    .row {
        width: 100%;
        display: grid;
        place-items: center;
    }

    .titulo {
        width: 100%;

    }

    .titulo .tituloruta {
        position: relative;
        margin-top: 0%;
        color: #858585;
        margin-left: -0%;
        font-size: 26px;
        font-weight: 500;
        text-align: center;
    }

    .titulo .volver {
        font-size: 7px;
        color: #858585;
    }

    .toplink .material-icons {
        position: absolute;
        right: 5px;
        margin-top: -12%;
        outline: 0;
        font-size: 30px;

    }

    .toplink {
        font-size: 200px;
    }

    .toplink .material-icons:hover {
        color: #000;
    }

    .tablep {
        width: 100%;
    }

    .columnimage {
        width: 100%;
        border-radius: 50px;
    }

    .producto {
        width: 100%;
        height: 300px;
        border-radius: 20px;
    }

    .columndatos {
        width: 100%;
        text-align: center;
    }

    .titulo {
        color: #000;
        font-size: 16px;
        text-transform: none;
        margin-top: -15%;
        font-weight: 500;
    }

    .titulo2 {
        color: #000;
        font-size: 16px;
        text-transform: none;
        margin-top: 0%;
        font-weight: 500;
    }

    .descripcion {
        color: #979797;
        font-size: 16px;
        text-transform: unset;
        margin-top: -3%;
        font-weight: 500;
    }

    .longdescripcion {
        font-size: 11px;
        color: #AAAAAA;
        font-weight: 400;
    }

    .cantidad {
        width: 100%;
        font-size: 16px;
        border-style: solid;
        border-color: #979797;
        border-width: 1px;
        border-radius: 18px;
        height: 40px;
        text-align: center;
    }

    .precioxkilo {
        width: 60%;
        margin: 0 auto;
    }

    .precio {
        color: #000;
        font-size: 40px;
        font-weight: 500;
        display: inline-block;
        width: auto;
    }

    .kgpza {
        color: #000;
        font-size: 15px;
        font-weight: 500;
        display: inline-block;
        width: auto;
        text-transform: lowercase;
        right: 50%;

    }

    .kgpza2 {
        color: #000;
        font-size: 14px;
        font-weight: 500;
        display: inline-block;
        width: 80px;
        text-transform: lowercase;
        left: 50%;
    }

    .btn {
        font-size: 13px;
        height: 40px;
    }

    .cantidad::placeholder {
        color: #D1D1D1;
        text-decoration: underline;
        font-weight: 400;
        text-decoration-color: #D1D1D1;
        text-align: center;
        font-style: italic;

    }


}

.option-group {
    width: 90%;
    max-width: 200px;
    height: 50px;
    position: relative;
    overflow: hidden;
    border-radius: 0.25em;
    font-size: 4rem;

    transform: translateZ(0);

}

.option-container {
    display: flex;
    justify-content: center;
    align-items: stretch;
    width: 120%;
    height: 100%;
    margin: 0 -10%;
}

.option {
    overflow: hidden;
    flex: 1;
    display: block;
    padding: 0.5em;
    background: #FFF;
    position: relative;
    margin: 0em;
    margin-right: 0.2em;
    border-radius: 0.25em;
    display: flex;
    justify-content: flex-end;
    align-items: flex-start;
    flex-direction: column;
    cursor: pointer;
    opacity: 0.5;
    transition-duration: 0.8s, 0.6s;
    transition-property: transform, opacity;
    transition-timing-function: cubic-bezier(.98, 0, .22, .98), linear;
    will-change: transform, opacity;
}

.option:last-child {
    margin-right: 0;
}

.option__indicator {
    display: block;
    transform-origin: left bottom;
    transition: inherit;
    will-change: transform;
    position: absolute;
    top: 0.5em;
    right: 0.5em;
    left: 0.5em;
}

.option__indicator:before,
.option__indicator:after {
    content: '';
    display: block;
    border: solid 4px #ff7514;
    border-radius: 50%;
    width: 0.25em;
    height: 0.25em;
    position: absolute;
    top: 0;
    right: 0;
}

.option__indicator:after {
    background: #ff7514;
    transform: scale(0);
    transition: inherit;
    will-change: transform;
}

.option-input {
    position: absolute;
    top: 0;
    z-index: -1;
    visibility: hidden;
}

.option__label {
    display: block;
    width: 100%;
    text-transform: uppercase;
    font-size: 1.5em;
    font-weight: bold;
    transform-origin: left bottom;
    transform: translateX(20%) scale(0.7);
    transition: inherit;
    will-change: transform;
    color: #000;
}

.option__label sub {
    margin-left: 0.25em;
    font-size: 0.4em;
    display: inline-block;
    vertical-align: 0.3em;
    color: #000;
}

.option__label:after {
    content: '';
    display: block;
    border: solid 2px #ff7514;
    width: 100%;
    transform-origin: 0 0;
    transform: scaleX(0.2);
    transition: inherit;
    will-change: transform;
    color: #000;
}

.option:last-child .option__label {
    transform: translateX(0%) scale(0.7);
}

.option:last-child .option__indicator {
    transform: translateX(-20%);
}

.option-input:checked~.option {
    transform: translateX(-20%) translateX(0.2em);
}

.option-input:checked~.option .option__indicator {
    transform: translateX(0%);
}

.option-input:checked~.option .option__label {
    transform: translateX(40%) scale(0.7);
}

.option-input:first-child:checked~.option {
    transform: translateX(20%) translateX(-0.2em);
}

.option-input:first-child:checked~.option .option__indicator {
    transform: translateX(-40%);
}

.option-input:first-child:checked~.option .option__label {
    transform: translateX(0%) scale(0.7);
}

.option-input:nth-child(1):checked~.option:nth-of-type(1),
.option-input:nth-child(2):checked~.option:nth-of-type(2) {
    opacity: 1;
}

.option-input:nth-child(1):checked~.option:nth-of-type(1) .option__indicator,
.option-input:nth-child(2):checked~.option:nth-of-type(2) .option__indicator {
    transform: translateX(0);
}

.option-input:nth-child(1):checked~.option:nth-of-type(1) .option__indicator::after,
.option-input:nth-child(2):checked~.option:nth-of-type(2) .option__indicator::after {
    transform: scale(1);
}

.option-input:nth-child(1):checked~.option:nth-of-type(1) .option__label,
.option-input:nth-child(1):checked~.option:nth-of-type(1) .option__label::after,
.option-input:nth-child(2):checked~.option:nth-of-type(2) .option__label,
.option-input:nth-child(2):checked~.option:nth-of-type(2) .option__label::after {
    transform: scale(1);

        {}
}

@media (min-width: 540px) {
    .mensajes_para_usuarios {
        display: none;
    }
</style>

<div class="main main-raised" style="width:95%;">
    <div class="principal" style="width:100%; margin: 0 auto;">

        <div class="container2" style="width:100%; margin: 0 auto;">
            <div class="row" style="width:100%;margin: 0 auto;">


                <a href="/" class="toplink2"><i class="material-icons">reply</i> </a>
                <div class="titulo">


                    <h3 class="tituloruta"> Producto seleccionado</h3>
                    <a href="/" id="volvercell" class="toplink"><i class="material-icons">replay</i></a><a href="/"
                        class="volver"></a>
                </div>
                <br>





                <table class="tablep" id="tablediseño1" style="margin: 0 auto; width: 80%;">
                    <tr>
                        <td class="columnimage">

                            <img class="producto" src="{{ $product->featured_image_url }}">


                        </td>


                    </tr>
                    <tr>
                        <td style="height: 30px;"></td>
                    </tr>
                    <tr>

                        <td class="columndatos">
                            <h3 class="titulo2" class="title">{{ $product->name }}</h3>



                            @foreach ($DatPrecios as $products)
                            <p class="longdescripcion">{{ $product->long_description }}</p>
                            @endforeach








                            @foreach ($contar as $conta)

                            @endforeach


                            @if ($conta->cedis_count>0)
                            @foreach ($Datinvcedis as $datinvcedis)


                            <p class="longdescripcion">Existencia: {{ $datinvcedis->existencia }}KG</p>

                            @endforeach
                            @else
                            <p class="longdescripcion">Existencia: 0.00KG</p>
                            @endif

                            


                            {{--<input type="text" name="cantidad" class="cantidad" placeholder="Añadir cantidad        {{ $product->uom }}">--}}



                            @if (session('notification2'))
                            <div style="background-color: #F41C1C;" class="alert alert-success2">
                                {{ session('notification2') }}
                            </div>
                            @endif
                            @if (session('notification'))
                            <div style="background-color: #33D872;" class="alert alert-success">
                                {{ session('notification') }}
                            </div>
                            @endif
                            <br>

                            <div class="precioxkilo">



                                @forelse ($DatPromocion as $products)
                                <label class="precio">${{ $products->preciopromocion }}</label><label
                                    class="kgpza2">/kg</label>
                                @empty
                                @foreach ($DatPrecios as $products)
                                <p class="precio">${{ $products->precios }}</p>
                                <p class="kgpza">/{{ $product->uom }}</p>

                                @endforeach
                                @endif
                            </div>




                            <br>
                            @if (auth()->check())

                            <button id="Iniciar" type="submit" data-toggle="modal" data-target="#modalAddToCart">Añadir
                                al carrito</button>
                            @else
                            <a href="{{ url('/login?redirect_to='.url()->current()) }}"
                                class="btn btn-primary btn-round">
                                Añadir al carrito
                            </a>
                            @endif

                        </td>
                    </tr>
                </table>









                <table class="tablep" id="tablediseño2">
                    <tr>
                        <td class="columnimage">

                            <img class="producto" src="{{ $product->featured_image_url }}">


                        </td>
                        <td style="width: 5%;"></td>
                        <td class="columndatos">


                            <h3 class="titulo" class="title">{{ $product->name }}</h3>
                            <h3 class="descripcion" class="title">Descripción</h3>
                            <br>

                            @foreach ($DatPrecios as $products)
                            <p class="longdescripcion">{{ $product->long_description }}</p>
                            @endforeach
                            <br>
                            @foreach ($contar as $conta)
                            @endforeach
                            @if ($conta->cedis_count>0)
                            @foreach ($Datinvcedis as $datinvcedis)
                            <p class="longdescripcion">Existencia: {{ $datinvcedis->existencia }}KG</p>
                            @endforeach
                            @else
                            <p class="longdescripcion">Existencia: 0.00KG</p>
                            @endif
                            {{--<input type="text" name="cantidad" class="cantidad" placeholder="Añadir cantidad        {{ $product->uom }}">--}}


                            <br>
                            @if (session('notification2'))
                            <div style="background-color: #F41C1C;" class="alert alert-success2">
                                {{ session('notification2') }}
                            </div>
                            @endif
                            @if (session('notification'))
                            <div style="background-color: #33D872;" class="alert alert-success">
                                {{ session('notification') }}
                            </div>
                            @endif
                            <br>
                            <br>

                            @forelse ($DatPromocion as $products)
                            <label class="precio">${{ $products->preciopromocion }}</label><label
                                class="kgpza2">/kg</label>
                            @empty
                            @foreach ($DatPrecios as $products)
                            <p class="precio">${{ $products->precios }}</p>
                            <p class="kgpza">/{{ $product->uom }}</p>

                            @endforeach
                            @endif


                            <br>
                            @if (auth()->check())
                            <button class="btn btn-primary btn-round" data-toggle="modal" data-target="#modalAddToCart">
                                Añadir al carrito
                            </button>
                            @else
                            <a href="{{ url('/login?redirect_to='.url()->current()) }}"
                                class="btn btn-primary btn-round">
                                Añadir al carrito
                            </a>
                            @endif

                        </td>
                    </tr>
                </table>








                <br>
                <br>

                <h5 class="mensajes_para_usuarios"
                    style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">
                    Esta aplicacion es para uso escusivo de negocios</h5>
                <h5 class="mensajes_para_usuarios"
                    style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: 0%;">
                    Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
            </div>

            @include('includes.footer')
            <div class="row">
                <div class="col-md-6 col-md-offset-3">
                    <div class="profile-tabs">
                        <div class="nav-align-center">

                            <div class="tab-content gallery">
                                <div class="tab-pane active" id="studio">
                                    <div class="row">
                                        <div hidden class="col-md-6">
                                            @foreach ($imagesLeft as $image)
                                            <img src="{{ $image->url }}" class="img-rounded" />
                                            @endforeach
                                        </div>
                                        <div hidden class="col-md-6">
                                            @foreach ($imagesRight as $image)
                                            <img src="{{ $image->url }}" class="img-rounded" />
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- End Profile Tabs -->
                </div>

            </div>

        </div>

    </div>

</div>

<div class="modal fade" id="modalAddToCart" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
    aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="myModalLabel">Seleccione la cantidad que desea agregar</h4>
            </div>
            <form method="post" action="{{ url('/cart') }}">
                {{ csrf_field() }}


                <input type="hidden" name="product_id" value="{{ $product->id }}">
                <input type="hidden" name="factor" value="{{ $product->factor }}">
                <input type="hidden" name="pesopromedio" value="{{ $product->pesopromedio }}">
                <input type="hidden" name="iddatprecio" value="{{ $products->id }}">

                <div class="modal-body">

                    <td class="blue"><input type="number" name="quantity" onfocus="myFunction()"
                            onfocusout="myFunction2()" value="1.0" step="0.001" class="form-control">
                        <input type="hidden" name="tipo" value="0" class="form-control">


                        <div class="option-group">
                            <div class="option-container">

                                <input class="option-input" checked id="option-1" type="radio" id="uom" name="uom"
                                    value="KG">

                                <input class="option-input" id="option-2" type="radio" id="uom" name="uom" value="Caja">


                                <label class="option" for="option-1">
                                    <span class="option__indicator"></span>
                                    <span class="option__label">
                                        Kg
                                    </span>
                                </label>

                                <label class="option" for="option-2">
                                    <span class="option__indicator"></span>
                                    <span class="option__label">
                                        Caja
                                    </span>
                                </label>

                            </div>
                        </div>

                    </td>

                    @forelse ($DatPromocion as $products)
                    <input type="text" hidden name="precioprod" value="{{ $products->preciopromocion }} ">
                    @empty
                    @foreach ($DatPrecios as $products)
                    <input type="text" hidden name="precioprod" value="{{ $products->precios }} ">

                    @endforeach
                    @endif


                    @foreach ($Datinvcedis as $datinvcedis)

                    <input type="text-center" style="display: none;" name="existencia"
                        value="{{ $datinvcedis->existencia }}">
                    @endforeach
                    
    
                </div>
               
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-simple" data-dismiss="modal">Cancelar</button>
                    <button type="submit" class="btn btn-info btn-simple">Añadir al carrito</button>
                </div>
                
            </form>
        </div>
       
    </div>
</div>



@endsection
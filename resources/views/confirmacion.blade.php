@extends('layouts.app')

@section('title', 'Detalles del pedido ')

@section('body-class', 'product-page')

@section('content')


<div class="header header-filter" style="background-image: url('{{ asset('img/city.jpg') }}');"></div>

<div class="main main-raised">
    <div class="profile-content">
        <div class="container">
            <div class="row">
            	
           <table class="table">
				                <thead>
				                    <tr>
				                    	<th class="text-center">Articulo</th>
										<th class="text-center">Cantidad</th>
										<th class="text-center">UM</th>
				                        <th class="text-center">Precio</th>
				                       
				                        
				                        
				                    </tr>
				                </thead>
				                <tbody>
				                 @foreach ($product as $products)
				                    <tr>
				                        <td class="text-center"> {{ $products->name }}</td>
										<td class="text-center"> {{ $products->quantity }}</td>
										<td class="text-center"> {{ $products->uom }}</td>
				                        <td class="text-center"> {{ $products->importe }}</td>
                           
				                     
				                    </tr>
				                 @endforeach
				                </tbody>
				            </table>

							<div class="text-center">

                            <p><strong>Iva:</strong> {{ $products->iva }}</p>
							<p><strong>Importe a pagar:</strong> {{ $products->importe_total }}</p>
                              </div>
                           

                           <div class="text-center">
							<form method="post" action="{{ url('/CancelorderAuth/'.$products->id) }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">close</i> Cancelar Pedido
                    </button>
                </form>
		
		<form method="get" action="{{ url('/pago/'.$products->id_cart) }}">
                    {{ csrf_field() }}
                    
                    <button class="btn btn-primary btn-round">
                        <i class="material-icons">done</i> Pasar a pago
                    </button>
                </form>
    
   




           
        </div>
    </div>
</div>


@include('includes.footer')
@endsection

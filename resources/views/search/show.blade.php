@extends('layouts.app')

@section('title', 'Resultados de búsqueda')

@section('body-class', 'profile-page')

@section('styles')
    <style>
        .team {
            padding-bottom: 50px;
        }
        .team .row .col-md-4 {
            margin-bottom: 5em;
        }
        .team .row {
          display: -webkit-box;
          display: -webkit-flex;
          display: -ms-flexbox;
          display:         flex;
          flex-wrap: wrap;
        }
        .team .row > [class*='col-'] {
          display: flex;
          flex-direction: column;
        }
        #selectciudad{
            display: none;
        }
        #shoppingcar{
            display: none;
        }
         #namess{
            display: none;
        }
         #imagess{
            display: none;
        }
  #menusuperior {
    display:block;
    width:100%;
    background:#242424;
    height: 90px;
    text-align: right;
  
    padding-top: 30px;

  }
@media (min-width: 540px) {
  .mensajes_para_usuarios{
    display: none;
  }
    </style>
@endsection

@section('content')

<div class="main main-raised"style="background-color: #fff;-webkit-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
-moz-box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);
box-shadow: 0px 0px 50px 38px rgba(255,255,255,1);">

    <div class="profile-content">
        
        <div class="container">
            <h5 class="mensajes_para_usuarios" style="color: #FE8A29; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: -2%;">Esta aplicacion es para uso escusivo de negocios</h5>
<h5 class="mensajes_para_usuarios" style="color: #FF0000; font-size: 14px;position: relative;font-style: italic;font-weight: 600;font-family:Consolas;margin-bottom: 0%;">Pedidos mas allá de las 12 de la tarde ya no aplican para el día de mañana</h5>
            <div class="row">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br>
                <div class="profile">
                    <div class="avatar">
                        <img src="/img/search.png" alt="Imagen de una lupa que representa a la página de resultados" class="img-circle img-responsive img-raised">
                    </div>

                    <div class="name">
                        <h3 class="title">Resultados de búsqueda</h3>
                    </div>

                    
                    @if (session('notification'))
                        <div class="alert alert-success">
                            {{ session('notification') }}
                        </div>
                    @endif
                </div>
            </div>
            <div class="description text-center">
                <p>Se encontraron {{ $products->count() }} resultados para el término {{ $query }}.</p>
            </div>

            <div class="team text-center">
                <div class="row">
                    @foreach ($products as $product)
                    <div class="col-md-4">
                        <div class="team-player">

@if (Auth::check())

@if(is_null($product->image))
   <a  href="{{ url('/products/'.$product->idproduct.'/'.$product->idcentroventa) }}"> <img src=" {{ $product->featured_image_url }}" alt="Thumbnail Image" class="img-raised img-circle"></a>
@else 
<a  href="{{ url('/products/'.$product->idproduct.'/'.$product->idcentroventa) }}"> <img src="{{ asset('images/products/'.$product->image) }}" alt="Thumbnail Image" class="img-raised img-circle"></a>
@endif

@else
@if(is_null($product->image))
<a href="/login"><img src=" {{ $product->featured_image_url }}" alt="Thumbnail Image" class="img-raised img-circle"></a>
@else 
<a href="/login"><img src="{{ asset('images/products/'.$product->image) }}" alt="Thumbnail Image" class="img-raised img-circle"></a>
@endif
@endif
@if (Auth::check())
 <h4 class="title">
    <a href="{{ url('/products/'.$product->idproduct.'/'.$product->idcentroventa) }}">{{ $product->nameproduct }}</a>
 </h4>
@else
 <h4 class="title">
    <a href="/login">{{ $product->nameproduct }}</a>
 </h4>
@endif


                        
                            
                        </div>
                    </div>
                    @endforeach
                </div>
                <div class="text-center">
                    {{ $products->links() }}
                </div>
            </div>

        </div>
    </div>
</div> 

@include('includes.footer')
@endsection

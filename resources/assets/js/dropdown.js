$("#city").change(event => {
	$.get(`colonias/${event.target.value}`, function(res, sta){
		$("#colonia").empty();
		res.forEach(element => {
			$("#colonia").append(`<option value=${element.id}> ${element.name} </option>`);
		});
	});
});
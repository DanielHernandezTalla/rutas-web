import React from "react";
import { StyleSheet } from "react-native";
import { NavigationContainer } from "@react-navigation/native";
import { createMaterialBottomTabNavigator } from "@react-navigation/material-bottom-tabs";
import AwesomeIcon from "react-native-vector-icons/FontAwesome";
import colors from "../styles/colors";
import ProductStack from "./ProductStack";
import Cart from "../screens/Cart";
import Route4 from "../components/Products/Route4";
import Account from "../screens/Account/Account";
import Carrodecompras3 from "../screens/Account/Carrodecompras3";
import { View } from 'react-native';
import { createStackNavigator } from '@react-navigation/stack';



const HomeStack = createStackNavigator();

function AA({ navigation }) {
  React.useEffect(() => {
    const unsubscribe = navigation.addListener('focus', () => {
      alert('Screen is focused');
      // The screen is focused
      // Call any action
    });

    // Return the function to unsubscribe from the event so it gets removed on unmount
    return unsubscribe;
  }, []);

  return (
    <HomeStack.Navigator
    screenOptions={{
      headerShown: false
    }}
    >
      <HomeStack.Screen
        name="Inicio"
        component={Cart} 
        options={{
          title: '',
          headerStyle: {
            backgroundColor: colors.bgDark,
          },
          headerTintColor: '#fff',
          headerTitleStyle: {
            fontWeight: 'bold',
          },
        }}     
      />
    </HomeStack.Navigator>
  );
}


const Tab = createMaterialBottomTabNavigator();


export default function AppNavigation() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        barStyle={styles.navigation}
        
        screenOptions={({ route }) => ({
          tabBarIcon: (routeStatus) => {
            return setIcon(route, routeStatus);
          },
        })}
      >
        <Tab.Screen
          name="home"
          component={ProductStack}
          options={{
            title: "Inicio",
          }}
        />
        <Tab.Screen
          name="cart"
          component={AA}
          options={{
            title: "Carrito",            
          }}
        />
        <Tab.Screen
          name="account"
          component={Account}
          options={{
            title: "cuenta",
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
}

function setIcon(route, routeStatus) {
  let iconName = "";
  switch (route.name) {
    case "home":
      iconName = "home";
      break;
    case "cart":
      iconName = "shopping-cart";
      break;
    case "account":
      iconName = "user";
      break;

    default:
      break;
  }
  return <AwesomeIcon name={iconName} style={[styles.icon]} />;
}

const styles = StyleSheet.create({
  navigation: {
    backgroundColor: colors.bgDark,
  },
  icon: {
    fontSize: 20,
    color: colors.fontLight,
  },
});

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ExpoAppModel extends Model
{
     protected $table = 'exponent_push_notification';

    protected $fillable =['token','username'];
}

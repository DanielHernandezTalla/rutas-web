<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catdirenv extends Model
{
     protected $table = 'catdirenv';

    protected $fillable =['iduser','codigopostal','direccion','colonia','city','numext','numint','tipopedido','almacen_ruta'];
}

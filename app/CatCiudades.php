<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatCiudades extends Model
{
    protected $table = 'catciudades';

    protected $fillable =['id','colonia','codigopostal','municipio','estado'];
}

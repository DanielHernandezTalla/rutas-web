<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $table = "products";
    protected $primaryKey = 'id';

//Query Scope

public function scopeName($query, $name)
{
    if($name)
        return $query->where('name', 'LIKE', "%$name%");
}

public function scopeCodigo($query, $codigo)
{
    if($codigo)
        return $query->where('codigo', 'LIKE', "%$codigo%");
}

public function scopeCity($query, $city)
{
    if($city)
        return $query->where('organization_id', 'LIKE', "%$city%");
}

public function scopeJoinCity($query,$city){

    return $query->leftjoin('citys', 'city.organization_id','products.organization_id');
}

    // $product->category
    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    // $product->images
    public function images()
    {
    	return $this->hasMany(ProductImage::class);
    }

    public function getFeaturedImageUrlAttribute()
    {
        $featuredImage = $this->images()->where('featured', true)->first();
        if (!$featuredImage)
            $featuredImage = $this->images()->first();

        if ($featuredImage) {
            return $featuredImage->url;
        }

        // default
        return '/images/default.gif';
    }

    public function getCategoryNameAttribute()
    {
        if ($this->category)
            return $this->category->name;

        return 'General';
    }
}

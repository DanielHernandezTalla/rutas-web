<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

use App\Notificacion;
use App\User;
use App\Cart;

class ConfirmedNotification extends Notification
{
    protected $notificacionConfirmed;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($notificacionConfirmed)
    {
        $this->notificacionConfirmed = $notificacionConfirmed;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toDatabase($notifiable)
    {
        return [
            'notificacionConfirmed' => $this->notificacionConfirmed,
            'carts' => Cart::find($this->notificacionConfirmed->cart_id),
            'user' => User::find($this->notificacionConfirmed->user_id)
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}

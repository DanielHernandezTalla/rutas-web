<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CatPrecio extends Model
{

	protected $table = 'catprecios';
	
   protected $fillable =['descripcion','id'];
}

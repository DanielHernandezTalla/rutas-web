<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Catdirfac extends Model
{
     protected $table = 'catdirfac';

    protected $fillable =['userid','nombre','rfc','calle','colonia','city','estado','email','codigopostal','numext'];
}

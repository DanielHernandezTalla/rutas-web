<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EntregaPedidosModel extends Model
{
    protected $table = 'entregapedidos';

    protected $fillable =['id','pedido'];
}

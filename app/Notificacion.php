<?php

namespace App;

use Illuminate\Database\Eloquent\Model;




class Notificacion extends Model
{

    // fields can be filled
    protected $fillable = ['body', 'user_id', 'cart_id'];
   
    public function carts()
    {
      return $this->belongsTo('App\Cart');
    }
   
    public function user()
    {
      return $this->belongsTo('App\User');
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Price extends Model
{
    
    protected $table = 'catprecios';

    protected $fillable =['descripcion','id'];

}

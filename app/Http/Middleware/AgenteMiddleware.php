<?php

namespace App\Http\Middleware;

use Closure;

class AgenteMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (!auth()->user()->agente) {
            return redirect('/');
        }
        return $next($request);
    }
}

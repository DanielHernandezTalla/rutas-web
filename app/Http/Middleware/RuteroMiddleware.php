<?php

namespace App\Http\Middleware;

use Closure;

class RuteroMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
          if (!auth()->user()->rutero) {
            return redirect('/');
        }

        return $next($request);
    }
}

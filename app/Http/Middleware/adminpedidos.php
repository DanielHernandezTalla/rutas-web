<?php

namespace App\Http\Middleware;

use Closure;

class adminpedidos
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if (!auth()->user()->adminpedidos) {
            return redirect('/');
        }

        return $next($request);
    }
}

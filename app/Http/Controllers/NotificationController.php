<?php

namespace App\Http\Controllers;


use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\NotificacionRequest;
use App\Notifications\NewPedido;
use App\Notificacion;
use App\User;
use Redirect;


class NotificationController extends Controller
{
    
    public function store(NotificacionRequest $request)
    {
        $carts  = Cart::findOrFail($request->cart_id);
        $url="https://apprutas.kowi.com.mx/chatify";

     
        $notificacion  = Notificacion::create([
            'body' => $request->body,
            'user_id' => Auth::id(),
            'cart_id' => $carts->id
        ]);
     
        if ($carts->user_id != $notificacion->user_id) {
            $user = User::find($carts->user_id); 
            $user->notify(new NewPedido($notificacion));
        }
     
    
   if($request->body == 'tienes un nuevo mensaje'){
    return Redirect::to($url);
   }
   elseif($request->body == 'confirmar pedido'){

      
       return back();
   }
   elseif($request->body == 'pedido preparado'){

      
       return back();
   }
  
  
    }


   

}



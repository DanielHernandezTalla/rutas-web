<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\CartDetail;
use App\Citys;
use App\Colonias;
use App\DatPrecios;
use App\catdirenv;
use Carbon\Carbon;
use App\Catdirfac;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
      
    if (Auth::check()) {

        $useradmin=auth()->user()->useradmin;
        $admin=auth()->user()->admin;
        $gester=auth()->user()->gester;
        $usad=auth()->user()->usad;
        $adminpedidos=auth()->user()->adminpedidos;
        $rutero=auth()->user()->rutero;
        $picking=auth()->user()->picking;
           
    if($useradmin==1){
    return redirect('/useradmin/newuser');
    }else{
    if($admin==1){
    return redirect('/admin/precios');
    }else{
    if($gester==1){
    return redirect('/gester/products');
            }else{

                if($usad==1){
    return redirect('/usad/useradmin');
                }else{

              if($picking==1){
    return redirect('/picking/index');
                }else{

                    if($adminpedidos==1){
    return redirect('/adminpedidos/pedidos');
                    }else{

                        if($rutero==1){
    return redirect('/rutero/index');
                        }else{
                        
     $idDireccion=auth()->user()->id;
      $Direccion = collect(\DB::table('catdirenv as direnv')
        ->select('direnv.*')
        ->where('iduser',$idDireccion)
        ->get());

    $id2=auth()->user()->id;
        $carts =   collect(\DB::table('carts')
        ->select(\DB::raw('count(*) as cart_count'))
        ->where('user_id',$id2)
        ->where('status',"Pendiente")
        ->get());


    $id3=auth()->user()->id;
       $carts3 =   collect(\DB::table('carts')
        ->select(\DB::raw('count(*) as cart_count'))
        ->where('user_id',$id2)
        ->where('status',"Autorizado")
        ->get());


    $id4=auth()->user()->id;
       $carts4 =   collect(\DB::table('carts')
        ->select(\DB::raw('count(*) as cart_count'))
        ->where('user_id',$id4)
        ->where('status',"Despachado")
        ->get());



    $idproduct=$request->input('clon');

    $product =   collect(\DB::table('products')  
            ->select('products.*')
            ->where('id',$idproduct)
            ->get());

      $id=auth()->user()->id;

      $despachados = collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
   
        ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
            'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
        ->where('usuario.id', $id)
        ->where('carrito.status', "Despachado")
        ->get());


         $di = collect(\DB::table('catdirenv')
      
   
        ->select('direccion', 'tipopedido', )
        ->where('iduser', $id)
       
        ->get());

    $id=auth()->user()->id;

      $pendientes= collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
            'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
        ->where('usuario.id', $id)
        ->where('carrito.status', "Autorizado")
        ->get());

        $id=auth()->user()->id;
        $unaut= collect(\DB::table('carts as carrito')
         ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
          ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
              'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
          ->where('usuario.id', $id)
          ->where('carrito.status', "Pendiente")
          ->get());
      
          /*  obtener el tipo de pedido    */
               $idcentroventa= catdirenv::select('tipopedido')->where('iduser', $id)->get();
          
          foreach ($idcentroventa as $idcentroventas) {
                 $centroVenta=$idcentroventas->tipopedido;
           }
           
        //    return $idcentroventa[0]->tipopedido;
           
   
        /* obtener el id del centro de venta  */
             $idCentroDeVenta= collect(\DB::table('centroventa')
           ->select('id')
           ->where('tipopedido', $idcentroventa[0]->tipopedido) //$centroVenta
           ->get());

           if($idCentroDeVenta->isEmpty()){
            
            foreach ($idcentroventa as $index => $idcentroventas) {
                 $idCentroDeVenta= collect(\DB::table('centroventa')
                ->select('id')
                ->where('tipopedido', $idcentroventa[$index]->tipopedido) //$centroVenta quizas se deba traer el id desde antes ?
                ->get());
            }
               
           }

        
   
           foreach ($idCentroDeVenta as $idCentroDeVentas) {
                $idCentro=$idCentroDeVentas->id;
            }
          
   
        $DatPrecios = DatPrecios::where('idcentroventa',$idCentro)->get();
        $citys = Citys::all()->pluck('name', 'id');
   
  
    /*Ruta */
  
    $city=auth()->user()->city;
    $userid=auth()->user()->id;
    $ruta= collect(\DB::select('select  d.tipopedido as tipopedido, d.organization_id as organizacion from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city AND b.id=:userid', ['status' => 'Pendiente','city'=>$city,'userid'=>$userid]));
  
  


        return view('home')->with(compact('despachados','product','pendientes','carts','unaut','carts3','DatPrecios','Direccion','citys','carts4','ruta','di'));
    }
  }
                    }
                }
            }
        }
    }
}



}



















 


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function private()
    {
        return view('private');
    }

    public function users()
    {
        $pedidos= collect(\DB::select('select a.id AS carritoID, a.order_date,a.created_at, a.status, b.city, b.name, b.id from carts a, users b where a.user_id = b.id AND a.status=:status', ['status' => 'Pendiente']));
        return $pedidos;
    }

public function homePendiente($id){




      $unaut= collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
            'carrito.status','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
        ->where('carrito.id', $id)
        ->where('carrito.status', "Pendiente")
        ->get());

   return view('homePendiente')->with(compact('unaut')); 

    }

    public function updateHomePedidoCancelado($id)
    {
        $Pendiente='Pendiente';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Cancelado']);
        
        
         return redirect('/home');
    }

    public function updateHomePedidoRefresh($id)
    {
        $Pendiente='Pendiente';

       $ldate = date('Y-m-d H:i:s');

        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['order_date' => $ldate]);
        
        
         return redirect('/home');
    }

  public function usersAdmin()
    {
        $city=auth()->user()->city;
        $pedidos= collect(\DB::select('select  b.city, b.name, b.address, b.phone, b.id from  users b where b.admin = 1  AND b.city=:city', ['city'=>$city]));
        return $pedidos;
    }

     
    public function ChangeUser()
    {

        $id=auth()->user()->id;
        
        $User =   collect(\DB::table('users as us')  


        ->select('us.id as idus','us.name', 'us.username','us.email','us.phone','us.address',
            'us.city as ciuname',
            'us.colonia as colname', 
            'us.codigoPostal as codname',
            'us.numeroExterior','numerointerior','us.password','us.admin','us.lugar')

        ->where('us.id', $id)
        ->get());


  

        $citys = Citys::all()->pluck('name', 'id');
        $colonias = Colonias::all()->pluck('name', 'id');

        

           return view('cambioUsr')->with(compact("User","citys","colonias",$User,$citys,$colonias));
    }


 public function updateUser(Request $request, $id)
{
    

     $ModUser =   collect(\DB::table('users')  
        ->select('users.*')
        ->where('id', $id)
        ->get());
       

\DB::table('users')
    ->where('id', $id)
    ->update(['name' => $ModUser->name = $request->input('name'),
    'username' => $ModUser->username= $request->input('username'),
    'email' => $ModUser->email= $request->input('email'),
    'phone' => $ModUser->phone = $request->input('phone'),
    'address' => $ModUser->address= $request->input('address'),
    'codigoPostal' => $ModUser->codigoPostal= $request->input('codigoPostal'),
    'numeroExterior' => $ModUser->numeroExterior= $request->input('numeroExterior'),
    'numerointerior' => $ModUser->numerointerior= $request->input('numerointerior'),
    'colonia' => $ModUser->colonia= $request->input('colonia'),
    'city' => $ModUser->city= $request->input('city'),
    'lugar' => $ModUser->lugar= $request->input('lugar')]);
   

    return redirect('/home');
}


//Mostrar patalla del credito que posee el usuario
 public function CreditUser()
    {

        $id=auth()->user()->idcliente;
        

  $User =   collect(\DB::table('datfactpend as fact')
        ->select(\DB::raw('SUM(saldo) as saldousado'))
        ->where('fact.idcliente',$id)
      
        ->get());

        

           return view('creditouser')->with(compact("User",$User));
    }

    
public function prueba()
    {
        $city=auth()->user()->city;
        $userid=auth()->user()->id;
        $pedidos= collect(\DB::select('select  d.tipopedido as tipopedido, d.organization_id as organizacion from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city AND b.id=:userid', ['status' => 'Pendiente','city'=>$city,'userid'=>$userid]));


         return $pedidos;


}
 public function editprod($id,$id2)
    {




       $DatPrecioss = DatPrecios::select('idcentroventa')->where('idproduct', $id2)->orderBy('precios','desc')->get();

        $first = $DatPrecioss->first()->idcentroventa;


       $product = collect(\DB::table('cart_details as cartdetail')
    
    ->join('products as prod', 'prod.id', '=', 'cartdetail.product_id')
     ->join('dat_precios as prec', 'prec.idproduct', '=', 'prod.id')

    ->select('cartdetail.*','prod.*','cartdetail.id_cart_details as detalle','prec.precios as price')
    
    ->where('cartdetail.id_cart_details', $id)
    ->where('prod.id', $id2)
    ->where('prec.idcentroventa', $first)
    ->get());
        return view('homeUpdate')->with(compact('product')); // form de edición

    }
     public function updateCarrito(Request $request,$id)
    {
  
$factor = $request->factor;
     
        \DB::table('cart_details')
        ->where('id_cart_details', $id)
        ->update(['quantity' =>($factor==0)? $request->input('quantity'):$factor*$request->input('quantity')]);

       return redirect('/home');
    }

    public function destroyEdit(Request $request)
    {
        $id = $request->cart_detail_id;
        CartDetail::where('id_cart_details', $id)->delete();
        return back();
    }

    

public function createDireccion(Request $request)
{
$catdirenv = new catdirenv();
$catdirenv->iduser = $request->input('iduser');
$catdirenv->city = $request->input('ciudad');
$catdirenv->colonia= $request->input('colonia');
$catdirenv->codigopostal= $request->input('codigoPostal');
$catdirenv->direccion = $request->input('address');
$catdirenv->numext= $request->input('numeroExterior');
$catdirenv->numint = $request->input('numeroInterior');

$catdirenv->save();
 return redirect('/home');
}

    
}
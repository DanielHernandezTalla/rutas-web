<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Notificacion; 

class AuthPedido extends Controller
{

    public function updatePedido($id,$idcvta){

        
$useractivo = collect(\DB::table('carts')
    ->join('users' , 'users.id', '=', 'carts.user_id')
    ->select('users.activo')
    ->where('carts.id',$id)
    ->get());

 foreach ($useractivo as $useractivos) {
        $statusactivo=$useractivos->activo;
    }

if ($statusactivo!=1) {
   
$agente=auth()->user()->agente;

$idcent = collect(\DB::table('carts')
    
    ->select('idcentrovta')
    ->where('id',$id)
    ->get());

foreach ($idcent as $idcentr) {
    $idcentro=$idcentr->idcentrovta;
}

  $actcentro = collect(\DB::table('centroventa')
     
    ->select('centroventa.*')
    ->where('id',$idcentro)
    //->where('activo',true)
    ->get());

    //return $idcentro;

foreach ($actcentro as $actcentros) {
        $centroact=$actcentros->tipopedido;
    }


 $clave = collect(\DB::table('catrepartidor')
     
        ->select('catrepartidor.*')
        ->where('id',$idcvta)
        ->where('tipo_pedido',$centroact)
        ->where('tipo',"RUTA")
        ->where('rol',"R")
        ->get());

        //return $clave;


foreach ($clave as $claves) {
    $claveus=$claves->clave_vendedor;
}

    $Pendiente='Pendiente';
    //return $claveus;


    $client = \DB::table('carts')
    ->where('id', $id)
    ->where('status', $Pendiente)
    ->update(['status' => 'Aceptado', 'clavevendedor' => $agente, 'claverutero' => $claveus, 'ruta' => $centroact]);
    $admins = User::where('adminpedidos', true)->get();

        return redirect('/adminpedidos/pedidos');

        
    }else{

        $notification2 = 'Para seguir adelante el usuario debe de ser dado de alta, por favor espere.';

    return redirect('/adminpedidos/pedidos')->with(compact('notification2'));  

    }
}


    public function updatePedidoCancelado($id)
    {
        $Pendiente='Pendiente';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Cancelado']);
    	$admins = User::where('admin', true)->get();
    	
    	 return redirect('/adminpedidos/pedidos');
    }
    public function updatePedidoCanceladoConMotivo($id, Request $request)
    {
        $Pendiente='Preparado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Cancelado', 'MotivoCancelacion' =>  $request->input('motivoCancelacion'), 'Fecha_cancelacion' => Carbon::now() ]);

        return redirect('/adminpedidos/pedidos');
    }

    public function updatePedidoConfirmado($id)
    {
        $Pendiente='Autorizado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Confirmado']);
    	$admins = User::where('admin', true)->get();
    	
    	 return redirect('/admin/pedidos');
    }


    public function updatePedidoCanceladoAuth($id)
    {
        $Pendiente='Autorizado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Cancelado']);
    	$admins = User::where('admin', true)->get();
    	
    	 return redirect('/adminpedidos/pedidos');
    }




}

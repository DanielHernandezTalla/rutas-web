<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Category;
use App\Citys;
use App\catError;
use App\Product;
use App\DatPrecios;
use App\CentroVenta;
use App\User;


use File;



class TestController extends Controller
{


    public function welcome()
    {

        if (Auth::check()) {

       
            $idUsuario=auth()->user()->id;
            $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$idUsuario)
        ->get());

               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }
         $useradmin=auth()->user()->useradmin;
      $admin=auth()->user()->admin;
        $gester=auth()->user()->gester;
          $usad=auth()->user()->usad;
            $adminpedidos=auth()->user()->adminpedidos;
              $rutero=auth()->user()->rutero;
               $picking=auth()->user()->picking;
           
if($useradmin==1){
 return redirect('/useradmin/newuser');
}else{

    if($admin==1){
return redirect('/admin/precios');
    }else{

        if($gester==1){
return redirect('/gester/products');
        }else{

            if($picking==1){
return redirect('/picking/index');
        }else{

            if($usad==1){
return redirect('/usad/useradmin');
            }else{

                if($adminpedidos==1){
return redirect('/adminpedidos/pedidos');
                }else{

                    if($rutero==1){
return redirect('/rutero/index');
                    }else{

$activoss=auth()->user()->activo;

                        if($activoss == 0){


             $id=1;


             $allcategories = collect(\DB::table('categories')
        ->select('categories.*')
        ->get());


        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());



        $DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
        ->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')
        
        ->where('dat_precios.idcategory', $id)
        ->where('centroventa.subinventory_name', $rutas)
        ->whereNotNull('product_images.image')
        ->orderBy('dat_precios.precios','desc')
        ->get();


      
      $DatPrecios1 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa')
       ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
       ->where('dat_precios.idcategory', $id)
       ->where('centroventa.subinventory_name', $rutas)
       ->get();


       $id2=2;
       $categories2 = collect(\DB::table('categories')
       ->select('categories.*')
       ->where('id', $id2)
       ->get());
       
       

$DatPrecios2 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')

->where('dat_precios.idcategory', $id2)
->where('centroventa.subinventory_name', $rutas)
->whereNotNull('product_images.image')
->orderBy('dat_precios.precios','desc')
->get();


       $id3=3;
       $categories3 = collect(\DB::table('categories')
       ->select('categories.*')
       ->where('id', $id3)
       ->get());
       

$DatPrecios3 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')

->where('dat_precios.idcategory', $id3)
->where('centroventa.subinventory_name', $rutas)
->whereNotNull('product_images.image')
->orderBy('dat_precios.precios','desc')
->get();





        $id4=4;
        $categories4 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id4)
        ->get());
        
        
$DatPrecios4 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
 ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')  
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
 
 ->where('dat_precios.idcategory', $id4)
 ->orderBy('dat_precios.precios','desc')
 ->where('centroventa.subinventory_name', $rutas)

 ->get();

     

$provisional=1;
  $centro = collect(\DB::table('centroventa as centro')
        ->join('citys as cit', 'cit.id', '=', 'centro.idciudad')
        ->join('catprecios as catpre', 'catpre.id', '=', 'centro.idcatprecio')
       
        
        ->select('centro.id as id_centro', 'centro.descripcion as des_centro','centro.tipopedido as tipopedido_centro','cit.name as name_city','catpre.descripcion as descripcion_catpre')  
        ->where('centro.provisional',$provisional) 
        ->get());

$ciudades = Citys::all()->pluck('name', 'id');


        return view('welcome')->with(compact('categories1','categories2','categories3','categories4','DatPrecios','DatPrecios2','DatPrecios3','DatPrecios4','ciudades','centro','DatPrecios1','allcategories'));

        }else{

        //USUARIO NO LOGEADO


$allcategories = collect(\DB::table('categories')
        ->select('categories.*')
        ->get());

             $id=1;
        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());


$DatPrecioss = DatPrecios::select('idcentroventa')->where('idcategory', $id)->orderBy('precios','desc')->get();

$first = $DatPrecioss->first()->idcentroventa;

        
$DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
         ->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')
         
         ->where('dat_precios.idcategory', $id)
         ->where('centroventa.subinventory_name', $rutas)
         ->whereNotNull('product_images.image')
         ->orderBy('dat_precios.precios','desc')
         ->get();


       
       $DatPrecios1 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa')
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->where('dat_precios.idcategory', $id)
        ->where('centroventa.subinventory_name', $rutas)
        ->get();


        $id2=2;
        $categories2 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id2)
        ->get());
        
        

$DatPrecios2 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')

->where('dat_precios.idcategory', $id2)
->where('centroventa.subinventory_name', $rutas)
->whereNotNull('product_images.image')
->orderBy('dat_precios.precios','desc')
->get();


        $id3=3;
        $categories3 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id3)
        ->get());
        

$DatPrecios3 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
->leftjoin('product_images', 'products.codigo', '=', 'product_images.codigo')

->where('dat_precios.idcategory', $id3)
->where('centroventa.subinventory_name', $rutas)
->whereNotNull('product_images.image')
->orderBy('dat_precios.precios','desc')
->get();



        $id4=4;
        $categories4 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id4)
        ->get());

        $DatPrecioss4 = DatPrecios::select('idcentroventa')->where('idcategory', $id2)->orderBy('precios','desc')->get();

$first4 = $DatPrecioss4->first()->idcentroventa;

        

     

     $DatPrecios4 = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
 ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')  
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->where('dat_precios.idcategory', $id4)
 ->where('dat_precios.idcentroventa', $first4)
 ->orderBy('dat_precios.precios','desc')

 ->get();

$provisional=1;
  $centro = collect(\DB::table('centroventa as centro')
        ->join('citys as cit', 'cit.id', '=', 'centro.idciudad')
        ->join('catprecios as catpre', 'catpre.id', '=', 'centro.idcatprecio')
       
        
        ->select('centro.id as id_centro', 'centro.descripcion as des_centro','centro.tipopedido as tipopedido_centro','cit.name as name_city','catpre.descripcion as descripcion_catpre')  
        ->where('centro.provisional',$provisional) 
        ->get());

$ciudades = Citys::all()->pluck('name', 'id');


        return view('welcome')->with(compact('categories1','categories2','categories3','categories4','DatPrecios','DatPrecios2','DatPrecios3','DatPrecios4','ciudades','centro','allcategories'));

        }
                       
                         }
                    }
                }
            } }
        }


        
    }


        






        }


        else{
         
        $id=1;
        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

 $DatPrecios = DatPrecios::distinct('dat_precios.nameproduct')
 ->select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','product_images.image')
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->where('dat_precios.idcategory', $id)

 ->orderBy('dat_precios.precios','desc')
 ->get();
       

     
       
     $DatPrecios1 = collect(\DB::table('dat_precios')
        ->select('dat_precios.nameproduct')
        ->where('idcategory', $id)
        ->distinct()->get());


        $id2=2;
        $categories2 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id2)
        ->get());
        

           $DatPrecios2 = DatPrecios::distinct('dat_precios.nameproduct')
 ->select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','product_images.image')
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->where('dat_precios.idcategory', $id2)

 ->orderBy('dat_precios.precios','desc')
 ->get();

        $id3=3;
        $categories3 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id3)
        ->get());
        



 $DatPrecios3 = DatPrecios::distinct('dat_precios.nameproduct')
 ->select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','product_images.image')
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->where('dat_precios.idcategory', $id3)

 ->orderBy('dat_precios.precios','desc')
 ->get();

        $id4=4;
        $categories4 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id4)
        ->get());
        
        
  $DatPrecios4 = DatPrecios::distinct('dat_precios.nameproduct')
 ->select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','product_images.image')
 ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
 ->where('dat_precios.idcategory', $id4)

 ->orderBy('dat_precios.precios','desc')
 ->get();
     

$provisional=1;
  $centro = collect(\DB::table('centroventa as centro')
        ->join('citys as cit', 'cit.id', '=', 'centro.idciudad')
        ->join('catprecios as catpre', 'catpre.id', '=', 'centro.idcatprecio')
       
        
        ->select('centro.id as id_centro', 'centro.descripcion as des_centro','centro.tipopedido as tipopedido_centro','cit.name as name_city','catpre.descripcion as descripcion_catpre')  
        ->where('centro.provisional',$provisional) 
        ->get());

$ciudades = Citys::all()->pluck('name', 'id');


        return view('welcome')->with(compact('categories1','categories2','categories3','categories4','DatPrecios','DatPrecios2','DatPrecios3','DatPrecios4','ciudades','centro','DatPrecios1'));

       }
    }

    public function welcomeEstadio(){

        $id=13;
        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());
        
        $DatPrecios = DatPrecios::where('idcategory',$id)->get();


        $Estadios=collect(\DB::table('centroventa')
        ->select('centroventa.*')
        ->where('provisional',true)
        ->get());;


return view('welcomeEstadios')->with(compact('categories1','DatPrecios','Estadios'));
}
    

 public function centroventa(Request $request)
    {
        $id=5;
        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());
        $DatPrecios = DatPrecios::where('idcategory',$id)->get();
       
     $ciudades = Citys::all()->pluck('name', 'id');


        $id2=6;
        $categories2 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id2)
        ->get());
        $DatPrecios2 = DatPrecios::where('idcategory',$id2)->get();
        $id3=7;
        $categories3 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id3)
        ->get());
        $DatPrecios3 = DatPrecios::where('idcategory',$id3)->get();
        $id4=8;
        $categories4 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id4)
        ->get());
        $DatPrecios4 = DatPrecios::where('idcategory',$id4)->get();
        $id5=9;
        $categories5 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id5)
        ->get());
        $DatPrecios5 = DatPrecios::where('idcategory',$id5)->get();
        $id6=10;
        $categories6 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id6)
        ->get());
        $DatPrecios6 = DatPrecios::where('idcategory',$id6)->get();

        $Estadios=collect(\DB::table('centroventa')
        ->select('centroventa.*')
        ->where('provisional',true)
        ->get());;


         $city = $request->get('CITY');
         $provisional=1;
        $centro = collect(\DB::table('centroventa as centro')
        ->join('citys as cit', 'cit.id', '=', 'centro.idciudad')
        ->join('catprecios as catpre', 'catpre.id', '=', 'centro.idcatprecio')
       
        
        ->select('centro.id as id_centro', 'centro.descripcion as des_centro','centro.tipopedido as tipopedido_centro','cit.name as name_city','catpre.descripcion as descripcion_catpre','centro.idciudad as id_city')  
        ->where('centro.idciudad', $city) 
        ->where('centro.provisional',$provisional) 
        ->get());




return view('welcome')->with(compact('categories1','categories2','categories3','categories4','categories5','categories6','DatPrecios','DatPrecios2','DatPrecios3','DatPrecios4','DatPrecios5','DatPrecios6','Estadios','ciudades','centro'));
    }
       
       

    

    public function centroventa2(Request $request,$id)
    {
        

         $city = $request->get('CITY');
         $provisional=1;
        $centro = collect(\DB::table('centroventa as centro')
        ->join('citys as cit', 'cit.id', '=', 'centro.idciudad')
        ->join('catprecios as catpre', 'catpre.id', '=', 'centro.idcatprecio')
       
        
        ->select('centro.id as id_centro', 'centro.descripcion as des_centro','centro.tipopedido as tipopedido_centro','cit.name as name_city','catpre.descripcion as descripcion_catpre','centro.idciudad as id_city')  
        ->where('centro.idciudad', $id) 
        ->where('centro.provisional',$provisional) 
        ->get());




return view('welcome')->with(compact('centro'));
    }

function getTableRows($id)
{
$centros = CentroVenta::where('idciudad', '=', $id);

foreach ($centros as $centro)
{

echo "<tr><td>".$centro->descripcion."</td><td>".$centro->idciudad."</td></tr>";
}
}



 public function reporteerror(){

return view('reporterror');

}

public function reporteerrorcreate(Request $request)
    {

$error = $request->get('mensajerror'); 

if($error == ""){

   $notification2 = 'Debe llenar el el campo para enviar su notificacion.';  

     return back()->with(compact('notification2'));

}else{

    $id=auth()->user()->id;


    $caterror = new catError();
    $caterror->description = $request->input('mensajerror');
    $caterror->id_user= $id;
    $caterror->save();
     $notification = 'Su error a sido enviado a los encargados de soporte.';  

     return back()->with(compact('notification'));
    
    }
 }

 public function test()
    {

$from = date('2021-05-19');
$to = date('2021-05-19');

$pedidos = collect(\DB::table('rutas.carts as carrito')
     ->join('rutas.users as usuario', 'usuario.id', '=', 'carrito.user_id')
        
    ->select('carrito.*','usuario.name')

    ->whereBetween('order_date', [$from, $to])
   
    ->get());




    $pdf=\PDF::loadview('tests',compact("pedidos"))->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf');
  
       return $pdf->stream('primerpdf.pdf');
   }


 public function downloadJSONFile(Request $request){

 
$nombre="polla";
      $data = json_encode($nombre);
      $file = time() .rand(). '_file.txt';
      $destinationPath=public_path()."/upload/";
      if (!is_dir($destinationPath)) {  mkdir($destinationPath,0777,true);  }
      File::put($destinationPath.$file,$data);
      return response()->download($destinationPath.$file);
    }

     public function ticket(){


return view('Ticket');

}



}

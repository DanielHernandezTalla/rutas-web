<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Citys;
use App\DatPrecios;
use App\CentroVenta;
use App\User;

class SearchController extends Controller
{
    public function show(Request $request)
    {   
        $query = $request->input('query');
       
        $id2=1;

        $DatPrecioss = DatPrecios::select('idcentroventa')->where('idcategory', $id2)->orderBy('precios','desc')->get();

        $first = $DatPrecioss->first()->idcentroventa;

        
        $products = DatPrecios::distinct('nameproduct')
        ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
        ->select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image')
       
        ->where('dat_precios.idcentroventa', $first)
        ->where('dat_precios.nameproduct', 'like', "%$query%")

        ->orderBy('dat_precios.precios','desc')->paginate(20);

        //return $request;
        return view('search.show')->with(compact('products', 'query'));
    }

    public function data()
    {
        $products = Product::pluck('name');
        return $products;
    }
}

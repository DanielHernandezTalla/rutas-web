<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Cart;
use App\CartDetail;
use Carbon\Carbon;
use App\Catdirenv;
use App\ModificacionPedido;
use App\EntregaPedidosModel;
use App\DatPrecios;
use App\catError;
use App\CentroVenta;
use App\User;
use App\Datinvcedis;
use App\catPagos;
use App\DatPromocion;
use App\Citys;
use App\Colonias;
use Mail;
use App\Mail\NewOrder;
use App\Notifications\MeetReminder;
class apiController extends Controller
{

    public function selectByColonia($city){

        $colonias = collect(\DB::table('catciudades')
        ->select('catciudades.*')
        ->where('ciudad',$city)
        ->orderBy('colonia', 'ASC')
        ->get());

        return $colonias;
    }

public function welcomePrueba($id)
    {
        $almacenRuta = collect(\DB::table('catdirenv')
    ->select('catdirenv.almacen_ruta')
    ->where('iduser',$id)
    ->get());

           foreach ($almacenRuta as $almacen)
    {
              $rutas=$almacen->almacen_ruta;
    }

 $idCategory1=1;
 $idCategory2=2;
 $idCategory3=3;
 $idCategory4=4;

$a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory1);

$b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory2);

$c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory3);

$d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory4)
         ->union($a)
         ->union($b)
         ->union($c)       
         ->get();                           

$DatPrecios= $d;

         return $DatPrecios;


  }

public function index1($id,$iduser)
{
 $products = Product::find($id);
 $codigo = $products->codigo;


 $sacarid= collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select('carrito.id')
        ->where('usuario.id', $iduser)
        ->where('carrito.status', "Active")
        ->get());

 foreach ($sacarid as $sacarids) {
 $idcarrito=$sacarids->id;                    
 }

 $cartanduser = Cart::find($idcarrito);

 $contador =  collect(\DB::table('datinvcedis')      
        ->select(\DB::raw('count(datinvcedis.id) as products_count'))
        ->where('codigo',$codigo)           
        ->get());

        foreach ($contador as $contadores) {
            $totales=$contadores->products_count;
                            
        }
        if ($totales==0) {

            $preuab= 0;

        return compact('products','preuab','cartanduser');
        }else{
            $sacar= Datinvcedis::where('codigo', '=', $codigo)->select('id')->get();

        foreach ($sacar as $saca) {
            $ids=$saca->id;
        } 
        //Obtiene el codigo
        $product = Product::find($id);
        $codigo = $product->codigo;
        
        //Obtiene la organizacionid del usuario
        $users = User::find($iduser);
        $organization_id = $users->organization_id;

        //Si la organizacion viene vacia
        if (empty($organization_id)){

            $Usuarios = collect(\DB::table('users')
            ->select('users.*')
            ->where('id',$iduser)
            ->get());
        
            foreach ($Usuarios as $usuario) {
                $city=$usuario->city;
                
            }
            $organization_id= Citys::where('name', '=', $city)->first()->organization_id;

            $preuab= Datinvcedis::where('codigo', '=', $codigo)->where('organization_id','=', $organization_id)->first();
          


        }else{
            $preuab= Datinvcedis::where('codigo', '=', $codigo)->where('organization_id','=', $organization_id)->first();
        //return $preuab;
        }
        

		//$preuab= Datinvcedis::where('codigo', '=', $codigo)->first();
		
    return compact('products','preuab','cartanduser');
    //return $preuab;
    }
 }

 public function itemProducto($id,$iduser)
{
        $products = Product::find($id);
        $codigo = $products->codigo;


        $sacarid= collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select('carrito.id')
        ->where('usuario.id', $iduser)
        ->where('carrito.status', "Active")
        ->get());

        foreach ($sacarid as $sacarids) {
            $idcarrito=$sacarids->id;                    
        }

        $cartanduser = Cart::find($idcarrito);

        $contador =  collect(\DB::table('datinvcedis')      
        ->select(\DB::raw('count(datinvcedis.id) as products_count'))
        ->where('codigo',$codigo)           
        ->get());

        foreach ($contador as $contadores) {
            $totales=$contadores->products_count;
                            
        }
        if ($totales==0) {
            $preuab= 0;
            return compact('products','preuab','cartanduser');
        }else{
            $sacar= Datinvcedis::where('codigo', '=', $codigo)->select('id')->get();

        foreach ($sacar as $saca) {
            $ids=$saca->id;
        } 
        //Obtiene el codigo
        $product = Product::find($id);
        $codigo = $product->codigo;
        
        //Obtiene la organizacionid del usuario
        $users = User::find($iduser);
        $organization_id = $users->organization_id;

        //Si la organizacion viene vacia
        if (empty($organization_id)){

            $Usuarios = collect(\DB::table('users')
            ->select('users.*')
            ->where('id',$iduser)
            ->get());
        
            foreach ($Usuarios as $usuario) {
                $city=$usuario->city;
                
            }
            $organization_id= Citys::where('name', '=', $city)->first()->organization_id;

            $preuab= Datinvcedis::where('codigo', '=', $codigo)->where('organization_id','=', $organization_id)->first();


        }else{
            $preuab= Datinvcedis::where('codigo', '=', $codigo)->where('organization_id','=', $organization_id)->first();
        }
        
        foreach ($cartanduser as $cart) {
            $gg=$cartanduser->fechaE;
            if($gg==null){
                $cartanduser=[];
            }
            return compact('cartanduser');
            
             
            
        }
		
    //return compact('products','preuab','cartanduser');
    
    }
 }

 //Productos por categoria
 public function productCategory1($idCategoriaProd){
     $prodCategory1 = DB::select("SELECT DISTINCT
     productos.id as idproducto, 
     productos.name as producto,
     productos.activo,
     prod_img.image as imagen,
     categories.name as categoria,
     categories.id as idcategoria,
     precios.precios as precio,
     centroventa.id as idcentroventa, 
     centroventa.descripcion as centroventa 
     FROM rutas.products as productos
     LEFT JOIN rutas.product_images as prod_img ON prod_img.product_id=productos.id
     LEFT JOIN rutas.categories as categories ON categories.id=productos.category_id 
     LEFT JOIN rutas.dat_precios as precios ON precios.id=productos.id
     LEFT JOIN rutas.centroventa as centroventa ON centroventa.id=precios.idcentroventa   
     WHERE prod_img.image is not null AND categories.id='$idCategoriaProd'");

     return $prodCategory1;
 }


 public function productshow($iduser,$idproduct)
    {

    $codigo_cliente = Catdirenv::take(1)->where('iduser',$iduser)->get();
  // return $codigo_cliente;

      foreach ($codigo_cliente as $codigo_clientes) {
    $clave=$codigo_clientes->clavecliente;
    $idCentroDeVenta=$codigo_clientes->tipopedido;
    } 

    if ($clave=="") {
    $rutero = collect(\DB::table('catrepartidor')
    ->select('catrepartidor.tipo_pedido')
    ->where('rol',"R")
    ->get());

    $first = $rutero->first()->tipo_pedido;
    $idCentroDeVenta=$first;
    
    }

    //return $idCentroDeVenta;

      $idCentroDeVenta2 =   collect(\DB::table('centroventa')
        ->select('centroventa.*')
        ->where('tipopedido',$idCentroDeVenta)       
        ->get());
    
    //return $idCentroDeVenta2;

        foreach ($idCentroDeVenta2 as $idCentroDeVenta2s) {
    $ruta=$idCentroDeVenta2s->id;
    } 

    ///INICIO BLOQUE//
    $almacenRuta = collect(\DB::table('catdirenv')
    ->join('centroventa', 'centroventa.tipopedido', '=', 'catdirenv.tipopedido')
    ->select('catdirenv.almacen_ruta')
    ->where('iduser',$iduser)
    ->where('centroventa.activo',1)
    ->get());

           foreach ($almacenRuta as $almacen)
    {
              $rutas=$almacen->almacen_ruta;
    }


    $Usuarios = collect(\DB::table('users')
    ->select('users.*')
    ->where('id',$iduser)
    ->get());
    $catdirenv= Catdirenv::where('iduser',$iduser)->get();

    foreach ($catdirenv as $catdirenvs) {
        $ClaveCliente=$catdirenvs->clavecliente;
        
    }
    //FIN BLOQUE//

    $product = Product::find($idproduct);
    $codigo = $product->codigo;
      
$usuarioandcarrito= collect(\DB::table('carts as carrito')
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select('carrito.id as carrito_ids', 'usuario.id as usuario_ids')
        ->where('usuario.id', $iduser)
        ->where('carrito.status', "Active")
        ->get());

        $DatPrecios = DB::select("SELECT DISTINCT dp.id,dp.idproduct,dp.idcatprecio,dp.idcentroventa,dp.nameproduct,dp.idcategory,cv.subinventory_name,dp.nameproduct as nameProd,
        CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
        dp.precios
        ELSE 
        FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
        dp.idcentroventa,dp.codigo,prod_img.image
        FROM rutas.dat_precios as dp
        LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id AND cv.activo=true
        LEFT JOIN rutas.products as products ON dp.idproduct=products.id
        LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
        LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
        WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' and products.id='$idproduct' and prod_img.image 
        is not null and cv.organization_id=cv.organization_id order by dp.precios desc;");   

    $Datinvcedis= Datinvcedis::where('codigo', '=', $codigo)->get();

    $preuab =   collect(\DB::table('datinvcedis')
        ->select('datinvcedis.*')        
        ->where('codigo',$codigo)             
        ->get());
    

        $contar =   collect(\DB::table('datinvcedis as cedis')
        ->select(\DB::raw('count(cedis.id) as cedis_count'))
        ->where('cedis.codigo',$codigo)       
        ->get());

    return compact('DatPrecios');
    }
     public function productsearchprueba($query,$iduser)
    {
 
 $almacenRuta = collect(\DB::table('catdirenv')
    ->select('catdirenv.almacen_ruta')
    ->where('iduser',$iduser)
    ->get());

           foreach ($almacenRuta as $almacen)
    {
              $rutas=$almacen->almacen_ruta;
    }

 $idCategory1=1;
 $idCategory2=2;
 $idCategory3=3;
 $idCategory4=4;

 $e = DatPrecios::select(\DB::raw('count(dat_precios.id) as product_count')) 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->where('centroventa.subinventory_name', $rutas)   
         ->where('dat_precios.nameproduct','like',"%$query%")
         ->get(); 


$a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory1)
         ->where('dat_precios.nameproduct','like',"%$query%");

$b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory2)
         ->where('dat_precios.nameproduct','like',"%$query%");

$c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory3)
         ->where('dat_precios.nameproduct','like',"%$query%");

$d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         ->where('centroventa.subinventory_name', $rutas)         
         ->where('dat_precios.idcategory', $idCategory4)
         ->where('dat_precios.nameproduct','like',"%$query%")
         ->union($a)
         ->union($b)
         ->union($c)   
         ->get();                           



        $DatPrecios= $d;
         return $DatPrecios;

   
}
public function searchPrueba($query,$iduser)
{

$almacenRuta = collect(\DB::table('catdirenv')
->select('catdirenv.almacen_ruta')
->where('iduser',$iduser)
->get());

       foreach ($almacenRuta as $almacen)
{
          $rutas=$almacen->almacen_ruta;
}

$idCategory1=1;
$idCategory2=2;
$idCategory3=3;
$idCategory4=4;

$e = DatPrecios::select(\DB::raw('count(dat_precios.id) as product_count')) 
     ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
     ->where('centroventa.subinventory_name', $rutas)   
     ->where('dat_precios.nameproduct','like',"%$query%")
     ->get(); 


$a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
     ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
     ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
     ->where('centroventa.subinventory_name', $rutas)         
     ->where('dat_precios.idcategory', $idCategory1)
     ->where('dat_precios.nameproduct','like',"%$query%");

$b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
     ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
     ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
     ->where('centroventa.subinventory_name', $rutas)         
     ->where('dat_precios.idcategory', $idCategory2)
     ->where('dat_precios.nameproduct','like',"%$query%");

$c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
     ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
     ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
     ->where('centroventa.subinventory_name', $rutas)         
     ->where('dat_precios.idcategory', $idCategory3)
     ->where('dat_precios.nameproduct','like',"%$query%");

$d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
     ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
     ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
     ->where('centroventa.subinventory_name', $rutas)         
     ->where('dat_precios.idcategory', $idCategory4)
     ->where('dat_precios.nameproduct','like',"%$query%")
     ->union($a)
     ->union($b)
     ->union($c)   
     ->get();                           



$DatPrecios= $d;


     return $DatPrecios;


}

public function homePrueba($iduser)
{

  $Direccion = collect(\DB::table('catdirenv as direnv')     
    ->select('direnv.*')
    ->where('iduser',$iduser)
    ->get());

    //return $Direccion;

   $carts =   collect(\DB::table('carts')
    ->select(\DB::raw('count(*) as cart_count'))
    ->where('user_id',$iduser)
    ->where('status',"Pendiente")
    ->get());

    

   $carts3 =   collect(\DB::table('carts')
    ->select(\DB::raw('count(*) as cart_count'))
    ->where('user_id',$iduser)
    ->where('status',"Autorizado")
    ->get());

  

   $carts4 =   collect(\DB::table('carts')
    ->select(\DB::raw('count(*) as cart_count'))
    ->where('user_id',$iduser)
    ->where('status',"Despachado")
    ->get());

  

  $despachados = collect(\DB::table('carts as carrito')
   ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')   
    ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
        'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
    ->where('usuario.id', $iduser)
    ->where('carrito.status', "Despachado")
    ->get());

    


$di = collect(\DB::table('catdirenv')
   ->select('direccion', 'tipopedido', )
    ->where('iduser', $iduser)       
    ->get());


   

  $pendientes= collect(\DB::table('carts as carrito')
   ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
    ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
        'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
    ->where('usuario.id', $iduser)
    ->where('carrito.status', "Autorizado")
    ->get());

   


    $unaut= collect(\DB::table('carts as carrito')
    ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
     ->select('carrito.id', 'carrito.importe_total', 'carrito.order_date', 'carrito.created_at', 
         'carrito.status','carrito.fechaE','usuario.city', 'usuario.name', 'usuario.address','usuario.phone')
     ->where('usuario.id', $iduser)
     ->where('carrito.status', "Pendiente")
     ->get());
 
     

    /*  obtener el tipo de pedido    */
    /*$idcentroventa= Catdirenv::select('tipopedido')
    ->where('iduser', $iduser)
    ->get();*/

    $idcentroventa = collect(\DB::table('catdirenv')
        ->select('catdirenv.tipopedido')
        ->leftjoin('centroventa', 'catdirenv.almacen_ruta', '=', 'centroventa.subinventory_name')
        ->where('catdirenv.iduser',$iduser)
        ->where('centroventa.activo',true)
        ->get());
    
    //return $idcentroventa;

    foreach ($idcentroventa as $idcentroventas) {
        $centroVenta=$idcentroventas->tipopedido;
     } 

     //return $idcentroventa;
     
     

  /* obtener el id del centro de venta  */
     $idCentroDeVenta= collect(\DB::table('centroventa')
     ->select('id')
     ->where('tipopedido', $centroVenta)
     //Se agrego este AND para filtrar por activos true
     ->where('activo', 1)
     ->get());

     //return $centroVenta;

     foreach ($idCentroDeVenta as $idCentroDeVentas) {
         $idCentro=$idCentroDeVentas->id; 
         } 

    $DatPrecios = DatPrecios::where('idcentroventa',$idCentro)->get();
   $citys = Citys::all()->pluck('name', 'id');


$almacenRuta = collect(\DB::table('users')
->select('users.city')
->where('id',$iduser)
->get());

       foreach ($almacenRuta as $almacen)
{
          $city=$almacen->city;
}

$ruta= collect(\DB::select('select  d.tipopedido as tipopedido, d.organization_id as organizacion from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city AND b.id=:userid', ['status' => 'Pendiente','city'=>$city,'userid'=>$iduser]));



$activo = Cart::where('user_id', $iduser)
            ->where('status', "Active")
            ->get();


$carritosids =   collect(\DB::table('carts')
    ->select(\DB::raw('carts.*'))
    ->where('user_id',$iduser)
    ->where('status',"Active")
    ->get());

foreach ($carritosids as $carritosid)
{
          $idcarrito=$carritosid->id;
}

$usuarios = User::find($iduser);

$carritos = Cart::find($idcarrito);

$wordlist = CartDetail::where('cart_id', $idcarrito)->get();
$wordCount = $wordlist->count();

$wordlist2 = Cart::where('user_id', $iduser)->where('status',"Pendiente")->get();
$wordCount2 = $wordlist2->count();



    return compact('carritos','usuarios','wordCount','wordCount2','idCentro','DatPrecios');

} public function homes($iduser)
    {


$idcarrito= collect(\DB::table('carts')      
        ->select('carts.*')
        ->where('user_id', $iduser)
        ->where('status', "Active")
        ->get());

foreach ($idcarrito as $idcarritos)
    {
              $idcarrr=$idcarritos->id;
    }

    $CartDetails= collect(\DB::table('cart_details')      
        ->select('cart_details.*')
        ->where('cart_id', $idcarrr)
        ->get());

        return $CartDetails;

}
//Prueba query tbl carrito
public function homesErzu($iduser){

    $products = DB::select("SELECT * FROM rutas.carts as a 
    LEFT JOIN rutas.cart_details as b ON a.id=b.cart_id
    LEFT JOIN rutas.products as c ON b.product_id=c.id
    WHERE a.user_id=$iduser and a.status='Active' ");
    //AND c.name_short is not null;

    return $products;
}
//FIN Prueba query tbl carrito

 public function homes2($iduser)
    {

$direcciones= Catdirenv::where('iduser','=',$iduser)->pluck('id');

$direc = Catdirenv::find($direcciones);

return compact('direc');
}

 public function storeprueba(Request $request)
    {       
        //return $request; 
/*
        
    $CartDetailExistente = CartDetail::where('cart_id', $request->id)
    ->whereIn('product_id',[$request->product_id])
    ->first();

   

    $unidadMedida = $request->uom;
    $pesoPromedio = $request->pesopromedio;
    $unidadMedida != 'KG' ? $unidadMedida = $request->uom : $unidadMedida = $unidadMedida; 
    ($unidadMedida == 'KG' ? $unidadMedida = $unidadMedida : $pesoPromedio == 0) ?  $request->quantity = $request->quantity : $pesoPromedio = $pesoPromedio * $request->quantity;

    if(!empty($CartDetailExistente)){

        $sumQuantity = $CartDetailExistente->quantity + $request->quantity;
        CartDetail::where('product_id', $request->product_id)
        ->where('cart_id', $request->id)
        ->update([
            'quantity' => $sumQuantity,
        ]);
        
        $response = "agregado al carrito correctamente";
        return response($response, 200);


    }else{

        $cartDetail = new CartDetail();
        $cartDetail->cart_id = $request->id;
        $cartDetail->uom1 = $unidadMedida;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->quantity = $request->quantity;
        $cartDetail->quantity1 =0;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->quantitycaja = 0;
        $cartDetail->save();

        $response = "agregado al carrito correctamente";
        return response($response, 200);

    }

 
*/
/*
 if($request->uom == "KG"){

        $factor = $request->factor;
        $pesopromedio = $request->pesopromedio;
        $exi = $request->existencia;
        $cantidad=$request->quantity;
        
   if ($exi<=$cantidad) {
        
        $respuesta = "Actualmente_no_hay";
        $statusCode= 422;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
        return response($response,422);  
          

       }else {
           
        $cartDetail = new CartDetail();
        $cartDetail->cart_id = $request->id;
        $cartDetail->uom1 = $request->uom;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->quantity = $cantidad;
        $cartDetail->quantity1 =0;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->quantitycaja = 0;
        $cartDetail->save();
       
     
        $respuesta = "Producto_cargado";
        $statusCode= 200;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
        return response($response,200);  
       }    

      
    }else{

 $factor = $request->factor;
 $pesopromedio = $request->pesopromedio;
 $exi = $request->existencia;
 $cantidad=$request->quantity;
 $cantidadtotal=$cantidad*$pesopromedio;

 if ($cantidad>=$exi) {
        $respuesta = "Actualmente_no_hay";
        $statusCode= 422;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
        return response($response,422); 
 }else {

    $cartDetail = new CartDetail();
        $cartDetail->cart_id = $request->id;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->uom1 = $request->uom;
        $cartDetail->quantity = ($pesopromedio==0)?$request->quantity:$pesopromedio*$request->quantity;
        $cartDetail->quantity1 = $request->quantity;
        $cartDetail->quantitycaja = $request->quantity;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->save();

        $respuesta = "Producto_cargado";
         $statusCode= 200;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
        return response($response,200); 
 } 

       

    }
    */
    //return $request->uom;
    if ($request->uom == "KG") {

        $factor = $request->factor;
            $pesopromedio = $request->pesopromedio;
            $exi = $request->existencia;
            $cantidad=$request->quantity;
            
       if ($exi<=$cantidad) {
            
            $respuesta = "Actualmente_no_hay";
            $statusCode= 422;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,422);  
              
    
           }else {
            $cartDetail = new CartDetail();
            $cartDetail->cart_id = $request->id;
            $cartDetail->uom1 = $request->uom;
            $cartDetail->product_id = $request->product_id;
            $cartDetail->quantity = $cantidad;
            $cartDetail->quantity1 =0;
            $cartDetail->iddatprecio = $request->iddatprecio;
            $cartDetail->precio_producto = $request->precioprod;
            $cartDetail->quantitycaja = 0;
            $cartDetail->save();
           
         
            $respuesta = "Producto_cargado";
            $statusCode= 200;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,200);  
           }    
    
    } elseif ($request->uom == "CAJA") {

     $factor = $request->factor;
     $pesopromedio = $request->pesopromedio;
     $exi = $request->existencia;
     $cantidad=$request->quantity;
     $cantidadtotal=$cantidad*$pesopromedio;
        
     //Valida que si no tiene peso promedio no lo agregue al carrito
      if($pesopromedio == 0 || $pesopromedio == null){
            $respuesta = "No cuenta con peso promedio";
            $statusCode= 200;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,422); 
        }
    
     if ($cantidad>=$exi) {
            $respuesta = "Actualmente_no_hay";
            $statusCode= 422;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,422); 
     }else {
    
        $cartDetail = new CartDetail();
            $cartDetail->cart_id = $request->id;
            $cartDetail->product_id = $request->product_id;
            $cartDetail->uom1 = $request->uom;
            $cartDetail->quantity = ($pesopromedio==0)?$request->quantity:$pesopromedio*$request->quantity;
            $cartDetail->quantity1 = $request->quantity;
            $cartDetail->quantitycaja = $request->quantity;
            $cartDetail->iddatprecio = $request->iddatprecio;
            $cartDetail->precio_producto = $request->precioprod;
            $cartDetail->save();
    
            $respuesta = "Producto_cargado";
            $statusCode= 200;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,200); 
     }
    } else {
        $respuesta = "De_momento_no_hay_piezas";
            $statusCode= 201;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
            return response($response,201); 
        }
    

    }





    public function updatecarrito(Request $request)
    {
        $id = $request->id;
        $idcentroventa = $request->idcentroventa;
        $userid = $request->userid;
        $city = $request->city;


        $fechaE = $request->fechaE;
        $factura = $request->factura;
        $direccion = $request->direccionselect;
        $tokencode = $request->tokencode;
        $organization_id = $request->organization_id;      
         
        $tokendos="";

        if ($organization_id=="1") {
        $tokendos="";
        }else{
            $tokendos=$tokencode;
        }




        $tiempo = collect(\DB::table('catctehorasentrega')
                ->select('horas')
                ->where('tipocliente', "Nuevo")
                ->get());

        foreach ($tiempo as $tiempos) {
            $tiempoentrega=$tiempos->horas;
            }

        $end = Carbon::parse($fechaE);

        $now = Carbon::now();

        $length = $end->diffInHours($now);

        /*
        if($length < $tiempoentrega){

                $respuesta = "La_fecha_de_entraga_debe_ser_mayor_a_48_horas";
                $statusCode= 422;
                $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];           
                return response($response, 422);
        }else{ */


        $date = $fechaE; 

        if(date('w', strtotime($date)) == 7 || date('w', strtotime($date)) == 0) {

                $respuesta = "La_fecha_no_puede_ser_domingo";
                $statusCode= 422;
                $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];           
                return response($response, 422);
        } else {
        $ruta=collect(\DB::select('select   a.organization_id as organizacion from 
        citys a, users b  where  a.name=:city ', ['city'=>$city]));
            


        $direnv = collect(\DB::table('catdirenv')
            
                ->select('id','tipopedido')
                ->where('direccion', $direccion)
                ->where('iduser', $userid)
                ->get());


        foreach ($direnv as $direnvs) {
            $direnvid=$direnvs->id;
            $tipopedido=$direnvs->tipopedido;
            }      

        foreach ($ruta as $rutas) {
            $organizacion=$rutas->organizacion;
            }

            $organizacion2=$organizacion;
            $direnvid2=$direnvid;


        if ($factura=="si") {
          $facturas="si";
        }else{
           $facturas="no";
        }
        
         $datefinal= Carbon::now();

        $dt = $datefinal->subHours(2);
       
       $tiempo=$dt->toDateTimeString(); 

       $todayDate = Carbon::now()->format('H:i:m');

       $fechaTotal=$fechaE. ' ' .$todayDate;

       $client = \DB::table('carts')
        ->where('id', $id)        
         ->update(['status' => "Pendiente",
          'idcentrovta' => $idcentroventa,
             'fechaE' => $fechaTotal,
                'ruta' => $tipopedido,
                   'factura' => $factura,
                      'iddirenv' => $direnvid2,
                         'tokenNotificacion' => $tokendos,
                  'organization_id' => $organizacion2,
              'order_date' => $datefinal,
         'order_datetime' => $tiempo]);
       
        $newCart = new Cart();
        $newCart->user_id = $userid;
        $newCart->status = "Active";        
        $newCart->save();


        $respuesta = "Pedido_realizado";
        $statusCode= 200;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
        return response($response,200);

    

}


     
      
    }


    public function registerPrueba(){ 
        $Colonias = Colonias::orderBy('colonia')->get();
        return compact('Colonias');
    }
    

    public function registerPrueba2(){
        $City = Citys::all();
        return compact('City');
    }

    public function registerPrueba3($city){ 

        $direcciones= Colonias::where('ciudad','=',$city)->pluck('id');
        $direc2 = Colonias::find($direcciones);
       
        return compact('direc2');
    }

    public function registeruser(Request $request)
    {
        try{
            $name= $request->name;
            $username= $request->username;
            $email= $request->email;
            $phone= $request->phone;
            $city= $request->city;
            $codigopostal= $request->codigopostal;
            $colonia= $request->colonia;
            $address= $request->address;
            $numeroexterior= $request->numeroexterior;
            $numerointerior= $request->numerointerior;
            $rfc= $request->rfc;
            $password= $request->password;
            $usuarioApp = 1;

            $insertuser = new User();
            $insertuser->name = $name;
            $insertuser->username= $username;
            $insertuser->email= $email;
            $insertuser->phone= $phone;
            $insertuser->city= $city;
            $insertuser->codigoPostal= $codigopostal;
            $insertuser->colonia= $colonia;
            $insertuser->address= $address;
            $insertuser->numeroExterior= $numeroexterior;
            $insertuser->numerointerior= $numerointerior;
            $insertuser->rfc= $rfc;
            $insertuser->password = bcrypt($password);
            $insertuser->usuarioApp=$usuarioApp;
            $insertuser->save();

            $usuario = collect(\DB::table('users')
            ->select('users.*')
            ->where('username',$username)
            ->get());

            foreach ($usuario as $usuarios)
                {
                    $userid=$usuarios->id;
                }

            $newCart = new Cart();
            $newCart->user_id = $userid;
            $newCart->status = "Active";        
            $newCart->save();

            $respuesta = "Usuario registrado con éxito";
            $statusCode= 200;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];

            return response($response);
        }
        catch(Exception $e){
            $respuesta = "El usuario ya existe, favor de validar sus datos";
            $statusCode= 500;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];

            return response($response);
        }
  }

 public function userSetting($idusuario)
{
$users = User::find($idusuario);
return compact('users');
}


public function updateUser(Request $request)
    {
$iduser= $request->iduser;
$name= $request->name;
$name2= $request->name2;



if ($name2=="") {
  $name2=$name;  
}

 $client = \DB::table('users')
        ->where('id', $iduser)
        ->update(['name' => $name2]);
    
$response = "usuario_modificado";
return response($response, 200);
  
  }


  public function updateUser2(Request $request)
    {
$iduser= $request->iduser;
$email = $request->email;
$email2= $request->email2;

if ($email2=="") {
  $email2=$email;  
}

 $client = \DB::table('users')
        ->where('id', $iduser)
        ->update(['email' => $email2]);
    
$response = "usuario_modificado";
return response($response, 200);
  
  }

public function updateUser3(Request $request)
    {
$iduser= $request->iduser;
$username = $request->username;
$username2= $request->username2;



if ($username2=="") {
  $username2=$username;  
}

 $client = \DB::table('users')
        ->where('id', $iduser)
        ->update(['username' => $username2]);
    
$response = "usuario_modificado";
return response($response, 200);
  
  }


  public function updateUser4(Request $request)
    {
$iduser= $request->iduser;
$address = $request->address;
$address2= $request->address2;

if ($address2=="") {
  $address2=$address;  
}

 $client = \DB::table('users')
        ->where('id', $iduser)
        ->update(['address' => $address2]);
    
$response = "usuario_modificado";
return response($response, 200);
  
  }

public function ReportError(Request $request)
    {

$iduser= $request->iduser;
$reporte= $request->reporte;

if($reporte == ""){

$response = "Reporte_en_blano";
return response($response, 500);

}else{

    $caterror = new catError();
    $caterror->description = $reporte;
    $caterror->id_user= $iduser;
    $caterror->save();

$response = "Reporte_listo";
return response($response, 200);
    
    }
 }


 public function prodimage($query)
{

         $direnv = collect(\DB::table('products')      
        ->select('id','name')
        ->where('name','like',"%$query%")
        ->get());
return compact('direnv');
}



 public function deleteEdit($id)
    {

$idproducto = collect(\DB::table('cart_details')
    ->select('cart_details.*')    
    ->where('id_cart_details', $id)   
    ->get());

foreach ($idproducto as $idproductos) {
    $id2=$idproductos->product_id;
    }

       $DatPrecioss = DatPrecios::select('idcentroventa')->where('idproduct', $id2)->orderBy('precios','desc')->get();

        $first = $DatPrecioss->first()->idcentroventa;


       $product = collect(\DB::table('cart_details as cartdetail')
    
    ->join('products as prod', 'prod.id', '=', 'cartdetail.product_id')
     ->join('dat_precios as prec', 'prec.idproduct', '=', 'prod.id')

    ->select('cartdetail.*','prod.*','cartdetail.id_cart_details as detalle','prec.precios as price')
    
    ->where('cartdetail.id_cart_details', $id)
    ->where('prod.id', $id2)
    ->where('prec.idcentroventa', $first)
    ->get());



        return compact('product'); // form de edición

    }


    public function destroyEdit(Request $request)
    {
        $id = $request->cart_detail_id;

        $cartdetail = collect(\DB::table('cart_details')
            ->select('cart_details.*')
            ->where('id_cart_details',$id)
            ->get());

        foreach ($cartdetail as $cartdetails) {
            $idcart=$cartdetails->cart_id;
            $cartdetailimporte=$cartdetails->importe;
            }

        $cart = collect(\DB::table('carts')
            ->select('carts.*')
            ->where('id',$idcart)
            ->get());

        foreach ($cart as $carts){
              $importe_total=$carts->importe_total;
        }

        $resultado=$importe_total-$cartdetailimporte;

        if ($id=="") {
            $response = "no_hay_producto_que_eliminar";
            return response($response, 400);   
        }else{  

     $client2 = CartDetail::where('id_cart_details', $id)->delete();

        $modificacionusuario = collect(\DB::table('carts')
        ->select('carts.*')
        ->where('id', $idcart)
        ->get());
        
        \DB::table('carts')
        ->where('id', $idcart)
        ->update(['importe_total' => $modificacionusuario->importe_total = $resultado,]);

        $response = "producto_eliminado";
            return response($response, 200);
        }
    }

    //Elimina los datos del usuario desde la app
    public function deleteUserApp(Request $request){
        try {
            $usuario = \DB::table('users')
            ->where('id', $request->userid)
            ->update(['eliminado' => 1,'username'=>null]);

            $response = "Eliminado en tabla users correcto";
            return response($response, 200);
        } catch (\Throwable $th) {
            return response($response, 500);
        }

    }



 public function historialpedidos($iduser)
   {

$car= Cart::where('user_id','=',$iduser)->pluck('id');

$carritos = Cart::find($car);

       
        return compact('carritos'); // form de edición

    }

    public function productos1($id) {
        /*$almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$id)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }*/

        $rutas = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->leftjoin('centroventa', 'catdirenv.almacen_ruta', '=', 'centroventa.subinventory_name')
        ->where('catdirenv.iduser',$id)
        ->where('centroventa.activo',true)
        ->get());

        //return $rutas;

        foreach ($rutas as $almacen)
        {
            $rutas=$almacen->almacen_ruta;
        }
    
        //return $rutas;
    
        $Usuarios = collect(\DB::table('users')
        ->select('users.*')
        ->where('id',$id)
        ->get());
    
        foreach ($Usuarios as $usuario) {
            $idusuario=$usuario->id;
            $city=$usuario->city;
            
        }
        $catdirenv= Catdirenv::where('iduser',$idusuario)->get();
        //dump($catdirenv);
        foreach ($catdirenv as $catdirenvs) {
            $ClaveCliente=$catdirenvs->clavecliente;
            
        }
    
    if (empty($rutas)) {
            
            $organization_id= Citys::where('name', '=', $city)->first()->organization_id;
            $rutas= CentroVenta::where('organization_id', '=', $organization_id)->first()->subinventory_name;

            $DatPrecios = DB::select("SELECT DISTINCT dp.nameproduct,dp.idproduct,cv.subinventory_name,
            CASE WHEN FN_PROMOCION(dp.codigo,$organization_id,'',cv.subinventory_name)=0 THEN 
            dp.precios
            ELSE 
            FN_PROMOCION(dp.codigo,$organization_id,'',cv.subinventory_name) END as precios,
            dp.idcentroventa,dp.codigo,prod_img.image
            FROM rutas.dat_precios as dp
            LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id
            LEFT JOIN rutas.products as products ON dp.idproduct=products.id
            LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
            LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto
            WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' AND existencia.organization_id=$organization_id and prod_img.image 
            is not null order by dp.precios desc;");  

        
    
        return $DatPrecios;
    
    }else{
        //Sacar organization_id
        
        $organization_id= Citys::where('name', '=', $city)->first()->organization_id;
    
        /*$DatPrecios = DB::select("SELECT DISTINCT dp.nameproduct,dp.idproduct,cv.subinventory_name,
        CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
        dp.precios
        ELSE 
        FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
        dp.idcentroventa,dp.codigo,prod_img.image
        FROM rutas.dat_precios as dp
        LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id
        LEFT JOIN rutas.products as products ON dp.idproduct=products.id
        LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
        LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
        WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' AND existencia.organization_id=cv.organization_id AND prod_img.image 
        is not null order by dp.precios desc;");*/
        
        //New query 

        $DatPrecios = DB::select("SELECT distinct nameproduct,idproduct, subinventory_name,precios,idcentroventa,codigo,image from(
            SELECT dp.nameproduct,dp.idproduct,cv.subinventory_name, 
                 CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
                    dp.precios
                    ELSE 
                    FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
                    dp.idcentroventa,dp.codigo,prod_img.image,        
                    (existencia.existencia- rutas.FN_PEDIDOS_RESERVA(cv.organization_id,dp.idproduct)) reserva
                    FROM rutas.dat_precios as dp
                    LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id AND cv.activo=true
                    LEFT JOIN rutas.products as products ON dp.idproduct=products.id
                    LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
                    LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
                    WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' 
                     AND existencia.organization_id=cv.organization_id        
                     AND  existencia.existencia >0 
                    AND prod_img.image is not null ) a        
                    WHERE reserva>0 ORDER BY a.nameproduct ASC;");
        
        return $DatPrecios;
        
        
        }
        

    }




    public function pruebaproductos1($id) {
   
        $rutas = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->leftjoin('centroventa', 'catdirenv.almacen_ruta', '=', 'centroventa.subinventory_name')
        ->where('catdirenv.iduser',$id)
        ->where('centroventa.activo',true)
        ->get());

        //return $rutas;

        foreach ($rutas as $almacen)
        {
            $rutas=$almacen->almacen_ruta;
        }
    
        //return $rutas;
    
        $Usuarios = collect(\DB::table('users')
        ->select('users.*')
        ->where('id',$id)
        ->get());
    
        foreach ($Usuarios as $usuario) {
            $idusuario=$usuario->id;
            $city=$usuario->city;
            
        }
        $catdirenv= Catdirenv::where('iduser',$idusuario)->get();
        //dump($catdirenv);
        foreach ($catdirenv as $catdirenvs) {
            $ClaveCliente=$catdirenvs->clavecliente;
            
        }
    
    if (empty($rutas)) {
            
            $organization_id= Citys::where('name', '=', $city)->first()->organization_id;
            $rutas= CentroVenta::where('organization_id', '=', $organization_id)->first()->subinventory_name;

            $DatPrecios = DB::select("SELECT DISTINCT dp.nameproduct,dp.idproduct,cv.subinventory_name,
            CASE WHEN FN_PROMOCION(dp.codigo,$organization_id,'',cv.subinventory_name)=0 THEN 
            dp.precios
            ELSE 
            FN_PROMOCION(dp.codigo,$organization_id,'',cv.subinventory_name) END as precios,
            dp.idcentroventa,dp.codigo,prod_img.image
            FROM rutas.dat_precios as dp
            LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id
            LEFT JOIN rutas.products as products ON dp.idproduct=products.id
            LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
            LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto
            WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' AND existencia.organization_id=$organization_id and prod_img.image 
            is not null order by dp.precios desc;");  

        
    
        return $DatPrecios;
    
    }else{
        //Sacar organization_id
        
        $organization_id= Citys::where('name', '=', $city)->first()->organization_id;
    
        /*$DatPrecios = DB::select("SELECT DISTINCT dp.nameproduct,dp.idproduct,cv.subinventory_name,
        CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
        dp.precios
        ELSE 
        FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
        dp.idcentroventa,dp.codigo,prod_img.image
        FROM rutas.dat_precios as dp
        LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id
        LEFT JOIN rutas.products as products ON dp.idproduct=products.id
        LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
        LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
        WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' AND existencia.organization_id=cv.organization_id AND prod_img.image 
        is not null order by dp.precios desc;");*/
        
        //New query 

        $DatPrecios = DB::select("SELECT nameproduct,idproduct, subinventory_name,precios,idcentroventa,codigo,image from(
            SELECT dp.nameproduct,dp.idproduct,cv.subinventory_name, 
                 CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
                    dp.precios
                    ELSE 
                    FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
                    dp.idcentroventa,dp.codigo,prod_img.image,        
                    (existencia.existencia- rutas.FN_PEDIDOS_RESERVA(cv.organization_id,dp.idproduct)) reserva
                    FROM rutas.dat_precios as dp
                    LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id AND cv.activo=true
                    LEFT JOIN rutas.products as products ON dp.idproduct=products.id
                    LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
                    LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
                    WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' 
                     AND existencia.organization_id=cv.organization_id        
                     AND  existencia.existencia >0 
                    AND prod_img.image is not null ) a        
                    WHERE reserva>0 ORDER BY a.nameproduct ASC;");
        
        return $DatPrecios;
        
        
        }
    
}

    public function productos1Prueba($id) {

        $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$id)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }
    
    if ($rutas=="") {
    
        $rutero = collect(\DB::table('catrepartidor')
        ->select('catrepartidor.ruta')
        ->where('rol',"R")
        ->get());
    
               $first = $rutero->first()->ruta;
    
            $idCategory1=1;
            $idCategory2=2;
            $idCategory3=3;
            $idCategory4=4;
                  
           
           $a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                    ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                    ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                    ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
    
                    ->where('centroventa.subinventory_name', $first)         
                    ->where('dat_precios.idcategory', $idCategory1)
                    ->where('products.activo',1);
           
           $b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                    ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                    ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                    ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
    
                    ->where('centroventa.subinventory_name', $first)         
                    ->where('dat_precios.idcategory', $idCategory2)
                    ->where('products.activo',1);
           
           $c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                    ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                    ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                    ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id') 
    
                    ->where('centroventa.subinventory_name', $first)         
                    ->where('dat_precios.idcategory', $idCategory3)
                    ->where('products.activo',1);
           
           $d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                    ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                    ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                    ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')
    
                    ->where('centroventa.subinventory_name', $first)         
                    ->where('dat_precios.idcategory', $idCategory4)
                    ->where('products.activo',1)
                    ->union($a)
                    ->union($b)
                    ->union($c)   
                    ->get();                           
           
                  
           $DatPrecios= $d;
       
    
             return  $DatPrecios;


}else{

    $fechaAhora= Carbon::now();
        
        $Promocion = collect(\DB::table('datpromocion')
        ->select('datpromocion.*')
        ->whereDate('datpromocion.fecha_final', '>=', $fechaAhora)
        ->get());

        $idCategory1=1;
        $idCategory2=2;
        $idCategory3=3;
        $idCategory4=4;
              
       
       $a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')

                ->where('centroventa.subinventory_name', $rutas)         
                ->where('dat_precios.idcategory', $idCategory1)
                ->where('products.activo',1);
       
       $b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')

                ->where('centroventa.subinventory_name', $rutas)         
                ->where('dat_precios.idcategory', $idCategory2)
                ->where('products.activo',1);
       
       $c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id') 

                ->where('centroventa.subinventory_name', $rutas)         
                ->where('dat_precios.idcategory', $idCategory3)
                ->where('products.activo',1);
       
       $d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
                ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
                ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
                ->leftjoin('products', 'dat_precios.idproduct', '=', 'products.id')

                ->where('centroventa.subinventory_name', $rutas)         
                ->where('dat_precios.idcategory', $idCategory4)
                ->where('products.activo',1)
                ->union($a)
                ->union($b)
                ->union($c)   
                ->get();                           
       
              
       $DatPrecios= $d;
   

         return  $Promocion;
}
    }

    public function productos2($id) {

        $idCategory=2;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

        $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$id)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }

        $DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
        
        ->where('dat_precios.idcategory', $idCategory)
        ->where('centroventa.subinventory_name', $rutas)
        ->orderBy('dat_precios.precios','desc')
        ->get();

   return $DatPrecios;

    }

    public function productos3($id) {

        $idCategory=3;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

        $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$id)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }

        $DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
        
        ->where('dat_precios.idcategory', $idCategory)
        ->where('centroventa.subinventory_name', $rutas)
        ->orderBy('dat_precios.precios','desc')
        ->get();
   return $DatPrecios;

    }

    
    public function productos4($id) {

        $idCategory=4;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

        $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$id)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }

        $DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
        
        ->where('dat_precios.idcategory', $idCategory)
        ->where('centroventa.subinventory_name', $rutas)
        ->orderBy('dat_precios.precios','desc')
        ->get();

   return $DatPrecios;

    }




           public function RouteroIndex1($id)
    {


$tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$id)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

$city="CULIACAN";

 $categories1 = collect(\DB::table('users')
        ->select('users.*')
        ->where('id', $id)
        ->get());


 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',"Despachado") 
        ->orderBy('a.id','desc')
        ->get());


        return compact('pedidos');

    
  } 


  public function RouteroIndex2($id)
    {

      $tiposs = collect(\DB::table('catdirenv')     
        ->select('tipopedido')
        ->where('iduser',$id)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

$city="CULIACAN";



 $pedidosEnt = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',"Entregado") 
        ->orderBy('a.id','desc')
        ->get());

        return compact('pedidosEnt');

    
  } 

   public function RouteroIndex3($id)
    {

$tiposs = collect(\DB::table('catdirenv')     
        ->select('tipopedido')
        ->where('iduser',$id)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

$city="CULIACAN";


 $pedidosAuth = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_entrega as impoentre','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',"Pagado") 
        ->orderBy('a.id','desc')
        ->get());

        return compact('pedidosAuth');

    
  } 


  public function RuteroEdit($id)
    {

 $cart = Cart::find($id);

$purchases = \DB::table('datpackingpedido')
    ->select(\DB::raw('SUM(importe_diferencia) AS sumafinal'))
    ->where('pedido', $id)
    ->get();


               return compact('cart','purchases');
    }

  

    public function RouteroIndex4($id)
    {



$unaut= collect(\DB::table('modificacionpedido as detalles')

  
->join('products as producto', 'producto.id', '=', 'detalles.id_product')
->leftJoin('datpackingpedido as packing', function($join)
                  {
                      $join->on('packing.pedido', '=', 'detalles.id_cart');
                      $join->on('producto.codigo','=', 'packing.codigo');
                    
                  })             
->join('carts as carrito', 'carrito.id', '=', 'detalles.id_cart')
->join('dat_precios as precios', 'precios.id', '=', 'detalles.iddtaprecio')

 ->select('carrito.id','detalles.id as idmodificacion','producto.id as idpro', 'detalles.importe','carrito.importe_modificar','producto.name','detalles.price','detalles.quantity','detalles.uom','detalles.iva','packing.packing','packing.cantidad','detalles.quantityentrega','packing.codeb','packing.codigo','packing.entregado','detalles.importe_entrega')

 ->where('carrito.id', $id)
 ->where('packing.packing', '!=' ,"")
 
 ->get());



               return compact('unaut');
    }

     public function RouteroImprimir($id)
    {

 $cart = Cart::find($id);

$purchases = \DB::table('datpackingpedido')
    ->select(\DB::raw('SUM(importe_diferencia) AS sumafinal'))
    ->where('pedido', $id)
    ->get();

return compact('cart','purchases');
    }    
   



public function elegirtipopago($id)
    {
   
 

    $stat="Entregado";


$modi = Cart::find($id);

  $iduser = $modi->user_id;

      
$creditu= Catdirenv::query()->where('iduser','=',$iduser)->first()->credito_contado;


    
$pedidos= collect(\DB::table('carts as carrito')
    
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
       ->join('citys as ciudad', 'ciudad.name', '=', 'usuario.city')
       ->join('centroventa as centro', 'carrito.idcentrovta', '=', 'centro.id')


        ->select('carrito.id','centro.descripcion as descentro','carrito.importe_total', 'carrito.order_date','carrito.created_at','carrito.status','ciudad.name as city','usuario.name','usuario.address','usuario.phone','carrito.importe_entrega')

        ->where('carrito.id', $id)
        ->where('carrito.status', $stat)
        ->get());
   

       $di = collect(\DB::table('cattipopago')
        ->select('id', 'descripcion', )       
        ->get());

       $ei = collect(\DB::table('catformapago')
        ->select('valor', 'Descripcion', )       
        ->get());

  
        
        return compact('modi','creditu');
    }  



    public function ticket($id){

$pedidos = collect(\DB::table('carts as carrito')    
->join('modificacionpedido as modi', 'modi.id_cart', '=', 'carrito.id') 
->join('users as us', 'us.id', '=', 'carrito.user_id')
        
    ->select('carrito.id as idcar','carrito.importe_entrega as total','carrito.ruta','carrito.fechaE','modi.*','us.colonia','us.numeroexterior','us.numerointerior','us.address','us.city','us.name as nombrecliente','us.codigopostal')

->where('carrito.id', $id) 

    ->get());


      $modifi = DB::table('rutas.modificacionpedido')
      ->select('nameProduct','quantityentrega','price','importe_entrega')
         ->where('id_cart',$id)
         ->orderBy('id', 'desc')
         ->get();


return view('Ticket')->with(compact('pedidos','modifi'));
}



public function entregado(Request $request)
    {
   $id= $request->id;
   

        $faltante=0;

 $contarmodificacionPedido =   collect(\DB::table('modificacionpedido')
        ->join('products as producto', 'producto.id', '=', 'modificacionpedido.id_product')
        ->leftJoin('datpackingpedido as packing', function($join)
                  {
                      $join->on('packing.pedido', '=', 'modificacionpedido.id_cart');
                      $join->on('producto.codigo','=', 'packing.codigo');
                    
                  })  
        ->select(\DB::raw('count(modificacionpedido.id) as faltante_count'))
        ->where('modificacionpedido.entregado',$faltante)
        ->where('modificacionpedido.id_cart',$id)     
        ->where('packing.packing', '!=' ,"")      
        ->get());

 foreach ($contarmodificacionPedido as $contador) {
                       $totales=$contador->faltante_count;
                     
                   }
 if ($totales==0) {                  

    $Despachado='Despachado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Despachado)
        ->update(['status' => 'Entregado']);

    $response = "agregado al carrito correctamente";
        return response($response, 200);

    }else{

     $Despachado='Despachado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Despachado)
        ->update(['status' => $Despachado]);

    $response = "agregado al carrito correctamente";
        return response($response, 200);
    }    
  }


  

     public function indexsearchrepoterutero($idstatus,$idFechaI,$idFechaF)
    {
 
 $idruta="AC3";

 if ($idstatus=="0") {

  $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('a.ruta',$idruta) 
        ->where(function($q) {
         $q->where('a.status', "Despachado")
         ->orWhere('a.status', "Entregado")
         ->orWhere('a.status', "Pagado");
     })
         ->whereBetween('order_date', [$idFechaI, $idFechaF])
        ->orderBy('a.id','desc')
        ->get());


return $pedidos;
 }else{
 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('a.ruta',$idruta) 
        ->where('a.status',$idstatus) 
         ->whereBetween('order_date', [$idFechaI, $idFechaF])
        ->orderBy('a.id','desc')
        ->get());


return $pedidos;
}

   
}


  public function updatePago(Request $request)
    {
$datefinal= Carbon::now();

$dt = $datefinal->subHours(2);

$id= $request->id;

$catPagos = new catPagos();
$catPagos->order_id = $id;
$catPagos->idforma_pago= $request->idforma_pago;
$catPagos->idtipopago= $request->idtipopago;
$catPagos->importe = $request->importe;
$catPagos->fechapago = $dt->toDateTimeString();
$catPagos->save();



    $Pendiente='Entregado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Pagado']);
      

    
    $response = "Pagado correctamente";
        return response($response, 200);
  
  }



 public function updatecarroruta(Request $request)
   {


$idcart= $request->idcart;
$idmodifi= $request->idmodifi;
$idproduct= $request->idproduct;
$codeb= $request->codeb;



$iduser=1680;
$cantidad = $request->cantidad;


$datefinal= Carbon::now();

$dt = $datefinal->subHours(2);

$catEntregaPedido = new EntregaPedidosModel();
$catEntregaPedido->pedido = $idcart;

$catEntregaPedido->codigo= $request->codigoProducto;
$catEntregaPedido->codeb = $codeb;
$catEntregaPedido->cantidad= $cantidad;
$catEntregaPedido->id_product= $idproduct;

$catEntregaPedido->Fecha_entrega = $dt->toDateTimeString();
$catEntregaPedido->user_id= $iduser;
$catEntregaPedido->save();


     
      $precio = $request->price;
       $ivas = $request->iva;
       $ImpoteFina = $cantidad*$precio;
       $ImpoteInitial=$request->Importe_actual;
       $ImpoteDefinitivo=$ImpoteInitial-$ImpoteFina;


            
        $client = \DB::table('modificacionpedido')
        ->where('id', $idmodifi)
        ->where('id_cart', $idcart)
        ->update(['entregado' => 1,
          'importe_diferencia' => $ImpoteDefinitivo]);


    $response = "Pagado correctamente";
        return response($response, 200);

}


  public function Ruteroeditmodi($idmodificacion)
    {

 $cart = ModificacionPedido::find($idmodificacion);


       $ei = collect(\DB::table('modificacionpedido')
        ->select('modificacionpedido.*' )    
        ->where('id',$idmodificacion)   
        ->get());

  
        
        return compact('cart','ei');
    }




    public function RouteroIndex5($idmodificacion)
    {




$unaut= collect(\DB::table('modificacionpedido as detalles')

  
->join('products as producto', 'producto.id', '=', 'detalles.id_product')
->leftJoin('datpackingpedido as packing', function($join)
                  {
                      $join->on('packing.pedido', '=', 'detalles.id_cart');
                      $join->on('producto.codigo','=', 'packing.codigo');
                    
                  })             
->join('carts as carrito', 'carrito.id', '=', 'detalles.id_cart')
->join('dat_precios as precios', 'precios.id', '=', 'detalles.iddtaprecio')

 ->select('carrito.id','detalles.id as idmodificacion','producto.id as idpro', 'detalles.importe','carrito.importe_modificar','producto.name','detalles.price','detalles.quantity','detalles.uom','detalles.iva','packing.packing','packing.cantidad','detalles.quantityentrega','packing.codeb','packing.codigo','packing.entregado','detalles.importe_entrega')

 ->where('detalles.id', $idmodificacion)
 ->where('packing.packing', '!=' ,"")
 
 ->get());




               return compact('unaut');
    }





public function repotepedidos($id)
    {

      
    $city="CULIACAN";

 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$id) 
        ->where(function($q) {
         $q->where('a.status', "Despachado")
         ->orWhere('a.status', "Entregado")
         ->orWhere('a.status', "Pagado");
     })
        ->orderBy('a.id','desc')
        ->get());



        return compact('pedidos');
    }



    public function  show($id,$idCentroDeVenta) {

        $product = Product::find($id);
		$codigo = $product->codigo;
    	


        $DatPrecios = DatPrecios::select('dat_precios.*','product_images.image') 
        ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
        ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
        ->where('dat_precios.idcentroventa', $idCentroDeVenta)
        ->where('dat_precios.idproduct', $id)
        ->get();

		$Datinvcedis= Datinvcedis::where('codigo', '=', $codigo)->get();
		

        $contar =   collect(\DB::table('datinvcedis as cedis')
        ->select(\DB::raw('count(cedis.id) as cedis_count'))
        ->where('cedis.codigo',$codigo)
       
        ->get());

    	return compact('product');

    }

    public function  showExistencia($id) {

        $product = Product::find($id);
        $codigo = $product->codigo;
        
    	
		$Datinvcedis= Datinvcedis::where('codigo', '=', $codigo)->first();
		
    	return compact('Datinvcedis');

    }

    public function buscarVersionAnterior($query,$iduser)
    {   
    //dump("buscar");
        $almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$iduser)
        ->get());
    
               foreach ($almacenRuta as $almacen)
        {
                  $rutas=$almacen->almacen_ruta;
        }

        if ($rutas=="") {

            $rutero = collect(\DB::table('catrepartidor')
            ->select('catrepartidor.ruta')
            ->where('rol',"R")
            ->get());
        
                   $first = $rutero->first()->ruta;
    
     $idCategory1=1;
     $idCategory2=2;
     $idCategory3=3;
     $idCategory4=4;
    
     $e = DatPrecios::select(\DB::raw('count(dat_precios.id) as product_count')) 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->where('centroventa.subinventory_name', $rutas)   
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->get(); 
    
    
             $a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory1)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
           
             
    
    $b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory2)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
            
    
    $c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory3)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
             
    $d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory4)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image')

             ->union($a)
             ->union($b)
             ->union($c)   
             ->get();                           
    
$products= $d;

return $products;

}else{
    $idCategory1=1;
     $idCategory2=2;
     $idCategory3=3;
     $idCategory4=4;
    
     $e = DatPrecios::select(\DB::raw('count(dat_precios.id) as product_count')) 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->where('centroventa.subinventory_name', $rutas)   
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->get(); 
    
    
    $a = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory1)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
           
             
    
    $b = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory2)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
            
    
    $c = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory3)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image');
             
    $d = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
             ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
             ->leftjoin('product_images', 'dat_precios.codigo', '=', 'product_images.codigo')
             ->where('centroventa.subinventory_name', $rutas)         
             ->where('dat_precios.idcategory', $idCategory4)
             ->where('dat_precios.nameproduct','like',"%$query%")
             ->whereNotNull('product_images.image')
           
             ->union($a)
             ->union($b)
             ->union($c)   
             ->get();                           
    
$products= $d;

return $products;
}
    
    }

    public function buscar($query,$iduser){ 

        $codigo_cliente = Catdirenv::take(1)->where('iduser',$iduser)->get();
        //return $codigo_cliente;

            foreach ($codigo_cliente as $codigo_clientes) {
                $clave=$codigo_clientes->clavecliente;
                $idCentroDeVenta=$codigo_clientes->tipopedido;
            } 

            if ($clave=="") {
                $rutero = collect(\DB::table('catrepartidor')
                ->select('catrepartidor.tipo_pedido')
                ->where('rol',"R")
                ->get());

                $first = $rutero->first()->tipo_pedido;
                $idCentroDeVenta=$first;
            }

            $idCentroDeVenta2 =   collect(\DB::table('centroventa')
                ->select('centroventa.*')
                ->where('tipopedido',$idCentroDeVenta)       
                ->get());

            foreach ($idCentroDeVenta2 as $idCentroDeVenta2s) {
                $ruta=$idCentroDeVenta2s->id;
            }
        
        ///INICIO BLOQUE//
        /*$almacenRuta = collect(\DB::table('catdirenv')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$iduser)
        ->get());*/

       

        $almacenRuta = collect(\DB::table('catdirenv')
        ->join('centroventa', 'centroventa.tipopedido', '=', 'catdirenv.tipopedido')
        ->select('catdirenv.almacen_ruta')
        ->where('iduser',$iduser)
        ->where('centroventa.activo',1)
        ->get());
      


            foreach ($almacenRuta as $almacen)
            {
                    $rutas=$almacen->almacen_ruta;
            }


        /*$Usuarios = collect(\DB::table('users')
        ->select('users.*')
        ->where('id',$iduser)
        ->get());*/
        $catdirenv= Catdirenv::where('iduser',$iduser)->get();

        //return $catdirenv;

        //return $catdirenv;

        foreach ($catdirenv as $catdirenvs) {
            $ClaveCliente=$catdirenvs->clavecliente;
        }

        //FIN BLOQUE//
        
        /*$DatPrecios = DB::select("SELECT DISTINCT dp.id,dp.idproduct,dp.idcatprecio,dp.idcentroventa,dp.nameproduct,dp.idcategory,cv.subinventory_name,dp.nameproduct as nameProd,
        CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
        dp.precios
        ELSE 
        FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios,
        dp.idcentroventa,dp.codigo,prod_img.image
        FROM rutas.dat_precios as dp
        LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id
        LEFT JOIN rutas.products as products ON dp.idproduct=products.id
        LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
        LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
        WHERE dp.idcategory IN (1,2,3,4) and cv.subinventory_name = '$rutas' 
        and dp.nameproduct like '%$query%' AND existencia.organization_id=cv.organization_id and prod_img.image 
        is not null  and cv.organization_id=cv.organization_id order by dp.precios desc;");*/

        //return 

        //New query para existencia  
        $DatPrecios = DB::select("SELECT * from(
            SELECT dp.id,dp.idproduct,dp.idcatprecio,dp.nameproduct,dp.idcategory,cv.subinventory_name, 
                 CASE WHEN FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name)=0 THEN 
                    dp.precios
                ELSE 
                FN_PROMOCION(dp.codigo,cv.organization_id,'$ClaveCliente',cv.subinventory_name) END as precios ,
                    dp.idcentroventa,dp.codigo,prod_img.image,(existencia.existencia- rutas.FN_PEDIDOS_RESERVA(cv.organization_id,dp.idproduct)) reserva
                FROM rutas.dat_precios as dp
                LEFT JOIN rutas.centroventa as cv ON dp.idcentroventa=cv.id AND cv.activo=true
                LEFT JOIN rutas.products as products ON dp.idproduct=products.id
                LEFT JOIN rutas.product_images as prod_img ON products.codigo=prod_img.codigo
                LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=products.idproducto AND existencia.organization_id=cv.organization_id
                WHERE dp.idcategory in (1,2,3,4) and cv.subinventory_name = '$rutas' 
                    and dp.nameproduct like '%$query%'
                    AND existencia.organization_id=cv.organization_id        
                    and  existencia.existencia>0 
                AND prod_img.image is not null ) a        
                    WHERE reserva>0        
                order by a.nameproduct asc;");

        return  $DatPrecios;
    }

    public function usuario($username) {

 

$Usuario= User::select('id')->where('username', $username)->get();
return compact('Usuario');

    }

    public function store(Request $request)

    
    {

        switch ($request->uom) {
            case 1:
                $query->where([
                    ['meterfrom', '>=', '5'],
                    ['meterto', '<=', '50']
                ]);
                break;
            case 2:
                $query->where([
                    ['meterfrom', '>=', '50'],
                    ['meterto', '<=', '85']
                ]);
                break;
                case 3:
                    $query->where([
                        ['meterfrom', '>=', '50'],
                        ['meterto', '<=', '85']
                    ]);
                    break;
            }
            

            
            }
        


    public function cart(Request $request)
    {
        $cart = new Cart();
        $cartDetail->cart_id = $request->cart_id;
        $cartDetail->uom1 = $request->uom;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->quantity = $request->quantity;
        $cartDetail->quantity1 =0;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->quantitycaja = 0;
        $cartDetail->save();


        $response = "agregado al carrito correctamente";
        return response($response, 200);
 


    }
 



public function repotepedidos2()
    {

      $id="AC3";
    $city="CULIACAN";

 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$id) 
        ->where(function($q) {
         $q->where('a.status', "Despachado")
         ->orWhere('a.status', "Entregado")
         ->orWhere('a.status', "Pagado");
     })
        ->orderBy('a.id','desc')
        ->get());



        return $pedidos;
    }

    public function ruterorutero($id)
    {

$tiposs = collect(\DB::table('users')     
        ->select('rutero')
        ->where('id',$id)
        ->get());
        return compact('tiposs');   
  } 



    public function cartDetailInfo($idCart) {

$CartDetail = CartDetail::where("cart_id","=",$idCart)->get();

         return $CartDetail;

    }

    public function cartInfo($idUser) {

      /*  $Cart = Cart::where("user_id","=",$idUser)->select("id")->get()->last();
                 return $Cart; */

           $Cart =Cart::where("user_id","=",$idUser)->select("id")->orderby('id','DESC')->take(1)->get();  
           foreach ($Cart as $carts)
           {
                     $idCart=$carts->id;
           }
           return $idCart;
        
            }

            public function cartDetail(Request $request)
            {
        
        
         if($request->uom == "KG"){
        
                $factor = $request->factor;
                $pesopromedio = $request->pesopromedio;
                $exi = $request->existencia;
                $cantidad=$request->quantity;
                
           if ($exi<=$cantidad) {
                $notification2 = 'Actualmente no contamos con esa cantidad en existencia';
                return back()->with(compact('notification2'));
        
               }else {
                   
                $cartDetail = new CartDetail();
                $cartDetail->cart_id = auth()->user()->cart->id;
                $cartDetail->uom1 = $request->uom;
                $cartDetail->product_id = $request->product_id;
                $cartDetail->quantity = $request->quantity;
                $cartDetail->quantity1 =0;
                $cartDetail->iddatprecio = $request->iddatprecio;
                $cartDetail->precio_producto = $request->precioprod;
                $cartDetail->quantitycaja = 0;
                $cartDetail->save();
        
                $notification = 'El producto se ha cargado a tu carrito de compras exitosamente!';
                return back()->with(compact('notification'));
               }    
        
                
            }else{
        
        
         $factor = $request->factor;
         $pesopromedio = $request->pesopromedio;
         $exi = $request->existencia;
         $cantidad=$request->quantity;
         $cantidadtotal=$cantidad*$pesopromedio;
        
         if ($cantidad>=$exi) {
              $notification2 = 'Actualmente no contamos con esa cantidad en existencia';
                return back()->with(compact('notification2'));
         }else {
        
            $cartDetail = new CartDetail();
            $cartDetail->cart_id = auth()->user()->cart->id;
            $cartDetail->product_id = $request->product_id;
            $cartDetail->uom1 = $request->uom;
            $cartDetail->quantity = ($pesopromedio==0)?$request->quantity:$pesopromedio*$request->quantity;
            $cartDetail->quantity1 = $request->quantity;
            $cartDetail->quantitycaja = $request->quantity;
            $cartDetail->iddatprecio = $request->iddatprecio;
            $cartDetail->precio_producto = $request->precioprod;
            $cartDetail->save();
    
            $notification = 'El producto se ha cargado a tu carrito de compras exitosamente!';
            return back()->with(compact('notification'));
         }
        
               
        
            }
        
            }




            public function userDetail($id) {

                $usuario = User::find($id);

                return $usuario;

            }
    



        public function pedidosActivos($iduser)
        {
            return Cart::select('id','status','importe_total as importe_modificar','fechaE','factura')
            ->with('productos')
            ->where('status','=','Pendiente')
            ->where('user_id','=',$iduser)
            ->get();
        
        }

        public function numeroEjuctivo($iduser){

         $ciudad=collect(\DB::table('users')     
         ->select('city')
         ->where('id',$iduser)
         ->first());
                 
         
         $numeroEjecutivo = User::where('adminpedidos','=',true)->where('city',$ciudad)->select('phone')->first();

         return compact('numeroEjecutivo'); 
         
        }

        public function updateStatusPedido(Request $request){
            //return $request;
            try {
                \DB::table('carts')
                ->where('id', $request->idpedido)
                ->update(['status' => $request->status]);
    
                return response()->json(['response'=>'Pedido modificado correctamente']);
    
            } catch (\Throwable $th) {
                return response()->json(['response'=>'ERROR'+$th]);
            }
        }

             





}
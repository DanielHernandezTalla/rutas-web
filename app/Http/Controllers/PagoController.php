<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Routing\Controller as BaseController;  // <<< See here - no real class, only an alias
use App\Http\Controllers\Controller;
use App\User;
use Conekta\Conekta;
use Conekta\Order;
use Conekta\Charge;
use View;

class PagoController extends Controller
{
  

  public function updatePedidoConfirmado()
    {
      
        return back();
      
    }

public function store(Request $request)
    {

   

  

   
    
    }

    public function pagar(Request $request) {
        Conekta::setApiKey("key_CzhzfUoYGgqxFJRmGTzWHg");
        try{

         $order = \Conekta\Order::create(
    [
      "line_items" => [
        [
          "name" => $request->input('name'),
          "unit_price" => $request->input('importe')*100,
          "quantity" => 1
        ]
      ],
   
      "currency" => "MXN",
      "customer_info" => [
        "name" => $request->input('username'),
        "email" => $request->input('email'),
        "phone" => $request->input('phone')
      ],
      "shipping_contact" => [
        "address" => [
          "street1" => $request->input('address'),
           "postal_code" => "06100",
          "country" => "MX"
        ]
      ], //optional - shipping_contact is only required for physical goods
      "metadata" => ["reference" => $request->input('cart_id'), "more_info" => $request->input('city')  ],
         "charges" => array(
          array(
              "payment_method" => array(
                      "type" => "card",
                       "token_id" => $request->input('conektaTokenId')
              ) //payment_method - use customer's default - a card
                //to charge a card, different from the default,
                //you can indicate the card's source_id as shown in the Retry Card Section
          ) //first charge
      ) //charges
    ]
  );
} catch (\Conekta\ProcessingError $error){
  return View::make('pagoRechazado');

} catch (\Conekta\ParameterValidationError $error){
  return View::make('pagoRechazado');
} catch (\Conekta\Handler $error){
  return View::make('pagoRechazado');
}
return View::make('pago');
}

}

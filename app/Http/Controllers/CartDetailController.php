<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CartDetail;

class CartDetailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Request $request)
    {

        
 if($request->uom == "KG"){

        $factor = $request->factor;
        $pesopromedio = $request->pesopromedio;
        $exi = $request->existencia;
        $cantidad=$request->quantity;
        
   if ($exi<=$cantidad) {
        $notification2 = 'Actualmente no contamos con esa cantidad en existencia';
        return back()->with(compact('notification2'));

       }else {
   
           
         $cartDetail = new CartDetail();
        $cartDetail->cart_id = auth()->user()->cart->id;
        $cartDetail->uom1 = $request->uom;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->quantity = $request->quantity;
        $cartDetail->quantity1 =0;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->quantitycaja = 0;
        $cartDetail->save();

        $notification = 'El producto se ha cargado a tu carrito de compras exitosamente!';
        return back()->with(compact('notification'));
       }    

    	
    }else{

    $factor = $request->factor;
    $pesopromedio = $request->pesopromedio;
    $exi = $request->existencia;
    $cantidad=$request->quantity;
    $cantidadtotal=$cantidad*$pesopromedio;
    //Valida el peso promedio vacio
    if ($pesopromedio==0) {
        $notification2 = 'Actualmente no hay peso promedio para este articulo';
            return back()->with(compact('notification2'));
    }

    if ($cantidad>=$exi) {
        $notification2 = 'Actualmente no contamos con esa cantidad en existencia';
            return back()->with(compact('notification2'));
    }else {

    $cartDetail = new CartDetail();
        $cartDetail->cart_id = auth()->user()->cart->id;
        $cartDetail->product_id = $request->product_id;
        $cartDetail->uom1 = $request->uom;
        $cartDetail->quantity = ($pesopromedio==0)?$request->quantity:$pesopromedio*$request->quantity;
        $cartDetail->quantity1 = $request->quantity;
        $cartDetail->quantitycaja = $request->quantity;
        $cartDetail->iddatprecio = $request->iddatprecio;
        $cartDetail->precio_producto = $request->precioprod;
        $cartDetail->save();

        $notification = 'El producto se ha cargado a tu carrito de compras exitosamente!';
        return back()->with(compact('notification'));
 }

       

    }

    }

    public function destroy($id)
    {
    	CartDetail::where('id_cart_details', $id)->delete();
        $notification2 = 'El producto se ha eliminado del carrito de compras correctamente.';
        return back()->with(compact('notification2'));
}

}
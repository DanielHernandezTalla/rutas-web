<?php

namespace App\Http\Controllers;
namespace  App\Http\Controllers\ProcessController;

use Illuminate\Http\Request;

class BaseController extends Controller
{
   	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
}

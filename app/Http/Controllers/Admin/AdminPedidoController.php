<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\CartDetail;
use App\CentroVenta;
use App\Product;
use App\Colonias;
use App\User;
use App\Price;
use App\Citys;
use App\CatPrecio;
use App\Notificacion;
use App\ModificacionPedido;
use App\Mail\NewOrder;
use App\DatPrecios;
use App\catdirenv;
use App\ProductImage;
use Mail;
use Redirect;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class AdminPedidoController extends Controller
{

public function index2(){
    
    return view('admin.pedidos.firebase');
}


public function getUsers(){

    $city=auth()->user()->city;

    $users = collect(\DB::select("SELECT users.id, users.name, users.colonia, users.email, users.city,users.address,users.phone,users.rfc,users.numeroexterior
    FROM rutas.users
    WHERE users.city='$city' AND users.name IS NOT NULL 
    GROUP BY users.id, users.name, users.avatar, users.email ORDER BY users.name ASC"));
    
    return view('admin.pedidos.usuarios_pedidos',['users'=> $users]);
}

     public function index()
    { 
       
    $city=auth()->user()->city;

    //$pedidos= collect(\DB::select('select a.id,a.fechaE, a.ruta, d.descripcion as descentro, a.idcentrovta , a.importe_total, a.order_date,a.created_at, a.status, a.MotivoRegreso, c.name as city, b.name, b.address, b.phone from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city', ['status' => 'Pendiente','city'=>$city]));

    $pedidos= collect(\DB::select("SELECT distinct a.id,a.factura,d.clavevendedor as claverutero,a.fechaE, e.tipo_pedido as ruta,e.id as idreparto, e.name as descentro, a.idcentrovta , a.importe_modificar as importe_total,
                        a.order_date,a.created_at, a.status, a.MotivoRegreso,c.name as city, b.name, f.direccion, b.phone
                        FROM carts as a
                        LEFT JOIN rutas.users as b ON b.id=a.user_id
                        LEFT JOIN rutas.citys as c ON c.organization_id=b.organization_id
                        LEFT JOIN rutas.centroventa as d ON d.id=a.idcentrovta
                        LEFT JOIN rutas.catdirenv as f ON f.id=a.iddirenv
                        LEFT JOIN (SELECT * FROM rutas.catrepartidor  where rol='R') as e ON e.plaza=b.organization_id and e.ruta=d.subinventory_name and d.clavevendedor=e.clave_vendedor
                    WHERE a.user_id = b.id 
                    and c.name = b.city
                    and a.idcentrovta = d.id  AND a.status='Pendiente' AND b.city='$city';"));
    
    //Se modifico la direccion, ahora la toma del catdirenv(En caso de tener varias direcciones)
    /*$pedidos= collect(\DB::select("SELECT a.id,a.fechaE, a.ruta, d.descripcion as descentro, a.idcentrovta, a.importe_total, a.order_date,a.created_at, a.status, a.MotivoRegreso,
    c.name as city, b.name, b.address, b.phone ,e.direccion
    FROM carts a, users b, citys c,centroventa d , rutas.catdirenv e
    WHERE a.user_id = b.id AND c.name = b.city AND a.idcentrovta = d.id AND a.iddirenv=e.id AND a.status='Pendiente' AND b.city='$city';"));*/

        $contarpendientes =   collect(\DB::table('carts as carrito')
        ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
        ->select(\DB::raw('count(carrito.id) as cart_count'))
        ->where('carrito.status',"Pendiente")
        ->where('usuario.city',$city)
           
        ->get());

        $fecha = strtotime(date('Y-m-d')."- 7 days");
        $semanaAnterior = date('Y-m-d', $fecha);


    $pedidosAuth = DB::select(
        "select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, 
        a.status, c.name as city, b.name, b.address, b.phone  
        from carts as a
        left join users as b on b.id=a.user_id
        left join citys as c on c.name=b.city
        where a.order_date >= '". $semanaAnterior ."'
        and a.status = 'Entregado'
        and b.city = '". $city ."'"
    );

    /*$pedidosAuth= collect(\DB::select(
        "select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, 
        a.status, c.name as city, b.name, b.address, b.phone 
        from carts a, 
        users b, 
        citys c 
        where a.user_id = b.id and c.name = b.city
        AND a.status=:status
        AND b.city=:city
        and a.order_date = 2023-02-16 "
        , ['status' => 'Despachado','city'=>$city] 
    ));*/
    
    //AND a.order_date  >= '".$semanaAnterior."'

    /*$pedidosConf= collect(\DB::select(
        'select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone
         from carts a, users b, citys c 
         where a.user_id = b.id and c.name = b.city 
         AND a.status=:status 
         AND b.city=:city', ['status' => 'Pagado','city'=>$city]));*/

         $pedidosConf = DB::select(
            "select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone
            from carts as a
            left join users as b on b.id=a.user_id
            left join citys as c on c.name=b.city
            where a.order_date >= '". $semanaAnterior ."'
            and a.status = 'Pagado'
            and b.city = '". $city ."'"
        );

    /*$pedidosCancel= collect(\DB::select(
        'select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone
        from carts a, users b, citys c
        where a.user_id = b.id and c.name = b.city 
        AND a.status=:status 
        AND b.city=:city', ['status' => 'Cancelado','city'=>$city]));*/

    $pedidosCancel = DB::select(
            "select a.id,a.fechaE, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone 
            from carts as a
            left join users as b on b.id=a.user_id
            left join citys as c on c.name=b.city
            where a.order_date >= '". $semanaAnterior ."'
            and a.status = 'Cancelado'
            and b.city = '". $city ."'"
        );

    /*$despachados= collect(\DB::select('select a.id,a.fechaE, d.descripcion as descentro, a.clavevendedor,
    a.claverutero, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, 
    b.name, b.address, b.phone from carts a, users b, citys c, centroventa d 
    where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  
    AND a.status=:status 
    AND b.city=:city', ['status' => 'Despachado','city'=>$city]));
*/

    $despachados = DB::select(
        "select a.id,a.fechaE, d.descripcion as descentro, a.clavevendedor,a.claverutero, a.importe_total, a.order_date,a.created_at, 
        a.status, c.name as city, b.name, b.address, b.phone
        from carts as a
        left join users as b on b.id=a.user_id
        left join citys as c on c.name=b.city
        left join centroventa as d on d.id=a.idcentrovta
        where a.order_date >= '". $semanaAnterior ."'
        and a.status = 'Despachado'
        and b.city = '". $city ."'"
    );


   //$preparados= collect(\DB::select('select a.id,a.fechaE, d.descripcion as descentro, a.clavevendedor, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city', ['status' => 'Preparado','city'=>$city]));

   $preparados = DB::select(
        "select a.id,a.fechaE, d.descripcion as descentro, a.clavevendedor, a.importe_total, a.order_date,a.created_at, 
        a.status, c.name as city, b.name, b.address, b.phone 
        from carts as a
        left join users as b on b.id=a.user_id
        left join citys as c on c.name=b.city
        left join centroventa as d on d.id=a.idcentrovta
        where a.order_date >= '". $semanaAnterior ."'
        and a.status = 'Preparado'
        and b.city = '". $city ."'"
    );


    $rutas = User::all()->where('rutero', 1)->pluck('name', 'id');

       $pedidoDetail = new User();
       $pedidoDetail->city = auth()->user()->city;

       $carts = collect(\DB::select('select a.name,  c.order_date , d.username, d.phone, d.address, d.city from products a, cart_details b,  carts c , users d where a.id = b.product_id AND d.id = c.user_id AND b.cart_id = c.id AND c.status=:status', ['status' => 'Pending']));

      

    /*$centroventa = collect(\DB::table('centroventa')
            ->join('citys' , 'citys.id', '=', 'centroventa.idciudad')
            ->select('centroventa.*','citys.name')
            ->where('citys.name',$city)
            ->get());*/

    $organizationid=auth()->user()->organization_id;

    //Query cambio para Luis

    $centroventa = collect(\DB::select("SELECT distinct a.id,a.tipopedido,b.name as descripcion,c.name FROM(SELECT * FROM rutas.centroventa  /*WHERE activo=true*/) as a
                    LEFT JOIN (SELECT * FROM rutas.catrepartidor  where rol='R') as b on a.organization_id=b.plaza AND a.subinventory_name=b.ruta and a.clavevendedor=b.clave_vendedor
                    LEFT JOIN  rutas.citys as c on a.organization_id=c.organization_id   
                    WHERE a.organization_id='$organizationid';"));

    $repartidores = collect(\DB::table('catrepartidor')
    
    ->select('catrepartidor.*')
    ->where('tipo',"RUTA")
    ->where('rol',"R")

    ->get());

  

    $pedidosRegresados =collect(\DB::table('carts')
    ->whereNotNull('MotivoRegreso')->get());

       
        return  view('admin.pedidos.index')->with(compact('rutas','pedidos', 'carts', 'pedidosAuth', 'pedidosConf', 'pedidosCancel','despachados','centroventa','preparados','repartidores','contarpendientes'));

    }  
public function updatecarro(Request $request, $id)
   {
      $modificacioncarro = collect(\DB::table('carts')
    ->select('carts.*')
    ->where('id', $id)
    ->where('status', "Despachado")
    ->get());


        \DB::table('carts')
       
       ->where('id', $id)
         ->where('status', "Despachado")
        ->update(['clavevendedor' => $modificacioncarro->clavevendedor = $request->input('clavevendedor'),
        
    ]);
        $notification = 'Se ha modificado un carrito correctamente.';
         return back()->with(compact('notification'));


}

    public function despachado($id)
    {

        $Preparado='Preparado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Preparado)
        ->update(['status' => 'Despachado']);

        
        return back();
    
    }

    public function cambiorutero(Request $request, $id)
    {
        $claveagente=auth()->user()->agente;
        $act = $request->act;
        $objCentroVenta=CentroVenta::where('id',$act)->first();
        $ruta=$objCentroVenta->tipopedido;
        $claverutero=$objCentroVenta->clavevendedor;
        $fechaE= $request->fechaE;
        //return $ruta;
       
        $client = \DB::table('carts')
        ->where('id', $id)
        ->update(['idcentrovta' => $act,'fechaE'=>$fechaE,'claverutero'=>$claverutero,'clavevendedor'=>$claveagente,'ruta'=>$ruta]);

        
        return back();
    
    }

     public function show($id,$ruta)
    {

       //return $ruta;
        
       $city=auth()->user()->city;
                                                                                                                                                                                                  
         $product = collect(\DB::table('users') 
        ->join('carts' , 'users.id', '=', 'carts.user_id')
        ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
        ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
        // ->join('exponent_push_notification', 'exponent_push_notification.username', '=', 'users.username')
        ->select('users.phone','users.id as userid','users.address','users.username','carts.id','carts.tokenNotificacion','carts.user_id',
         'products.name','products.uom','modificacionpedido.price','modificacionpedido.quantity','carts.importe_total','modificacionpedido.iva',
         'modificacionpedido.id_cart','modificacionpedido.iva','modificacionpedido.importe','carts.importe_modificar')
        ->where('id_cart', $id)
        ->where('city', $city)
        ->get());

        /*$product = collect(\DB::select("SELECT users.phone,users.id as userid,users.address,users.username,carts.id,carts.tokenNotificacion,carts.user_id,products.name,
        products.uom,modificacionpedido.price,modificacionpedido.quantity,carts.importe_total,modificacionpedido.iva,modificacionpedido.id_cart,modificacionpedido.iva,
        modificacionpedido.importe,carts.importe_modificar
        FROM rutas.users as users
        LEFT JOIN rutas.carts as carts ON carts.user_id=users.id
        LEFT JOIN rutas.modificacionpedido as modificacionpedido ON carts.id=modificacionpedido.id_cart
        LEFT JOIN rutas.products as products ON modificacionpedido.id_product=products.id
        WHERE carts.id='$id' AND users.city='$city';"));*/

        
        return view('admin.pedidos.show')->with(compact('product','ruta')); // form de edición
    }

    public function cancelacion($id)
    {

       $city=auth()->user()->city;
       $product = collect(\DB::table('users')
    ->join('carts' , 'users.id', '=', 'carts.user_id')
    ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
   

    ->select('users.phone','users.address','users.username','carts.id','carts.user_id', 'products.name','products.uom','modificacionpedido.price','modificacionpedido.quantity','carts.importe_total','modificacionpedido.iva','modificacionpedido.id_cart','modificacionpedido.iva','modificacionpedido.importe','carts.importe_modificar')
    ->where('id_cart', $id)
    ->where('city', $city)
    ->get());

    return view('admin.pedidos.cancelacion')->with(compact('product',$product)); // form de edición
    }

    public function showDes($id)
    {
        $city=auth()->user()->city;
       $product = collect(\DB::table('users')
    ->join('carts' , 'users.id', '=', 'carts.user_id')
    ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
    ->select('users.phone','users.address','users.username','users.email','carts.id', 'products.name','products.uom','modificacionpedido.importe','modificacionpedido.quantity','carts.importe_total','modificacionpedido.iva','modificacionpedido.id_cart','carts.status')
    ->where('id_cart', $id)
    ->where('city', $city)
    ->get());

        return view('admin.pedidos.despachado')->with(compact('product',$product)); // form de edición
    }
    
    public function confirmed($id)
    {
        $city=auth()->user()->city;
        $product = collect(\DB::table('users')
        ->join('carts as carrito', 'users.id', '=', 'carrito.user_id')
        
      ->join('modificacionpedido', 'carrito.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')

        ->select('users.*', 'products.name','products.price','products.uom','carrito.id','carrito.importe_total','modificacionpedido.iva','modificacionpedido.importe','modificacionpedido.quantity')
        ->where('carrito.id', $id)
        ->where('city', $city)
        ->get());

        return view('admin.pedidos.auth')->with(compact('product',$product)); // form de edición
    }

    public function pagado($id)
    {
        $city=auth()->user()->city;
        $product = collect(\DB::table('users')
        ->join('carts as carrito', 'users.id', '=', 'carrito.user_id')
        ->join('cart_details', 'carrito.id', '=', 'cart_details.cart_id')
        ->join('products', 'cart_details.product_id', '=', 'products.id')
        ->select('users.*', 'products.name','products.price','products.uom','carrito.id')
        ->where('cart_id', $id)
        ->where('city', $city)
        ->get());

        return view('admin.pedidos.pagado')->with(compact('product',$product)); // form de edición
    }

    public function cancelado($id)
    {
        $city=auth()->user()->city;
        $product = collect(\DB::table('users')
        ->join('carts as carrito', 'users.id', '=', 'carrito.user_id')
        ->join('cart_details', 'carrito.id', '=', 'cart_details.cart_id')
        ->join('products', 'cart_details.product_id', '=', 'products.id')
        ->select('users.*', 'products.name','products.price','carrito.id','carrito.importe_total')
        ->where('cart_id', $id)
        ->where('city', $city)
        ->get());

        return view('admin.pedidos.cancelado')->with(compact('product',$product)); // form de edición
    }


   public function productos($id)
    {
        $product = collect(\DB::table('users')
    ->join('carts', 'users.id', '=', 'carts.user_id')
    ->join('cart_details', 'carts.id', '=', 'cart_details.cart_id')
    ->join('products', 'cart_details.product_id', '=', 'products.id')
    ->select('users.*', 'products.name','products.price')
    ->where('cart_id', $id)
    ->get());
        return $product; // form de edición
    }

    public function edit( $id)
    {
        

       $product = collect(\DB::table('users')
    ->join('carts' , 'users.id', '=', 'carts.user_id')
    ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
    ->select('users.*','products.name','products.uom','modificacionpedido.*','carts.importe_total','carts.id as carroid','carts.importe_modificar')
    ->where('id_cart', $id)
    ->get());
        return view('admin.pedidos.edit')->with(compact('product')); // form de edición

    }

    public function prueba(Request $request)
    {
     
        $city=auth()->user()->city;

        $pedidos= collect(\DB::select('select a.id, d.descripcion as descentro, a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone from carts a, users b, citys c, centroventa d where a.user_id = b.id and c.name = b.city and a.idcentrovta = d.id  AND a.status=:status AND b.city=:city', ['status' => 'Pendiente','city'=>$city]));
        return $pedidos;

}


public function create(Request $request)
{

   
$codigo = $request->product_codigo;
$cantidad = $request->cantidadbuena;
$carro = $request->cart_id;
$organizationid=auth()->user()->organization_id;

//$codigoProducto = Datprecios::select('idproduct','idcentroventa')->where('codigo',$codigo)->get();

$codigoProducto = Product::select('id','name')->where('codigo',$codigo)->where('organization_id',$organizationid)->get();

$carritoProducto = Cart::select('idcentrovta')->where('id',$carro)->first();


foreach ($codigoProducto as $codigoProductos) {
    //$idproduct=$codigoProductos->idproduct;
    $idproduct=$codigoProductos->id;
}

$producto = collect(\DB::table('dat_precios')

->select('dat_precios.*')
->where('idproduct', $idproduct)
->where('idcentroventa', $carritoProducto->idcentrovta)
->get());

foreach ($producto as $productos) {
  
    $productoindicado=$productos->precios;
    $idprecio=$productos->id;
    $productName=$productos->nameproduct;

}

$importecerca=$cantidad*$productoindicado;

//Start--Con esto sacamos el precio con promo
    $userId = Cart::select('user_id')->where('id',$carro)->get()->first()->user_id;
    $userAli = Catdirenv::select('clavecliente')->where('iduser',$userId)->get()->first()->clavecliente;
    $userAlmacen = Catdirenv::select('almacen_ruta')->where('iduser',$userId)->get()->first()->almacen_ruta;
    $userPrecio=collect(\DB::select("SELECT FN_PROMOCION('$codigo','$organizationid','$userAli','$userAlmacen') as precios;"));

    foreach ($userPrecio as $user) {
        $precioActualizado= $user->precios;
    }
    
//End--

    $cartDetail = new ModificacionPedido();
    $cartDetail->id_cart = $carro;
    $cartDetail->iddtaprecio = $idprecio;
    $cartDetail->nameProduct = $productName;
    $cartDetail->id_product = $idproduct;
    $cartDetail->quantity = $cantidad;
    $cartDetail->price = $precioActualizado=='0.00'?$productoindicado:$precioActualizado;
    $cartDetail->importe = $importecerca;
    $cartDetail->codigo = $codigo;
    
    $cartDetail->save();
    return back();
   
}

    public function createPedidoEditado(Request $request)
    {
        $cartDetail = new CartDetail();
        $cartDetail->cart_id = $request->input('cart_id');
        $cartDetail->product_id = $request->input('product_id');
        $cartDetail->quantity = $request->input('quantity');
        $cartDetail->save();
        return back();
    }


    
    public function update(Request $request)
    {
        $cartDetail = new CartDetail();
        $cartDetail->cart_id = $request->input('cart_id');
        $cartDetail->quantity = $request->input('quantity');
        $cartDetail->save();
        return back();
    }

        public function updatePedido(Request $request, $id)
    {

       

$iva = $request->ivabueno;
 $cantidad = $request->cantidadbuena;

      $precio = $request->preciobueno;

if ($iva==0) {

  
     


        $modificacionpedido = collect(\DB::table('modificacionpedido')
    ->select('modificacionpedido.*')
    ->where('id', $id)
    ->get());


        \DB::table('modificacionpedido')
       
        ->where('id', $id)
        ->update(['quantity' => $modificacionpedido->quantity = $cantidad,
                  'importe' => $modificacionpedido->importe = $cantidad*$precio]);

        return back(); 
}else{

 
     


        $modificacionpedido = collect(\DB::table('modificacionpedido')
    ->select('modificacionpedido.*')
    ->where('id', $id)
    ->get());


        \DB::table('modificacionpedido')
       
        ->where('id', $id)
        ->update(['quantity' => $modificacionpedido->quantity = $cantidad,
                  'importe' => $modificacionpedido->importe = $cantidad*$precio*$iva]);  
}


        


    }

     public function updateDespachado( Request $request,$id)
    {

     $status="Despachado";
        $carts = collect(\DB::table('carts')
        ->select('carts.*')
        ->where('id', $id)
        ->get());

        \DB::table('carts')
        ->where('id', $id)
        ->update(['status' => $carts->status = $status]);

        $client = auth()->user(); 
        $cart = $client->cart;
       

       $admins = collect(\DB::table('users')
        ->join('carts as carrito', 'users.id', '=', 'carrito.user_id')
        ->select('users.*')
        ->where('carrito.id', $id)     
        ->get());
        Mail::to($admins)->send(new NewOrder($client, $cart));

     return redirect('/admin/pedidos');
    


    }

    public function destroy($id)
    {
        CartDetail::where('id_cart_details', $id)->delete();
        return back();
    }



public function destroyEdit($id)
    {
        ModificacionPedido::where('id', $id)->delete();
        return back();
    }









 public function correosusers()
{
    $listusers = collect(\DB::table('users')
        
    ->select('users.*')
    ->where('activo', 0)
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
   
   ->WhereNull('email')

   
    ->get());


   

    return view('admin.correouser.correouser',compact("listusers",$listusers));
} 

public function productosactivos(Request $request)
{
        //return request()->all();
        $name  = $request->get('name');
    	$codigo = $request->get('codigo');
        $city = $request->get('city');

        //GET de sucursales para el select de productos activos
        $sucursales = collect(\DB::table('citys')
        ->select('citys.*')
        ->get());
        
    	$productos = DB::table('products as a')
        ->leftjoin('citys as b','b.organization_id','a.organization_id')
        ->select('a.*', 'b.name as city')
        ->where('a.name', 'LIKE', '%'.$name.'%')
        ->where('a.codigo', 'LIKE', '%'.$codigo.'%')
        ->where('a.organization_id', 'LIKE', '%'.$city.'%')
        ->paginate(10);

    return view('admin.productosactivos.productosactivos', compact(['productos','sucursales','city','name','codigo']));
} 

public function UpdateStatusProduct(Request $request){ 

    $fechaEliminacion = Carbon::now();
    $usuarioEliminacion= auth()->user()->id;

    $productos = Product::find($request->id);
    $productos->activo = $request->status;
    $productos->fecha_eliminacion = $fechaEliminacion;
    $productos->usuario_elimina = $usuarioEliminacion;
    $productos->save();

    if($request->estatus == 0)  {
        $newStatus = '<br> <button type="button" class="btn btn-sm btn-danger">Inactiva</button>';
    }else{
        $newStatus ='<br> <button type="button" class="btn btn-sm btn-success">Activa</button>';
    }

    return response()->json(['var'=>''.$newStatus.'']);
    }

    //Update para setear img default a productos por codigo
    public function UpdateImgProducto(Request $request){ 

        try {
            //Valida si el producto ya esta registrado
            $lstProductImg =collect(\DB::table('product_images')
            ->where('codigo',$request->codigo)
            ->get());

            //return $lstProductImg; 

            //No debe de traer registros
            if($lstProductImg->count()==0){
                //Insert a ala tabla de product_image    
                $ProductImage = new ProductImage();
                $ProductImage->image = 'default.gif';
                $ProductImage->product_id = 0;
                $ProductImage->created_at = 0;
                $ProductImage->updated_at =null;
                $ProductImage->dat_precios_id = 0;
                $ProductImage->codigo = $request->codigo;
                $ProductImage->save();
            }

            //Update a la tabla de productos que setea la cantidad de piezas en el campo factor
            if($request->cantPieza>=0 && !$request->cantPieza==null){
                \DB::table('products')
                ->where('id', $request->idproducto)
                ->update(['factor' => $request->cantPieza]);
            }
            
        } catch(\Exception $e){
             return response()->json(['response'=>'ERROR'+$e]);
        }

        return response()->json(['response'=>'Producto modificado']);
    }

    //Update de usuarios oracle
    public function updateUserOracle(Request $request){
        return $request->all();
    }

    //Update al status del pedido
    public function updateStatusPedido(Request $request){
        return $request;
        try {
            \DB::table('carts')
            ->where('id', $request->idpedido)
            ->update(['status' => $request->status]);

            return response()->json(['response'=>'Pedido modificado correctamente']);

        } catch (\Throwable $th) {
            return response()->json(['response'=>'ERROR'+$th]);
        }
       
    }

public function updateusercorreo(Request $request, $iduser)
   {
   
 $email = $request->input('email');

 if ($email) {
     $modiuseremail = collect(\DB::table('users')
    ->select('users.*')
    ->where('id', $iduser)
    

    ->get());


        \DB::table('users')

       ->where('id', $iduser)
        ->update(['email' => $modiuseremail->email = $request->input('email'),
         

        
    ]);
        $notification = 'Se ha modificado un producto correctamente.';
         return back()->with(compact('notification'));
 }else{
      $notification = 'Se ha modificado un producto correctamente.';
         return back()->with(compact('notification'));
 }

      


}

 public function newuserejecutivo()
{
      $citys = Citys::all()->pluck('name', 'id');
      $colonia=auth()->user()->colonia;

    $listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
        ->join('catciudades as catCiudad', 'usuario.colonia', '=', 'catCiudad.colonia') 

    ->select('usuario.*','catCiudad.estado')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 0)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->groupBy('id','catCiudad.id')

  
    ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
         ->orWhere('carrito.status', "Despachado");
     })

   
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->groupBy('id')
    ->get());


    return view('admin.pedidos.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));
}


public function newuserejecutivo_edit($id)
{
    $city=auth()->user()->city;

$centroventa = collect(\DB::table('centroventa')
    ->join('citys' , 'citys.id', '=', 'centroventa.idciudad')
    ->select('centroventa.*','citys.name')
    ->where('citys.name',$city)
    ->get());


    $listusers = collect(\DB::table('users as usuario')
    ->select('usuario.*')
    ->where('usuario.id', $id)
    ->get());


  

    return view('admin.pedidos.newusersdr',compact("listusers",$listusers,"centroventa",$centroventa));
}


    public function newuserejecutivo_update(Request $request,$id)
    {
       
      $lunes = $request->lunes;
      if ($lunes != 1) {
      $lunesb=0;   
      }else{
      $lunesb=1;  
      }
      $martes = $request->martes;
      if ($martes != 1) {
      $martesb=0;   
      }else{
      $martesb=1;  
      }
      $miercoles = $request->miercoles;
      if ($miercoles != 1) {
      $miercolesb=0;   
      }else{
      $miercolesb=1;  
      }
      $jueves = $request->jueves;
      if ($jueves != 1) {
      $juevesb=0;   
      }else{
      $juevesb=1;  
      }
      $viernes = $request->viernes;
      if ($viernes != 1) {
      $viernesb=0;   
      }else{
      $viernesb=1;  
      }
      $sabado = $request->sabado;
      if ($sabado != 1) {
      $sabadob=0;   
      }else{
      $sabadob=1;  
      }


 $rutalata=1;


        $modificacionusuariosadmin = collect(\DB::table('catdirenv')
        
       

        ->select('catdirenv.*')
        
        ->where('iduser', $id) 
        ->get());

      

        \DB::table('catdirenv')
       
        
        ->where('iduser', $id) 
        ->update(['lunes' => $modificacionusuariosadmin->lunes = $lunesb,
            'martes' => $modificacionusuariosadmin->martes = $martesb,
            'miercoles' => $modificacionusuariosadmin->miercoles = $miercolesb,
            'jueves' => $modificacionusuariosadmin->jueves = $juevesb,
            'viernes' => $modificacionusuariosadmin->viernes = $viernesb,
            'sabado' => $modificacionusuariosadmin->sabado = $sabadob,
       
            'tipopedido' => $modificacionusuariosadmin->tipopedido = $request->input('rutauser'),
                   ]);



$modificacionusuarioadminstrator = collect(\DB::table('users')
    
        ->select('users.*')        
        ->where('id', $id) 
        ->get());      

        \DB::table('users')        
        ->where('id', $id) 
        ->update(['alta_oracle' => $modificacionusuarioadminstrator->alta_oracle = $rutalata,
           
                   ]);

         return redirect('/adminpedidos/newuser');
    }


public function listprice()
    {
       
 

        $price = collect(\DB::table('catprecios as catpre')
        ->join('cattipoprecio as tippre', 'tippre.id', '=', 'catpre.idtipoprecio')
        ->select('catpre.id as filtro', 'catpre.descripcion as namecat','tippre.descripcion as tipo')        
        ->get());

        

        return view('admin.precios.precios')->with(compact('price',$price)); // form de edición 
    }


public function editprecio()
    {
       
 

        $price = collect(\DB::table('ecommerce.citys as cit')
        ->join('ecommerce.centroventa as cen', 'cit.id', '=', 'cen.idciudad')
        ->join('ecommerce.dat_precios as pre', 'pre.idcatprecio', '=', 'cen.idcatprecio')
        ->join('ecommerce.catprecios as catpre', 'catpre.id', '=', 'pre.idcatprecio')
        ->join('ecommerce.products as prd', 'prd.id', '=', 'pre.idproduct')
        ->join('ecommerce.categories as cat', 'cat.id', '=', 'prd.category_id')
        ->select('prd.codigo as code', 'prd.name as nameproduct','pre.precios as actual','pre.precios as nuevo','pre.idcatprecio as filtro')        
          ->distinct()->get());

        $precios = Price::all()->pluck('descripcion', 'id');

        return view('admin.precios.precios')->with(compact('price','precios',$price,$precios)); // form de edición 
    }

    

    public function listpricefilter($id)
    {
       
        

        $price = collect(\DB::table('ecommerce.citys as cit')
        ->join('ecommerce.centroventa as cen', 'cit.id', '=', 'cen.idciudad')
        ->join('ecommerce.dat_precios as pre', 'pre.idcatprecio', '=', 'cen.idcatprecio')
        ->join('ecommerce.catprecios as catpre', 'catpre.id', '=', 'pre.idcatprecio')
        ->join('ecommerce.products as prd', 'prd.id', '=', 'pre.idproduct')
        ->join('ecommerce.categories as cat', 'cat.id', '=', 'prd.category_id')
        ->select('prd.codigo as code', 'prd.name as nameproduct','pre.precios as actual','pre.precios as nuevo','pre.idcatprecio as filtro')  
        ->where('pre.idcatprecio', $id) 
        ->distinct()
        ->get());

       

        return view('admin.precios.preciosEdit')->with(compact('price',$price)); // form de edición 
    }

public function editprecio1($id,$id2)
    {
        

       $price = collect(\DB::table('ecommerce.citys as cit')
        ->join('ecommerce.centroventa as cen', 'cit.id', '=', 'cen.idciudad')
        ->join('ecommerce.dat_precios as pre', 'pre.idcatprecio', '=', 'cen.idcatprecio')
        ->join('ecommerce.catprecios as catpre', 'catpre.id', '=', 'pre.idcatprecio')
        ->join('ecommerce.products as prd', 'prd.id', '=', 'pre.idproduct')
        ->join('ecommerce.categories as cat', 'cat.id', '=', 'prd.category_id')
        ->select('prd.codigo as code', 'prd.name as nameproduct','pre.precios as actual','pre.precios as nuevo','pre.idcatprecio as filtro')  
        ->where('prd.codigo', $id) 
        ->where('pre.idcatprecio', $id2) 
        ->distinct()
        ->get());

        return view('admin.precios.preciosEdit')->with(compact('price')); // form de edición

    }

 public function UpdatePrecios(Request $request,$id)
    {

   
   

        $modificacionprecio = collect(\DB::table('ecommerce.dat_precios')
        
        ->select('ecommerce.dat_precios.*')
        
        ->where('idcatprecio', $id) 
        ->get());

      

        \DB::table('ecommerce.dat_precios')
       
        
        ->where('idcatprecio', $id) 
        ->update(['precios' => $modificacionprecio->precios = $request->input('nuevo')]);

        return back();


    }

    public function listcentros()
    {
       
 

        $center = collect(\DB::table('ecommerce.centroventa as cen')
        ->join('ecommerce.citys as cit', 'cit.id', '=', 'cen.idciudad')   
        ->join('ecommerce.catprecios as catpre', 'catpre.id', '=', 'cen.idcatprecio')
      
        ->select('cen.id as code', 'cen.descripcion as descrip','cen.tipopedido as tipope','cit.name as nombre','catpre.descripcion as precio','cen.provisional as provi')        
        ->get());


       

      
        return view('admin.centroventas.centroventa')->with(compact('center',$center)); // form de edición 
    }
public function editcentros($id)
    {
        

       $center = collect(\DB::table('ecommerce.centroventa as cen')
        ->join('ecommerce.citys as cit', 'cit.id', '=', 'cen.idciudad')   
        ->join('ecommerce.catprecios as catpre', 'catpre.id', '=', 'cen.idcatprecio')
      
        ->select('cen.id as code', 'cen.descripcion as descrip','cen.tipopedido as tipope','cit.name as nombrecity','cit.id as idcity','catpre.descripcion as desprecio','catpre.id as idprecio','cen.provisional as provi')        
        ->where('cen.id', $id) 
        ->get());
        

          $citys = Citys::all()->pluck('name', 'id');
        $catPrecio = Price::all()->pluck('descripcion', 'id');

        return view('admin.centroventas.editcentroventa')->with(compact('center','citys','catPrecio',$center,$citys,$catPrecio)); // form de edición

    }
    public function UpdateCentros(Request $request,$id)
    {
 

        $modificacionCentro = collect(\DB::table('ecommerce.centroventa as cen')
        
        ->select('cen.*')
        ->where('cen.id', $id) 
        
        ->get());

      

        \DB::table('ecommerce.centroventa as cen')
       
       ->where('cen.id', $id) 
        
        ->update(['descripcion' => $modificacionCentro->descripcion = $request->input('Descripcion'),'tipopedido' => $modificacionCentro->tipopedido = $request->input('tipopedido'),'idciudad' => $modificacionCentro->idciudad = $request->input('Ciudad'),'idcatprecio' => $modificacionCentro->idcatprecio = $request->input('CatPrecio'),'provisional' => $modificacionCentro->provisional = $request->input('Provisional')]);

        return redirect('/admin/centrodeventas');


    }
    public function listcentrosAdd()
    {
       

        $citys = Citys::all()->pluck('name', 'id');
        $catPrecio = Price::all()->pluck('descripcion', 'id');

      
        return view('admin.centroventas.agregarcentroventa')->with(compact('citys','catPrecio',$citys,$catPrecio)); // form de edición 
    }

    public function createcentro(Request $request)
    {
        $CentroVenta = new CentroVenta();
        $CentroVenta->descripcion = $request->input('Descripcion');
        $CentroVenta->tipopedido = $request->input('tipopedido');
        $CentroVenta->idciudad = $request->input('Ciudad');
        $CentroVenta->idcatprecio = $request->input('CatPrecio');
        $CentroVenta->provisional = $request->input('Provisional');
        $CentroVenta->save();

        return view('admin.centroventas.centroventa');
    }









 public function newuser()
{
      $citys = Citys::all()->pluck('name', 'id');
    
      

    $listusers = collect(\DB::table('users as usuario')
    ->join('catciudades as catCiudad', 'usuario.colonia', '=', 'catCiudad.colonia') 
    ->join('catdirenv as direccion', 'usuario.id', '=', 'direccion.iduser') 
    ->select('usuario.*','direccion.tipopedido','direccion.lunes','direccion.martes','direccion.miercoles','direccion.jueves','direccion.viernes','direccion.sabado','direccion.city2','direccion.giro')
    ->whereNull('usuario.rfc')
    ->where('usuario.alta_oracle', 0)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.alta_oracle', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->where('usuario.agente', 0)
    ->where('usuario.alta_ejecutivo', 1)
    ->distinct()->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('users.alta_oracle',1)
    ->get());

  
        foreach ($listusers as $listuser) {
            $tipoPedido=$listuser->tipopedido;
        }

        $prevendedores = collect(\DB::table('catprevendedor')
        ->select('catprevendedor.*')
        ->where('tipo',"RUTA")
        ->where('tipo_pedido',$tipoPedido)
        ->get());
    

  

    
    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys,"prevendedores",$prevendedores));
}

public function newuseroracleEjecutivo()
{
      $citys = Citys::all()->pluck('name', 'id');
    
      
    $listusers = collect(\DB::table('users as usuario')
    ->join('catciudades as catCiudad', 'usuario.colonia', '=', 'catCiudad.colonia') 
    ->select('usuario.*')
    ->whereNull('usuario.rfc')
    ->where('usuario.alta_oracle', 0)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.alta_oracle', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->where('usuario.agente', 0)
    ->where('usuario.alta_ejecutivo',0)
    ->distinct()->get());

  


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('users.alta_oracle',1)
    ->get());

    $prevendedores = collect(\DB::table('catprevendedor')
    
    ->select('catprevendedor.*')
    ->where('tipo',"RUTA")
    ->get());


    return view('admin.nesusers.newuserejecutivo',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys,"prevendedores",$prevendedores));
}

public function updateuseradmin(Request $request, $iduser)
   {
   
$altaOracle=1;
$now = Carbon::now();

       $modificacionusuario = collect(\DB::table('users')
    ->select('users.*')
    ->where('id', $iduser)
    ->get());


        \DB::table('users')
       ->where('id', $iduser)
        ->update(['alta_oracle' => $modificacionusuario->alta_oracle = $altaOracle,
            'update_user_date' => $modificacionusuario->update_user_date = $now,

    ]);
       
         return back();


}
public function indexsearch2(Request $request)
    {

   

        $nombre = $request->get('name');
        $ciudad = $request->get('city');

if ($nombre!="") {

$listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)

    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('name','like',"%$nombre%")
    ->groupBy('id')
    ->get());
     $citys = Citys::all()->pluck('name', 'id'); 

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));

}




if ($ciudad!="") {
$listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)

    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('city','like',"%$ciudad%")
    ->groupBy('id')
    ->get());
     $citys = Citys::all()->pluck('name', 'id'); 

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));
}




if ($ciudad!="" and $nombre!="") {

 $listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)
  
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('name','like',"%$nombre%")
    ->where('city','like',"%$ciudad%")
    ->groupBy('id')
    ->get());
     $citys = Citys::all()->pluck('name', 'id'); 

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));   

}

   
    }
public function indexsearch(Request $request)
    {
 
        
        $nombre = $request->get('name');
        $ciudad = $request->get('city');

    if ($nombre!="") {
           $listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->where('usuario.name','like',"%$nombre%")
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)

    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->groupBy('id')

    ->get());

    $citys = Citys::all()->pluck('name', 'id');

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));

        }



         if ($ciudad!="") {
           $listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->where('usuario.city','like',"%$ciudad%")
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)

    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->groupBy('id')

    ->get());

    $citys = Citys::all()->pluck('name', 'id');

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));

        }
             

  if ($ciudad!="" and $nombre!="") {
           $listusers = collect(\DB::table('users as usuario')
        ->join('carts as carrito', 'usuario.id', '=', 'carrito.user_id') 
    ->select('usuario.*')
    ->where('usuario.activo', 1)
    ->where('usuario.alta_oracle', 1)
    ->where('usuario.useradmin', 0)
    ->where('usuario.admin', 0)
    ->where('usuario.gester', 0)
    ->where('usuario.usad', 0)
    ->where('usuario.adminpedidos', 0)
    ->where('usuario.rutero', 0)
    ->where('usuario.picking', 0)
    ->where('usuario.name','like',"%$nombre%")
    ->where('usuario.city','like',"%$ciudad%")
    ->groupBy('id')
     ->where(function($q) {
         $q->where('carrito.status', "Pendiente")
         ->orWhere('carrito.status', "Pagado")
           ->orWhere('carrito.status', "Despachado");
     })
    
    ->get());


    $listuserss2 = collect(\DB::table('users')
       
    ->select('users.*')
    ->where('activo', 0)
   
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->groupBy('id')

    ->get());

    $citys = Citys::all()->pluck('name', 'id');

    return view('admin.nesusers.newusers',compact("listusers",$listusers,"listuserss2",$listuserss2,"citys",$citys));

        }




   
    }








public function updateuseradmins(Request $request, $iduser)
   {
   $now = Carbon::now();


      $modificacionusuarios = collect(\DB::table('users')
    ->select('users.*')
    ->where('id', $iduser)
    

    ->get());


        \DB::table('users')

       ->where('id', $iduser)
       ->update(['alta_oracle' => $modificacionusuarios->alta_oracle = $request->input('quantits'),
        'update_user_date' => $modificacionusuarios->update_user_date = $now,
          

        
    ]);
        $notification = 'Se ha modificado un producto correctamente.';
         return back()->with(compact('notification'));


}












public function userpromociones()
{
 $app = "CREDITO";
    $listusers = collect(\DB::table('users')
    ->select('users.*')
    ->where('credito_contado', $app)
    ->get());

    return view('admin.promo.promo',compact("listusers",$listusers));
}
public function asignarpromo($id)
{
 
    $listusers = collect(\DB::table('users')
    ->select('users.*')
    ->where('id', $id)
    ->get());

    return view('admin.promo.asignarpromo',compact("listusers",$listusers));
}

public function tiempoentrega()
{
 
    $listiempos = collect(\DB::table('catctehorasentrega')
    ->select('catctehorasentrega.*')
    ->get());

    return view('admin.tiempoentrega.tiempoentrega',compact("listiempos",$listiempos));
}

public function adduseradmin(Request $request)

    {

        $name = $request->input('name');

        $email = $request->input('email');

        $citys = Citys::all()->pluck('name', 'id');

        $colonias = Colonias::all()->pluck('colonia', 'id');

        return view('admin.useradmin.useradmin')->with(compact('name', 'email','citys','colonias'));

    }

    public function cambiarRuta($id)
    {

        $product = collect(\DB::table('users')
        ->where('id', $id)
        ->update(['edad' => 20])
        ->get());

        return view('admin.pedidos.pagado')->with(compact('product',$product)); // form de edición
    }



   
    public function repoterutas($id)
    {

      


$pedidos = collect(\DB::table('carts as carrito')
        ->leftjoin('users as usuario', 'usuario.id', '=', 'carrito.user_id') 
        ->leftjoin('citys as ciudad', 'ciudad.id', '=', 'carrito.idcentrovta')
        ->leftjoin('centroventa as centro', 'centro.id', '=', 'carrito.idcentrovta')
    ->select('carrito.id','centro.descripcion as descentro','carrito.importe_total','carrito.order_date',
        'carrito.created_at','carrito.status','ciudad.name as city','usuario.name','usuario.address','usuario.phone','carrito.ruta as rutades')

    ->where('carrito.ruta', $id)
   
    ->get());




    $pdf=\PDF::loadview('admin.pedidos.reportes_ruteros',compact("pedidos"))->setPaper('a4', 'landscape')->setWarnings(false)->save('myfile.pdf');
  
       return $pdf->stream('primerpdf.pdf');


    }


    public function repoteusuarios()
    {

      


$pedidos = collect(\DB::table('users')
     
  ->select('users.*')
    ->where('activo', 0)
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('lugar',"apprutas")
    ->groupBy('id')
    ->get());




      return view('admin.pedidos.reportes_usuarios_alta')->with(compact('pedidos',$pedidos)); 

    }

    public function indexsearchrepoteusuarios(Request $request)
    {
 
        
        $fechai = $request->get('fechaI');
        $fechaf = $request->get('fechaF');

if ($fechai=="" or $fechaf=="") {

         $notification2 = 'Para filtrar se necesitan ambas fechas';
         return back()->with(compact('notification2'));
}else{

$pedidos = collect(\DB::table('users')
  
           ->select('users.*')
    ->where('activo', 0)
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('lugar',"apprutas")
    ->groupBy('id')
    ->whereBetween('update_user_date', [$fechai, $fechaf])
    
    ->get());


    return view('admin.pedidos.reportes_usuarios_alta')->with(compact('pedidos',$pedidos)); 


}
   
    }

    public function asignarPassword()
    {

        $city=auth()->user()->city;
        //return $city;
        $listusers = collect(\DB::table('users')
        ->select('users.*')
        ->where('users.city', $city)
        ->where('users.eliminado', 0)
        ->orderBy('users.name', 'ASC')
        ->get());

        //return $listusers;


        /*$listusers = collect(\DB::table('users as usuario')
        ->leftjoin('catdirenv as catdir', 'usuario.id', '=', 'catdir.iduser') 
        ->select('usuario.id',
        'usuario.name',
        'usuario.username',
        'usuario.email',
        'usuario.city',
        'catdir.direccion',
        'usuario.phone',
        'usuario.password',
        'catdir.clavecliente')
        ->where('usuario.city', $city)
        ->where('usuario.eliminado', 0)
        ->orderBy('usuario.name', 'ASC')
        ->get());*/
        

        return view('admin.nesusers.asignarpassword',compact("listusers",$listusers));
    }

    public function cambiarPassword(Request $request, $iduser)
    {
         
        $new_password = $request->input('password');

        $users = collect(\DB::table('users')
        ->select('users.*')
        ->where('id', $iduser)
        ->get());

    
            \DB::table('users')
        ->where('id', $iduser)
        ->update(['password' => $users->password = bcrypt($new_password)]);
        
        $notification = 'Se ha modificado la contraseña.';
        return back()->with(compact('notification'));

 
    }

    public function cambiarUsername(Request $request, $iduser)
    {
        
        $username = $request->input('username');
        $username2 = $request->input('username2');

        if($username == $username2){
            $users = collect(\DB::table('users')
            ->select('users.*')
            ->where('id', $iduser)
            ->get());
    
            try{
                //Cambia el username
                \DB::table('users')
                ->where('id', $iduser)
                ->update(['username' => $users->username = $request->input('username'),]);
                    
                $newCart = new Cart();
                $newCart->user_id = $iduser;
                $newCart->status = "Active";        
                $newCart->save();

                //Setea en automatico la password
                $new_password = "123456";

                $users = collect(\DB::table('users')
                ->select('users.*')
                ->where('id', $iduser)
                ->get());

                \DB::table('users')
                ->where('id', $iduser)
                ->update(['password' => $users->password = bcrypt($new_password)]);

            }catch(\Exception $e){
                //dump($e->getMessage());
                return redirect()->back()->with('alertError', 'El usuario ya existe, favor que ingresar un usuario valido.');
            }

            return redirect()->back()->with('alert', 'Se ha modificado el usuario correctamente');

        }else{
            return redirect()->back()->with('alertError', 'El nombre de usuario a actualizar debe coincidir, favor de revisar atentamente!');
        }
        
    }

    public function asignarRuta(Request $request,$id)
{


    $tipoPedido = $request->input('ruta');
    $name = $request->name;
    $address = $request->direccion;
    $giro = $request->giro;
    $altaEjecutivo=1;

    //return $name . $address;
    

  
    foreach($request->input('dias') as $value){

        \DB::table('catdirenv')        
        ->where('iduser', $id) 
        ->update([$value => 1,

                   ]);
      }

      \DB::table('users')        
        ->where('id', $id) 
        ->update(['alta_ejecutivo' => $altaEjecutivo,'name' => $name,'address'=>$address
                   ]);
                   

                   \DB::table('catdirenv')        
        ->where('iduser', $id) 
        ->update(['tipopedido' => $tipoPedido,'giro' =>$giro
                   ]);


         return redirect('/admin/newuserejecutivo');
}

public function consultarExistencias(Request $request){
        
    $codigo = $request->codigo;
    $organizationid = auth()->user()->organization_id;

    if(strlen($codigo) > 8){
        return redirect()
            ->back()
            ->withErrors('Escribe un codigo valido');
    }

    $existencias = collect(\DB::select("SELECT codigo_producto,nombre_producto,inventario,reserva,(inventario-reserva) existencia from(
        SELECT pr.codigo codigo_producto,pr.name nombre_producto,if(existencia.existencia is null,0,existencia.existencia) inventario,rutas.FN_PEDIDOS_RESERVA(pr.organization_id,pr.id) reserva
        FROM rutas.products pr 
        LEFT JOIN rutas.datinvcedis as existencia ON existencia.idproducto=pr.idproducto AND existencia.organization_id=pr.organization_id 
        WHERE pr.organization_id={$organizationid}
        AND existencia.existencia>0
        AND pr.codigo LIKE \"%{$codigo}%\"
        ) result"));
    
    return view('admin.pedidos.consultar_existencias',['existencias'=> $existencias]);
}

}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\CartDetail;
use App\CentroVenta;
use App\Product;
use App\Colonias;
use App\User;
use App\Price;
use App\Citys;
use App\CatPrecio;
use App\Notificacion;
use App\ModificacionPedido;
use App\Mail\NewOrder;
use App\DatpackingPedido;


use App\DatPrecios;
use App\catdirenv;
use Carbon\Carbon;
use App\Catdirfac;
use Mail;
use Redirect;

class picking extends Controller
{
    public function index()
    {

        // return " ♥";
       
    $city=auth()->user()->city;

    $iduspick=auth()->user()->id;

    $stat="Aceptado";

     $tiposs = collect(\DB::table('catdirenv')
    ->select('tipopedido')
    ->where('iduser',$iduspick)
    ->get());

    foreach ($tiposs as $tiposss) {
                $dat=$tiposss->tipopedido;
    }

    //Fecha
     $fecha = strtotime(date('Y-m-d')."- 7 days");
    $semanaAnterior = date('Y-m-d', $fecha);


     $contar =   collect(\DB::table('carts as carrito')
    ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
    ->join('catdirenv as direnv', 'usuario.id', '=', 'direnv.iduser')
    ->select(\DB::raw('count(carrito.id) as cart_count'))
    ->where('carrito.status',$stat)
    ->where('usuario.city',$city)
    ->where('carrito.order_date','>=',$semanaAnterior)
    ->get());

      $pedidos= collect(\DB::select('select a.id,a.interfazado, d.descripcion as descentro, a.importe_modificar as importe_total, a.user_id as idususario,
    a.idcentrovta as centro,a.order_date,a.order_datetime, a.status, c.name as city, b.name, b.address, b.phone
    	from carts a, users b, citys c,	centroventa d
    	where a.user_id = b.id and 
    	 c.name = b.city and
    	 a.idcentrovta = d.id AND
    	 a.status=:status AND
    	 b.city=:city AND 
         a.order_date>=:fecha
         order by a.order_datetime DESC',
         ['city'=>$city,'status' => 'Aceptado', 'fecha'=> $semanaAnterior]));



         $pedidosAuth=  collect(\DB::select('select a.id,  a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, b.address, b.phone 
    	from carts a, users b, citys c
    	where 
        a.user_id = b.id and 
    	c.name = b.city and
    	a.status=:status AND
    	b.city=:city' ,
    	['city'=>$city,'status' => 'Pagado']));

        // return $pedidos;

        return  view('admin.picking.index')->with(compact('city','pedidos', 'pedidosAuth','contar'));

		
	}  



	public function preparado($id,Request $request)
    {

    $modificacionpedidoss = collect(\DB::table('modificacionpedido')
        ->select('modificacionpedido.*')
        ->where('id_cart', $id)
        ->get());

        \DB::table('modificacionpedido')      
        ->where('id_cart', $id)
        ->update(['packing' => $modificacionpedidoss->packing = $request->input('packingss'),        
        ]); 

        $aceptado='Aceptado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $aceptado)
        ->update(['status' => 'Preparado']);

        
        return redirect('/picking/index');
	
	}

    public function regresoEjecutivo($id,Request $request)
    {
        $interfazado = $request->interfazado;

if ($interfazado==0) {


    $cart = collect(\DB::table('carts')
    ->select('carts.*')
    ->where('id', $id)
    ->get());
    


    \DB::table('carts')
    ->where('id', $id)
    ->update(['MotivoRegreso' => $cart->MotivoRegreso = $request->input('MotivoRegreso')]);


    $aceptado='Aceptado';
    $client = \DB::table('carts')
    ->where('id', $id)
    ->where('status', $aceptado)
    ->update(['status' => 'Pendiente']);


return redirect('/picking/index');
   
   }else {
   
      $notificationInterfazado = 'El pedido ya esta interfazado';
   
           return back()->with(compact('notificationInterfazado'));  
     }
	
	}





public function modificarpedidospicking(Request $request,$idcar,$idcentro)
    {
    //    return $idcentro;

        $contarpendientes =   collect(\DB::table('datpackingpedido as packin')
        ->select(\DB::raw('count(packin.id) as packin_count'))
        ->where('packin.pedido',$idcar)
        ->get());


    //    foreach ($contarpendientes as $contarpendiente) {
    //         $contador=$contarpendiente->packin_count;
    //     }

    if($contarpendientes[0]->packin_count==0){
        $notification2 = 'El pedido ' .$idcar. ' aun no cuenta con un packinglist';

        return back()->with(compact('notification2'));  
    }
    

      

    // if ($contador>0) {

      $unaut= collect(\DB::table('modificacionpedido as detalles')
       ->join('products as producto', 'producto.id', '=', 'detalles.id_product')
       ->leftjoin('datpackingpedidosum as packing', function($join)
                         {
                             $join->on('packing.pedido', '=', 'detalles.id_cart');
                             $join->on('packing.codigo', '=', 'producto.codigo');
                         })
       ->join('carts as carrito', 'carrito.id', '=', 'detalles.id_cart')
       ->join('dat_precios as precios', 'precios.idproduct', '=', 'detalles.id_product')
        ->select('carrito.id','detalles.id as idmodificacion','producto.id as idpro', 'detalles.importe','carrito.importe_total',
        'producto.name','detalles.price','detalles.quantity','detalles.uom','detalles.iva','packing.packing','packing.cantidad')
        ->where('carrito.id', $idcar)
        ->distinct()
        // ->where('precios.idcentroventa', $idcentro)
        ->get());

        if(!$unaut->isEmpty()){
            return view('admin.picking.modificar')->with(compact('unaut'));
        }else{
            
            $notification2 = 'El pedido ' .$idcar. ' aun no cuenta con detalle packinglist';

            return back()->with(compact('notification2'));  
        }


    //  return view('admin.picking.modificar')->with(compact('unaut'));

// }else {
    
//         $notification2 = 'El pedido ' .$idcar. ' aun no cuenta con un packinglist';

//         return back()->with(compact('notification2'));  
//   }
    

     

     /* --------Codigo Antes---------
      $unaut= collect(\DB::table('datpackingpedido')
      ->select('datpackingpedido.*')
      ->where('pedido',$idcar)
      ->get());
 */
       
    }

    public function regresarpedido(Request $request,$idcar,$idcentro,$interfazado)
    {

        //return $request;
        if(!$interfazado){
            $contarpendientes =   collect(\DB::table('datpackingpedido as packin')
            ->select(\DB::raw('count(packin.id) as packin_count'))
            ->where('packin.pedido',$idcar)
            ->get());

        foreach ($contarpendientes as $contarpendiente) {
            $contador=$contarpendiente->packin_count;
        }

        $unaut= collect(\DB::table('modificacionpedido as detalles')
        ->join('products as producto', 'producto.id', '=', 'detalles.id_product')
        ->leftjoin('datpackingpedidosum as packing', function($join)
                            {
                                $join->on('packing.pedido', '=', 'detalles.id_cart');
                                $join->on('packing.codigo', '=', 'producto.codigo');
                            })
        ->join('carts as carrito', 'carrito.id', '=', 'detalles.id_cart')
        ->join('dat_precios as precios', 'precios.idproduct', '=', 'detalles.id_product')

            ->select('carrito.id','detalles.id as idmodificacion','producto.id as idpro', 'detalles.importe','carrito.importe_total',
            'carrito.interfazado','producto.name','detalles.price','detalles.quantity','detalles.uom','detalles.iva','packing.packing','packing.cantidad')           
            ->where('carrito.id', $idcar)
            ->where('precios.idcentroventa', $idcentro)
            ->get());


        return view('admin.picking.regresar')->with(compact('unaut'));

        }else{
            $notification2 = 'El pedido ya se encuentra interfazado, ya no se puede regresar al ejecutivo';
            return back()->with(compact('notification2'));
        }
      

    }



 public function updatepedidospicking(Request $request, $idcart,$idmodifi)
   {
     $cantidad = $request->quantity;
      $precio = $request->price;
       $ivas = $request->iva;


      $modificacionpedido = collect(\DB::table('modificacionpedido')
    ->select('modificacionpedido.*')
    ->where('id', $idmodifi)
    ->where('id_cart', $idcart)

    ->get());


        \DB::table('modificacionpedido')

       ->where('id', $idmodifi)
       ->where('id_cart', $idcart)
        ->update(['packing' => $modificacionpedido->packing = $request->input('packing'),
          'quantitypacking' => $modificacionpedido->quantitypacking = $request->input('quantitypacking'),

        
    ]);
        $notification = 'Se ha modificado un producto correctamente.';
         return back()->with(compact('notification'));


}


/*----------Reporte Picking--------------*/
    public function reportePicking()
    {

$pedidos = collect(\DB::table('users')
     
  ->select('users.*')
    ->where('activo', 0)
    ->where('useradmin', 0)
    ->where('admin', 0)
    ->where('gester', 0)
    ->where('usad', 0)
    ->where('adminpedidos', 0)
    ->where('rutero', 0)
    ->where('picking', 0)
    ->where('lugar',"apprutas")
    ->groupBy('id')
    ->get());




      return view('admin.pedidos.reportes_usuarios_alta')->with(compact('pedidos',$pedidos)); 

    }



/*----------Reporte pedido en el picking--------------*/

       public function reportepedidos()
    {
$interfazado=1;
$pedidos = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('cart_details.*','carts.*','usuario.name as usuarioname','products.name as productoname')
    ->where('interfazado',$interfazado)
    ->get());


/*----------Sacar la primera fecha--------------*/

$orderfecha = Cart::select('order_date')->where('interfazado',$interfazado)->orderBy('order_date','asc')
->get();

$first = $orderfecha->first()->order_date;

$pedidos2 = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('carts.*')
    ->where('interfazado',$interfazado)
    ->where('order_date',$first)
->orderBy('order_date','asc')
->get()
);


/*----------Sacar la ultima fecha--------------*/
$orderfechas = Cart::select('order_date')->where('interfazado',$interfazado)->orderBy('order_date','desc')
->get();

$firsts = $orderfechas->first()->order_date;

$pedidos3 = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('carts.*')
    ->where('interfazado',$interfazado)
    ->where('order_date',$firsts)
->orderBy('order_date','desc')
->get()
);

return view('admin.picking.reportepedido')->with(compact('pedidos',$pedidos,'pedidos2',$pedidos2,'pedidos3',$pedidos3));
    }

/*----------Reporte pedido en el picking filtrado--------------*/
 public function reportepedidosfiltro(Request $request)
    {


        $fechai = $request->get('fechaI');
        $fechaf = $request->get('fechaF');

if ($fechai=="" or $fechaf=="") {

         $notification2 = 'Para filtrar se necesitan ambas fechas';
         return back()->with(compact('notification2'));
}else{

$interfazado=1;

$pedidos = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('cart_details.*','carts.*','usuario.name as usuarioname','products.name as productoname')
    ->where('interfazado',$interfazado)
    ->whereBetween('carts.order_date', [$fechai, $fechaf])
    ->get());



/*----------Sacar la primera fecha--------------*/

$orderfecha = Cart::select('order_date')->where('interfazado',$interfazado) ->whereBetween('carts.order_date', [$fechai, $fechaf])->orderBy('order_date','asc')
->get();

$first = $orderfecha->first()->order_date;

$pedidos2 = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('carts.*')
    ->where('interfazado',$interfazado)
    
    ->where('order_date',$first)
->orderBy('order_date','asc')
->get()
);


/*----------Sacar la ultima fecha--------------*/
$orderfechas = Cart::select('order_date')->where('interfazado',$interfazado) ->whereBetween('carts.order_date', [$fechai, $fechaf])->orderBy('order_date','desc')
->get();

$firsts = $orderfechas->first()->order_date;

$pedidos3 = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('products', 'products.id', '=', 'cart_details.product_id')
    ->join('users as usuario','usuario.id' ,'=', 'carts.user_id')
    ->select('carts.*')
    ->where('interfazado',$interfazado)
 
    ->where('order_date',$firsts)
->orderBy('order_date','desc')
->get()
);




return view('admin.picking.reportepedido')->with(compact('pedidos',$pedidos,'pedidos2',$pedidos2,'pedidos3',$pedidos3));
    }
  }


/*----------Reporte pedido Buscar--------------*/

        public function indexsearchreportepedidos(Request $request)
    {
 
        
        $fechai = $request->get('fechaI');
        $fechaf = $request->get('fechaF');

if ($fechai=="" or $fechaf=="") {

         $notification2 = 'Para filtrar se necesitan ambas fechas';
         return back()->with(compact('notification2'));
}else{

$pedidos = collect(\DB::table('carts')
    ->join('cart_details' , 'cart_details.cart_id', '=', 'carts.id')
    ->join('users','users.id' ,'=', 'carts.user_id')
    ->select('cart_details.*','carts.*','users.name')
    ->groupBy('id_cart_details')
    ->whereBetween('order_date', [$fechai, $fechaf])
    ->get());


    return view('admin.picking.reporte_pedido')->with(compact('pedidos',$pedidos)); 


}
   
    }
}
<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\catPagos;
use App\CartDetail;
use App\CentroVenta;
use App\Product;
use App\Colonias;
use App\User;
use App\Price;
use App\Citys;
use App\CatPrecio;
use App\Notificacion;
use App\ModificacionPedido;
use App\EntregaPedidosModel;
use App\Mail\NewOrder;
use App\DatPrecios;
use App\catdirenv;
use Carbon\Carbon;
use App\Catdirfac;
use Mail;
use Redirect;
use DB;
use File;

use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\StreamedResponse;


class ruterocontrol extends Controller
{
       public function index()
    {


   
    $city=auth()->user()->city;

    $idrepartidor=auth()->user()->id;

    $stat="Despachado";


  $tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$idrepartidor)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }



  $contar =   collect(\DB::table('carts as carrito')
  ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
  ->join('catdirenv as direnv', 'usuario.id', '=', 'direnv.iduser')
        ->select(\DB::raw('count(carrito.id) as cart_count'))

        ->where('carrito.status',$stat)
        ->where('usuario.city',$city)
           
        ->get());



  $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',"Despachado") 
        ->get());

    
        

 
    $pedidosAuth=  collect(\DB::select('select a.id,  a.importe_total, a.order_date,a.created_at, a.status, c.name as city, b.name, a.importe_entrega, b.address, b.phone 

      from carts a, users b, citys c

      where 

      a.user_id = b.id and 

       c.name = b.city and

       a.status=:status AND

       a.ruta=:ruta AND

       b.city=:city',

        ['city'=>$city, 'ruta'=>$dat, 'status' => 'Pagado']));

   
        




  $pedidosEnt = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_entrega','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',"Entregado") 

        ->whereBetween('a.order_date', [
    Carbon::parse('last monday')->startOfDay(),
    Carbon::parse('next saturday')->endOfDay(),
])
        ->get());
     

      
        return  view('ruteros.index')->with(compact('city','pedidos', 'pedidosAuth','contar','pedidosEnt'));


 
    
  }  


  public function entregado($id)
    {
      $faltante=0;

 $contarmodificacionPedido =   collect(\DB::table('modificacionpedido')
        ->join('products as producto', 'producto.id', '=', 'modificacionpedido.id_product')
        ->leftJoin('datpackingpedido as packing', function($join)
                  {
                      $join->on('packing.pedido', '=', 'modificacionpedido.id_cart');
                      $join->on('producto.codigo','=', 'packing.codigo');
                    
                  })  
        ->select(\DB::raw('count(modificacionpedido.id) as faltante_count'))
        ->where('modificacionpedido.entregado',$faltante)
        ->where('modificacionpedido.id_cart',$id)     
        ->where('packing.packing', '!=' ,"")      
        ->get());

 foreach ($contarmodificacionPedido as $contador) {
                       $totales=$contador->faltante_count;
                     
                   }
 if ($totales==0) {                  

    $Despachado='Despachado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Despachado)
        ->update(['status' => 'Entregado']);

      $notification = 'Un pedido se ha entregado';  


    return redirect("rutero/index/$id")->with(compact('notification'));


    }else{

      $notification2 = 'Tiene que confirmar todos los productos del pedido para continuar';  

     return back()->with(compact('notification2'));
    }
  }

public function elegirtipopago($id)
    {
   
    $city=auth()->user()->city;

    $idrepartidor=auth()->user()->id;


  $tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$idrepartidor)
        
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
                     


        }

 


    
$pedidos= collect(\DB::table('carts as carrito')
    
       ->join('users as usuario', 'usuario.id', '=', 'carrito.user_id')
       ->join('citys as ciudad', 'ciudad.name', '=', 'usuario.city')
       ->join('centroventa as centro', 'carrito.idcentrovta', '=', 'centro.id')


        ->select('carrito.id','centro.descripcion as descentro','carrito.importe_total', 'carrito.order_date','carrito.created_at','carrito.status','ciudad.name as city','usuario.name','usuario.address','usuario.phone','carrito.importe_entrega')

        ->where('carrito.id', $id)
        ->where('carrito.ruta', $dat)
        
        ->get());
   

       $di = collect(\DB::table('cattipopago')
        ->select('id', 'descripcion', )       
        ->get());

       $ei = collect(\DB::table('catformapago')
        ->select('valor', 'Descripcion', )       
        ->get());

        $fi = collect(\DB::table('carts')
        ->select('carts.*') 
        ->where('id', $id)          
        ->get());

         $sacarUser = collect(\DB::table('carts')
        ->select('carts.user_id') 
        ->where('id', $id)          
        ->get());

      
      foreach ($sacarUser as $sacarUsers) {
                       $id_usuario=$sacarUsers->user_id;

        }

        $tipodeusuario = collect(\DB::table('catdirenv')
        ->select(\DB::raw('count(catdirenv.id) as cart_creditos'))
        ->where('iduser', $id_usuario)  
        ->where('credito_contado', "CREDITO")         
        ->get());

        return  view('ruteros.pago')->with(compact('city','pedidos','di','ei','fi','tipodeusuario'));

        
    }  
	public function updatePago($id, Request $request)
    {
$datefinal= Carbon::now();

$dt = $datefinal->subHours(2);

$catPagos = new catPagos();
$catPagos->order_id = $id;
$catPagos->idforma_pago= $request->input('TipoVenta');
$catPagos->importe = $request->input('importeAcobrar');
$catPagos->idtipopago= $request->input('FormaDePago');
$catPagos->fechapago = $dt->toDateTimeString();
$catPagos->save();



		$Pendiente='Entregado';
        $client = \DB::table('carts')
        ->where('id', $id)
        ->where('status', $Pendiente)
        ->update(['status' => 'Pagado']);


    	$admins = User::where('admin', true)->get();


      

		
		return back();
	
	}






public function modificarcarrito(Request $request,$idcar,$idcentro){

  $pedidos = ModificacionPedido::where('id_cart',$idcar)->get();

  foreach ($pedidos as $tiposss) {
    $dat=$tiposss->packing;
  }

    $unaut= collect(\DB::table('modificacionpedido as detalles')
    ->join('products as producto', 'producto.id', '=', 'detalles.id_product')
    ->leftJoin('datpackingpedido as packing', function($join){
                          $join->on('packing.pedido', '=', 'detalles.id_cart');
                          $join->on('producto.codigo','=', 'packing.codigo');
                        })             
    ->join('carts as carrito', 'carrito.id', '=', 'detalles.id_cart')
    ->join('dat_precios as precios', 'precios.id', '=', 'detalles.iddtaprecio')
    ->select('carrito.id','detalles.id as idmodificacion','producto.id as idpro', 'detalles.importe','carrito.importe_modificar','producto.name','detalles.price','detalles.quantity','detalles.uom','detalles.iva','packing.packing','packing.cantidad','detalles.quantityentrega','packing.codeb','packing.codigo','packing.entregado','detalles.importe_entrega')
    ->where('carrito.id', $idcar)
    ->where('packing.packing', '!=' ,"")
    ->get());

    $purchases = \DB::table('datpackingpedido')
        ->select(\DB::raw('SUM(importe_diferencia) AS sumafinal'))
        ->where('pedido', $idcar)
        ->get();

  return view('ruteros.modificar')->with(compact('unaut','purchases'));
}


 public function updatecarroruta(Request $request, $idcart,$idmodifi,$idproduct,$idcodeb)
   {
    $cantidad = $request->quantitypacking;
    $precio = $request->price;
    $ivas = $request->iva;
    $ImpoteFina = $cantidad*$precio;
    $ImpoteInitial=$request->Importe_actual;
    $ImpoteDefinitivo=$ImpoteInitial-$ImpoteFina;
    $iduser=auth()->user()->id;
    $datefinal= Carbon::now();
    $dt = $datefinal->subHours(2);

    $catEntregaPedido = new EntregaPedidosModel();
    $catEntregaPedido->pedido = $idcart;
    $catEntregaPedido->codigo= $request->input('codigoProducto');
    $catEntregaPedido->codeb = $request->input('codebProducto');
    $catEntregaPedido->cantidad= $request->input('quantitypacking');
    $catEntregaPedido->id_product= $request->input('id_productoss');
    $catEntregaPedido->Fecha_entrega = $dt->toDateTimeString();
    $catEntregaPedido->user_id= $iduser;
    $catEntregaPedido->save();
    $entregado = 1;
    
    $modificacionpedido = collect(\DB::table('datpackingpedido')
    ->select('datpackingpedido.*')    
    ->where('pedido', $idcart)
    ->where('codeb', $idcodeb)
    ->get());

    \DB::table('modificacionpedido')
    ->where('id', $idmodifi)
    ->where('id_cart', $idcart)
    ->where('id_product', $idproduct)
    ->update(['entregado' => $modificacionpedido->entregado = $entregado,'importe_diferencia' => $modificacionpedido->importe_diferencia = $ImpoteDefinitivo,]);
    $notification = 'Se ha guardado el nuevo precio correctamente.';
    
    return back()->with(compact('notification'));

}



public function repotepedidos()
    {

      
    $city=auth()->user()->city;

    $idrepartidor=auth()->user()->id;

  $tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$idrepartidor)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where(function($q) {
         $q->where('a.status', "Despachado")
         ->orWhere('a.status', "Entregado")
         ->orWhere('a.status', "Pagado");
     })
        ->orderBy('a.id','desc')
        ->get());


      return view('ruteros.reporte_pedidos_rutero')->with(compact('pedidos',$pedidos)); 

    }



     public function indexsearchrepoterutero(Request $request)
    {
 
         $status = $request->get('statusseelect');

        $fechai = $request->get('fechaI');
        $fechaf = $request->get('fechaF');


if ($status=="0") {

      if ($fechai=="" or $fechaf=="") {

         $notification2 = 'Para filtrar se necesitan ambas fechas';
         return back()->with(compact('notification2'));
}else{



   $city=auth()->user()->city;

    $idrepartidor=auth()->user()->id;

  $tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$idrepartidor)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where(function($q) {
         $q->where('a.status', "Despachado")
         ->orWhere('a.status', "Entregado")
         ->orWhere('a.status', "Pagado");
     })
          ->whereBetween('order_date', [$fechai, $fechaf])
        ->orderBy('a.id','desc')
        ->get());


    return view('ruteros.reporte_pedidos_rutero')->with(compact('pedidos',$pedidos)); 


}
}else{
  if ($fechai=="" or $fechaf=="") {

         $notification2 = 'Para filtrar se necesitan ambas fechas';
         return back()->with(compact('notification2'));
}else{



   $city=auth()->user()->city;

    $idrepartidor=auth()->user()->id;

  $tiposs = collect(\DB::table('catdirenv')
     
        ->select('tipopedido')
        ->where('iduser',$idrepartidor)
        ->get());


foreach ($tiposs as $tiposss) {
                       $dat=$tiposss->tipopedido;
        }

 $pedidos = collect(\DB::table('carts as a')
        ->join('users as b', 'a.user_id', '=', 'b.id')
        ->join('citys as c', 'c.name', '=', 'b.city')
        ->join('centroventa as d', 'a.idcentrovta', '=', 'd.id')
       
        
        ->select('a.id','d.descripcion as descentro','a.importe_total','a.user_id as idususario','a.idcentrovta as centro','a.order_date','a.created_at','a.status','c.name as city','b.name','a.importe_modificar','b.address','b.phone')  
        ->where('b.city', $city) 
        ->where('a.ruta',$dat) 
        ->where('a.status',$status) 
          ->whereBetween('order_date', [$fechai, $fechaf])
        ->orderBy('a.id','desc')
        ->get());


    return view('ruteros.reporte_pedidos_rutero')->with(compact('pedidos',$pedidos)); 


}
}




   
    }



   public function downloadJSONFile(Request $request){
 $id = $request->id_car;

$pedidos = collect(\DB::table('carts as carrito')    
->join('modificacionpedido as modi', 'modi.id_cart', '=', 'carrito.id') 
->join('users as us', 'us.id', '=', 'carrito.user_id')
        
    ->select('carrito.id as idcar','carrito.importe_entrega as total','carrito.ruta','carrito.fechaE','modi.*','us.colonia','us.numeroexterior','us.numerointerior','us.address','us.city','us.name as nombrecliente','us.codigopostal')

->where('carrito.id', $id) 

    ->get());


      $modifi = DB::table('rutas.modificacionpedido')
      ->select('nameProduct','quantityentrega','price','importe_entrega')
         ->where('id_cart',$id)
         ->orderBy('id', 'desc')
         ->get();


  $content = \View::make('ruteros.ticketPrueba')->with(compact('pedidos','modifi'));

  // Set the name of the text file
  $filename = 'WhateverYouWant.txt';

  // Set headers necessary to initiate a download of the textfile, with the specified name
  $headers = array(
      'Content-Type' => 'plain/txt',
      'Content-Disposition' => sprintf('attachment; filename="%s"', $filename),
  );

  return \Response::make($content, 200, $headers);
    }

}

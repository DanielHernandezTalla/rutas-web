<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class newusercontroller extends Controller
{
    public function index()
{
    $colonia=auth()->user()->colonia;
    $listusers = collect(\DB::table('users')
    ->join('catciudades as estado', 'estado.colonia', '=', 'users.colonia')
    ->select('users.*','estado.*')
    ->where('lugar', "apprutas")
    ->where('esdado.colonia', $colonia)
    ->get());
    return view('admin.nesusers.newusers',compact("listusers",$listusers));
}

}

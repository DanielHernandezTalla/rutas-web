<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Mail;
use Redirect;
use Carbon\Carbon;

class ReportesController extends Controller
{
    public function getPedidosDia(Request $request){

        $fechaFactura=[];
        $city=auth()->user()->city;
        $test="TEST";

        /*Pedidos diarios*/ 
        $reportByday = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente,productos.name as producto,
        detalle_pedido.precio_producto as importe,pedidos.order_date as fecha_pedido, pedidos.fechaE as fecha_entrega, pedidos.status 
        FROM rutas.carts as pedidos 
        LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
        LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
        LEFT JOIN rutas.products as productos ON detalle_pedido.product_id=productos.id
        LEFT JOIN rutas.dat_precios as precios ON precios.idproduct=productos.id
        WHERE usuarios.city='$city' AND DATE(pedidos.order_date) = DATE(NOW())"));

        /*Clientes que han pedido ultimos 30 dias*/ 
        $reportBy30 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
        FROM rutas.carts as pedidos 
        LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
        LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
        WHERE usuarios.city='$city' AND pedidos.order_date > now() - INTERVAL 30 day order by usuarios.name"));

        /*Clientes que han pedido ultimos 15 dias*/ 
        $reportBy15 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
        FROM rutas.carts as pedidos 
        LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
        LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
        WHERE usuarios.city='$city' AND pedidos.order_date > now() - INTERVAL 15 day order by usuarios.name"));

        /*Clientes que NO han pedido ultimos 15 dias*/ 
        $reportBy15SP = collect(\DB::select("SELECT DISTINCT * FROM(SELECT a.user_id, d.name as cliente, d.phone 
        from rutas.carts as a
        left join rutas.cart_details b on a.id=b.cart_id
        left join rutas.products c on b.product_id=c.id
        left join rutas.users d on a.user_id=d.id 
        WHERE d.city='$city' AND a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 30 day) as f
        WHERE f.user_id not in(SELECT a.user_id 
        from rutas.carts as a
        left join  rutas.cart_details b on a.id=b.cart_id
        left join  rutas.products c on b.product_id=c.id
        left join rutas.users d on a.user_id=d.id 
        WHERE d.city='$city' AND a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 15 day
        )"));

        /*Clientes que NO han pedido ultimos 7 dias*/ 
        $reportBy7SP = collect(\DB::select("SELECT DISTINCT * FROM(SELECT a.user_id, d.name as cliente, d.phone 
        from rutas.carts as a
        left join rutas.cart_details b on a.id=b.cart_id
        left join rutas.products c on b.product_id=c.id
        left join rutas.users d on a.user_id=d.id 
        WHERE d.city='$city' AND a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 15 day) as f
        WHERE f.user_id not in(SELECT a.user_id 
        from rutas.carts as a
        left join  rutas.cart_details b on a.id=b.cart_id
        left join  rutas.products c on b.product_id=c.id
        left join rutas.users d on a.user_id=d.id 
        WHERE d.city='$city' AND a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 7 day
        )"));
    
        /*Clientes que han pedido ultimos 7 dias*/ 
        $reportBy7 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
        FROM rutas.carts as pedidos 
        LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
        LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
        WHERE usuarios.city='$city' AND pedidos.order_date > now() - INTERVAL 7 day order by usuarios.name"));

        /*Clientes que han realizado pedidos con factura en los ultimos 30 dias*/ 
        $reportFactura = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente,productos.name as producto,
        precios.precios as precio,pedidos.order_date as fecha_pedido, pedidos.fechaE as fecha_entrega, pedidos.status , pedidos.factura
        FROM rutas.carts as pedidos 
        LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
        LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
        LEFT JOIN rutas.products as productos ON detalle_pedido.product_id=productos.id
        LEFT JOIN rutas.dat_precios as precios ON precios.idproduct=productos.id
        WHERE usuarios.city='$city' AND  pedidos.order_date > now() - INTERVAL 30 day AND pedidos.factura='Si' AND pedidos.status='Entregado' ORDER BY pedidos.order_date DESC"));

        /*Productos mas pedidos de los ultimos 30 dias*/ 
        $reportProducts= collect(\DB::select("SELECT c.id,c.name as producto, b.price as precio, SUM(quantity) AS totalKg, SUM(b.price*quantity) as totalFeria 
        from rutas.modificacionpedido as b
        left join  rutas.products c on b.id_product=c.id
        left join  rutas.carts a on a.id=b.id_cart
        left join rutas.users usuarios on a.user_id=usuarios.id 
        WHERE usuarios.city='$city' AND a.order_date > now() - INTERVAL 30 day  GROUP BY c.id, b.price ORDER BY SUM(b.price*quantity) DESC"));

        /*Clientes con mas pedidos de los ultimos 30 dias*/
        $reportClientes= collect(\DB::select("SELECT usuario.id,usuario.name as cliente,productos.name as producto , b.precio_producto as precio,
        SUM(quantity) AS totalKg,SUM(b.precio_producto*quantity) as totalFeria, a.order_date as fechaPedido 
        FROM rutas.users as usuario
        left join  rutas.carts as a on usuario.id=a.user_id
        left join  rutas.cart_details as b on a.id=b.cart_id  
        left join  rutas.products as productos on productos.id=b.product_id
        WHERE usuario.city='$city' AND a.order_date > now() - INTERVAL 30 day GROUP BY productos.id ORDER BY SUM(b.precio_producto*quantity) DESC"));
   
        
        return view('admin.reportes.reporte_por_dia')->with(compact('reportByday','reportBy30','reportBy15','reportBy15SP','reportBy7SP',
    'reportBy7','reportFactura','reportProducts','reportClientes','fechaFactura', 'test'));
    }

    public function getReporteGeneral(Request $request){

        $custom="♥";
        $fechadesde=$request->fechadesde;
        $fechahasta=$request->fechahasta;

        $sucursales = collect(\DB::table('citys')
        ->select('citys.*')
        ->get());

        if(empty($fechadesde)){
            $totalBySucursal=[];
            return view('admin.reportes.reporte_general')->with(compact('custom', 'sucursales', 'totalBySucursal','fechadesde','fechahasta'));
        }

        $totalBySucursal=collect(\DB::select("SELECT organization_name,organization_id,sum(importe_producto) AS total
        FROM (SELECT a.user_id no_cliente, d.name Nombre_cliente,d.usuarioapp,
        a.id pedido, a.order_date fecha_pedido,a.importe_total,a.status,c.codigo,c.name descripcion_producto,
        b.quantity, b.precio_producto, b.importe importe_producto, suc.organization_id,suc.organization_name
        FROM(SELECT * FROM rutas.carts WHERE order_date IS NOT NULL AND STATUS<>'Cancelado' ) a
        LEFT JOIN (SELECT * from rutas.cart_DETAILS) b on a.id=b.CART_ID
        LEFT JOIN (SELECT * from rutas.products) c on b.product_id=c.ID
        LEFT JOIN (SELECT * FROM rutas.users WHERE organization_id IS NOT NULL) d ON a.user_id=d.id
        LEFT JOIN (SELECT distinct organization_id,organization_name FROM rutas.xxkw_org_almacen) suc on suc.organization_id=a.organization_id
        WHERE a.CREATED_AT>='$fechadesde' 
        AND a.CREATED_AT<='$fechahasta') a
        GROUP BY organization_id;"));

        return view('admin.reportes.reporte_general')->with(compact('custom', 'sucursales', 'totalBySucursal','fechadesde','fechahasta'));

       }

       public function getBuscarMonto(Request $request){

       return $request;


       }
   
    
   

}
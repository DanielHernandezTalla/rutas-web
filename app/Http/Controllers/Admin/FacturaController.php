<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

use Mail;
use Redirect;
use Carbon\Carbon;

class FacturaController extends Controller
{

    public function getFechaFactura(Request $request){

        /*FACTURAS POR FECHA*/
       $fecha = $request->get('fecha');

       $test="TEST fecha";

       $fechaFactura= collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente,productos.name as producto,
       precios.precios as precio,pedidos.order_date as fecha_pedido, pedidos.fechaE as fecha_entrega, pedidos.status , pedidos.factura
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       LEFT JOIN rutas.products as productos ON detalle_pedido.product_id=productos.id
       LEFT JOIN rutas.dat_precios as precios ON precios.idproduct=productos.id
       WHERE
       pedidos.fechaE LIKE '".$fecha."%' 
       AND pedidos.factura='Si' 
       AND pedidos.status='Entregado' 
       ORDER BY pedidos.order_date DESC;"));

       /*Pedidos diarios*/ 
       $reportByday = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente,productos.name as producto,
       precios.precios as importe,pedidos.order_date as fecha_pedido, pedidos.fechaE as fecha_entrega, pedidos.status 
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       LEFT JOIN rutas.products as productos ON detalle_pedido.product_id=productos.id
       LEFT JOIN rutas.dat_precios as precios ON precios.idproduct=productos.id
       WHERE DATE(pedidos.order_date) = DATE(NOW())"));

       /*Clientes que han pedido ultimos 30 dias*/ 
       $reportBy30 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       WHERE pedidos.order_date > now() - INTERVAL 30 day order by usuarios.name"));

       /*Clientes que han pedido ultimos 15 dias*/ 
       $reportBy15 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       WHERE pedidos.order_date > now() - INTERVAL 15 day order by usuarios.name"));

       /*Clientes que NO han pedido ultimos 15 dias*/ 
       $reportBy15SP = collect(\DB::select("SELECT DISTINCT * FROM(SELECT a.user_id, d.name as cliente, d.phone 
       from rutas.carts as a
       left join rutas.cart_details b on a.id=b.cart_id
       left join rutas.products c on b.product_id=c.id
       left join rutas.users d on a.user_id=d.id 
       WHERE a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 30 day) as f
       WHERE f.user_id not in(SELECT a.user_id 
       from rutas.carts as a
       left join  rutas.cart_details b on a.id=b.cart_id
       left join  rutas.products c on b.product_id=c.id
       left join rutas.users d on a.user_id=d.id 
       WHERE a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 15 day
       )"));

       /*Clientes que NO han pedido ultimos 7 dias*/ 
       $reportBy7SP = collect(\DB::select("SELECT DISTINCT * FROM(SELECT a.user_id, d.name as cliente, d.phone 
       from rutas.carts as a
       left join rutas.cart_details b on a.id=b.cart_id
       left join rutas.products c on b.product_id=c.id
       left join rutas.users d on a.user_id=d.id 
       WHERE a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 15 day) as f
       WHERE f.user_id not in(SELECT a.user_id 
       from rutas.carts as a
       left join  rutas.cart_details b on a.id=b.cart_id
       left join  rutas.products c on b.product_id=c.id
       left join rutas.users d on a.user_id=d.id 
       WHERE a.status<>'Cancelado' AND a.order_date > now() - INTERVAL 7 day
       )"));

       /*Clientes que han pedido ultimos 7 dias*/ 
       $reportBy7 = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente, usuarios.phone
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       WHERE pedidos.order_date > now() - INTERVAL 7 day order by usuarios.name"));

       /*Clientes que han realizado pedidos con factura en los ultimos 30 dias*/ 
       $reportFactura = collect(\DB::select("SELECT DISTINCT usuarios.id as user_id,usuarios.name as cliente,productos.name as producto,
       precios.precios as precio,pedidos.order_date as fecha_pedido, pedidos.fechaE as fecha_entrega, pedidos.status , pedidos.factura
       FROM rutas.carts as pedidos 
       LEFT JOIN rutas.users as usuarios ON pedidos.user_id=usuarios.id
       LEFT JOIN rutas.cart_details as detalle_pedido ON detalle_pedido.cart_id=pedidos.id
       LEFT JOIN rutas.products as productos ON detalle_pedido.product_id=productos.id
       LEFT JOIN rutas.dat_precios as precios ON precios.idproduct=productos.id
       WHERE  pedidos.order_date > now() - INTERVAL 30 day AND pedidos.factura='Si' AND pedidos.status='Entregado' ORDER BY pedidos.order_date DESC"));

       /*Productos mas pedidos de los ultimos 30 dias*/ 
       $reportProducts= collect(\DB::select("SELECT c.id,c.name as producto, b.price as precio, SUM(quantity) AS totalKg, SUM(b.price*quantity) as totalFeria 
       from rutas.modificacionpedido as b
       left join  rutas.products c on b.id_product=c.id
       left join  rutas.carts a on a.id=b.id_cart 
       WHERE a.order_date > now() - INTERVAL 30 day  GROUP BY c.id, b.price ORDER BY c.id"));

       /*Clientes con mas pedidos de los ultimos 30 dias*/
       $reportClientes= collect(\DB::select("SELECT usuario.id,usuario.name as cliente,productos.name as producto , b.precio_producto as precio,
       SUM(quantity) AS totalKg,SUM(b.precio_producto*quantity) as totalFeria, a.order_date as fechaPedido 
       FROM rutas.users as usuario
       left join  rutas.carts as a on usuario.id=a.user_id
       left join  rutas.cart_details as b on a.id=b.cart_id  
       left join  rutas.products as productos on productos.id=b.product_id
       WHERE a.order_date > now() - INTERVAL 30 day GROUP BY productos.id ORDER BY SUM(b.precio_producto*quantity) DESC"));


       return view('admin.reportes.reporte_por_dia')->with(compact('reportByday','reportBy30','reportBy15','reportBy15SP','reportBy7SP',
       'reportBy7','reportFactura','reportProducts','reportClientes','fechaFactura','test'));
       

       //return back()->with(['test'=> $test]);
      
    }

    

}
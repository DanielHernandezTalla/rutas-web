<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Message;

class adminMessageController extends Controller
{

	public function index()
    {
        return view('admin.messages.index');
    }

    public function fetch()
    {
        return Message::with('user')->get();
    }

    public function sentMessage(Request $request)
    {
        $user = Auth::user();

        $message = Message::create([
            'message' => $request->message,
            'user_id' => Auth::user()->id,
        ]);

        broadcast(new MessageSentEvent($user, $message))->toOthers();
    }
}

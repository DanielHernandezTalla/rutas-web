<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class notificacionesFirebase extends Controller
{
    public function bulksend(Request $req){
        $url = 'https://fcm.googleapis.com/fcm/send';
        $dataArr = array('click_action' => 'FLUTTER_NOTIFICATION_CLICK', 'id' => $req->id,'status'=>"done");
        $notification = array('title' =>$req->title, 'text' => $req->body, 'image'=> $req->img, 'sound' => 'default', 'badge' => '1',);
        $arrayToSend = array('to' => "/topics/all", 'notification' => $notification, 'data' => $dataArr, 'priority'=>'high');
        $fields = json_encode ($arrayToSend);
        $headers = array (
            'Authorization: key=' . "AAAA12mw5X0:APA91bEsMcWRREk1Z-OCfkRXsoEJ2ntH3xT3cBEiTx9E0KZaeY901Jl1PE41pHEPSV30GJCt0uW3Yk-iVkzxa4vxmE11Oq3a0T7zrTcBuk7R4arDiVmrCKdetrICNYoeTALfjgtc-gaX",
            'Content-Type: application/json'
        );
   
        $ch = curl_init ();
        curl_setopt ( $ch, CURLOPT_URL, $url );
        curl_setopt ( $ch, CURLOPT_POST, true );
        curl_setopt ( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt ( $ch, CURLOPT_POSTFIELDS, $fields );
   
        $result = curl_exec ( $ch );
        //var_dump($result);
        curl_close ( $ch );
        return $result;
   
    }
}

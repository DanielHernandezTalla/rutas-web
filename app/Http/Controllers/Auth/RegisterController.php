<?php

 

namespace App\Http\Controllers\Auth;

 

use App\User;

use App\Http\Controllers\Controller;

use GuzzleHttp\Psr7\Request;

use Illuminate\Support\Facades\Validator;

use Illuminate\Foundation\Auth\RegistersUsers;

use App\Citys;

use App\Colonias;

 

use Illuminate\Http\Request as Req;

 

class RegisterController extends Controller

{

    /*

    |--------------------------------------------------------------------------

    | Register Controller

    |--------------------------------------------------------------------------

    |

    | This controller handles the registration of new users as well as their

    | validation and creation. By default this controller uses a trait to

    | provide this functionality without requiring any additional code.

    |

    */

 

    use RegistersUsers;

 

    /**

     * Where to redirect users after registration.

     *

     * @var string

     */

    protected $redirectTo = '/';

 

    /**

     * Create a new controller instance.

     *

     * @return void

     */

    public function __construct()

    {

        $this->middleware('guest');

    }

 

    /**

     * Get a validator for an incoming registration request.

     *

     * @param  array  $data

     * @return \Illuminate\Contracts\Validation\Validator

     */

    protected function validator(array $data)

    {

        return Validator::make($data, [

            'name' => 'required|string|max:255',

            'email' => 'nullable|email|max:255|unique:users',

            'password' => 'required|string|min:6|confirmed',

            'phone' => 'required',

            'address' => 'required',

            'codigoPostal' => 'required',

            'colonia' => 'required',

            'city' => 'required',

            'username' => 'required|unique:users'

        ]);

    }

 

    /**

     * Create a new user instance after a valid registration.

     *

     * @param  array  $data

     * @return \App\User

     */

    protected function create(array $data)

    {

        return User::create([

            'name' => $data['name'],

            'username' => $data['username'],

            'email' => $data['email'] ?: '',

            'phone' => $data['phone'],

            'city' => $data['city'],

            'codigoPostal' => $data['codigoPostal'],

            'colonia' => $data['colonia'],

            'address' => $data['address'],

            'numeroExterior' => $data['numeroExterior'],

            'numerointerior' => $data['numeroInterior'],

            'password' => bcrypt($data['password']),

            'lugar' => $data['lugar'],
            
            'activo' => $data['activo'],


             'rfc' => $data['rfc'],

            
            

           

            
           

        ]);

    }

 

    public function showRegistrationForm(Req $request)

    {

        $name = $request->input('name');

        $email = $request->input('email');

        $citys = Citys::all()->pluck('name', 'id');

        $colonias = Colonias::all()->pluck('colonia', 'id');

        return view('auth.register')->with(compact('name', 'email','citys','colonias'));

    }




}
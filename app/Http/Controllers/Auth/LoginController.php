<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request as IlluminateRequest;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{

    use AuthenticatesUsers;

         protected $redirectTo = '/';
   
protected $redirectTo1 = 'useradmin/newuser';

protected $redirectTo2 = '/admin/precios';

protected $redirectTo3 = '/gester/products';

protected $redirectTo4 = '/usad/useradmin';

protected $redirectTo5 = '/adminpedidos/pedidos';

protected $redirectTo6 = '/rutero/index'; 

protected $redirectTo7 = '/picking/index';   

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function username()
    {
        return 'username';
    }

    public function showLoginForm(IlluminateRequest $request)
    {
        if ($request->has('redirect_to')) {
            session()->put('redirect_to', $request->input('redirect_to'));
        }

        return view('auth.login');
    }

    public function redirectTo()
    {
        if (session()->has('redirect_to'))
            return session()->pull('redirect_to');


    if (Auth::check()) {

    $useradmin=auth()->user()->useradmin;
      $admin=auth()->user()->admin;
        $gester=auth()->user()->gester;
          $usad=auth()->user()->usad;
            $adminpedidos=auth()->user()->adminpedidos;
              $rutero=auth()->user()->rutero;
                $picking=auth()->user()->picking;
           
if($useradmin==1){
 return $this->redirectTo1;
}else{

    if($admin==1){
 return $this->redirectTo2;
    }else{

        if($gester==1){
 return $this->redirectTo3;
        }else{

            if($usad==1){
 return $this->redirectTo4;
            }else{

                if($adminpedidos==1){
 return $this->redirectTo5;
                }else{

                    if($rutero==1){
 return $this->redirectTo6;
                    }else{

                 if($picking==1){
 return $this->redirectTo7;
                    }else{         
                        
 return $this->redirectTo;
                        }
                    }
                }
            }
        }
    }
}
}

       
    }
}

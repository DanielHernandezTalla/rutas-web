<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate \ Support \ Facades \ Auth;
use App\Cart;

class PedidosController extends Controller
{
   

    public function index()
    {
       $pedidos= collect(\DB::select('select a.id, a.order_date,a.created_at, a.status, b.city from carts a, users b where a.user_id = b.id AND a.status=:status', ['status' => 'Pending']));
       
       $carts = collect(\DB::select('select a.name, a.price, c.order_date , d.username, d.phone, d.address, d.city from products a, cart_details b,  carts c , users d where a.id = b.product_id AND d.id = c.user_id AND b.cart_id = c.id AND c.status=:status', ['status' => 'Pending']));
       return view('pedidos')->with(compact('pedidos', 'carts'));

    }  


  
}





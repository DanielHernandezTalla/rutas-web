<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\DatPrecios;
use App\Datinvcedis;
use App\DatPromocion;
use App\Catdirenv;
use Carbon\Carbon;

class ProductController extends Controller
{
	//Aqui falta mandar la organization_id del usuario logeado para filtrarlo en products y mandar la organzacion correspondiente y no la del producto
    public function show($id,$idCentroDeVenta)
    {
    	$product = Product::find($id);
		$images = $product->images;
		$codigo = $product->codigo;
		$organization_id=$product->organization_id;
    	
    	$imagesLeft = collect();
    	$imagesRight = collect();
        
    	foreach ($images as $key => $image) {
    		if ($key%2==0)
    			$imagesLeft->push($image);
    		else
    			$imagesRight->push($image);
    	}
		


		$hoy = Carbon::today();
		$DatPrecios = DatPrecios::where('idcentroventa',$idCentroDeVenta)->where('idproduct', '=', $id)->get();
		//Se obtiene la organizacion del user logeado y no la de productos
		$user_organization_id=auth()->user()->organization_id;
		//dump($user_organization_id);
		$Datinvcedis= Datinvcedis::where('codigo', '=', $codigo)->where('organization_id','=', $user_organization_id)->get();
		
		$contar =   collect(\DB::table('datinvcedis as cedis')
        ->select(\DB::raw('count(cedis.id) as cedis_count'))
        ->where('cedis.codigo',$codigo)
		->get());


		 $iduser=auth()->user()->id;

		$codigo_cliente = Catdirenv::take(1)->where('iduser',$iduser)->get();

		 	foreach ($codigo_cliente as $codigo_clientes) {
    $clave=$codigo_clientes->clavecliente;
    $ruta_user=$codigo_clientes->tipopedido;
    } 


		$a=DatPromocion::where('producto', '=', $codigo)
		->where('fecha_final','>=',$hoy)
		->where('fecha_final','>=',$hoy)
		->where('idcliente','=',$clave);
		

		$b=DatPromocion::where('producto', '=', $codigo)
		->where('fecha_final','>=',$hoy)
		->where('tipopromocion','=','RUTA')
		->where('ruta','=',$ruta_user);
	

		$c=DatPromocion::where('producto', '=', $codigo)
		->where('fecha_final','>=',$hoy)
		->where('tipopromocion','=','PRODUCTO')
		->where('organization_id','=',$user_organization_id)
		->union($a)
		->union($b)
		->get();


		$DatPromocion= $c;
		
		


return view('products.show')->with(compact('product', 'imagesLeft', 'imagesRight','DatPrecios','Datinvcedis','contar','DatPromocion'));
    }
}

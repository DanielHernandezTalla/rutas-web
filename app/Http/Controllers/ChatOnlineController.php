<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Messages;
use Pusher\Pusher;
use DB;

class ChatOnlineController extends Controller
{
    public function index(){ 

        //auth trae los datos del usuario logeado
        $city=auth()->user()->city;
        //Cuenta cuantos mensajes han sido leidos
        $users = DB::select("SELECT users.id, users.name, users.avatar, users.email, users.city, count(is_read) AS unread 
        FROM users 
        LEFT JOIN  message ON users.id = message.from AND is_read = 0 AND message.to = " . Auth::id() . "
        WHERE users.city='$city' AND users.name IS NOT NULL AND users.id != " . Auth::id() . "
        GROUP BY users.id, users.name, users.avatar, users.email ORDER BY users.name ASC");

        //return a la vista con la variable
        return view('chatOnline.chatOnline',['users'=> $users]);
    }

    public function getMessage($user_id){ 
        
        $my_id=Auth::id();
        // Make read all unread message
        Messages::where(['from' => $user_id, 'to' => $my_id])->update(['is_read' => 1]);
        
        $messages= Messages::where(function($query) use ($user_id,$my_id){
            $query->where('from',$my_id)->where('to',$user_id);
        })->orWhere(function($query) use ($user_id,$my_id){
            $query->where('from',$user_id)->where('to',$my_id);
        })->get();
        return view('messages.index',['messages'=> $messages]);
    }

    public function sendMessage(Request $request){
        $from = Auth::id();
        $to = $request->receiver_id;
        $message = $request->message;

        $data = new Messages();
        $data->from=$from;
        $data->to=$to;
        $data->message=$message;
        $data->is_read=0;
        $data->save();

        return view('messages.index');

        //Pusher
        $options = array(
            'cluster' => 'us2',
            'useTLS' => true
        );
        $pusher = new Pusher(
            env('PUSHER_APP_KEY'),
            env('PUSHER_APP_SECRET'),
            env('PUSHER_APP_ID'),
            $options
        );
        
        $data = ['from' => $from, 'to' => $to];//Envia el mensaje cuando da enter
        $pusher->trigger('my-channel', 'my-event', $data);

    }

}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use App\User;
use App\Mail\NewOrder;
use App\Notifications\MeetReminder;
use Mail;
use Illuminate\Support\Facades\DB;


class CartController extends Controller
{
    public function update(Request $request,$id){

      //return $request;

    if($request->contador == 0  && $request->count_products != 0){

  if($request->fechaE == "" && $request->fechaE2 == ""){

   $notification2 = 'Favor de seleccionar una fecha de entrega';  

     return redirect('/home')->with(compact('notification2'));

}elseif ($request->fechaE2 == "") {

  $direcciones = $request->get('direccionselect'); 
  $city=auth()->user()->city;
  $userid=auth()->user()->id;

  
  $ruta=collect(\DB::select('select   a.organization_id as organizacion from 
  citys a, users b  where  a.name=:city ', ['city'=>$city]));
      


   return $direnv = collect(\DB::table('catdirenv')
      
        ->select('id')
        ->where('tipopedido', $direcciones)
        ->where('iduser', $userid)
        ->get());

  //La variable $direnv no debe iterarse
   foreach ($direnv as $direnvs) {
    $direnvid=$direnvs->id;
    }      

  foreach ($ruta as $rutas) {
    $organizacion=$rutas->organizacion;
    }

    $organizacion2=$organizacion;
    $direnvid2=$direnvid;
 

 
 $tiempo = collect(\DB::table('tiempoprueba')
        ->select('Tiempo')
        ->get());

   foreach ($tiempo as $tiempos) {
    $tiempoentrega=$tiempos->Tiempo;
    }
$tiempoespera = 24;
$end = Carbon::parse($request->input('fechaE'));

$now = Carbon::now();

$length = $end->diffInHours($now);

$dt2 = $now->subHours(2);

$dt=$dt2->toTimeString();

if($dt >= $tiempoentrega){
  if($length < $tiempoespera){
   $notification2 = 'Despues de las 12 del día no se pueden hacer pedidos para mañana';  
     return back()->with(compact('notification2'));
   }else{

$date = $request->get('fechaE'); 

if(date('w', strtotime($date)) == 7 || date('w', strtotime($date)) == 0) {
    $notification2 = 'La fecha no puede ser domingo';  

     return back()->with(compact('notification2'));
} else {

        $fechaE = $request->get('fechaE');
        $factura = $request->get('factura');
        $direccion = $request->get('direccionselect');
        $date = Carbon::createFromFormat('m/d/Y', $fechaE)->format('Y-m-d H:i:s');

        if ($factura=="si") {
          $facturas="si";
        }else{
           $facturas="no";
        }
        
         $datefinal= Carbon::now();

        $dt = $datefinal->subHours(2);
       

        $client = auth()->user(); 
        $cart = $client->cart;
        $cart->status = 'Pendiente';
        $cart->idcentrovta = $id;
        $cart->fechaE = $date;
        $cart->ruta = $direccion;
        $cart->factura=$facturas;
        $cart->iddirenv=$direnvid2;
        $cart->organization_id =$organizacion2;
       

        $cart->order_date = Carbon::now();
        $cart->order_datetime = $dt->toDateTimeString(); 
        $cart->save(); // UPDATE
       

        $admins = User::where('admin', true)->get();
        try {
          Mail::to($admins)->send(new NewOrder($client, $cart));
        } catch (\Throwable $th) {
          //throw $th;
        }
       

      $notification = 'En breve se confirmara el pedido para proceder al pago';  

     return redirect('/home')->with(compact('notification'));
    

}

 }  
}else{


$date = $request->get('fechaE'); 

if(date('w', strtotime($date)) == 7 || date('w', strtotime($date)) == 0) {
    $notification2 = 'La fecha no puede ser domingo';  

     return back()->with(compact('notification2'));
} else {


   

        $fechaE = $request->get('fechaE');
        $factura = $request->get('factura');
        $direccion = $request->get('direccionselect');




 $date = Carbon::createFromFormat('m/d/Y', $fechaE)->format('Y-m-d H:i:s');

        if ($factura=="si") {
          $facturas="si";
        }else{
           $facturas="no";
        }
        
         $datefinal= Carbon::now();

        $dt = $datefinal->subHours(2);
       

        $client = auth()->user(); 
        $cart = $client->cart;
        $cart->status = 'Pendiente';
        $cart->idcentrovta = $id;
        $cart->fechaE = $date;
        $cart->ruta = $direccion;
        $cart->factura=$facturas;
        $cart->iddirenv=$direnvid2;
        $cart->organization_id =$organizacion2;
       

        $cart->order_date = Carbon::now();
        $cart->order_datetime = $dt->toDateTimeString(); 
        $cart->save(); // UPDATE
       

        $admins = User::where('admin', true)->get();
        try {
          Mail::to($admins)->send(new NewOrder($client, $cart));
        } catch (\Throwable $th) {
          //throw $th;
        }

      $notification = 'En breve se confirmara el pedido para proceder al pago';  

     return redirect('/home')->with(compact('notification'));
    

}
}

   }elseif ($request->fechaE == "") {





    $direcciones = $request->get('direccionselect'); 

  $city=auth()->user()->city;
  $userid=auth()->user()->id;

  
  $ruta=collect(\DB::select('select   a.organization_id as organizacion from 
  citys a, users b  where  a.name=:city ', ['city'=>$city]));
      


   $direnv = collect(\DB::table('catdirenv')
      
        ->select('id')
        ->where('tipopedido', $direcciones)
        ->where('iduser', $userid)
        ->get());


   foreach ($direnv as $direnvs) {
    $direnvid=$direnvs->id;
    }      

  foreach ($ruta as $rutas) {
    $organizacion=$rutas->organizacion;
    }

    $organizacion2=$organizacion;
    $direnvid2=$direnvid;
 

 
 $tiempo = collect(\DB::table('catctehorasentrega')
      
        ->select('horas')
        ->get());

   foreach ($tiempo as $tiempos) {
    $tiempoentrega=$tiempos->horas;
    }

$end = Carbon::parse($request->input('fechaE2'));

$now = Carbon::now();

$length = $end->diffInHours($now);

if($length < $tiempoentrega){

   $notification2 = 'La fecha de entraga debe ser mayor a 48 horas';  

     return back()->with(compact('notification2'));
}else{


$date = $request->get('fechaE2'); 

if(date('w', strtotime($date)) == 7 || date('w', strtotime($date)) == 0) {
    $notification2 = 'La fecha no puede ser domingo';  

     return back()->with(compact('notification2'));
} else {


   

        $fechaE2 = $request->get('fechaE2');
        $factura = $request->get('factura');
        $direccion = $request->get('direccionselect');



        if ($factura=="si") {
          $facturas="si";
        }else{
           $facturas="no";
        }
        
         $datefinal= Carbon::now();

        $dt = $datefinal->subHours(2);
       

        $client = auth()->user(); 
        $cart = $client->cart;
        $cart->status = 'Pendiente';
        $cart->idcentrovta = $id;
        $cart->fechaE = $fechaE2;
        $cart->ruta = $direccion;
        $cart->factura=$facturas;
        $cart->iddirenv=$direnvid2;
        $cart->organization_id =$organizacion2;
       

        $cart->order_date = Carbon::now();
        $cart->order_datetime = $dt->toDateTimeString(); 
        $cart->save(); // UPDATE
        
        //Ejecuta un update de organization_id para los productos que no corresponden a su propia organizacion
        DB::statement("CALL rutas.SP_BUSCARCART(".$cart->id.")");

        //Ejecuta un update del total de los importes para hacer match con el total correcto
        DB::statement("CALL SP_CART_REVISAR_IMPORTE(".$cart->id.")");

        $admins = User::where('admin', true)->get();
        
        try {
          Mail::to($admins)->send(new NewOrder($client, $cart));
        } catch (\Throwable $th) {
          //throw $th;
        }

        $title = 'Some title';
        $description = "Some description with ";
       //$postOwner->notify(new MeetReminder($title, $description));

      $notification = 'En breve se confirmara el pedido para proceder al pago';  

     return redirect('/home')->with(compact('notification'));
    

}
}

   }


   } if($request->count_products == 0){

       $notification2 = 'El carrito de compras debe llevar al menos un producto';

       return redirect('/home')->with(compact('notification2'));  
   } 

   if($request->contador != 0  && $request->count_products == 0){

 $notification2 = 'Debe terminar con un pedido que aun está pendiente y el carrito de compras debe llevar al menos un producto';

       return redirect('/home')->with(compact('notification2'));
   }
   
   if($request->contador != 0){

      $notification2 = 'Debe terminar con un pedido que aun está pendiente';

       return redirect('/home')->with(compact('notification2'));  
    
   } 
      
    }

       public function prueba(Request $request)
    {
      
      $city=auth()->user()->city;
  $userid=auth()->user()->id;
  

    $ruta=collect(\DB::select('select   a.organization_id as organizacion from 
    citys a, users b  where  a.name=:city ', ['city'=>$city]));
  foreach ($ruta as $rutas) {
    $organizacion=$rutas->organizacion;
    }

    $organizacion2=$organizacion;
      return $organizacion2;

}


   }  
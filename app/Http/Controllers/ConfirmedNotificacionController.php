<?php

namespace App\Http\Controllers;

use App\Cart;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Http\Requests\NotificacionRequest;
use App\Notifications\ConfirmedNotification;
use App\Notificacion;
use App\User;

class ConfirmedNotificacionController extends Controller
{
    public function store(NotificacionRequest $request)
    {
        $carts  = Cart::findOrFail($request->cart_id);
        $status ='Pending';
  
     
        $notificacionConfirmed  = Notificacion::create([
            'body' => $request->body,
            'user_id' => Auth::id(),
            'cart_id' => $carts->id
        ]);
     
        if ($carts->status != $status) {
            $user = User::find($carts->user_id); 
            $user->notify(new ConfirmedNotification($notificacionConfirmed));
        }
     
        return redirect()->route('confirmacion.show', $carts->id);
    }

     public function markNotification(Request $request)
    {
        auth()->user()->unreadNotifications
                ->when($request->input('id'), function($query) use ($request){
                    return $query->where('id', $request->input('id'));
                })->markAsRead();
        return response()->noContent();
    }
}

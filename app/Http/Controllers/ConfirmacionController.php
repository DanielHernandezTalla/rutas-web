<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Citys;
use App\Colonias;
use App\catCiudades;
use App\Catdirfac;

class ConfirmacionController extends Controller
{


    public function index()
    {
      
    
       return view('pago');
    }

      public function pagoRechazado()
    {
      
    
       return view('pagoRechazado');
    }




     public function show($id)
    {

       



           $product = collect(\DB::table('users')
    ->join('carts' , 'users.id', '=', 'carts.user_id')
    ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
    ->select('users.*', 'products.name','products.uom','modificacionpedido.importe','modificacionpedido.quantity','carts.importe_total','modificacionpedido.iva','modificacionpedido.id_cart')
    ->where('id_cart', $id)
    ->get());

           

        return view('confirmacion')->with(compact('product',$product)); // form de edición
    }


    



     public function listado()
    {

      $id=auth()->user()->id;
    $listado = collect(\DB::table('carts')
    ->select('carts.*')
    ->where('status', 'Authorized')
    ->where('user_id', $id)
    ->get());

        return view('listadoPedidosConfirmados')->with(compact('listado',$listado)); // form de edición
    }






     public function pago($id)
    {
       $id2=auth()->user()->id;
        
        $User =   collect(\DB::table('users as us')  


        ->select('us.id as idus','us.name', 'us.username','us.email','us.phone','us.address',
            'us.city as ciuname',
            'us.colonia as colname', 
            'us.codigoPostal as codname',
            'us.numeroExterior','us.numerointerior','us.password','us.admin')

        ->where('us.id', $id2)
        ->get());


      $product = collect(\DB::table('users')
    ->join('carts' , 'users.id', '=', 'carts.user_id')
    ->join('modificacionpedido', 'carts.id', '=', 'modificacionpedido.id_cart')
    ->join('products', 'modificacionpedido.id_product', '=', 'products.id')
    ->select('users.phone','users.address','users.username','users.email','users.city','carts.id', 'products.name','products.uom','modificacionpedido.importe','modificacionpedido.quantity','modificacionpedido.iva','modificacionpedido.id_cart','carts.importe_total')
    ->where('id_cart', $id)
    ->get());


      $citys = Citys::all()->pluck('name', 'id');
        $colonias = Colonias::all()->pluck('name', 'id');
        $estados = catCiudades::groupBy('estado')->pluck('estado');

        auth()->user()->unreadNotifications->markAsRead();

       return \View::make('main')->with(compact('product','citys','colonias','User','estados',$product,$citys,$colonias,$User,$estados)); // form de edición
    }






    public function createFactura(Request $request)
{
$catdirfac = new Catdirfac();
$catdirfac->userid = $request->input('idus');
$catdirfac->nombre = $request->input('name');
$catdirfac->rfc = $request->input('rfc');
$catdirfac->calle = $request->input('address');
$catdirfac->colonia = $request->input('colonia');
$catdirfac->city = $request->input('city');
$catdirfac->estado = $request->input('estado');
$catdirfac->email = $request->input('email');
$catdirfac->codigopostal= $request->input('codigoPostal');
$catdirfac->numext = $request->input('numeroExterior');

$catdirfac->save();
 return back();
}
}

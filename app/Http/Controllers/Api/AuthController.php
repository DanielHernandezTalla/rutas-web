<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Password;
use Illuminate\Auth\Events\PasswordReset;
use App\Cart;
use App\Catdirenv;
use DB;

class AuthController extends Controller
{
    
public function register (Request $request) {

    $validator = Validator::make($request->all(), [
        'name' => 'required|string|max:255',
        'email' => 'required|string|email|max:255|unique:users',
        'password' => 'required|string|min:6|confirmed',
    ]);

    if ($validator->fails())
    {
        return response(['errors'=>$validator->errors()->all()], 422);
    }

    $request['password']=Hash::make($request['password']);
    $user = User::create($request->toArray());

    $token = $user->createToken('Laravel Password Grant Client')->accessToken;
    $response = ['token' => $token];

    return response($response, 200);

}


public function login (Request $request) {

    $user = User::where('username', $request->username)->first();
    $Usuarios = collect(\DB::table('users')
    ->select('users.*')
    ->where('username',$request->username)
    ->get());

    

    if ($user) {
        foreach ($Usuarios as $usuario) {
            $idusuario=$usuario->id;
            
        }
        //$catdirenv= Catdirenv::where('iduser',$idusuario)->get();

        $catdirenv = collect(\DB::table('catdirenv')
        ->select('catdirenv.*')
        ->leftjoin('centroventa', 'catdirenv.almacen_ruta', '=', 'centroventa.subinventory_name')
        ->where('catdirenv.iduser',$idusuario)
        ->where('centroventa.activo',true)
        ->get());

        //return $catdirenv;

        if (Hash::check($request->password, $user->password)) {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token, 'user' => $user,  'catdirenv' => $catdirenv ];
            return response($response, 200);

        } else {
            $respuesta = "Contraseña incorrecta";
            $statusCode= 422;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
           
            return response($response, 422);
        }

    } else {
        $respuesta = 'El usuario es incorrecto o no existe';
        $statusCode= 422;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];

        return response($response, 422);
    }

}

public function loginFake (Request $request) {

    $user = User::where('username', $request->username)->first();
    $Usuarios = collect(\DB::table('users')
    ->select('users.*')
    ->where('username',$request->username)
    ->get());

    if ($user) {
        foreach ($Usuarios as $usuario) {
            $idusuario=$usuario->id;
            
        }
        $catdirenv= Catdirenv::where('iduser',$idusuario)->get();

        if (Hash::check($request->password, $user->password)) {
            $token = $user->createToken('Laravel Password Grant Client')->accessToken;
            $response = ['token' => $token, 'user' => $user,  'catdirenv' => $catdirenv ];
            return response($response, 200);

        } else {
            $respuesta = "Contraseña incorrecta";
            $statusCode= 422;
            $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];
           
            return response($response, 422);
        }

    } else {
        $respuesta = 'El usuario es incorrecto o no existe';
        $statusCode= 422;
        $response = ['respuesta' => $respuesta,  'statusCode' => $statusCode ];

        return response($response, 422);
    }

}

public function logout (Request $request) {

    $token = $request->user()->token();
    $token->revoke();

    $response = 'You have been succesfully logged out!';
    return response($response, 200);

}


public function forgot_password(Request $request)
{
    $input = $request->all();
    $rules = array(
        'email' => "required|email",
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {
        try {
            $response = Password::sendResetLink($request->only('email'), function (Message $message) {
                $message->subject($this->getEmailSubject());
            });
            switch ($response) {
                case Password::RESET_LINK_SENT:
                    return \Response::json(array("status" => 200, "message" => trans($response), "data" => array()));
                case Password::INVALID_USER:
                    return \Response::json(array("status" => 400, "message" => trans($response), "data" => array()));
            }
        } catch (\Swift_TransportException $ex) {
            $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
        } catch (Exception $ex) {
            $arr = array("status" => 400, "message" => $ex->getMessage(), "data" => []);
        }
    }
    return \Response::json($arr);
}


public function change_password(Request $request)
{
    $input = $request->all();
    $userid = Auth::guard('api')->user()->id;
    $rules = array(
        'old_password' => 'required',
        'new_password' => 'required|min:6',
        'confirm_password' => 'required|same:new_password',
    );
    $validator = Validator::make($input, $rules);
    if ($validator->fails()) {
        $arr = array("status" => 400, "message" => $validator->errors()->first(), "data" => array());
    } else {
        try {
            if ((Hash::check(request('old_password'), Auth::user()->password)) == false) {
                $arr = array("status" => 400, "message" => "Check your old password.", "data" => array());
            } else if ((Hash::check(request('new_password'), Auth::user()->password)) == true) {
                $arr = array("status" => 400, "message" => "Please enter a password which is not similar then current password.", "data" => array());
            } else {
                User::where('id', $userid)->update(['password' => Hash::make($input['new_password'])]);
                $arr = array("status" => 200, "message" => "Password updated successfully.", "data" => array());
            }
        } catch (\Exception $ex) {
            if (isset($ex->errorInfo[2])) {
                $msg = $ex->errorInfo[2];
            } else {
                $msg = $ex->getMessage();
            }
            $arr = array("status" => 400, "message" => $msg, "data" => array());
        }
    }
    return \Response::json($arr);
}


}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{
    public function productos1() {

        $id=1;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

$DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         
         ->where('dat_precios.idcategory', $id)
         ->orderBy('dat_precios.precios','desc')
         ->get();

   return compact('categories1','DatPrecios');

    }

    public function productos2() {

        $id=2;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

$DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         
         ->where('dat_precios.idcategory', $id)
         ->orderBy('dat_precios.precios','desc')
         ->get();

   return compact('categories1','DatPrecios');

    }

    public function productos3() {

        $id=3;

        $categories1 = collect(\DB::table('categories')
        ->select('categories.*')
        ->where('id', $id)
        ->get());

$DatPrecios = DatPrecios::select('dat_precios.nameproduct','dat_precios.idproduct','dat_precios.precios','dat_precios.idcentroventa','product_images.image') 
         ->leftjoin('centroventa', 'dat_precios.idcentroventa', '=', 'centroventa.id')
         ->leftjoin('product_images', 'dat_precios.idproduct', '=', 'product_images.product_id')
         
         ->where('dat_precios.idcategory', $id)
         ->orderBy('dat_precios.precios','desc')
         ->get();

   return compact('categories1','DatPrecios');

    }



    
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class catPagos extends Model
{
      protected $table = 'pagos';

    protected $fillable =['order_id','idforma_pago'];
}

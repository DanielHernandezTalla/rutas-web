<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

  protected $table = "carts";
  protected $primaryKey = 'id';


    public function details()
    {
    	return $this->hasMany(CartDetail::class);
    }

    public function getTotalAttribute()
    {
    	$total = 0;
    	foreach ($this->details as $detail) {
        $total += $detail->quantity * $detail->precio_producto + $detail->iva;
    	}
    	return $total;
    }


	
public function notificaciones()
{
  return $this->hasMany('App\Notificacion');
}


public function user()
{
  return $this->hasMany('App\User');
}
/*Obtiene un array de productos en base a un idPedido*/ 
public function productos()
    {
      return $this->belongsToMany('App\Product','cart_details','cart_id','product_id','id');
    }


}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CartDetail extends Model
{
	protected $table = "cart_details";
    protected $primaryKey = 'id_cart_details';

    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
}

<?php


use  GuzzleHttp\Client ;
use Illuminate\Support\Facades\Input;
use App\Colonias;
use App\Citys;
use App\CatCiudades;


Route::get('download-jsonfile', array('as'=> 'file.download', 'uses' => 'TestController@downloadJSONFile'));



Route::get('/samples/document/txt/sample3.txt', 'TestController@ticket'); // listado

Route::get('/test/prueba.txt', 'TestController@test'); // listado

Route::get('/', 'TestController@welcome');
Route::get('/kowiEstadio/{id}', 'TestController@welcomeEstadio');
Route::get('/reportar', 'TestController@reporteerror');
Route::post('/reportar', 'TestController@reporteerrorcreate');


//api de la app en expo
Route::post('/curlNotification','NotificacionesFirebase@curlNotification');
Route::get('/productos1', 'apiController@productos1');






Auth::routes();




    //vendor package route files alymosul/laravel-exponent-push-notifications/Http/routes.php exluded in package.json
    //this is override alymosul/laravel-exponent-push-notifications/Http/routes.php
    //for suit middleware
	Route::group(['prefix' => 'api/exponent/devices', 'middleware' => ['bindings']], function () { 
	    Route::post('subscribe', [
	        'as'    =>  'register-interest',
	        'uses'  =>  'NotificationChannels\ExpoPushNotifications\Http\ExpoController@subscribe',
	    ]);

	    Route::post('unsubscribe', [
	        'as'    =>  'remove-interest',
	        'uses'  =>  'NotificationChannels\ExpoPushNotifications\Http\ExpoController@unsubscribe',
	    ]);
	});
 
 
Route::get('/ajax-colonias',function(){
	
	$cit_id = Input::get('cit_id');

	$colonias = CatCiudades::where('ciudad', '=', $cit_id)->distinct()->get(['codigopostal']);

	return Response::json($colonias);
});


Route::get('/ajax-codigoPostales',function(){
	
	$col_id = Input::get('col_id');

	$colonias =CatCiudades::where('codigopostal', '=', $col_id)->distinct()->get(['colonia']);

	return Response::json($colonias);
});


Route::get('/json-api', function() {
	$client = new Client();

	$response = $client->request('GET', 'http://appkowi.kowi.com.mx/home');
	$statusCode = $response->getStatusCode();
	$body = $response->getBody()->getContents();

	if ($statusCode === 200 ) {
        return View::make('pago');
    }

	return $statusCode;
});

Route::get('/live_search/action', 'LiveSearch@action')->name('live_search.action');

Route::get('/live_search2/action', 'LiveSearch2@action')->name('live_search2.action');

Route::get('/live_search3/action', 'LiveSearch3@action')->name('live_search3.action');

Route::post('/createdirec', 'HomeController@createDireccion');

Route::post('/createfactura', 'ConfirmacionController@createFactura');

Route::get('/listcentros', 'TestController@centroventa');
Route::get('/listcentros?CITY={id}', 'TestController@centroventa2');

Route::get('/search', 'SearchController@show');
Route::get('/products/json', 'SearchController@data');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/homePendiente/{id}', 'HomeController@homePendiente');
Route::post('/CancelHomePendiente/{id}', 'HomeController@updateHomePedidoCancelado');
Route::post('/RefreshHomePendiente/{id}', 'HomeController@updateHomePedidoRefresh');
Route::get('/homeedit/{id}/{id2}', 'HomeController@editprod');
Route::post('/updateCarrito/{id}', 'HomeController@updateCarrito'); // update home
Route::delete('/deleteDetalle', 'HomeController@destroyEdit'); // form eliminar

Route::get('/private', 'HomeController@private')->name('private');
Route::get('/users', 'HomeController@users')->name('users');
Route::get('/usersAdmin', 'HomeController@usersAdmin')->name('usersAdmin');
Route::get('/modificarUsuario', 'HomeController@ChangeUser'); //  home  
Route::post('/modificarUsuario/{id}/editUser', 'HomeController@updateUser'); // actualizar

Route::get('/creditouser', 'HomeController@CreditUser'); //  credito del usuario 


Route::get('/products/{id}/{idCentroDeVenta}', 'ProductController@show');
Route::get('/categories/{category}', 'CategoryController@show');

Route::post('/cart', 'CartDetailController@store');
Route::delete('/cart/{id}', 'CartDetailController@destroy');

Route::post('/order/{id}', 'CartController@update');
Route::post('/Authorder/{id}/{idcvta}', 'AuthPedido@updatePedido'); // listado
Route::post('/Cancelorder/{id}', 'AuthPedido@updatePedidoCancelado'); // listado
Route::post('/CancelorderCancelacion/{id}', 'AuthPedido@updatePedidoCanceladoConMotivo'); // listado
Route::post('/CancelorderAuth/{id}', 'AuthPedido@updatePedidoCanceladoAuth'); // listado
Route::post('/Confirmorder/{id}', 'AuthPedido@updatePedidoConfirmado'); // listado

//New chat Online Erzu

Route::get('/chatOnline', 'ChatOnlineController@index')->name('chatOnline');
Route::get('/message/{id}', 'ChatOnlineController@getMessage')->name('message');
Route::post('/message', 'ChatOnlineController@sendMessage');

//Fin New chat Online


Route::get('/private-messages/{user}', 'MessageController@privateMessages')->name('privateMessages');
Route::post('/private-messages/{user}', 'MessageController@sendPrivateMessage')->name('privateMessages.store');

Route::get('/confirmacion/show/{id}', 'ConfirmacionController@show'); //
Route::get('/pago/{id}', 'ConfirmacionController@pago')->name('pagar');

Route::post('/procces', 'ProcessController@pagar');
Route::resource('pagar', 'PagoController');

Route::get('/pago', 'ConfirmacionController@index'); //
Route::get('/pagoRechazado', 'ConfirmacionController@pagoRechazado'); //



 	
Route::get('/pruebahome', 'HomeController@prueba');
Route::get('/listadoPedidos', 'pedidoModificadoController@prueba')->name('listado');
Route::get('/confirmado', 'NotificationController@confirmado')->name('confirmado');

Route::get('/pruebaCart', 'CartController@prueba');

Route::post('/updatePedido', 'AdminPedidoController@updatePedido'); // listado  



Route::resource('notificaciones', 'NotificationController');
Route::get('/markAsRead', 'NotificationController@markAsRead')->name('markAsRead'); //
Route::post('/mark-as-read', 'NotificationController@markNotification')->name('markNotification');




Route::middleware(['auth', 'admin'])->prefix('admin')->namespace('Admin')
->group(function () {
	


	Route::get('/correosusers', 'AdminPedidoController@correosusers'); // listado
	Route::post('/correosusers/modificar/{iduser}', 'AdminPedidoController@updateusercorreo');
	Route::get('/productosactivos', 'AdminPedidoController@productosactivos')->name('productos'); // listado
	Route::get('/StatusProducto', 'AdminPedidoController@UpdateStatusProduct')->name('StatusProducto');
	//Update img por codigo
	Route::get('/updateImgProducto', 'AdminPedidoController@UpdateImgProducto')->name('updateImgProducto');
	//Update usuarios oralce
	Route::post('/updateUserOracle', 'AdminPedidoController@updateUserOracle')->name('updateUserOracle');

	
	Route::get('/precios', 'AdminPedidoController@listprice'); // listado
	
    Route::get('/preciosfiltrados', 'AdminPedidoController@listpricefilter1');
    Route::get('/precios/{id}/edit', 'AdminPedidoController@listpricefilter'); //
//	Route::post('/precios/{id}/{id2}/update', 'AdminPedidoController@UpdatePrecios');
    Route::post('/precios/{id}/update', 'AdminPedidoController@UpdatePrecios');

    Route::get('/centrodeventas', 'AdminPedidoController@listcentros'); // listado
    Route::get('/centrodeventas/{id}/edit', 'AdminPedidoController@editcentros'); //
 	Route::get('/centrodeventas/add', 'AdminPedidoController@listcentrosAdd'); // listado
	Route::post('/insertcentroventas', 'AdminPedidoController@createcentro');
	Route::post('/centrodeventas/{id}/update', 'AdminPedidoController@UpdateCentros');


Route::get('/promolist', 'AdminPedidoController@userpromociones'); // listado
Route::get('/promo/{id}', 'AdminPedidoController@asignarpromo'); // 
Route::get('/tiempoentrega', 'AdminPedidoController@tiempoentrega'); // 

    Route::get('/PedidosUsuario','AdminPedidoController@getUsers');
	Route::get('/ReportesPedidos','ReportesController@getPedidosDia');
	Route::get('/ReporteGeneral','ReportesController@getReporteGeneral')->name('searchMonto');
	Route::get('/getBuscarMonto', 'ReportesController@getBuscarMonto'); 

	Route::get('/ReportesPedidosFactura','FacturaController@getFechaFactura');
	Route::get('/newuser', 'AdminPedidoController@newuser'); // listado
	Route::get('/newuserejecutivo', 'AdminPedidoController@newuseroracleEjecutivo'); // listado
	Route::post('/asignarRuta/{id}', 'AdminPedidoController@asignarRuta'); // 


});
Route::middleware(['auth', 'gester'])->prefix('gester')->namespace('Admin')
->group(function () {

Route::get('/products', 'ProductController@index'); // listado
	Route::get('/products/create', 'ProductController@create'); // formulario
	Route::post('/products', 'ProductController@store'); // registrar
	Route::get('/products/{id}/edit', 'ProductController@edit'); // formulario edición
	Route::post('/products/{id}/edit', 'ProductController@update'); // actualizar
	Route::delete('/products/{id}', 'ProductController@destroy'); // form eliminar
	

	Route::get('/products/{id}/images', 'ImageController@index'); // listado
	Route::post('/products/{id}/images', 'ImageController@store'); // registrar
	Route::delete('/products/{id}/images', 'ImageController@destroy'); // form eliminar	
	Route::get('/products/{id}/images/select/{image}', 'ImageController@select'); // destacar

	Route::get('/categories', 'CategoryController@index'); // listado
	Route::get('/categories/create', 'CategoryController@create'); // formulario
	Route::post('/categories', 'CategoryController@store'); // registrar
	Route::get('/categories/{category}/edit', 'CategoryController@edit'); // formulario edición
	Route::post('/categories/{category}/edit', 'CategoryController@update'); // actualizar
	Route::delete('/categories/{category}', 'CategoryController@destroy'); 




});

Route::middleware(['auth', 'useradmin'])->prefix('useradmin')->namespace('Admin')
->group(function () {



Route::get('/newuser', 'AdminPedidoController@newuser'); // listado
Route::get('/newuser/search', 'AdminPedidoController@indexsearch'); // listado
Route::get('/newuser/search2', 'AdminPedidoController@indexsearch2'); // listado
Route::post('/modificar/{iduser}', 'AdminPedidoController@updateuseradmin');
Route::post('/update/{iduser}', 'AdminPedidoController@updateuseradmins');

});


Route::middleware(['auth', 'usad'])->prefix('usad')->namespace('Admin')
->group(function () {



Route::get('/useradmin', 'AdminPedidoController@adduseradmin'); // listado


});
Route::middleware(['auth', 'adminpedidos'])->prefix('adminpedidos')->namespace('Admin')
->group(function () {


	Route::get('/reporte/{id}', 'AdminPedidoController@repoterutas'); 

Route::get('/pedidos', 'AdminPedidoController@index'); // listado
    Route::get('/prueba', 'AdminPedidoController@prueba'); // prueba
	Route::post('/pedidos/create', 'AdminPedidoController@create'); // formulario
	Route::get('/pedidos/{id}/despachado', 'AdminPedidoController@showDes'); //
	Route::get('/reporteusuarios', 'AdminPedidoController@repoteusuarios'); //
	Route::get('/pedidos/{id}/show/{ruta}', 'AdminPedidoController@show'); //
	Route::get('/pedidos/{id}/cancelacion', 'AdminPedidoController@cancelacion'); //
	Route::get('/reporteusuarios', 'AdminPedidoController@repoteusuarios'); //
	Route::post('/firebasenotificacion', 'NotificacionesFirebase@notifyUser')->name('firebasenotificacion'); // Notificacion a movil
	Route::get('/PedidosUsuario','AdminPedidoController@getUsers');
	Route::get('/consultarExistencia','AdminPedidoController@consultarExistencias'); // Daniel
	Route::post('/consultarExistencia','AdminPedidoController@consultarExistencias')->name('adminpedidos.consultarExistencia');
	Route::get('/ReportesPedidos','ReportesController@getPedidosDia');

	Route::get('/ReportesPedidosFactura','FacturaController@getFechaFactura');
	
	
	Route::get('/fcm','AdminPedidoController@index2');
	Route::post('send','NotificacionesFirebase@bulksend')->name('bulksend');
	Route::get('/pedidos/{id}/confirmed', 'AdminPedidoController@confirmed'); //
	Route::get('/pedidos/{id}/pagado', 'AdminPedidoController@pagado'); // 
	Route::get('/pedidos/{id}/cancelado', 'AdminPedidoController@cancelado'); // 
	Route::get('/pedidos/{id}/productos', 'AdminPedidoController@productos'); // formulario edición
	Route::get('/pedidos/{id}/edit', 'AdminPedidoController@edit'); //
	Route::post('/pedidos/{id}/update', 'AdminPedidoController@updatePedido');
	Route::post('/pedidos/{id}/updateDes', 'AdminPedidoController@updateDespachado'); // actualizar 
	Route::get('/pedidos/prueba', 'AdminPedidoController@prueba'); // 
	Route::delete('/pedidos/{id}', 'AdminPedidoController@destroyEdit'); // form eliminar
Route::post('/pedidos/modificar/{id}', 'AdminPedidoController@updatecarro');

Route::post('/despachado/{id}', 'AdminPedidoController@despachado'); 
Route::post('/pedido/ruta/{id}', 'AdminPedidoController@cambiorutero'); 

Route::get('/reporteusuarios/search', 'AdminPedidoController@indexsearchrepoteusuarios'); //


Route::get('/newuser', 'AdminPedidoController@newuserejecutivo'); // listado
Route::get('/newuser/{id}', 'AdminPedidoController@newuserejecutivo_edit'); // duario dias
Route::post('/newuser/{id}/update', 'AdminPedidoController@newuserejecutivo_update'); //update dias y ruta

Route::get('/asignarPassword', 'AdminPedidoController@asignarPassword'); // 
Route::post('/changePassword/{iduser}', 'AdminPedidoController@cambiarPassword');
Route::post('/changeUsername/{iduser}', 'AdminPedidoController@cambiarUsername');
});



Route::middleware(['auth', 'rutero'])->prefix('rutero')->namespace('Admin')
->group(function () {

	Route::get('/index', 'ruterocontrol@index'); // indexm
	Route::get('/index/{id}', 'ruterocontrol@elegirtipopago'); // indexm
	Route::get('/modificar/{idcar}/{idcentro}', 'ruterocontrol@modificarcarrito');
	Route::post('/modificar/{idcar}/{idmodifi}/{idproduct}/{idcodeb}', 'ruterocontrol@updatecarroruta');
	Route::post('/pagoRutas/{id}', 'ruterocontrol@updatePago'); // update status a pagado
	Route::post('/cambio1/{id}', 'ruterocontrol@entregado'); // update status a pagado

	Route::get('/reportepedidos', 'ruterocontrol@repotepedidos'); //

Route::get('/reporteruteros/search', 'ruterocontrol@indexsearchrepoterutero'); //



Route::get('download-jsonfile', array('as'=> 'file.download', 'uses' => 'ruterocontrol@downloadJSONFile'));

});

Route::middleware(['auth', 'picking'])->prefix('picking')->namespace('Admin')
->group(function () {

Route::get('/index', 'picking@index'); // indexm
Route::get('/modificar/{idcar}/{idcentro}', 'picking@modificarpedidospicking');
Route::get('/Regresar/{idcar}/{idcentro}/{interfazado}', 'picking@regresarpedido');
Route::post('/modificar/{idcar}/{idmodifi}', 'picking@updatepedidospicking');
Route::post('/cambio/{id}', 'picking@preparado'); 
Route::post('/regresarEjecutivo/{id}', 'picking@regresoEjecutivo'); 
Route::get('/reportepicking', 'picking@reportePicking'); //
Route::get('/reportepedidos', 'picking@reportepedidos'); //
Route::get('/reportepedidos/search', 'picking@reportepedidosfiltro'); //

});


Route::middleware(['auth', 'agente'])->prefix('agente')->namespace('Admin')
->group(function () {


});

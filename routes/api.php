<?php

use Illuminate\Http\Request;
use App\ExpoAppModel;
use App\DatPrecios;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/welcomeprueba/{id}', 'apiController@welcomePrueba');
Route::get('/productselecteds/{id}/{iduser}', 'apiController@index1');
Route::get('/productselected/{iduser}/{idproduct}', 'apiController@productshow');
Route::get('/prduct/search/{query}/{iduser}', 'apiController@productsearchprueba');
Route::get('/prduct/buscador/{query}/{iduser}', 'apiController@searchPrueba');
Route::get('/homePrueba/{iduser}', 'apiController@homePrueba');
Route::get('/homes/{iduser}', 'apiController@homes');
Route::get('/homes2/{iduser}', 'apiController@homes2');
Route::get('/homesErzu/{iduser}', 'apiController@homesErzu');
Route::post('/storeprueba', 'apiController@storeprueba');
Route::post('/updateCarrito', 'apiController@updatecarrito');
Route::get('/registerprueba', 'apiController@registerPrueba');
Route::post('/registeruser', 'apiController@registeruser');
Route::get('/registerprueba2', 'apiController@registerprueba2');
Route::get('/registerprueba3/{city}', 'apiController@registerPrueba3');
Route::get('/userSetting/{idusuario}', 'apiController@userSetting');
Route::post('/updateUser', 'apiController@updateUser');
Route::post('/updateUser2', 'apiController@updateUser2');
Route::post('/updateUser3', 'apiController@updateUser3');
Route::post('/updateUser4', 'apiController@updateUser4');
Route::post('/ReportError', 'apiController@ReportError');
Route::get('/productosimage/{query}', 'apiController@prodimage');
Route::get('/userDetail/{id}', 'apiController@userDetail');
Route::get('/deleteEdit/{id}', 'apiController@deleteEdit');
Route::post('/deleteDetalle', 'apiController@destroyEdit'); // form eliminar

Route::get('/historialpedidos/{iduser}', 'apiController@historialpedidos');
Route::get('/pedidosActivos/{iduser}', 'apiController@pedidosActivos');
Route::get('/numeroEjuctivo/{iduser}', 'apiController@numeroEjuctivo');
//Productos seleccionados prueba
Route::get('/itemProducto/{idproducto}/{iduser}', 'apiController@itemProducto');

//Elimina usuario app
Route::post('/eliminarUsuarioApp', 'apiController@deleteUserApp');
Route::get('/getBuscarMonto/{fechaDesde}/{fechaHasta}', 'Admin\ReportesController@getBuscarMonto');
Route::get('/getReporteGeneral', 'Admin\ReportesController@getReporteGeneral');


Route::post('/token', 'ExpoAppController@insert');
Route::post('/cartDetail', 'apiController@store');
Route::get('/cartDetailInfo/{idCart}', 'apiController@cartDetailInfo');
Route::get('/cartInfo/{idUser}', 'apiController@cartInfo');

Route::get('/products1/{id}', 'apiController@productos1');
Route::get('/pruebaproducts1/{id}', 'apiController@pruebaproductos1');
Route::get('/productsCat1/{idCategory}', 'apiController@productCategory1');
Route::get('/test', 'apiController@test');
Route::get('/products1prueba/{id}', 'apiController@productos1Prueba');
Route::get('/products2/{id}', 'apiController@productos2');
Route::get('/products3/{id}', 'apiController@productos3');
Route::get('/products4/{id}', 'apiController@productos4');

Route::get('/coloniaByCity/{city}', 'apiController@selectByColonia');

Route::get('/rutero/index1/{id}', 'apiController@RouteroIndex1');
Route::get('/rutero/index2/{id}', 'apiController@RouteroIndex2');
Route::get('/rutero/index3/{id}', 'apiController@RouteroIndex3');

Route::get('/rutero/edit/{id}', 'apiController@RuteroEdit');
Route::get('/rutero/modificar/{id}', 'apiController@RouteroIndex4');
Route::get('/rutero/imprimir/{id}', 'apiController@RouteroImprimir');
Route::get('/rutero/edit/{id}', 'apiController@RuteroEdit');

Route::get('/rutero/elegirtipopago/{id}', 'apiController@elegirtipopago');
Route::get('/samples/document/txt/sample3.txt/{id}', 'apiController@ticket');
Route::get('/rutero/editmodi/{idmodificacion}', 'apiController@Ruteroeditmodi');
Route::get('/rutero/modipedi/{id}', 'apiController@RouteroIndex5');

Route::get('/rutero/reporte/{id}', 'apiController@repotepedidos');
Route::get('/rutero/reporte2/{idstatus}/{idFechaI}/{idFechaF}', 'apiController@indexsearchrepoterutero');

Route::get('/rutero/reporte2', 'apiController@repotepedidos2');

Route::get('/rutero/rutero/{id}', 'apiController@ruterorutero');

Route::post('/updatestatus', 'apiController@entregado');
Route::post('/pagar', 'apiController@updatePago');
Route::post('/modificado', 'apiController@updatecarroruta');

//Prueba hi
Route::get('/updateStatusPedido', 'apiController@updateStatusPedido');


Route::get('/products/{id}/{idCentroDeVenta}', 'apiController@show');
Route::get('/products/{id}', 'apiController@showExistencia');
Route::get('/usuario/{username}', 'apiController@usuario');
Route::get('/buscar/{query}/{iduser}', 'apiController@buscar');

Route::get('/buscarTest/{query}/{iduser}', 'apiController@buscarTest');

//New chat Online Erzu
Route::get('/chatOnline', 'ChatOnlineController@index')->name('chatOnline');
Route::get('/message/{id}', 'ChatOnlineController@getMessage')->name('message');
Route::post('/message', 'ChatOnlineController@sendMessage');
//Fin New chat Online

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

 Route::group(['middleware' => ['auth:api', 'user_accessible']], function () {
       Route::get('/colonias', 'HomeController@getColonias'); // your protected routes.
    });

    Route::group(['middleware' => ['json.response']], function () {

        Route::middleware('auth:api')->get('/user', function (Request $request) {
            return $request->user();
        });
    
        // public routes
        Route::post('/login', 'Api\AuthController@login')->name('login.api');
        Route::post('/loginFake', 'Api\AuthController@loginFake')->name('loginFake.api');
        Route::post('/register', 'Api\AuthController@register')->name('register.api');
    
        // private routes
        Route::middleware('auth:api')->group(function () {
            Route::get('/logout', 'Api\AuthController@logout')->name('logout');
        });
    
    });

    Route::post('/forgot-password', 'Api\AuthController@forgot_password');

    Route::group(['middleware' => 'auth:api'], function () {
    Route::post('/change-password', 'Api\AuthController@change_password');
    });




